package BusinessScenarioPage;

import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;

public class Simple_login_page extends ReusableLibrary{
	
	public Simple_login_page(ScriptHelper scriptHelper) {
		super(scriptHelper);
	}
	public static String Username = "//*[@id='username']";
	public static String Password = "//*[@id='password']";
	public static String Login = "//button[@class='btn btn-primary']";
	public static String Allproj = "//*[@id='all-projects']";
	public static String NextPage = "//a[contains(text(),'�')]";
	public static String StartProject = "//span[@id='Create-new-project']";
	public static String Cancel = "//span[contains(text(),'Cancel')]";
	public static String Account = "//ul[@class='nav navbar-nav navbar-right']/li[2]/a/span/span[2]";
	public static String Signin = "//span[contains(text(),'Sign in')]";
	public static String Signout = "//span[@class='glyphicon glyphicon-log-out']";
	public static String DraftProj = "//*[contains(text(),'SPDY-UAT5-FORMAL-NONOMP3')]";
	public static String GenProjInfo = "//span[contains(text(),'General Project Information')]";
	public static String ImpaField = "//input[@type='radio' and @id='field_impa_finalProtocolOrAmendment']";
	public static String CntryInfo = "//span[contains(text(),' Country & Depot Info ')]";
	public static String InfoKit = "//span[contains(text(),'Information per Kit Type')]";
	public static String GenKitInfo = "//a[contains(text(),'General Kit Type Info')]";
	public static String PackTypeInfo = "//a[contains(text(),'Pack Type Info')]";
	public static String DrugProdInfo = "//a[contains(text(),'Drug Product Info')]";
	public static String PrimPackInfo = "//a[contains(text(),'Primary Pack Info')]";
	public static String MelCompTab = "//a[contains(text(),'MEL Compiler')]";
	
	public static String Showfilter = "//button[@id='showFilter']";
	public static String CheckCntry = "//input[@type='checkbox' and @ng-model='isCountryDepotInfo']";
	//public static String CheckGenKit = "//input[@type='checkbox' and @ng-model='isGeneralKitTypeandPackTypeInfo']";
	public static String Applyfilter = "//button[@id='applyFiltersID']";
	public static String OpenStatus = "//input[@class='ng-pristine ng-untouched ng-valid ng-empty' and @ng-model='isOpen']";
	public static String FrstProj = "//tbody/tr[1]/td[1]";
	public static String ShowClusterbutton = "//button[@id='ShowDataClusterID']";
	public static String Searchbox = "//input[@placeholder='Search a workflow task by project ID']";
	public static String Searchbutton = "//button[@id='ShowDataCluster']";
	public static String ResultProj = "//tr[@id='tablerowdata1']/td[1]";
	
	
}
