package BusinessScenarioPage;

import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;

public class Updates_data_page extends ReusableLibrary{

	public Updates_data_page(ScriptHelper scriptHelper)
	{
		super(scriptHelper);
	}
	
	public static String Username = "//*[@id='username']";
	public static String Password = "//*[@id='password']";
	public static String Login = "//button[@class='btn btn-primary']";
	public static String Allproj = "//*[@id='all-projects']";
	public static String Arrow = "//a[contains(text(),'�')]";
	public static String Project = "//*[text()='SPDY-UAT5-FORMAL-NONOMP3']";
	
	public static String GenProj = "//span[contains(text(),'General Project Information')]";
	public static String IMPALabel = "//label[contains(text(),'IMPA is based upon Source Document')]";
	public static String SignOffDate = "//label[contains(text(),'Sign off date of the Source Document ')]";
	public static String GCDO = "//label[text()='GCDO or CRO run']";
	public static String GCDOCheckOther = "//input[@id='field_gcoOrCroRun_op4']";
	public static String GenProjOther = "//input[@id='field_gcoOrCroRun_otherValue']";
	
	public static String InfoPerkit = "//span[contains(text(),'Information per Kit Type')]";
	public static String GenKit = "//a[contains(text(),'General Kit Type Info')]";
	public static String PackPrim = "//*[@id='home']/div[1]/form/div/div[1]/div[5]/div/div/div[1]/input";
	public static String PackPrimOpt = "//div[contains(text(),'Janssen Beerse (Belgium)')]";
	public static String SecPrim = "//*[@id='home']/div[1]/form/div/div[1]/div[6]/div/div/div[1]/input";
	public static String SecPrimOpt = "//div[contains(text(),'Fisher Allentown PA (USA)')]";
	
	public static String PackType = "//a[contains(text(),'Pack Type Info')]";
	public static String DoseForm = "//select[@id='dosageForm']";
	
	public static String DrugProd = "//a[contains(text(),'Drug Product Info')]";
	public static String OxySens = "//input[@id='field_oxygenSensitiveProduct_true']";
	public static String FrozenProd = "//input[@id='field_frozen_product_na']";
	
	public static String Primary_pack = "//a[contains(text(),'Primary Pack Info')]";
	public static String OxyScavenger = "//*[@id='menu3']/div[1]/form/div/div/div[1]/div[5]/div/div/div[1]/span/span[1]";
}
