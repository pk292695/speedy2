package BusinessScenarioPage;


import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;

public class Update_task_page extends ReusableLibrary{

	public Update_task_page(ScriptHelper scriptHelper) {
		super(scriptHelper);
	}
	public static String Username = "//*[@id='username']";
	public static String Password = "//*[@id='password']";
	public static String Login = "//button[@class='btn btn-primary']";
	
	public static String StartProject = "//span[@id='Create-new-project']";
	public static String ProjectName = "//input[@id='field_name']";
	public static String Compound = "//textarea[@id='field_compound']";
	public static String StartButton = "//span[contains(text(),'Start project')]";
	
	public static String GenProjInfo = "//span[contains(text(),'General Project Information')]";
	public static String FieldImpa = "//input[@id='field_impa_pedDraftProtocoldraftAmendment']";
	public static String FieldDate = "//input[@id='field_dateOfFinalProtocol']";
	public static String FieldPhase = "//select[@id='field_phase']";
	public static String SponserName = "//select[@id='sponsorNameDrop']";
	public static String SponserAddr = "//select[@id='sponsorNameAddr']";
	public static String Therapeutic = "//input[@id='field_therapeuticArea']";
	public static String GcdoCro = "//input[@id='field_gcoOrCroRun_op2']";
	public static String PatientAlert = "//input[@id='field_patientAlertCard_yes']";
	public static String Submit = "//button[@id='GeneralTrialSubmitForApproval']";
	public static String SaveSubmit = "//button[@id='submit&saveForGeneralTriale']";
	
	public static String Home = "//span[contains(text(),'Home')]";
	public static String Searchbox = "//input[@placeholder='Search a workflow task by project ID']";
	public static String SearchButton = "//button[@id='ShowDataCluster']";
	public static String ResultProj = "//tr[@id='tablerowdata1']/td[1]";
	public static String Showcluster = "//button[@id='ShowDataClusterID']";
	public static String ApproveButton = "//button[@id='CSITSMapprove']";
	
	
	
}
