package BusinessScenarioPage;

import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;

public class Delete_Edit_MEL_Page extends ReusableLibrary{
	public Delete_Edit_MEL_Page(ScriptHelper scriptHelper)
	{
		super(scriptHelper);
	}
	
	public static String Username = "//*[@id='username']";
	public static String Password = "//*[@id='password']";
	public static String Login = "//button[@class='btn btn-primary']";
	public static String Account = "//ul[@class='nav navbar-nav navbar-right']/li[2]/a/span/span[2]";
	public static String Signin = "//span[contains(text(),'Sign in')]";
	public static String Signout = "//span[@class='glyphicon glyphicon-log-out']";
	
	public static String ProjMgmt = "//*[@class='navPrgMgmt']";
	public static String MELMgmt = "//a[@class='navMelMgmt']";
	
	public static String Delete = "//button[@class='btn btn-danger']";	
	public static  String Long_Des = "//input[@id='field_description']";
	public static  String Short_Des = "//input[@id='field_name']";
	public static  String Add_MEL = "//span[contains(text(),'Add MEL template')]";
	public static  String Long_Description = "//select[@id='melTemplate']";
	public static  String Add_Context_var = "//button[contains(text(),'Add Context variable')]";
	public static  String SearchBtn = "//button[@type='submit']";
	public static  String Context_var = "//span[@id='category3']";
	public static  String AddToMelTemp = "//button[contains(text(),'Add to MEL template')]";
	public static  String AddNGCtext = "//button[contains(text(),'Add NGC text reference')]";
	//public static  String SearchBtn= "//button[@type='submit']";
	public static  String NGC_Text = "//span[@id='ngcStatic4']";
	//public static  String AddToMelTemp1 = //button[contains(text(),'Add to MEL template')];
	public static  String Add_Speedy_var = "//button[contains(text(),'Add Speedy variable')]";
	public static  String Speedy_Var = "//span[@id='spdyVariable5']";
	//public static  String AddToMelTemp2 = "//button[contains(text(),'Add to MEL template')]";
	public static  String  Save_changeBtn = "//button[contains(text(),'Save Changes')]";
	public static  String RemoveMEL = "//a[contains(text(),'Remove this MEL template')]";
	
	public static String Allproj = "//*[@id='all-projects']";
	public static String Arrow = "//a[contains(text(),'�')]";
	public static  String Non_Omp_Draft = "//*[contains(text(),'Test_Browser_DataValidation')]";

	public static  String Info_Per_Kit = "//span[contains(text(),'Information per Kit Type')]";
	public static  String kit_Type = "//span[@id='select2-field_kitType_select-container']";
	public static  String MELCompiler = "//a[contains(text(),'MEL Compiler')]";
	public static  String Pack_Type = "//input[@id='packType0']";
	public static  String LabelGroup = "//input[@id='LblGroup0']";

	public static  String Edit_label = "//span[contains(text(),'Edit label groups')]";
	public static  String Name =  "//input[@id='newLbl']";
	public static  String SaveLabel= "//button[@id='saveLabelId']"; 
	public static  String Combinebtn = "//button[@id='melCombinebtn']";
	public static  String DoneBtn= "//button[contains(text(),'Done')]";

	public static  String Choose_Mel = "//input[@class='ui-select-search ui-select-toggle ng-pristine ng-valid ng-empty ng-touched']";
	//div[@class='selectize-input']

}
