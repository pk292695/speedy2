package BusinessScenarioPage;

import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;

public class AddRemoveKitTypePage extends ReusableLibrary{
	
	public AddRemoveKitTypePage(ScriptHelper scriptHelper)
	{
		super(scriptHelper);
	}
	
	
	public static String Allproj = "//*[@id='all-projects']";
	public static String AllProjTableHeader = "//*[@id='ProjVersionLinks']/div[2]/table/thead";
	public static String projecthead = "//*[@id='ProjVersionLinks']/div[2]/table/thead/tr/th[2]";
	public static String Typehead = "//*[@id='ProjVersionLinks']/div[2]/table/thead/tr/th[3]";
	public static String LastArrow ="//a[contains(text(),'�')]";
	public static String PrevPage = "//a[text()='�']";
	public static String filterButton = "//*[@id='showFilterProject']";
	public static String filterDraft = "//*[@id='Draft']";
	public static String filterNonOmp = "//*[@id='NonOmpTrial']";
	public static String applyFilterButton = "//*[@id='applyFiltersID']";
	
	public static String tableRow = "//*[@id='tablerowdata'][1]";
	public static String informationKitType = "//span[contains(text(),'Information per Kit Type')]";
	public static String selectKitType = "//*[@id='frm-KitType']/div[3]/div[1]/span";
	public static String Addnew = "//*[contains(text(),'Add new')]";
	public static String fieldCompound = "//input[@id='field_compound']";
	public static String saveButton = "//span[contains(text(),'Add Kit Type')]";
	 public static String yesButton = "//*[@id='removeKit']";
	
	public static String kitTypeSelect = "//*[@id='field_kitType_select']";
	public static String removeKitType = "//a[@id='removeKitType']";
	//public static String nacroticTrue = "//*[@id='field_narcotic_true']";
	
	
	
	

}
