package BusinessScenarioPage;

import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;

public class BrightstockSubmitPage extends ReusableLibrary{
	
	public BrightstockSubmitPage(ScriptHelper scriptHelper)
	{
		super(scriptHelper);
	}
	
	public static String Username = "//*[@id='username']";
	public static String Password = "//*[@id='password']";
	public static String Login = "//button[@class='btn btn-primary']";
	public static String Allproj = "//*[@id='all-projects']";
	public static String LastArrow ="//a[contains(text(),'�')]";
	public static String PrevPage = "//a[text()='�']";
	public static String BrightstkProj = "//*[contains(text(),'JIT_BS_17JUN_17JUN')]";
	public static String Syncbutton = "//a[contains(text(),'Sync with OMP')]";
	public static String BrightstkSubmit = "//button[@id='BrightStockProjectApproval']";
	
	public static String GenProjInfo = "//span[contains(text(),'General Project Information')]";
	public static String SponserName = "//select[@id='sponsorName']";
	public static String SponserAddr = "//select[@id='form-select-md51']";
	public static String KitNumber = "//input[@id='field_totalKitNumbersRequested']";
	public static String Format = "//input[@type='text' and @id='field_format']";
	public static String Prefix = "//textarea[@id='field_preFix']";
	
	public static String CntryInfo = "//span[contains(text(),' Country & Depot Info ')]";
	public static String InfoKit = "//span[contains(text(),'Information per Kit Type')]";
	
	public static String GenKitInfo = "//a[contains(text(),'General Kit Type Info')]";
	public static String OMPLabel = "//label[contains(text(),'OMP number')]";
	public static String BlindType = "//input[@type='radio' and @id='field_blindingType_OL']";
	public static String ChildRes = "//input[@id='field_childResistant_true']";
	//public static String ClinicAdm = "//input[@id='field_inClinicOrHospitalAdministration_true1']";
	//public static String PackSitesPrim = "//div[@class='ui-select-container ui-select-multiple ui-select-bootstrap dropdown form-control ng-pristine ng-empty ng-invalid ng-invalid-required ng-touched']";
	//public static String Option1 = "//div[contains(text(),'Fisher Allentown PA (USA)')]";
	//public static String PackSitesSec = "//div[@class='form-control ui-select-container ui-select-multiple ui-select-bootstrap dropdown form-control ng-pristine ng-untouched ng-empty ng-invalid ng-invalid-required']";
	//public static String Option2 = "//div[contains(text(),'Fisher Ahmedabad (India)')]";
	
	public static String PackTypeInfo = "//a[contains(text(),'Pack Type Info')]";
	public static String ProdAndStrength = "//select[@id='packTypeInfo.productNameAndStrength.ngcTxtId']";
	public static String DoseForm  = "//select[@id='dosageForm']";
	public static String ROA = "//select[@id='rout1']";
	public static String TempStore = "//select[@id='temprid']";
	public static String TearOff = "//input[@name='optradio' and @value='No']";
	public static String Remarks = "//textarea[@id='txatid2']";
	
	public static String DrugProdInfo = "//a[contains(text(),'Drug Product Info')]";
	//public static String DrugProduct = "//select[@id='field_drugProductCountryOfOrigin' and @class='form-control drugSelect ng-pristine ng-untouched ng-valid ng-not-empty ng-valid-required select2-hidden-accessible']";
	//public static String DrugSubCompany = "//input[@id='field_drugSubstanceCompany']";
	//public static String Drugsubstance = "//select[@id='field_drugSubstanceCountryOfOrigin']";
	//public static String ColdChain = "//input[@id='field_coldChainProduct_false']";
	//public static String MoistureProd = "//input[@id='field_moistureSensitiveProduct_false']";
	//public static String OxygenSens = "//input[@id='field_oxygenSensitiveProduct_false']";
	//public static String Lightsense = "//input[@id='field_maxMoistureOxygenExposureTimePL']";
	public static String FrozenProd = "//input[@id='field_frozen_product_frozen_platform']";
	
	public static String PrimPackInfo = "//a[contains(text(),'Primary Pack Info')]";
	public static String OptionPrim = "//input[@id='field_primary_Packaging_true']";
	
	public static String MelCompTab = "//a[contains(text(),'MEL Compiler')]";
	public static String PackOpt = "//input[@id='packType0']";
	public static String LabelOpt = "//input[@id='LblGroup0']";
	public static String Combine = "//button[@id='melCombinebtn']";
	public static String ChooseTemp = "//div[@class='selectize-input']"; 
	public static String Done = "//button[contains(text(),'Done')]";

}
