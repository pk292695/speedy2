package BusinessScenarioPage;

import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;

public class Multilevel_packtype_page extends ReusableLibrary{
	public Multilevel_packtype_page(ScriptHelper scriptHelper)
	{
		super(scriptHelper);
	}
	public static String Username = "//*[@id='username']";
	public static String Password = "//*[@id='password']";
	public static String Login = "//button[@class='btn btn-primary']";
	public static String Allproj = "//*[@id='all-projects']";
	public static String Arrow = "//a[contains(text(),'�')]";
	public static String Project = "//*[text()='SPDY-UAT5-FORMAL-NONOMP3']";
	public static String InfoPerKit = "//span[contains(text(),'Information per Kit Type')]";
	public static String PackType = "//a[contains(text(),'Pack Type Info')]";
	public static String Doseform = "//select[@id='dosageForm']";
	public static String AddDoseform = "//button[@id='addNewDosageForm']";
	
}