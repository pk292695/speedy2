package BusinessScenarioPage;

import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;



public class ViewLblgrpDetailsPage extends ReusableLibrary{
	
	public ViewLblgrpDetailsPage(ScriptHelper scriptHelper)
	{
		super(scriptHelper);
	}
	public static String Username = "//*[@id='username']";
	public static String Password = "//*[@id='password']";
	public static String Login = "//button[@class='btn btn-primary']";
	public static String Allproj = "//*[@id='all-projects']";
	public static String filterButton = "//*[@id='showFilterProject']";
	public static String filterDraft = "//*[@id='Draft']";
	public static String filterOmp = "//*[@id='OmpTrial']";
	public static String applyFilterButton = "//*[@id='applyFiltersID']";
	public static String OMPproj = "//*[contains(text(),'SPEEDY_DEV_BKD_SC2')]";
	public static String Syncbutton = "//span[@class='glyphicon glyphicon-refresh']";
	public static String InfoPerkit = "//span[contains(text(),'Information per Kit Type')]";
	public static String Kit_type = "//span[@class='selection']/span[1]";
	public static String MelComp = "//a[text()='MEL Compiler']";
	public static String ShowCntry = "//span[contains(text(),'Show Countries')]";
	public static String Label1 = "//*[@id='labelGroupList']/li[1]";
	public static String Label2 = "//*[@id='labelGroupList']/li[2]";
	
	
	
	
	
}
