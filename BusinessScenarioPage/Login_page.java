package BusinessScenarioPage;

import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;

public class Login_page extends ReusableLibrary{
	
	public Login_page(ScriptHelper scriptHelper) {
		super(scriptHelper);
	}
	public static String Username = "//*[@id='username']";
	public static String Password = "//*[@id='password']";
	public static String Login = "//button[@class='btn btn-primary']";
	public static String StartProject = "//span[@id='Create-new-project']";
	public static String Cancel = "//span[contains(text(),'Cancel')]";
	public static String Account = "//ul[@class='nav navbar-nav navbar-right']/li[2]/a/span/span[2]";
	public static String Signin = "//span[contains(text(),'Sign in')]";
	public static String Signout = "//span[@class='glyphicon glyphicon-log-out']";
	public static String Showfilter = "//button[@id='showFilter']";
	public static String CheckCntry = "//input[@type='checkbox' and @ng-model='isCountryDepotInfo']";
	public static String CheckGenKit = "//input[@type='checkbox' and @ng-model='isGeneralKitTypeandPackTypeInfo']";
	public static String Applyfilter = "//button[@id='applyFiltersID']";
	public static String Title = "//img[@class='logo-img']";
	
	

}
