package BusinessScenarioPage;

import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;

public class Select_task_page extends ReusableLibrary{
	
	public Select_task_page(ScriptHelper scriptHelper)
	{
		super(scriptHelper);
	}
	public static String Username = "//*[@id='username']";
	public static String Password = "//*[@id='password']";
	public static String Login = "//button[@class='btn btn-primary']";
	public static String ProjCol = "//th[@aria-label='Project: activate to sort column ascending']";
	public static String CompCol = "//th[@aria-label='Compound: activate to sort column ascending']"; 
	public static String DataCol = "//th[@aria-label='Data Cluster: activate to sort column ascending']";
	public static String submittedbyCol = "//th[@aria-label='Submitted By: activate to sort column ascending']";
	public static String SubmissionCol = "//th[@aria-label='Date of Submission: activate to sort column ascending']";
	public static String StatusCol = "//th[@aria-label='Status: activate to sort column ascending']";
	public static String FrstProj = "//tbody/tr[1]/td[1]";
	public static String ShowClusterbutton = "//button[@id='ShowDataClusterID']";
	public static String Account = "//ul[@class='nav navbar-nav navbar-right']/li[2]/a/span/span[2]";
	public static String Signin = "//span[contains(text(),'Sign in')]";
	public static String Signout = "//span[@class='glyphicon glyphicon-log-out']";
	public static String Showfilter = "//button[@id='showFilter']";
	public static String CheckCntry = "//input[@type='checkbox' and @ng-model='isCountryDepotInfo']";
	//public static String CheckGenKit = "//input[@type='checkbox' and @ng-model='isGeneralKitTypeandPackTypeInfo']";
	public static String Applyfilter = "//button[@id='applyFiltersID']";
	public static String OpenStatus = "//input[@class='ng-pristine ng-untouched ng-valid ng-empty' and @ng-model='isOpen']";
	public static String Projectmgmt = "//*[@class='navPrgMgmt']";
	public static String MelTempMgmt = "//a[@class='navMelMgmt']";
	
	public static String Searchbox = "//input[@placeholder='Search a workflow task by project ID']";
	public static String Searchbutton = "//button[@id='ShowDataCluster']";
	public static String ResultProj = "//tr[@id='tablerowdata1']/td[1]";
	

}
