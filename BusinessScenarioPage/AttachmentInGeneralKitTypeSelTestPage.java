package BusinessScenarioPage;

import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;

public class AttachmentInGeneralKitTypeSelTestPage extends ReusableLibrary{
	
	public AttachmentInGeneralKitTypeSelTestPage(ScriptHelper scriptHelper)
	{
		super(scriptHelper);
	}
	
	public static String Username = "//*[@id='username']";
	public static String Password = "//*[@id='password']";
	public static String Login = "//button[@id='login']";
	public static String StartnewProject = "//span[@id='Create-new-project']";
	public static String LastArrow ="//a[contains(text(),'�')]";
	public static String PrevPage = "//a[text()='�']";
	public static String projectId = "//*[@id='field_name']";
	public static String CompoundTextBox = "//*[@id='field_compound']";
	public static String StartProject = "//*[@class='btn btn-primary']";
	public static String applyFilterButton = "//*[@id='applyFiltersID']";
	
	public static String tableRow = "//*[@id='tablerowdata'][1]";
	public static String informationKitType = "//span[contains(text(),'Information per Kit Type')]";
	public static String selectKitType = "//*[@id='frm-KitType']/div[3]/div[1]/span";
	public static String Addnew = "//*[contains(text(),'Add new')]";
	public static String fieldCompound = "//input[@id='field_compound']";
	public static String saveButton = "//span[contains(text(),'Add Kit Type')]";
	 public static String yesButton = "//*[@id='removeKit']";
	
	public static String kitTypeSelect = "//*[@id='field_kitType_select']";
	public static String removeKitType = "//a[@id='removeKitType']";
	public static String nacroticTrue = "//*[@id='field_narcotic_true']";
	
	
	
	

}
