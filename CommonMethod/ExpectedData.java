package CommonMethod;

import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;

public class ExpectedData extends ReusableLibrary{

		public ExpectedData(ScriptHelper scriptHelper) {
			super(scriptHelper);
			// TODO Auto-generated constructor stub
		
		
		}
		 public String StudyID="AUTOTEST_008";
		 public String TherapeuticArea="Oncology";
		 public String StudyPhase="2a";
		 public String Status="Active";
		 public String RTSMFlag="No";
		 
		 public String CompoundID="autotestcompd";
		 public String CompoundName="AutoTestCompd_001";
		 public String CompoundRole="autotesting";
		 
		 public String IndicationName="autocancer_001";
		 
		 public String AliasName="cancer Autotest";
		 public String AliasValue="cancer Autotest";
		 
		 
		 
		 //Label
		 public String ConfirmationAfter="1";
		 public String ConfirmationAfterDays="Days";
		 
		 public String ResendTime="2";
		 public String ResendDays="Days";
		 public String Attempts="3";
				 
		 public String MedicationResendTime="1";
		 public String TimeWindow="2";
		 public String TimeWindowDays="Days";
		 
		 
		 //Adherence
		 public String Deferedtime="1";
		 public String DeferNotificationHours="Hours";
		 public String DeferNotificationCount="3";
		 public String NotificationTime="15";
		 public String NotificationTimeMin="Minutes";
		 public String NotificationTimeOffset="After";
		 
		 
		 public String Deferedtime2="1";
		 public String DeferNotificationHours2="Hours";
		 public String DeferNotificationCount2="2";
		 public String NotificationTime2="30";
		 public String NotificationTimeMin2="Minutes";
		 public String NotificationTimeOffset2="Before";

		 
		 public String AlertThreshold="30";
		 public String AlertActionStatus="ToDo";
		 public String TimeWindow2="1";
		 public String TimeWindowdays2="Days";

		 //BasicSettings
		 public String MaximumNotification="25";
		 public String WaitTime="1";
		 public String AmountofTime="60";
		 
		 public String DeferNotification="1";
		 public String DeferTime="Days";
		 public String DeferCount="2";
		 
		 //ContentLibrary
		 public String Category="cat02";
		 public String ContentTitle="Content02";
		 public String Language="English";
		 public String Country="United States of America";
		 public String SiteID="Site-BEL01 (Site-BEL01,BEL)";

		 
		//Epoch
				public String epochNAME ="AUTO_Open Label";
				public String epochDescr ="AUTO_Open Label";
				public String Order ="1";
				public String maxdur ="30";
				
				//Epochendpoint
				public String epochpointName ="End of Treatment";
				public String epochlabel="AUTO_Open Label";
				public String epochDESC ="End of Treatment - Ends all treatment EPOCHs";
				
				
				//Timepoint
				public String epoch1 ="AUTO_Open Label";
				public String timepointid="AOL Day 1";
				public String timepointname="AOL Day 1";
				public String descr="AOL Day 1";
				public String study="1";
				
				//Study Events
				public String studyname ="Auto Dispensing Visit";
				public String eventType ="Dispensing Visit";
				public String windowdays="2";
				
				
				//Kittype
				public String Type="Mk";
				public String Packtype="blister";
				public String Tquantity="1";
				public String Packunit="capsules";
				public String Quantity="10";
				public String Quantityunit="mg";
				
				//Dosing
				public String regId="Auto_DR1";
				public String desc="Auto Dosing Regimen ";
				public String treatduration="3";
				public String dosageamt="2";
				
				public String timeHrs="06";
				public String timeMins="00";
				
				public String timeHrs1="12";
				public String timeMins1="00";
				
				public String timeHrs2="18";
				public String timeMins2="00";
				
				public String window="2";
				public String interval="1";
				
				public String addinstr="take with water";
				
				//Reference Data
				
				public String studyID="AUTOTEST_008";
				public String siteID="Site-BEL01";
				
				//CCTActivationWeb
				
				public String CCTCountry="Belgium";
				public String ICFLanguage="English";
				public String TimeZone="Central European Time";
				public String code="491199";
				
				
				public String Subject_ID="JNJTEST008";
				
				
				public String PinCode1="1";
				public String PinCode2="2";
				public String PinCode3="3";
				public String PinCode4="4";
				public String PinCode5="5";
				public String PinCode6="7";
				
				
				
				public String Visit="1 (OL_Day1)";
				public String kitType="MK 20 mg";
				public String DosingRegimen="Regimen1020";
				
				public String NumberOfMedicationKitToDispence="1";
				public String manualSelectionOption="Scanning device error";
				
				public String BatchNumber="1";
				
				
				
}
