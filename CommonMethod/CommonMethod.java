package CommonMethod;

import java.awt.AWTException;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.asserts.SoftAssert;

import AutomationCoreFramework.Status;
import AutomationCoreFrameworkMobile.CraftDriver;
import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;



public class CommonMethod extends ReusableLibrary {
	public CommonMethod(ScriptHelper scriptHelper) {
		super(scriptHelper);
		// TODO Auto-generated constructor stub
	}
	/**
	 * @author sranja14
	 */

	By element;
	WebDriverWait wait = null;

	SoftAssert s_assert = new SoftAssert();
	Process process = null;

	public By selectLocator(String locatorType, String locatorValue) {
		switch (locatorType) {
		case "id":
			element = By.id(locatorValue);
			return element;
		case "xpath":
			element = By.xpath(locatorValue);
			return element;
		case "name":
			element = By.name(locatorValue);
			return element;
		case "tagName":
			element = By.tagName(locatorValue);
			return element;
		case "linkText":
			element = By.linkText(locatorValue);
			return element;
		case "partialLinkText":
			element = By.partialLinkText(locatorValue);
			return element;
		case "className":
			element = By.className(locatorValue);
			return element;
		case "cssSelector":
			element = By.cssSelector(locatorValue);
			return element;
		default:
			System.out.println("Locator : " + locatorType + " is invalid");

		}
		return element;

	}

	/**
	 * @author sranja14
	 * @param locator
	 * @param Desc
	 * @param expectedText
	 * @param driver
	 * @throws UnsupportedFlavorException
	 * @throws IOException
	 * @throws AWTException
	 * @desc To verify expected Text
	 */


	public void verifyExpectedText(By locator, String Desc ,String expectedText, CraftDriver driver) throws UnsupportedFlavorException, IOException, AWTException {
		wait = new WebDriverWait((WebDriver) driver, 20);
		// WebElement
		// element1=wait.until(ExpectedConditions.presenceOfElementLocated(locator));
		WebElement element1 = driver.findElement(locator);
		String actualText = element1.getText();
		if (actualText.equals(expectedText)) {
			
				report.updateTestLog("Verify",Desc,"SearchText", "Searching the text", Status.PASS);
			
		} else {
			report.updateTestLog("Verify","nothing","SearchText", "Searching the text", Status.FAIL);
		}
	}

	/**
	 * @author sranja14
	 * @param element
	 * @param elementName
	 * @throws IOException
	 * @throws AWTException 
	 * @throws UnsupportedFlavorException 
	 * @throws InterruptedException
	 * @desc To click on element
	 */
	public void clickOnElement(String xpath, String elementName, CraftDriver driver) throws IOException, UnsupportedFlavorException, AWTException {
		WebDriverWait wait = new WebDriverWait(driver.getWebDriver(), 20);
		WebElement element =driver.findElement(By.xpath(xpath));
		wait.until(ExpectedConditions.elementToBeClickable(element));
		try {
			
			report.updateTestLog("Click Element ","Click on element " +elementName,elementName +" should be displayed and clicked.", elementName + " is displayed and clicked as expected .", Status.PASS);
			element.click();
			
		} catch (Exception e) {
			report.updateTestLog("Click Element ","Click on element " +elementName,elementName +" should be displayed and clicked.", elementName + " is not displayed and clicked as expected .", Status.FAIL);
		}
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @author sranja14
	 * @param element
	 * @param elementName
	 * @param text
	 * @throws IOException
	 * @throws AWTException 
	 * @throws UnsupportedFlavorException 
	 * @desc To enter Text
	 */
	public void enterText(String xpath, String elementName, String text, CraftDriver driver) throws IOException, UnsupportedFlavorException, AWTException {
		WebDriverWait wait = new WebDriverWait(driver.getWebDriver(), 20);
		WebElement element =driver.findElement(By.xpath(xpath));
		
		wait.until(ExpectedConditions.elementToBeClickable(element));
		
		try {
			
			element.sendKeys(text);
		
			report.updateTestLog("Enter Text","Enter the text in the field "+elementName ,text + " must be entered in field " + elementName, text + " is successfully entered in field " + elementName, Status.PASS);

		} catch (Exception e) {
			report.updateTestLog("Enter Text","Enter the text in the field "+elementName,text + " must be entered in field " + elementName,
					text + " is not successfully entered in field " + elementName + " as expected.", Status.FAIL);
			
		}

	}

	/**
	 * @author sranja14
	 * @throws IOException
	 * @param elementName
	 * @throws AWTException 
	 * @throws UnsupportedFlavorException 
	 * @desc To clear element
	 */
	public void clearElement(String xpath, String elementName, CraftDriver driver) throws IOException, UnsupportedFlavorException, AWTException {
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		WebElement element = driver.findElement(By.xpath(xpath));
		try {
			element.clear();
			report.updateTestLog("Clear Element","Clear the text in the field "+elementName,elementName+" text must be cleared in the field", elementName+" text is successfully cleared the field", Status.PASS);
			
		}
		catch (Exception e) {
			report.updateTestLog("Clear Element","Clear the text in the field "+elementName,elementName+" text must be cleared in the field", elementName+" text is not successfully cleared the field", Status.FAIL);
		}
		}

	/**
	 * @author sranja14
	 * @throws IOException
	 * @throws AWTException 
	 * @throws UnsupportedFlavorException 
	 * @desc To verify expected data
	 */
	public void verifyExpectedText(String xPath, String expectedText, CraftDriver driver) throws IOException, UnsupportedFlavorException, AWTException {
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		WebElement locator = driver.findElement(By.xpath(xPath));
		String actualValue = locator.getText();

		if (actualValue.equals(expectedText)) {
			report.updateTestLog("Launch","nothing","SearchText", "Searching the text", Status.PASS);
		} else {
			report.updateTestLog("Launch","nothing","SearchText", "Searching the text", Status.FAIL);
		}
	}

	/**
	 * @author sranja14
	 * @throws IOException
	 * @throws AWTException 
	 * @throws UnsupportedFlavorException 
	 * @desc To navigate to url
	 */
	public void navigateToUrl(String url, CraftDriver driver) throws IOException, UnsupportedFlavorException, AWTException {
		

		try {
			driver.get(url);
			driver.manage().window().maximize();
			//updateReportPass(url + "should be launched ", url + "is launched successfully as expected .", driver);
			report.updateTestLog("Launch URL","Navigate page to "+url,url + " should be launched ", url + " is launched successfully as expected .", Status.PASS);
		} catch (Exception e) {
			report.updateTestLog("Launch URL","Navigate page to "+url,url + " should be launched ", url + " is not launched successfully as expected .", Status.FAIL);
		}

	}

	/**
	 * @author sranja14
	 * @throws IOException
	 * @desc To swipe down
	 */
	public void androidSwipeDown() throws IOException {
		process = Runtime.getRuntime().exec("adb shell input swipe 500 1000 300 300 ");
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @author sranja14
	 * @throws IOException
	 * @desc To swipe up
	 */
	public void androidSwipeUp() throws IOException {
		process = Runtime.getRuntime().exec("adb shell input swipe 500 1000 300 300 ");
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	

	/**
	 * @author sranja14
	 * @throws IOException
	 * @desc To close android view
	 */
	public void androidCloseKeypadView() throws IOException {
		process = Runtime.getRuntime().exec("adb shell input keyevent 111");

	}

	/**
	 * @author sranja14
	 * @throws IOException
	 * @throws AWTException 
	 * @throws UnsupportedFlavorException 
	 * @desc To select option from dropdown
	 */
	public void selectOptionFromDropDown(String xpath, String option, CraftDriver driver) throws IOException, UnsupportedFlavorException, AWTException {
		WebElement element =driver.findElement(By.xpath(xpath));
		Select s = new Select(element);
		

		
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		s.selectByVisibleText(option);
		System.out.println(element.getText());
		if (element.getText().contains(option))

		{
			report.updateTestLog("Select From Dropdrown ","Select "+option+" from dropdown list",option + " should be displayed and must be selected.  ",
					option + " text is available and selected as expected .", Status.PASS);
		} else {
			report.updateTestLog("Select From Dropdrown ","Select "+option+" from dropdown list",option + " should be displayed and must be selected.  ",
					option + " text is not available and not selected as expected .", Status.FAIL);
		}

	}
	
	public void getText(String w, String fieldName, CraftDriver driver) throws IOException, UnsupportedFlavorException, AWTException {
		WebElement e = driver.findElement(By.xpath(w));
		if (e.isDisplayed()) {
			String data = e.getText();
			report.updateTestLog("Get Text","The text from the field "+fieldName+" is visible",fieldName + " should be displayed ", fieldName + " is displayed and as expected ",
					 Status.PASS);
		} else {
			report.updateTestLog("Get Text","The text from the field "+fieldName+"is visible",fieldName + " should be displayed ", fieldName + " is displayed and as expected ",
					 Status.FAIL);
		}
	} 
	/**
	 * @author sranja14
	 * @param ValueName
	 * @param atributeType
	 * @param option
	 * @param driver
	 * @throws IOException
	 * @throws UnsupportedFlavorException
	 * @throws AWTException
	 * @desc to click on element
	 */

	
	public void ClickElementFromListOfElements(String ValueName, String atributeType, String option, CraftDriver driver)
			throws IOException, UnsupportedFlavorException, AWTException {
		boolean b = false;
		List<WebElement> ls = driver.findElements(By.id(ValueName));
		for (WebElement w : ls) {
			if (w.getAttribute(atributeType).equals(option)) {
				w.click();
				b = true;
				report.updateTestLog("Launch","nothing","SearchText", "Searching the text", Status.PASS);
			}
		}
		if (b == false) {
			report.updateTestLog("Launch","nothing","SearchText", "Searching the text", Status.FAIL);
		}
	}

	
	/**
	 * @author sranja14
	 * @param driver
	 * @desc To scrolldown the page
	 */
    public void scrollDown(CraftDriver driver) {

        JavascriptExecutor jse6 = (JavascriptExecutor)driver.getWebDriver();
        jse6.executeScript("window.scrollBy(0,500)", "");

    }

  
    // autor: sranja14
    
    public void clickOnElementWeb(WebElement element, String elementName,   CraftDriver driver) throws IOException, UnsupportedFlavorException, AWTException {
    	
		try {
			element.click();
			report.updateTestLog("Click Element ","Click on element " +elementName,elementName +" should be displayed and clicked.", elementName + " is displayed and clicked as expected .", Status.PASS);
			
			
		} catch (Exception e) {
			report.updateTestLog("Click Element ","Click on element " +elementName,elementName +" should be displayed and clicked.", elementName + " is not displayed and clicked as expected .", Status.FAIL);
		}
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
    
    public void enterText(WebElement element, String elementName, String text, WebDriver driver) throws IOException, UnsupportedFlavorException, AWTException {
	
    	WebDriverWait wait = new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.visibilityOf(element));
		try {
			
			element.sendKeys(text);
		
			report.updateTestLog("Enter Text","Enter the text in the field "+elementName ,text + " must be entered in field " + elementName, text + " is successfully entered in field " + elementName, Status.PASS,driver);

		} catch (Exception e) {
			report.updateTestLog("Enter Text","Enter the text in the field "+elementName,text + " must be entered in field " + elementName,
					text + " is not successfully entered in field " + elementName + " as expected.", Status.FAIL,driver);
			
		}

	}

    public void getText(WebElement w, String fieldName, WebDriver driver) throws IOException, UnsupportedFlavorException, AWTException {
		WebDriverWait wait = new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.visibilityOf(w));
		if (w.isDisplayed()) {
			String data = w.getText();
			report.updateTestLog("Get Text","The text from the field "+fieldName+" is visible",fieldName + " should be displayed ", fieldName + " is displayed and as expected ",
					 Status.PASS, driver);
		} else {
			report.updateTestLog("Get Text","The text from the field "+fieldName+"is visible",fieldName + " should be displayed ", fieldName + " is displayed and as expected ",
					 Status.FAIL, driver);
		}
	} 
    
    public void verifyElementPresent(String xpath, String elementName, CraftDriver driver) throws IOException, UnsupportedFlavorException, AWTException {
    	WebDriverWait wait = new WebDriverWait(driver.getWebDriver(), 20);
        WebElement element =driver.findElement(By.xpath(xpath));
        wait.until(ExpectedConditions.visibilityOf(element));
        if (element.isDisplayed() == true) {
        	report.updateTestLog("Verify Element Presence ","Element must present " +elementName,elementName +" should be displayed.", elementName + " is displayed  as expected .", Status.PASS);
               

        } else {
        	report.updateTestLog("Verify Element Presence ","Element must present " +elementName,elementName +" should be displayed.", elementName + " is not displayed  as expected .", Status.FAIL);
               // ATUReports.add(Text +" is displayed as expected .",LogAs.FAILED,null);
        }

 }
    public void verifyElementNotPresent(String xpath, String elementName, CraftDriver driver) throws IOException, UnsupportedFlavorException, AWTException {
    	WebDriverWait wait = new WebDriverWait(driver.getWebDriver(), 20);
        WebElement element =driver.findElement(By.xpath(xpath));
        wait.until(ExpectedConditions.visibilityOf(element));
        if (element.isDisplayed() == true) {
        	report.updateTestLog("Verify Element Presence ","Element must present " +elementName,elementName +" should be displayed.", elementName + " is displayed  as expected .", Status.FAIL);
               

        } else {
        	report.updateTestLog("Verify Element Presence ","Element must present " +elementName,elementName +" should be displayed.", elementName + " is not displayed  as expected .", Status.PASS);
               // ATUReports.add(Text +" is displayed as expected .",LogAs.FAILED,null);
        }

 } 
    public void ClickElementFromListOfElements(String ValueName, String atributeType, String option, WebDriver driver)
            throws IOException, UnsupportedFlavorException, AWTException {
     boolean b = false;
     List<WebElement> ls = driver.findElements(By.id(ValueName));
     for (WebElement w : ls) {
            if (w.getAttribute(atributeType).equals(option)) {
                  w.click();
                  b = true;
                  report.updateTestLog("Click Element from List","Verify "+option+" is present",option + " should be displayed . ", option + " is displayed as expected .", Status.PASS,
                          driver);
            }
     }
     if (b == false) {
    	 report.updateTestLog("Click Element from List","Verify "+option+" is present",option + " should be displayed . ", option + " is not displayed as expected .", Status.FAIL,
                 driver);
     }
}
    
    public void verifyElementNotEnabled(String xpath, String elementName, CraftDriver driver) throws IOException, UnsupportedFlavorException, AWTException {
    	WebDriverWait wait = new WebDriverWait(driver.getWebDriver(), 20);
        WebElement element =driver.findElement(By.xpath(xpath));
        wait.until(ExpectedConditions.visibilityOf(element));
        if (element.isEnabled() == false) {
        	report.updateTestLog("Verify Element not Enabled ","Element must not enable " +elementName,elementName +" should not be enabled.", elementName + " is not enabled  as expected .", Status.PASS);
               

        } else {
        	report.updateTestLog("Verify Element not Enabled ","Element must not enable " +elementName,elementName +" should not be enabled.", elementName + " is enabled  as expected .", Status.FAIL);
               // ATUReports.add(Text +" is displayed as expected .",LogAs.FAILED,null);
        }

 }
    public void VerifyElementEnabled(String xpath, String elementName, CraftDriver driver) throws IOException, UnsupportedFlavorException, AWTException {
    	WebDriverWait wait = new WebDriverWait(driver.getWebDriver(), 20);
        WebElement element =driver.findElement(By.xpath(xpath));
        wait.until(ExpectedConditions.visibilityOf(element));
        if (element.isEnabled() == true) {
        	report.updateTestLog("Verify Element Enabled ","Element must enable " +elementName,elementName +" should be enabled.", elementName + " is enabled  as expected .", Status.PASS);
               

        } else {
        	report.updateTestLog("Verify Element Enabled ","Element must enable " +elementName,elementName +" should not be enabled.", elementName + " is not enabled  as expected .", Status.FAIL);
               // ATUReports.add(Text +" is displayed as expected .",LogAs.FAILED,null);
        }

 } 
	public void clickDown(String xpath,CraftDriver driver) throws InterruptedException
	{
		driver.findElement(By.xpath(xpath)).sendKeys(Keys.ARROW_DOWN);
		
	}
	
	public void clickEnter(String xpath,CraftDriver driver) throws InterruptedException
	{
		driver.findElement(By.xpath(xpath)).sendKeys(Keys.ENTER);
		
	}
    

      
    
  


    
}


