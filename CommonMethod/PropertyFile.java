package CommonMethod;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;

public class PropertyFile extends ReusableLibrary{

	public PropertyFile(ScriptHelper scriptHelper) {
		super(scriptHelper);
		// TODO Auto-generated constructor stub
	}

	public Properties Prop;
	/**
	 * @author soumdash
	 * @param Filepath
	 * @desc to Set property file path
	 */
	public void setPropertyFile(String Filepath)
	{
		File file =new File(Filepath);
		FileInputStream fis = null ;
		try {
			fis =new FileInputStream(file);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			Prop=new Properties();
			Prop.load(fis);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public String getProperty(String key)
	{
		String keyData =null;
		keyData=Prop.getProperty(key);
		return keyData;
	}
	
}
