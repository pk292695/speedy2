package uri;

import java.awt.AWTException;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.util.List;
import java.util.UUID;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;
import uriPage.FillClusterPage;
import CommonMethod.CommonMethod;

/*
 * 
 * 
 * 
 * 
 * 
 * @author: Praveen Kumar K
 * FillCluster
 * AllProjectFilter
 * 
 * 
 * 
 * 
 * 
 */	
public class FillCluster extends ReusableLibrary {
	CommonMethod commonMethod = new CommonMethod(scriptHelper);
	public FillCluster(ScriptHelper scriptHelper){
		// TODO Auto-generated constructor stub
		super(scriptHelper);
	}
	/*
	 * Log-in
	 */
	public void LogIn(String SheetName,String UserName, String Password) throws IOException, UnsupportedFlavorException, AWTException{
		
		commonMethod.enterText(FillClusterPage.Username, "User Name", dataTable.getData("functional_flow", UserName), driver);
		commonMethod.enterText(FillClusterPage.Password, "Password", dataTable.getData("functional_flow", Password), driver);
		commonMethod.clickOnElement(FillClusterPage.Login, "Login button", driver);
	}
	/*
	 * General Project Information
	 */
	public void GeneralProjectInformation() throws IOException, UnsupportedFlavorException, AWTException{
		commonMethod.clickOnElement(FillClusterPage.GenProjInfo, "General Project Info link", driver);
		commonMethod.clickOnElement(FillClusterPage.FieldImpa, "Field IMPA ", driver);
		commonMethod.clearElement(FillClusterPage.FieldDate, "Field Date clear", driver);
		commonMethod.enterText(FillClusterPage.FieldDate, "Field Date Entered", dataTable.getData("Test_Data", "FieldDate"), driver);
		commonMethod.selectOptionFromDropDown(FillClusterPage.FieldPhase,( dataTable.getData("Test_Data", "FieldPhase")) , driver);
		commonMethod.selectOptionFromDropDown(FillClusterPage.SponserName,( dataTable.getData("Test_Data", "SponserName")) , driver);
		commonMethod.selectOptionFromDropDown(FillClusterPage.SponserAddr,( dataTable.getData("Test_Data", "SponserAddr")) , driver);
		commonMethod.enterText(FillClusterPage.Therapeutic, "Therapeutic Area", dataTable.getData("Test_Data", "Therapeutic"), driver);
		commonMethod.clickOnElement(FillClusterPage.GcdoCro, "GCDO or CRO", driver);
		commonMethod.clickOnElement(FillClusterPage.PatientAlert, "Patient Alert card", driver);
		
	}
	/*
	 * Submit for General Project Information
	*/
	public void SubmitGeneralProjectInformation() throws IOException, UnsupportedFlavorException, AWTException{
		commonMethod.clickOnElement(FillClusterPage.Submit, "Submit for Approval", driver);
		commonMethod.clickOnElement(FillClusterPage.SaveSubmit, "Save&Submit for approval", driver);
	}
	/*
	 * Country Information tab	
	*/
	public void CountryInformation() throws IOException, UnsupportedFlavorException, AWTException{
		commonMethod.clickOnElement(FillClusterPage.Country, "Country Info button", driver);
		commonMethod.clickOnElement(FillClusterPage.AddParticipatingCountry, "Add Participating Country button", driver);
		commonMethod.clickOnElement(FillClusterPage.participatingCountry, "Country",  driver);
		commonMethod.clickOnElement(FillClusterPage.selectCountry, "Select country", driver);
		commonMethod.enterText(FillClusterPage.countrySpecificInformation, "Country Specific Information", dataTable.getData("Test_Data", "CountrySpecific"), driver);
		commonMethod.clickOnElement(FillClusterPage.addParticipatingCountryButton, "Add Participating Country Button", driver);
	}
	/* 
	 * Submit for approval Country Information 
	 * */
	public void SubmitCountryInformation() throws IOException, UnsupportedFlavorException, AWTException{
		commonMethod.clickOnElement(FillClusterPage.CntrySubmit, "Submit for Approval", driver);
		commonMethod.clickOnElement(FillClusterPage.CntrySaveSubmit, "Save&Submit for approval", driver);
	} 
	/*
	 * General Kit Type Info
	*/
	public void GeneralKitTypeInfo() throws IOException, UnsupportedFlavorException, AWTException{
		commonMethod.clickOnElement(FillClusterPage.GeneralKitTypeInfo, "General Kit Type Info Tab", driver);
		commonMethod.clickOnElement(FillClusterPage.NarcoticRadioButton, "Narcotic Radio Button", driver);
		commonMethod.scrollDown(driver);
		/*commonMethod.clickOnElement(FillClusterPage.BlindingType, "Blinding type field", driver);
		String BindingTypeName = dataTable.getData("Test_Data", "BlindingType");
		System.out.println("Binding Type :"+ BindingTypeName);*/
		commonMethod.enterText(FillClusterPage.BlindingType, "Blinding Text Area", dataTable.getData("Test_Data", "BlindingType"), driver);
		WebElement enter0 = driver.findElement(By.xpath(FillClusterPage.BlindingType));
		enter0.sendKeys(Keys.ENTER);
		//commonMethod.selectOptionFromDropDown(FillClusterPage.BlindingType, dataTable.getData("Test_Data", "Prodnamestrength"), driver);
		
		commonMethod.clickOnElement(FillClusterPage.childResistant, "Child Resistant Radio Button", driver);
		commonMethod.clickOnElement(FillClusterPage.AccessoriesOutsideKitConstrucation, "Accessories Outside Kit Construction radio Button", driver);
		commonMethod.enterText(FillClusterPage.SapCodes, "Enter Accessories SAP Codes", dataTable.getData("Test_Data", "AccessSAP"), driver);
		commonMethod.clickOnElement(FillClusterPage.inClinic, "In Clinic/ Hospital Administration Radio Button", driver);
		commonMethod.enterText(FillClusterPage.packagingSitesPrimary, "Packaging Sites - Primary Text Box", dataTable.getData("Test_Data", "PackPrimary"), driver);
		WebElement enter1 = driver.findElement(By.xpath(FillClusterPage.packagingSitesPrimary));
		enter1.sendKeys(Keys.ENTER);
		commonMethod.enterText(FillClusterPage.packagingSitesSecondary, "packaging Sites - Secondary Text Box", dataTable.getData("Test_Data", "PackSecondary"), driver);
		WebElement enter2 = driver.findElement(By.xpath(FillClusterPage.packagingSitesSecondary));
		enter2.sendKeys(Keys.ENTER);
		commonMethod.enterText(FillClusterPage.general_Kitcomments, "Comments text Area", dataTable.getData("Test_Data", "Comments"), driver);
	}
	/*
	 *Pack Type Info
	 * 
	*/
	public void PackTypeInfo() throws IOException, UnsupportedFlavorException, AWTException{
		commonMethod.clickOnElement(FillClusterPage.PacktypeTab, "Pack type tab", driver);
		commonMethod.clickOnElement(FillClusterPage.Packoption1, "Pack type option", driver);
		commonMethod.selectOptionFromDropDown(FillClusterPage.ProdName, dataTable.getData("Test_Data", "Prodnamestrength"), driver);
		commonMethod.selectOptionFromDropDown(FillClusterPage.Dosage, dataTable.getData("Test_Data", "DoseForm"), driver);
		commonMethod.selectOptionFromDropDown(FillClusterPage.ROA, dataTable.getData("Test_Data", "ROA"), driver);
		commonMethod.selectOptionFromDropDown(FillClusterPage.ROAPict, dataTable.getData("Test_Data", "ROApict"), driver);
		commonMethod.selectOptionFromDropDown(FillClusterPage.DoseInstruction, dataTable.getData("Test_Data", "DosingInst"), driver);
		commonMethod.selectOptionFromDropDown(FillClusterPage.Tempstore, dataTable.getData("Test_Data", "TempStore"), driver);
		commonMethod.selectOptionFromDropDown(FillClusterPage.OtherStore, dataTable.getData("Test_Data", "OtherStore"), driver);
		commonMethod.selectOptionFromDropDown(FillClusterPage.HandIns, dataTable.getData("Test_Data", "HandInst"), driver);
		commonMethod.selectOptionFromDropDown(FillClusterPage.PeriodDes, dataTable.getData("Test_Data", "PeriodDes"), driver);
		commonMethod.selectOptionFromDropDown(FillClusterPage.Othervar, dataTable.getData("Test_Data", "OtherVariable"), driver);
		commonMethod.clickOnElement(FillClusterPage.TearOff, "Tear-off", driver);
		commonMethod.enterText(FillClusterPage.packtypeComments, "Comments",dataTable.getData("Test_Data", "Comments"), driver);
	}
	/*
	 * Primary PAck Filling info
	 */
	public void PrimaryPackInfoFill()throws IOException, AWTException, UnsupportedFlavorException{
		commonMethod.clickOnElement(FillClusterPage.PrimaryPack, "Primary Pack Indo", driver);
		commonMethod.clickOnElement(FillClusterPage.primarypacktrue, "Primary Pack Indo", driver);
		
		commonMethod.clickOnElement(FillClusterPage.bottle, "Primary Pack Indo", driver);
		List<WebElement> bottle =  driver.findElements(By.xpath(FillClusterPage.bottle));
		for (int i = 0; i <= bottle.size(); i++) {
				bottle.get(i).click();
				break;
		}
		commonMethod.clickOnElement(FillClusterPage.cap, "Primary Pack Indo", driver);
		List<WebElement> cap =  driver.findElements(By.xpath(FillClusterPage.cap));
		for (int i = 0; i <= cap.size(); i++) {
				cap.get(i).click();
				break;
		}
		commonMethod.clickOnElement(FillClusterPage.Desiccant, "Primary Pack Indo", driver);
		List<WebElement> Desiccant =  driver.findElements(By.xpath(FillClusterPage.Desiccant));
		for (int i = 0; i <= Desiccant.size(); i++) {
				Desiccant.get(i).click();
				break;
		}
		commonMethod.clickOnElement(FillClusterPage.oxygen, "Primary Pack Indo", driver);
		List<WebElement> oxygen =  driver.findElements(By.xpath(FillClusterPage.oxygen));
		for (int i = 0; i <= oxygen.size(); i++) {
				oxygen.get(i).click();
				break;
		}
		commonMethod.clickOnElement(FillClusterPage.Lidding, "Primary Pack Indo", driver);
		List<WebElement> Lidding =  driver.findElements(By.xpath(FillClusterPage.Lidding));
		for (int i = 0; i <= Lidding.size(); i++) {
				Lidding.get(i).click();
				break;
		}
		commonMethod.clickOnElement(FillClusterPage.Forming, "Primary Pack Indo", driver);
		List<WebElement> Forming =  driver.findElements(By.xpath(FillClusterPage.Forming));
		for (int i = 0; i <= Forming.size(); i++) {
				Forming.get(i).click();
				break;
		}
		
	}
	/* 
	 * Submit for approval General kit and pack type tab 
	 */ 
	public void SubmitGeneralKitPackType() throws IOException, UnsupportedFlavorException, AWTException{
		commonMethod.clickOnElement(FillClusterPage.GenKitPackSubmit, "Submit for Approval", driver);
		commonMethod.clickOnElement(FillClusterPage.GenKitPackSaveSubmit, "Save&Submit for approval", driver);
	} 
	/*
 	 * Drug Product Info
	*/
	public void DrugProductInfo() throws IOException, UnsupportedFlavorException, AWTException{
		commonMethod.clickOnElement(FillClusterPage.DrugProductInfo, "Drug Product info", driver);
		commonMethod.clickOnElement(FillClusterPage.AddSapMaterial, "ADD SAP Material", driver);
		commonMethod.enterText(FillClusterPage.SAPmaterialnNm, "SAP Material master number", dataTable.getData("Test_Data", "SAP"), driver);
		commonMethod.clickOnElement(FillClusterPage.SAPSearch, "Click on search button", driver);
		commonMethod.clickOnElement(FillClusterPage.Sap19001342, "Click on 19001342", driver);	
		commonMethod.clickOnElement(FillClusterPage.AddSapMaterialButton, "Click on ADD SAP for 19001342", driver);
		commonMethod.selectOptionFromDropDown(FillClusterPage.DrugProdCountry, dataTable.getData("Test_Data", "DrugCountry"), driver);
		commonMethod.enterText(FillClusterPage.DrugsubstanceCompany, "Drug Substance Company", dataTable.getData("Test_Data", "DrugSubstanceCompany"), driver);
		commonMethod.selectOptionFromDropDown(FillClusterPage.DrugSubtanceCountry, dataTable.getData("Test_Data", "DrugSubstanceCountry"), driver);
		commonMethod.clickOnElement(FillClusterPage.Coldchain, "Cold chain drug product", driver);
		commonMethod.clickOnElement(FillClusterPage.MoistureSensitive, "Moisture Sensitive drug product", driver);
		commonMethod.clickOnElement(FillClusterPage.LightSensitive, "Light Sensitive", driver);
		commonMethod.clickOnElement(FillClusterPage.OxygenSensitive, "Oxygen Sensitive", driver);
		commonMethod.clickOnElement(FillClusterPage.Frozenproduct, "Frozen product", driver);
	
	}
	
	/*
	 *Primary Pack Info 
	 */	
	public void PrimaryPackInfo() throws IOException, UnsupportedFlavorException, AWTException{
		commonMethod.clickOnElement(FillClusterPage.PrimaryPack, "Primary Pack Indo", driver);
		commonMethod.clickOnElement(FillClusterPage.PackInfoExecuted, "Pack Info Executed", driver);
	}
	
	/*
	 * Submit for approval-  Drug product and primary pack Info
	 */
	public void SubmitDrugPrimary() throws IOException, UnsupportedFlavorException, AWTException
	{
	commonMethod.clickOnElement(FillClusterPage.SubmitDrugPrim, "Drug product and Primary pack submit", driver);
	commonMethod.clickOnElement(FillClusterPage.SavesubmitDrugPrim, "Save and submit", driver);
	}
	
	
	
	/*
	 * Filter Non OMP Draft	
	*/
	public void FilterNonOMPDraft() throws IOException, UnsupportedFlavorException, AWTException{
		commonMethod.clickOnElement(FillClusterPage.filterButton, "All Project Filter Button", driver);
		commonMethod.clickOnElement(FillClusterPage.filterNonOmp, "Filter Non-OMP Check Box", driver);
		commonMethod.clickOnElement(FillClusterPage.filterActive, "Filter Active Check Box", driver);
		commonMethod.clickOnElement(FillClusterPage.filterDraft, "Filter Draft Button Check Box", driver);
		commonMethod.clickOnElement(FillClusterPage.applyFilterButton, "Apply Filter Button", driver);
	}
	/*
	 * Filter Non OMP Awaiting Approval	
	*/
	public void FilterNonOMPAwaiting() throws IOException, UnsupportedFlavorException, AWTException{
		commonMethod.clickOnElement(FillClusterPage.filterButton, "All Project Filter Button", driver);
		commonMethod.clickOnElement(FillClusterPage.filterNonOmp, "Filter Non-OMP Check Box", driver);
		//commonMethod.clickOnElement(FillClusterPage.filterActive, "Filter Active Check Box", driver);
		commonMethod.clickOnElement(FillClusterPage.filterAwaitingApproval, "Filter Awaiting Approval Check Box", driver);
		commonMethod.clickOnElement(FillClusterPage.applyFilterButton, "Apply Filter Button", driver);
	}
	/*
	 * Filter Non OMP Partial Approval	
	*/
	public void FilterNonOMPPartial() throws IOException, UnsupportedFlavorException, AWTException{
		commonMethod.clickOnElement(FillClusterPage.filterButton, "All Project Filter Button", driver);
		commonMethod.clickOnElement(FillClusterPage.filterNonOmp, "Filter Non-OMP Check Box", driver);
		commonMethod.clickOnElement(FillClusterPage.filterActive, "Filter Active Check Box", driver);
		commonMethod.clickOnElement(FillClusterPage.filterPartiallyApproved, "Filter Partially Approved Check Box", driver);
		commonMethod.clickOnElement(FillClusterPage.applyFilterButton, "Apply Filter Button", driver);
	}
	/*
	 * Filter Non OMP Fully Approved	
	*/
	public void FilternonOMPFull() throws IOException, UnsupportedFlavorException, AWTException{
		commonMethod.clickOnElement(FillClusterPage.filterButton, "All Project Filter Button", driver);
		commonMethod.clickOnElement(FillClusterPage.filterNonOmp, "Filter Non-OMP Check Box", driver);
		commonMethod.clickOnElement(FillClusterPage.filterActive, "Filter Active Check Box", driver);
		commonMethod.clickOnElement(FillClusterPage.filterFullyApproved, "Filter Fully Approved check Box", driver);
		commonMethod.clickOnElement(FillClusterPage.applyFilterButton, "Apply Filter Button", driver);
	}
	/*
	 * Filter OMP Draft	
	*/
	public void FilterOMPDraft()throws IOException, UnsupportedFlavorException, AWTException{
		commonMethod.clickOnElement(FillClusterPage.filterButton, "All Project Filter Button", driver);
		commonMethod.clickOnElement(FillClusterPage.filterOmp, "Filter OMP Check Box", driver);
		commonMethod.clickOnElement(FillClusterPage.filterActive, "Filter Active Check Box", driver);
		commonMethod.clickOnElement(FillClusterPage.filterDraft, "Filter Draft Check Box", driver);
		commonMethod.clickOnElement(FillClusterPage.applyFilterButton, "Apply Filter Button", driver);
	}
	/*
	 * Filter OMP Awaiting Approval	
	*/
	public void FilterOMPAwaiting()throws IOException, UnsupportedFlavorException, AWTException{
		commonMethod.clickOnElement(FillClusterPage.filterButton, "All Project Filter Button", driver);
		commonMethod.clickOnElement(FillClusterPage.filterOmp, "Filter OMP Check Box", driver);
		commonMethod.clickOnElement(FillClusterPage.filterActive, "Filter Active Check Box", driver);
		commonMethod.clickOnElement(FillClusterPage.filterAwaitingApproval, "Filter Awaiting Approval Check Box", driver);
		commonMethod.clickOnElement(FillClusterPage.applyFilterButton, "Apply Filter Button", driver);
	}
	/*
	 * Filter OMP Partial Approval	
	*/
	public void FilterOMPPartial()throws IOException, UnsupportedFlavorException, AWTException{
		commonMethod.clickOnElement(FillClusterPage.filterButton, "All Project Filter Button", driver);
		commonMethod.clickOnElement(FillClusterPage.filterOmp, "Filter OMP Check Box", driver);
		commonMethod.clickOnElement(FillClusterPage.filterActive, "Filter Active Check Box", driver);
		commonMethod.clickOnElement(FillClusterPage.filterPartiallyApproved, "Filter Partial Approved Check Box", driver);
		commonMethod.clickOnElement(FillClusterPage.applyFilterButton, "Apply Filter Button", driver);
	}
	/*
	 * Filter OMP Full Approved
	*/
	public void FilterOMPFul()throws IOException, UnsupportedFlavorException, AWTException{
		commonMethod.clickOnElement(FillClusterPage.filterButton, "All Project Filter Button", driver);
		commonMethod.clickOnElement(FillClusterPage.filterOmp, "Filter OMP Check Box", driver);
		commonMethod.clickOnElement(FillClusterPage.filterActive, "Filter Active Check Box", driver);
		commonMethod.clickOnElement(FillClusterPage.filterFullyApproved, "Filter Full Approed Check Box", driver);
		commonMethod.clickOnElement(FillClusterPage.applyFilterButton, "Apply Filter Button", driver);
	}
	/*
	 * Filter BrightStock Draft	
	*/
	public void FilterBrightDraft()throws IOException, UnsupportedFlavorException, AWTException{
		commonMethod.clickOnElement(FillClusterPage.filterButton, "All Project Filter Button", driver);
		commonMethod.clickOnElement(FillClusterPage.filterBrightStock, "Filter Bright Check Box", driver);
		commonMethod.clickOnElement(FillClusterPage.filterActive, "Filter Active Check Box", driver);
		commonMethod.clickOnElement(FillClusterPage.filterDraft, "Filter Draft Check Box", driver);
		commonMethod.clickOnElement(FillClusterPage.applyFilterButton, "Apply Filter Button", driver);
	}
	/*
	 * Filter BrightStock Awaiting Approval	
	*/
	public void FilterBrightAwaiting()throws IOException, UnsupportedFlavorException, AWTException{
		commonMethod.clickOnElement(FillClusterPage.filterButton, "All Project Filter Button", driver);
		commonMethod.clickOnElement(FillClusterPage.filterBrightStock, "Filter Bright Check Box", driver);
		commonMethod.clickOnElement(FillClusterPage.filterActive, "Filter Active Check Box", driver);
		commonMethod.clickOnElement(FillClusterPage.filterAwaitingApproval, "Filter Awaiting Approval Check Box", driver);
		commonMethod.clickOnElement(FillClusterPage.applyFilterButton, "Apply Filter Button", driver);
	}
	/*
	 * Filter BrightStock Partial Approval	
	*/
	public void FilterBrightPartial()throws IOException, UnsupportedFlavorException, AWTException{
		commonMethod.clickOnElement(FillClusterPage.filterButton, "All Project Filter Button", driver);
		commonMethod.clickOnElement(FillClusterPage.filterBrightStock, "Filter Bright Check Box", driver);
		commonMethod.clickOnElement(FillClusterPage.filterActive, "Filter Active Check Box", driver);
		commonMethod.clickOnElement(FillClusterPage.filterPartiallyApproved, "Filter partial Approved Check Box", driver);
		commonMethod.clickOnElement(FillClusterPage.applyFilterButton, "Apply Filter Button", driver);
	}
	/*
	 * Filter BrightStock Fully Approved	
	*/
	public void FilterBrightFull()throws IOException, UnsupportedFlavorException, AWTException{
		commonMethod.clickOnElement(FillClusterPage.filterButton, "All Project Filter Button", driver);
		commonMethod.clickOnElement(FillClusterPage.filterBrightStock, "Filter Bright Check Box", driver);
		commonMethod.clickOnElement(FillClusterPage.filterFullyApproved, "Filter Fully Approved Check Box", driver);
		commonMethod.clickOnElement(FillClusterPage.applyFilterButton, "Apply Filter Button", driver);
	}
	/*
	 * Filter Projectname with In active, non omp and Awaiting approval
	 */
	public void FilterProjectnameInactive(String ProjectName) throws IOException, UnsupportedFlavorException, AWTException{
		commonMethod.clickOnElement(FillClusterPage.filterButton, "All Project Filter Button", driver);
		commonMethod.enterText(FillClusterPage.filterProjectname, "Project Name Text Area", ProjectName,driver);
		commonMethod.clickOnElement(FillClusterPage.filterInActive, "click on Inactive Filter Radio Button", driver);
		commonMethod.clickOnElement(FillClusterPage.filterActive, "click on Inactive Filter Radio Button", driver);
		commonMethod.clickOnElement(FillClusterPage.filterNonOmp, "Filter Non-OMP Check Box", driver);
		//commonMethod.clickOnElement(FillClusterPage.filterActive, "Filter Active Check Box", driver);
		commonMethod.clickOnElement(FillClusterPage.filterAwaitingApproval, "Filter Awaiting Approval Check Box", driver);
		commonMethod.clickOnElement(FillClusterPage.applyFilterButton, "Apply Filter Button", driver);
	}
	/*
	 * Filter Projectname with active, non omp and Awaiting approval
	 */
	public void FilterProjectnameactive(String ProjectName) throws IOException, UnsupportedFlavorException, AWTException{
		commonMethod.clickOnElement(FillClusterPage.filterButton, "All Project Filter Button", driver);
		commonMethod.enterText(FillClusterPage.filterProjectname, "Project Name Text Area", ProjectName,driver);
		commonMethod.clickOnElement(FillClusterPage.filterActive, "click on active Filter Radio Button", driver);
	//	commonMethod.clickOnElement(FillClusterPage.filterInActive, "click on Inactive Filter Radio Button", driver);
		commonMethod.clickOnElement(FillClusterPage.filterNonOmp, "Filter Non-OMP Check Box", driver);
		//commonMethod.clickOnElement(FillClusterPage.filterActive, "Filter Active Check Box", driver);
		commonMethod.clickOnElement(FillClusterPage.filterAwaitingApproval, "Filter Awaiting Approval Check Box", driver);
		commonMethod.clickOnElement(FillClusterPage.applyFilterButton, "Apply Filter Button", driver);
	}
	/*
	 * Filter Projectname only
	 */
	public void FilteronlyName(String ProjectName) throws IOException, UnsupportedFlavorException, AWTException{
		commonMethod.clickOnElement(FillClusterPage.filterButton, "All Project Filter Button", driver);
		commonMethod.enterText(FillClusterPage.filterProjectname, "Project Name Text Area", ProjectName,driver);
		commonMethod.clickOnElement(FillClusterPage.applyFilterButton, "Apply Filter Button", driver);
	}
	/*
	 * Workflow inbox filter with General Project Information
	 */
	public void WIFilterGPI() throws IOException, UnsupportedFlavorException, AWTException{
		commonMethod.clickOnElement(FillClusterPage.WIFilterButton	,"Click Filter Button", driver);
		commonMethod.clickOnElement(FillClusterPage.WIFilterGeneralPrjInfo, "Select general Project Information Check box", driver);
		commonMethod.clickOnElement(FillClusterPage.applyFilterButton, "Click on Apply Filter Button", driver);
		commonMethod.clickOnElement(FillClusterPage.WIHideFilter, "Hide Filter Button", driver);
	}
	/*
	 * WorkFlow inbox filter with Country & Depot info
	 */
	public void WIFilterCountryDepot() throws IOException, UnsupportedFlavorException, AWTException{
		commonMethod.clickOnElement(FillClusterPage.WIFilterButton	,"Click Filter Button", driver);
		commonMethod.clickOnElement(FillClusterPage.WIFilterCountDepotinfo, "Select Country & Deport Information Check box", driver);
		//commonMethod.clickOnElement(FillClusterPage.applyFilterButton, "Click on Apply Filter Button", driver);
		//commonMethod.clickOnElement(FillClusterPage.WIHideFilter, "Hide Filter Button", driver);
	}
	/*
	 * WorkFlow inbox filter General Kit type & Pack type info
	 */
	public void WIFilterGeneralKitPack() throws IOException, UnsupportedFlavorException, AWTException{
		commonMethod.clickOnElement(FillClusterPage.WIFilterButton	,"Click Filter Button", driver);
		commonMethod.clickOnElement(FillClusterPage.WIFilterGeneralKitPackInfo, "Select general Kit Type & Pack Type Information Check box", driver);
		/*commonMethod.clickOnElement(FillClusterPage.applyFilterButton, "Click on Apply Filter Button", driver);
		commonMethod.clickOnElement(FillClusterPage.WIHideFilter, "Hide Filter Button", driver);*/
	}
	/*
	 * WorkFlow inbox filter Drug product & Primary Pack Info
	 */
	public void WIFilterDrugProductPrimaryPack() throws IOException, UnsupportedFlavorException, AWTException{
		commonMethod.clickOnElement(FillClusterPage.WIFilterButton	,"Click Filter Button", driver);
		commonMethod.clickOnElement(FillClusterPage.WiFilterDrugProductPrimaryPackInfo, "Select Drug Product & Primary Pack Information Check box", driver);
		commonMethod.clickOnElement(FillClusterPage.applyFilterButton, "Click on Apply Filter Button", driver);
		commonMethod.clickOnElement(FillClusterPage.WIHideFilter, "Hide Filter Button", driver);
	}
	/*
	 * WorkFlow inbox filter All project Info
	 */
	public void WIFilterAllProjectInfo() throws IOException, UnsupportedFlavorException, AWTException{
		commonMethod.clickOnElement(FillClusterPage.WIFilterButton	,"Click Filter Button", driver);
		commonMethod.clickOnElement(FillClusterPage.WIFilterAllProjectInfo, "Select All Project Information Check box", driver);
		commonMethod.clickOnElement(FillClusterPage.applyFilterButton, "Click on Apply Filter Button", driver);
		commonMethod.clickOnElement(FillClusterPage.WIHideFilter, "Hide Filter Button", driver);
	}
	/*
	 * WorkFlow inbox filter with Mel Compilation info
	 */
	public void WIFilterMelCompilation() throws IOException, UnsupportedFlavorException, AWTException{
		commonMethod.clickOnElement(FillClusterPage.WIFilterButton	,"Click Filter Button", driver);
		commonMethod.clickOnElement(FillClusterPage.WIFilterMELcompilation, "Select MEL Compilation Check box", driver);
		/*commonMethod.clickOnElement(FillClusterPage.applyFilterButton, "Click on Apply Filter Button", driver);
		commonMethod.clickOnElement(FillClusterPage.WIHideFilter, "Hide Filter Button", driver);*/
	}
	/*
	 * WorkFlow inbox filter with Bright Stock Project
	 */
	public void WIFilterBrightStockProject() throws IOException, UnsupportedFlavorException, AWTException{
		commonMethod.clickOnElement(FillClusterPage.WIFilterButton	,"Click Filter Button", driver);
		commonMethod.clickOnElement(FillClusterPage.WIFilterBrightStockProject, "Select BrightStockProject Project Information Check box", driver);
		commonMethod.clickOnElement(FillClusterPage.applyFilterButton, "Click on Apply Filter Button", driver);
		commonMethod.clickOnElement(FillClusterPage.WIHideFilter, "Hide Filter Button", driver);
	}
	/*
	 * WorkFlow inbox filter as open
	 */
	public void WIFilterOpen() throws IOException, UnsupportedFlavorException, AWTException{
		//commonMethod.clickOnElement(FillClusterPage.WIFilterButton	,"Click Filter Button", driver);
		commonMethod.clickOnElement(FillClusterPage.WIFilteropen, "Select Open Filter", driver);
		commonMethod.clickOnElement(FillClusterPage.applyFilterButton, "Click on Apply Filter Button", driver);
		commonMethod.clickOnElement(FillClusterPage.WIHideFilter, "Hide Filter Button", driver);
	}
	/*
	 * WorkFlow inbox filter as closed
	 */
	public void WIFilterclosed() throws IOException, UnsupportedFlavorException, AWTException{
		commonMethod.clickOnElement(FillClusterPage.WIFilterButton	,"Click Filter Button", driver);
		commonMethod.clickOnElement(FillClusterPage.WIFilterClosed, "Select closed Filter", driver);
		commonMethod.clickOnElement(FillClusterPage.applyFilterButton, "Click on Apply Filter Button", driver);
		commonMethod.clickOnElement(FillClusterPage.WIHideFilter, "Hide Filter Button", driver);
	}
	/*
	 * WorkFlow inbox filter as Submitted by text
	 */
	public void WiFilterSubmittedBy(String Text) throws IOException, UnsupportedFlavorException, AWTException{
		commonMethod.clickOnElement(FillClusterPage.WIFilterButton	,"Click Filter Button", driver);
		commonMethod.enterText(FillClusterPage.WIFiltersubmmittedText, "Submitted By Text Box", Text, driver);
		commonMethod.clickOnElement(FillClusterPage.applyFilterButton, "Click on Apply Filter Button", driver);
		commonMethod.clickOnElement(FillClusterPage.WIHideFilter, "Hide Filter Button", driver);
	}
	/*
	 * WorkFlow inbox Filter as Compound by Text
	 */
	public void WiFilterCompound(String Text) throws IOException, UnsupportedFlavorException, AWTException{
		commonMethod.clickOnElement(FillClusterPage.WIFilterButton	,"Click Filter Button", driver);
		commonMethod.enterText(FillClusterPage.WIFilterCompoundText, "Compound Text Box", Text, driver);
		commonMethod.clickOnElement(FillClusterPage.applyFilterButton, "Click on Apply Filter Button", driver);
		commonMethod.clickOnElement(FillClusterPage.WIHideFilter, "Hide Filter Button", driver);
	}
	/*
	 * Start new Project
	 */
	public static String name = null;
	public void StartNewProject() throws IOException, UnsupportedFlavorException, AWTException{
		commonMethod.clickOnElement(FillClusterPage.StartNewProject, "Start new Project", driver);
		name = UUID.randomUUID().toString();
		commonMethod.enterText(FillClusterPage.ProjectId, "Project Id Text Area", name, driver);
		String Compound = UUID.randomUUID().toString();
		commonMethod.enterText(FillClusterPage.Compound, "Compound Name", Compound, driver);
		commonMethod.clickOnElement(FillClusterPage.StartProjectcnfrmbutton, "Start Project confirm Button", driver);
	}
	/*
	 * Create Kit Type
	 */
	public void createKitType(String KitTypeName) throws IOException, UnsupportedFlavorException, AWTException{
		commonMethod.clickOnElement(FillClusterPage.selectKitType, "drop down kit type", driver);
		
		List<WebElement> kittypeoptionwithnew =  driver.findElements(By.xpath(FillClusterPage.selectKitType));
		int list1 = kittypeoptionwithnew.size();
		System.out.println(list1);
		
		commonMethod.clickOnElement(FillClusterPage.Addnew, "Add New Kit type", driver);
		commonMethod.enterText(FillClusterPage.kitTypeName, "Entering Kit Type Name", KitTypeName, driver);
		commonMethod.clickOnElement(FillClusterPage.KitsaveButton, "Add Kit Type Button", driver);
	
	}
	public void CanKitType() throws IOException, UnsupportedFlavorException, AWTException{
		commonMethod.clickOnElement(FillClusterPage.selectKitType, "drop down kit type", driver);
		List<WebElement> kittypeoptionwithnew =  driver.findElements(By.xpath(FillClusterPage.selectKitType));
		int list1 = kittypeoptionwithnew.size();
		System.out.println(list1);
		
		commonMethod.clickOnElement(FillClusterPage.Addnew, "Add New Kit type", driver);
		commonMethod.clickOnElement(FillClusterPage.newKitTypeCancel, "Cancel Create new  Kit type", driver);
	}
	/*
	 * Select Kit type
	 */
	public void SelectKitType(String KitTypeName) throws IOException, UnsupportedFlavorException, AWTException{
		commonMethod.clickOnElement(FillClusterPage.selectKitType, "drop down kit type", driver);
		List<WebElement> kittypeoptionwithnew2 =  driver.findElements(By.xpath(FillClusterPage.selectKitType));
		
		
		System.out.println(kittypeoptionwithnew2.toString());
	//	commonMethod.clickOnElement(FillClusterPage.selectKitType, "drop down kit type", driver);
		for (int i = 0; i <= kittypeoptionwithnew2.size(); i++) {
			
			String temp = kittypeoptionwithnew2.get(i).getText();
			System.out.println(temp);
			System.out.println("going into for loop");
			if(temp.equals(KitTypeName)){
				System.out.println("going into if block");
				kittypeoptionwithnew2.get(i).click();
				
				break;
			}
		}
	}
	/*
	 * OMP KIT type
	 */
	public void SelectKitTypeomp() throws IOException, UnsupportedFlavorException, AWTException{
		commonMethod.clickOnElement(FillClusterPage.selectKitType, "drop down kit type", driver);
		List<WebElement> kittypeoptionwithnew2 =  driver.findElements(By.xpath(FillClusterPage.selectKitType));
		
		
		System.out.println(kittypeoptionwithnew2.toString());
	//	commonMethod.clickOnElement(FillClusterPage.selectKitType, "drop down kit type", driver);
		for (int i = 0; i <= kittypeoptionwithnew2.size(); i++) {
			
			String temp = kittypeoptionwithnew2.get(i).getText();
			System.out.println(temp);
			System.out.println("going into for loop");
			
				System.out.println("going into if block");
				kittypeoptionwithnew2.get(i).click();
				
				break;
			
		}
	}
	/*
	 * General project Information(Brightstock project)
	 */
	public void BSGenProjInfo() throws IOException, UnsupportedFlavorException, AWTException
	{
		commonMethod.clickOnElement(FillClusterPage.GenProjInfo, "General project Info button", driver);
		commonMethod.selectOptionFromDropDown(FillClusterPage.SponserName, dataTable.getData("Test_Data", "SponserName"), driver);
		commonMethod.selectOptionFromDropDown(FillClusterPage.SponserAddr, dataTable.getData("Test_Data", "SponserAddr"), driver);
		commonMethod.clearElement(FillClusterPage.KitNumber, "Kit number field", driver);
		commonMethod.clearElement(FillClusterPage.Format, "Format field", driver);
		commonMethod.clearElement(FillClusterPage.Prefix, "Pre-fix field", driver);
	}
	
	/*
	 * General kit type (Brightstock Project)
	 */
	public void BSGeneralKit() throws IOException, UnsupportedFlavorException, AWTException
	{
		commonMethod.clickOnElement(FillClusterPage.GeneralKitTypeInfo, "General Kit Type Info Tab", driver);
		commonMethod.enterText(FillClusterPage.BlindingType, "Blinding Text Area", dataTable.getData("Test_Data", "BlindingType"), driver);
		WebElement enter0 = driver.findElement(By.xpath(FillClusterPage.BlindingType));
		enter0.sendKeys(Keys.ENTER);
		commonMethod.clickOnElement(FillClusterPage.childResistant, "Child Resistant Radio Button", driver);
		commonMethod.enterText(FillClusterPage.packagingSitesPrimary, "Packaging Sites - Primary Text Box", dataTable.getData("Test_Data", "PackPrimary"), driver);
		WebElement enter1 = driver.findElement(By.xpath(FillClusterPage.packagingSitesPrimary));
		enter1.sendKeys(Keys.ENTER);
		commonMethod.enterText(FillClusterPage.packagingSitesSecondary, "packaging Sites - Secondary Text Box", dataTable.getData("Test_Data", "PackSecondary"), driver);
		WebElement enter2 = driver.findElement(By.xpath(FillClusterPage.packagingSitesSecondary));
		enter2.sendKeys(Keys.ENTER);
	}
	/*
	 * Drug product Info(Brightstock Project)
	 */
	public void BSDrugproduct() throws IOException, UnsupportedFlavorException, AWTException
	{
		commonMethod.clickOnElement(FillClusterPage.DrugProductInfo, "Drug Product info", driver);
		commonMethod.selectOptionFromDropDown(FillClusterPage.DrugProdCountry, dataTable.getData("Test_Data", "DrugCountry"), driver);
		commonMethod.enterText(FillClusterPage.DrugsubstanceCompany, "Drug Substance Company", dataTable.getData("Test_Data", "DrugSubstanceCompany"), driver);
		commonMethod.selectOptionFromDropDown(FillClusterPage.DrugSubtanceCountry, dataTable.getData("Test_Data", "DrugSubstanceCountry"), driver);
		commonMethod.clickOnElement(FillClusterPage.Coldchain, "Cold chain drug product", driver);
		commonMethod.clickOnElement(FillClusterPage.MoistureSensitive, "Moisture Sensitive drug product", driver);
		commonMethod.clickOnElement(FillClusterPage.LightSensitive, "Light Sensitive", driver);
		commonMethod.clickOnElement(FillClusterPage.OxygenSensitive, "Oxygen Sensitive", driver);
		commonMethod.clickOnElement(FillClusterPage.Frozenproduct, "Frozen product", driver);
	}
	
	/*
	 * User Log-out from Application
	 */
	public void Logout() throws IOException, UnsupportedFlavorException, AWTException{
		commonMethod.clickOnElement(FillClusterPage.userIcon, "user Account Icon", driver);
		commonMethod.clickOnElement(FillClusterPage.LogOutButton, "Log out Button", driver);
	}

}
