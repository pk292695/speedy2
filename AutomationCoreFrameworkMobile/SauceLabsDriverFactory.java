package AutomationCoreFrameworkMobile;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Properties;

import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import AutomationCoreFramework.FrameworkException;
import AutomationCoreFramework.Settings;

public class SauceLabsDriverFactory {

	private static Properties mobileProperties;

	private SauceLabsDriverFactory() {
		// To prevent external instantiation of this class
	}

	/**
	 * Function to return the Saucelabs DesktopCloud {@link RemoteWebDriver}
	 * object based on the parameters passed
	 * 
	 * @param platformName
	 *            The platform to be used for the test execution (Windows, Mac,
	 *            etc.)
	 * @param version
	 *            The browser version to be used for the test execution
	 * @param browserName
	 *            The {@link Browser} to be used for the test execution
	 * @param sauceUrl
	 *            The Saucelabs URL to be used for the test execution
	 * @return The corresponding {@link RemoteWebDriver} object
	 */
	public static WebDriver getSauceRemoteWebDriver(String sauceURL, Browser browser, String browserVersion,
			Platform platformName, SeleniumTestParameters testParameters)
	{
		
		
		WebDriver driver = null;
	
		/*DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
		desiredCapabilities.setCapability("platform", platformName);
		desiredCapabilities.setCapability("version", browserVersion);
		desiredCapabilities.setCapability("browserName", browser);
		
		// desiredCapabilities.setCapability("screen-resolution","800x600");
		//desiredCapabilities.setCapability("name", testParameters.getCurrentTestcase());		
		
		String strTCName = testParameters.getCurrentScenario() + "_"+ testParameters.getCurrentTestcase() + "_" + testParameters.getCurrentTestInstance()+"_"+ testParameters.getRegion() + "_"+ testParameters.getLanguage() + "_"+testParameters.getBrowser();
		strTCName=strTCName.replace(" ", "_");
		desiredCapabilities.setCapability("name", strTCName);		
		
		try 
		{
			driver = new RemoteWebDriver(new URL(sauceURL), desiredCapabilities);
		} catch (MalformedURLException e) 
		{
			e.printStackTrace();
			throw new FrameworkException(
					"The RemoteWebDriver driver invokation has problem, please re-check the capabilities and check the SauceLabs details URL, Username and accessKey ");
		}
		return driver;*/
		
		DesiredCapabilities desiredCapabilities = null;	
		
		if (browserVersion == null) 
		{
			browserVersion="";
		}		
		
		if(browser.getValue().equalsIgnoreCase("SAFARI"))
		{
			desiredCapabilities = DesiredCapabilities.safari();			
			if(browserVersion.contains("11.0"))
			{
				desiredCapabilities.setCapability("platform", "macOS 10.13");
				desiredCapabilities.setCapability("version", "11.0");
				desiredCapabilities.setCapability("screenResolution", "1280x960");
			}
			else if(browserVersion.contains("10.0"))
			{
				desiredCapabilities.setCapability("platform", "OS X 10.11");
				desiredCapabilities.setCapability("version", "10.0");
				desiredCapabilities.setCapability("screenResolution", "1280x960");
			}
			else
			{
				desiredCapabilities.setCapability("platform", "OS X 10.11");
				desiredCapabilities.setCapability("version", "9.0");
				desiredCapabilities.setCapability("screenResolution", "1280x960");
			}
			
			
		}
		else if(browser.getValue().equalsIgnoreCase("internet explorer"))
		{
			desiredCapabilities = DesiredCapabilities.internetExplorer();
			desiredCapabilities.setCapability("platform", "Windows 7");
			desiredCapabilities.setCapability("version", "11.0");
			desiredCapabilities.setCapability("screenResolution", "1280x800");
		}
		else			
		{
			desiredCapabilities = DesiredCapabilities.chrome();
			desiredCapabilities.setCapability("platform", "Windows 7");	
			desiredCapabilities.setCapability("screenResolution", "1280x800");
			
			if(!browserVersion.isEmpty())
			{
				desiredCapabilities.setCapability("version", browserVersion);
			}
			else
			{
				desiredCapabilities.setCapability("version", "63.0");
			}
		}	
		
		String currentTestInstance = testParameters.getCurrentScenario() + "_"+ testParameters.getCurrentTestcase() + "_" + testParameters.getCurrentTestInstance()+"_"+ testParameters.getRegion() + "_"+ testParameters.getLanguage() + "_"+testParameters.getBrowser();		
		desiredCapabilities.setJavascriptEnabled(true);	// Pre-requisite for remote execution			
		desiredCapabilities.setCapability("name", currentTestInstance.replace(" ", "_"));	
		
		try 
		{
			driver = new RemoteWebDriver(new URL(sauceURL), desiredCapabilities);
		} catch (MalformedURLException e) 
		{
			e.printStackTrace();
			throw new FrameworkException(
					"The RemoteWebDriver driver invokation has problem, please re-check the capabilities and check the SauceLabs details URL, Username and accessKey ");
		}
		return driver;
	}

	@SuppressWarnings("rawtypes")
	public static AppiumDriver getSauceAppiumDriver(MobileExecutionPlatform executionPlatform, String deviceName,
			String sauceURL, SeleniumTestParameters testParameters) {

		AppiumDriver driver = null;

		mobileProperties = Settings.getMobilePropertiesInstance();
		DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
		try {
			switch (executionPlatform) {

			case ANDROID:
				desiredCapabilities.setCapability("appiumVersion",
						mobileProperties.getProperty("SaucelabAppiumDriverVersion"));
				desiredCapabilities.setCapability("platformName", "Android");
				desiredCapabilities.setCapability("platformVersion", testParameters.getMobileOSVersion());
				desiredCapabilities.setCapability("deviceName", deviceName);
				desiredCapabilities.setCapability("app", mobileProperties.getProperty("SauceAndroidIdentifier"));
				desiredCapabilities.setCapability("name", testParameters.getCurrentTestcase());
				try {
					driver = new AndroidDriver(new URL(sauceURL), desiredCapabilities);
				} catch (MalformedURLException e) {
					throw new FrameworkException(
							"The android driver invokation has problem, please re-check the capabilities and check the SauceLabs details URL, Username and accessKey ");
				}

				break;

			case IOS:

				desiredCapabilities.setCapability("appiumVersion",
						mobileProperties.getProperty("SaucelabAppiumDriverVersion"));
				desiredCapabilities.setCapability("platformName", "ios");
				desiredCapabilities.setCapability("deviceName", deviceName);
				desiredCapabilities.setCapability("browserName", "");
				desiredCapabilities.setCapability("name", testParameters.getCurrentTestcase());
				desiredCapabilities.setCapability("platformVersion", testParameters.getMobileOSVersion());
				desiredCapabilities.setCapability("app", mobileProperties.getProperty("SauceIosBundleID"));

				try {
					driver = new IOSDriver(new URL(sauceURL), desiredCapabilities);

				} catch (MalformedURLException e) {
					throw new FrameworkException(
							"The IOS driver invokation has problem, please re-check the capabilities and check the SauceLabs details URL, Username and accessKey ");
				}
				break;

			case WEB_ANDROID:
				desiredCapabilities.setCapability("appiumVersion",
						mobileProperties.getProperty("SaucelabAppiumDriverVersion"));
				desiredCapabilities.setCapability("deviceName", deviceName);
				desiredCapabilities.setCapability("deviceOrientation", "portrait");
				desiredCapabilities.setCapability("browserName", "chrome");
				desiredCapabilities.setCapability("platformVersion", testParameters.getMobileOSVersion());
				desiredCapabilities.setCapability("platformName", "Android");
				desiredCapabilities.setCapability("name", testParameters.getCurrentTestcase());

				try {
					driver = new AndroidDriver(new URL(sauceURL), desiredCapabilities);
				} catch (MalformedURLException e) {
					throw new FrameworkException(
							"The android driver/browser invokation has problem, please re-check the capabilities and check the SauceLabs details URL, Username and accessKey ");
				}
				break;

			case WEB_IOS:
				desiredCapabilities.setCapability("appiumVersion",
						mobileProperties.getProperty("SaucelabAppiumDriverVersion"));
				desiredCapabilities.setCapability("platformName", "ios");
				desiredCapabilities.setCapability("deviceName", deviceName);
				desiredCapabilities.setCapability("name", testParameters.getCurrentTestcase());
				desiredCapabilities.setCapability("browserName", "Safari");
				desiredCapabilities.setCapability("platformVersion", testParameters.getMobileOSVersion());

				try {
					driver = new IOSDriver(new URL(sauceURL), desiredCapabilities);

				} catch (MalformedURLException e) {
					throw new FrameworkException(
							"The IOS driver invokation/browser has problem, please re-check the capabilities and check the SauceLabs details URL, Username and accessKey ");
				}
				break;

			default:
				throw new FrameworkException("Unhandled ExecutionMode!");

			}
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new FrameworkException(
					"The Sauce appium driver invocation created a problem , please check the capabilities");
		}
		return driver;

	}

}
