package AutomationCoreFrameworkMobile;

import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.openqa.selenium.Platform;
import org.openqa.selenium.Proxy;
import org.openqa.selenium.Proxy.ProxyType;
import org.openqa.selenium.UnexpectedAlertBehaviour;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.safari.SafariOptions;
import org.openqa.selenium.opera.OperaDriver;
//import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.remote.*;

import AutomationCoreFramework.FrameworkException;
import AutomationCoreFramework.Settings;
import AutomationCoreFramework.TestParameters;
import AutomationCoreFramework.Util;
import AutomationCoreFrameworkMobile.SeleniumTestParameters;
import supportlibraries.DriverScript;

/**
 * Factory class for creating the {@link WebDriver} object as required
 * @author 
 */
public class WebDriverFactory {
	private static Properties properties;
	static DesiredCapabilities dc = new DesiredCapabilities();
	private WebDriverFactory() {
		// To prevent external instantiation of this class
	}
	/**
	 * Function to return the appropriate {@link WebDriver} object based on the parameters passed
	 * @param browser The {@link Browser} to be used for the test execution
	 * @return The corresponding {@link WebDriver} object
	 */
	public static WebDriver getWebDriver(Browser browser) {
		WebDriver driver = null;
		properties = Settings.getInstance();
		boolean proxyRequired =
				Boolean.parseBoolean(properties.getProperty("ProxyRequired"));
		
		String strDriverPath = Util.getAbsolutePath() + "\\BrowserDrivers\\";
		
		switch(browser) {
		case CHROME:
			// Takes the system proxy settings automatically
			
			/*System.setProperty("webdriver.chrome.driver",
									properties.getProperty("ChromeDriverPath"));*/
			
			dc.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR, UnexpectedAlertBehaviour.IGNORE);
			System.setProperty("webdriver.chrome.driver",
					strDriverPath +"chromedriver.exe");
			
			driver = new ChromeDriver(getDriverCapabilities("CHROME"));		
			break;
			
		case FIREFOX:
			// Takes the system proxy settings automatically
			
			/*System.setProperty("webdriver.gecko.driver",
					properties.getProperty("GeckoDriverPath"));*/
			dc.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR, UnexpectedAlertBehaviour.IGNORE);
			System.setProperty("webdriver.gecko.driver",
					strDriverPath +"geckodriver.exe");
			
			driver = new FirefoxDriver();
			break;
			
		case GHOST_DRIVER:
			// Takes the system proxy settings automatically (I think!)
			
			/*System.setProperty("phantomjs.binary.path",
									properties.getProperty("PhantomJSPath"));*/
			
			System.setProperty("phantomjs.binary.path",
					strDriverPath +"geckodriver.exe");
			
			//driver = new PhantomJSDriver();
			break;
			
		case INTERNET_EXPLORER:
			// Takes the system proxy settings automatically
			
			/*System.setProperty("webdriver.ie.driver",
									properties.getProperty("InternetExplorerDriverPath"));*/
			dc.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR, UnexpectedAlertBehaviour.IGNORE);
			dc.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS,true);
			System.setProperty("webdriver.ie.driver",
					strDriverPath +"IEDriverServer.exe");
			
			driver = new InternetExplorerDriver(getDriverCapabilities("INTERNET_EXPLORER"));			
			break;
			
		case OPERA:
			// Does not take the system proxy settings automatically!
			// NTLM authentication for proxy NOT supported
			
			if (proxyRequired) {
				DesiredCapabilities desiredCapabilities = getProxyCapabilities();
				driver = new OperaDriver(desiredCapabilities);
			} else {
				driver = new OperaDriver();
			}
			
			break;
			
		case SAFARI:
			// Takes the system proxy settings automatically
			dc.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR, UnexpectedAlertBehaviour.IGNORE);
			driver = new SafariDriver();
			break;
			
		default:
			throw new FrameworkException("Unhandled browser!");
		}
		
		return driver;
	}
	
	private static DesiredCapabilities getProxyCapabilities() 
	{
		properties = Settings.getInstance();
		String proxyUrl = properties.getProperty("ProxyHost") + ":" +
									properties.getProperty("ProxyPort");
		
		Proxy proxy = new Proxy();
		proxy.setProxyType(ProxyType.MANUAL);
		proxy.setHttpProxy(proxyUrl);
		proxy.setFtpProxy(proxyUrl);
		proxy.setSslProxy(proxyUrl);
		
		DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
		desiredCapabilities.setCapability(CapabilityType.PROXY, proxy);
		
		return desiredCapabilities;
	}
	
	/**
	 * Function to return the {@link RemoteWebDriver} object based on the parameters passed
	 * @param browser The {@link Browser} to be used for the test execution
	 * @param browserVersion The browser version to be used for the test execution
	 * @param platform The {@link Platform} to be used for the test execution
	 * @param remoteUrl The URL of the remote machine to be used for the test execution
	 * @return The corresponding {@link RemoteWebDriver} object
	 */
	public static WebDriver getRemoteWebDriver(Browser browser, String browserVersion,
												Platform platform, String remoteUrl, String currentTestInstance) 
	{
		// For running RemoteWebDriver tests in Chrome and IE:
		// The ChromeDriver and IEDriver executables needs to be in the PATH of the remote machine
		// To set the executable path manually, use:
		// java -Dwebdriver.chrome.driver=/path/to/driver -jar selenium-server-standalone.jar
		// java -Dwebdriver.ie.driver=/path/to/driver -jar selenium-server-standalone.jar
		
		properties = Settings.getInstance();
		boolean proxyRequired =
				Boolean.parseBoolean(properties.getProperty("ProxyRequired"));
		
		DesiredCapabilities desiredCapabilities = null;	
				
		if (browserVersion == null) 
		{
			browserVersion="";
		}		
		
		if(browser.getValue().equalsIgnoreCase("SAFARI"))
		{
			desiredCapabilities = DesiredCapabilities.safari();			
			if(browserVersion.contains("11.0"))
			{
				desiredCapabilities.setCapability("platform", "macOS 10.13");
				desiredCapabilities.setCapability("version", "11.0");
				desiredCapabilities.setCapability("screenResolution", "1280x960");
			}
			else if(browserVersion.contains("10.0"))
			{
				desiredCapabilities.setCapability("platform", "OS X 10.11");
				desiredCapabilities.setCapability("version", "10.0");
				desiredCapabilities.setCapability("screenResolution", "1280x960");
			}
			else
			{
				desiredCapabilities.setCapability("platform", "OS X 10.11");
				desiredCapabilities.setCapability("version", "9.0");
				desiredCapabilities.setCapability("screenResolution", "1280x960");
			}
			
			
		}
		else if(browser.getValue().equalsIgnoreCase("internet explorer"))
		{
			desiredCapabilities = DesiredCapabilities.internetExplorer();
			desiredCapabilities.setCapability("platform", "Windows 7");
			desiredCapabilities.setCapability("version", "11.0");
			desiredCapabilities.setCapability("screenResolution", "1280x800");
		}
		else			
		{
			desiredCapabilities = DesiredCapabilities.chrome();
			desiredCapabilities.setCapability("platform", "Windows 7");	
			desiredCapabilities.setCapability("screenResolution", "1280x800");
			
			if(!browserVersion.isEmpty())
			{
				desiredCapabilities.setCapability("version", browserVersion);
			}
			else
			{
				desiredCapabilities.setCapability("version", "63.0");
			}
		}
		
		desiredCapabilities.setJavascriptEnabled(true);	// Pre-requisite for remote execution			
		desiredCapabilities.setCapability("name", currentTestInstance.replace(" ", "_"));		
		URL url = getUrl(remoteUrl);
		
		return new RemoteWebDriver(url, desiredCapabilities);
	}
	
	private static URL getUrl(String remoteUrl) {
		URL url;
		try {
			url = new URL(remoteUrl);
		} catch (MalformedURLException e) {
			e.printStackTrace();
			throw new FrameworkException("The specified remote URL is malformed");
		}
		return url;
	}
	
	/**
	 * Function to return the {@link RemoteWebDriver} object based on the parameters passed
	 * @param browser The {@link Browser} to be used for the test execution
	 * @param remoteUrl The URL of the remote machine to be used for the test execution
	 * @return The corresponding {@link RemoteWebDriver} object
	 */
	public static WebDriver getRemoteWebDriver(Browser browser, String remoteUrl) 
	{
		return getRemoteWebDriver(browser, null, null, remoteUrl, null);
	}
	
	/**
	 * Function to return the {@link ChromeDriver} object emulating the device specified by the user
	 * @param deviceName The name of the device to be emulated (check Chrome Dev Tools for a list of available devices)
	 * @return The corresponding {@link ChromeDriver} object
	 */
	public static WebDriver getEmulatedWebDriver(String deviceName) {
		DesiredCapabilities desiredCapabilities = getEmulatedChromeDriverCapabilities(deviceName);
		
		properties = Settings.getInstance();
		System.setProperty("webdriver.chrome.driver",
											properties.getProperty("ChromeDriverPath"));
		
		return new ChromeDriver(desiredCapabilities);
	}
	
	private static DesiredCapabilities getEmulatedChromeDriverCapabilities(String deviceName) {
		Map<String, String> mobileEmulation = new HashMap<String, String>();
		mobileEmulation.put("deviceName", deviceName);
		
		Map<String, Object> chromeOptions = new HashMap<String, Object>();
		chromeOptions.put("mobileEmulation", mobileEmulation);
		
		DesiredCapabilities desiredCapabilities = DesiredCapabilities.chrome();
		desiredCapabilities.setCapability(ChromeOptions.CAPABILITY, chromeOptions);
		
		return desiredCapabilities;
	}
	
	/**
	 * Function to return the {@link RemoteWebDriver} object emulating the device specified by the user
	 * @param deviceName The name of the device to be emulated (check Chrome Dev Tools for a list of available devices)
	 * @param remoteUrl The URL of the remote machine to be used for the test execution
	 * @return The corresponding {@link RemoteWebDriver} object
	 */
	public static WebDriver getEmulatedRemoteWebDriver(String deviceName, String remoteUrl) {
		DesiredCapabilities desiredCapabilities = getEmulatedChromeDriverCapabilities(deviceName);
		desiredCapabilities.setJavascriptEnabled(true);	// Pre-requisite for remote execution
		
		URL url = getUrl(remoteUrl);
		
		return new RemoteWebDriver(url, desiredCapabilities);
	}
	
	/**
	 * Function to return the {@link ChromeDriver} object emulating the device attributes specified by the user
	 * @param deviceWidth The width of the device to be emulated (in pixels)
	 * @param deviceHeight The height of the device to be emulated (in pixels)
	 * @param devicePixelRatio The device's pixel ratio
	 * @param userAgent The user agent string
	 * @return The corresponding {@link ChromeDriver} object
	 */
	public static WebDriver getEmulatedWebDriver(int deviceWidth, int deviceHeight,
											float devicePixelRatio, String userAgent) {
		DesiredCapabilities desiredCapabilities =
						getEmulatedChromeDriverCapabilities(deviceWidth, deviceHeight,
															devicePixelRatio, userAgent);
		
		properties = Settings.getInstance();
		System.setProperty("webdriver.chrome.driver",
											properties.getProperty("ChromeDriverPath"));
		
		return new ChromeDriver(desiredCapabilities);
	}
	
	private static DesiredCapabilities getEmulatedChromeDriverCapabilities(
			int deviceWidth, int deviceHeight, float devicePixelRatio, String userAgent) {
		Map<String, Object> deviceMetrics = new HashMap<String, Object>();
		deviceMetrics.put("width", deviceWidth);
		deviceMetrics.put("height", deviceHeight);
		deviceMetrics.put("pixelRatio", devicePixelRatio);
		
		Map<String, Object> mobileEmulation = new HashMap<String, Object>();
		mobileEmulation.put("deviceMetrics", deviceMetrics);
		//mobileEmulation.put("userAgent", "Mozilla/5.0 (Linux; Android 4.2.1; en-us; Nexus 5 Build/JOP40D) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.166 Mobile Safari/535.19");
		mobileEmulation.put("userAgent", userAgent);
		
		Map<String, Object> chromeOptions = new HashMap<String, Object>();
		chromeOptions.put("mobileEmulation", mobileEmulation);
		
		DesiredCapabilities desiredCapabilities = DesiredCapabilities.chrome();
		desiredCapabilities.setCapability(ChromeOptions.CAPABILITY, chromeOptions);
		return desiredCapabilities;
	}
	
	/**
	 * Function to return the {@link RemoteWebDriver} object emulating the device attributes specified by the user
	 * @param deviceWidth The width of the device to be emulated (in pixels)
	 * @param deviceHeight The height of the device to be emulated (in pixels)
	 * @param devicePixelRatio The device's pixel ratio
	 * @param userAgent The user agent string
	 * @param remoteUrl The URL of the remote machine to be used for the test execution
	 * @return The corresponding {@link RemoteWebDriver} object
	 */
	public static WebDriver getEmulatedRemoteWebDriver(int deviceWidth, int deviceHeight,
								float devicePixelRatio, String userAgent, String remoteUrl) {
		DesiredCapabilities desiredCapabilities =
				getEmulatedChromeDriverCapabilities(deviceWidth, deviceHeight,
													devicePixelRatio, userAgent);
		desiredCapabilities.setJavascriptEnabled(true);	// Pre-requisite for remote execution
		
		URL url = getUrl(remoteUrl);
		
		return new RemoteWebDriver(url, desiredCapabilities);
	}
	
	/**
	 * Function to return the browser capabilities 
	 * @param browser type
	 * @return return the browser capabilites
	 * @date 14 Nov 2014
	 * @author poovaraj
	 */
	public static DesiredCapabilities getDriverCapabilities(String browserType)
	{
		DesiredCapabilities capabilities=null;
		
		if (browserType.equalsIgnoreCase("CHROME"))
			{
								
				ChromeOptions options = new ChromeOptions();
				options.addArguments("--start-maximized");				
				options.addArguments("disable-infobars");				
				Map<String, Object> prefs = new HashMap<String, Object>();
				//To Turns off multiple download warning
				prefs.put("profile.default_content_settings.popups", 0);				
				prefs.put("profile.default_content_setting_values.automatic_downloads", 1);
				//Turns off download prompt
				prefs.put("download.prompt_for_download", false);				
				options.setExperimentalOption("prefs", prefs);
				capabilities = DesiredCapabilities.chrome();
				capabilities.setCapability(ChromeOptions.CAPABILITY, options);
				capabilities.setCapability("platform", "Windows 7");	
				
			}
		   else if (browserType.equalsIgnoreCase("INTERNET_EXPLORER"))
			{
			   
				capabilities = DesiredCapabilities.internetExplorer();			
				capabilities.setCapability(InternetExplorerDriver.ENABLE_PERSISTENT_HOVERING,false);
				capabilities.setCapability(InternetExplorerDriver.REQUIRE_WINDOW_FOCUS, false);				
				capabilities.setCapability(InternetExplorerDriver.UNEXPECTED_ALERT_BEHAVIOR, false);
				capabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);				
				capabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true); 
				capabilities.setCapability(InternetExplorerDriver.UNEXPECTED_ALERT_BEHAVIOR, true);				
				capabilities.setCapability("platform", "Windows 7");
				
				//Updated on 06th Sep 2017
				// capabilities.setCapability("ignoreZoomSetting", true);
			}
		   else if (browserType.equalsIgnoreCase("SAFARI"))
			{					
				SafariOptions options = new SafariOptions();
				//options.setUseCleanSession(true);				
				 
				// For use with RemoteWebDriver:
				 capabilities = DesiredCapabilities.safari();
				 capabilities.setCapability(SafariOptions.CAPABILITY, options);				 
				 capabilities.setPlatform(Platform.MAC);
				//Note: If browser_version capability is not set, the test will run on the latest version of the browser set by browser capability.
				 capabilities.setVersion("9");				
				//capabilities.setVersion("8");
				
			
			}	
				
				//Common for all browser for Javascript enabled
				capabilities.setJavascriptEnabled(true);
				
				boolean proxyRequired =
						Boolean.parseBoolean(properties.getProperty("ProxyRequired"));
				if (proxyRequired)
				{
					capabilities.setCapability(CapabilityType.PROXY, getProxyCapabilitiesDetails());
				}			
		
		  return capabilities;		
	}
	
	/**
	 * Function to return proxy details 
	 * @param none
	 * @return return the  proxy
	 * @date 27 June 2017
	 * @author poovaraj
	 */
	 private static Proxy getProxyCapabilitiesDetails() 
		{
			properties = Settings.getInstance();
			String proxyUrl = properties.getProperty("ProxyHost") + ":" +
										properties.getProperty("ProxyPort");
			
			Proxy proxy = new Proxy();
			proxy.setProxyType(ProxyType.MANUAL);
			proxy.setHttpProxy(proxyUrl);
			proxy.setFtpProxy(proxyUrl);
			proxy.setSslProxy(proxyUrl);			
			return proxy;
		}
	
}