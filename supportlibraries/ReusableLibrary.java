package supportlibraries;

import java.awt.AWTException;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.pdfbox.cos.COSDocument;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DriverCommand;
import org.openqa.selenium.remote.RemoteExecuteMethod;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import AutomationCoreFramework.CraftDataTable;
import AutomationCoreFramework.ExcelDataAccess;
import AutomationCoreFramework.FrameworkException;
import AutomationCoreFramework.FrameworkParameters;
import AutomationCoreFramework.Settings;
import AutomationCoreFramework.Status;
import AutomationCoreFrameworkMobile.CraftDriver;
import AutomationCoreFrameworkMobile.SeleniumReport;
import AutomationCoreFrameworkMobile.WebDriverUtil;
//import businesscomponents.FunctionalComponents;
import io.appium.java_client.MobileElement;
import io.appium.java_client.ios.IOSDriver;
import microsoft.exchange.webservices.data.core.ExchangeService;
import microsoft.exchange.webservices.data.core.PropertySet;
import microsoft.exchange.webservices.data.core.enumeration.misc.ExchangeVersion;
import microsoft.exchange.webservices.data.core.enumeration.property.BasePropertySet;
import microsoft.exchange.webservices.data.core.enumeration.property.WellKnownFolderName;
import microsoft.exchange.webservices.data.core.enumeration.search.LogicalOperator;
import microsoft.exchange.webservices.data.core.enumeration.search.SortDirection;
import microsoft.exchange.webservices.data.core.service.item.Item;
import microsoft.exchange.webservices.data.core.service.schema.ItemSchema;
import microsoft.exchange.webservices.data.credential.ExchangeCredentials;
import microsoft.exchange.webservices.data.credential.WebCredentials;
import microsoft.exchange.webservices.data.search.FindItemsResults;
import microsoft.exchange.webservices.data.search.ItemView;
import microsoft.exchange.webservices.data.search.filter.SearchFilter;

/**
 * Abstract base class for reusable libraries created by the user
 * 
 * @author 
 */
public abstract class ReusableLibrary {

	int responseStatus;
	int responseCode;
	static boolean elementPresent;
	private static HttpURLConnection httpURLConnect;
	protected List<String> actualErrorDifference;

	protected Map<String, Object> perfectoCommand = new HashMap<>();
	Dimension winSize;
	/**
	 * The {@link CraftDataTable} object (passed from the test script)
	 */
	protected CraftDataTable dataTable;
	/**
	 * The {@link SeleniumReport} object (passed from the test script)
	 */
	protected SeleniumReport report;
	/**
	 * The {@link CraftDriver} object
	 */
	protected CraftDriver driver;
	protected String textbox="textbox";
	protected String textarea="textarea";
	protected String dropdown="dropdown";
	protected String button="button";
	protected String radio="radio";
	protected String checkbox="checkbox";
	protected String link="link";
	protected String focus="focus";
	protected ExcelDataAccess excelDataAccess;
	

	protected WebDriverUtil driverUtil;

	/**
	 * The {@link ScriptHelper} object (required for calling one reusable
	 * library from another)
	 */
	protected ScriptHelper scriptHelper;

	/**
	 * The {@link Properties} object with settings loaded from the framework
	 * properties file
	 */
	protected Properties properties;
	protected String fileDownloadPath;
	/**
	 * The {@link FrameworkParameters} object
	 */
	protected FrameworkParameters frameworkParameters;

	/**
	 * Constructor to initialize the {@link ScriptHelper} object and in turn the
	 * objects wrapped by it
	 * 
	 * @param scriptHelper
	 *            The {@link ScriptHelper} object
	 */
	protected WebDriverWait wait;
	protected WebDriverWait waitMin;
	protected WebDriverWait waitMedium;
	protected WebDriverWait waitMax;
	
	
	public ReusableLibrary(ScriptHelper scriptHelper) 
	{
		this.scriptHelper = scriptHelper;
		this.dataTable = scriptHelper.getDataTable();
		this.report = scriptHelper.getReport();
		this.driver = scriptHelper.getcraftDriver();
		this.driverUtil = scriptHelper.getDriverUtil();

		properties = Settings.getInstance();
		frameworkParameters = FrameworkParameters.getInstance();
		
		properties = Settings.getInstance();
		frameworkParameters = FrameworkParameters.getInstance();		
		wait=new WebDriverWait(driver.getWebDriver(),Integer.parseInt(properties.getProperty("ObjectSyncTimeout")));
		waitMin=new WebDriverWait(driver.getWebDriver(),Integer.parseInt(properties.getProperty("waitMin")));
		waitMedium=new WebDriverWait(driver.getWebDriver(),Integer.parseInt(properties.getProperty("waitMedium")));
		waitMax=new WebDriverWait(driver.getWebDriver(),Integer.parseInt(properties.getProperty("waitMax")));
		fileDownloadPath=System.getProperty("user.home")+properties.getProperty("doubleSlash")+properties.getProperty("userDownloadsPath");
	}

	/**
	 * All reusuable Appium Functions with Perfecto
	 */

	/**
	 * Function Applicable only when the ExecutionMode used is <b>PERFECTO
	 * 
	 * @param context
	 *            - Context of App like NATIVE_APP or WEB
	 * @param appName
	 *            - Name of the App as displayed in Mobile
	 */
	protected void openApp(final String context, final String appName) {
		if (context.equals("NATIVE_APP")) {
			final Map<String, Object> perfectoCommand = new HashMap<>();
			perfectoCommand.put("name", appName);
			driver.getAppiumDriver().executeScript("mobile:application:open",
					perfectoCommand);
		}
	}

	/**
	 * Function Applicable only when the ExecutionMode used is <b>PERFECTO
	 * 
	 * @param context
	 *            - Context of App like NATIVE_APP or WEB
	 * @param appName
	 *            - Identifier of the App.
	 */
	protected void openAppWithIdentifier(final String context,
			final String identifer) {
		if (context.equals("NATIVE_APP")) {
			perfectoCommand.put("identifier", identifer);
			driver.getAppiumDriver().executeScript("mobile:application:open",
					perfectoCommand);
			perfectoCommand.clear();
		}
	}

	/**
	 * Function Applicable only when the ExecutionMode used is <b>PERFECTO
	 * 
	 * @param type
	 *            - Type of report like pdf
	 */
	protected byte[] downloadReport(final String type) throws IOException {
		final String command = "mobile:report:download";
		final Map<String, String> params = new HashMap<>();
		params.put("type", type);
		final String report = (String) (driver.getRemoteWebDriver())
				.executeScript(command, params);
		final byte[] reportBytes = OutputType.BYTES
				.convertFromBase64Png(report);
		return reportBytes;
	}

	/**
	 * Function Applicable only when the ExecutionMode used is <b>PERFECTO
	 * 
	 */
	protected byte[] downloadWTReport() {
		final String reportUrl = (String) driver.getAppiumDriver()
				.getCapabilities().getCapability("windTunnelReportUrl");
		String returnString = "<html><head><META http-equiv=\"refresh\" content=\"0;URL=";
		returnString = returnString + reportUrl + "\"></head><body /></html>";

		return returnString.getBytes();
	}

	/**
	 * Function Applicable only when the ExecutionMode used is <b>PERFECTO
	 * 
	 * @param context
	 *            - Context of App like NATIVE_APP or WEB
	 * @param appName
	 *            - Name of the App.
	 */
	protected void closeApp(final String context, final String appName) {
		if (context.equals("NATIVE_APP")) {
			perfectoCommand.put("name", appName);
			try {
				driver.getAppiumDriver().executeScript(
						"mobile:application:close", perfectoCommand);
			} catch (final WebDriverException e) {
			}
		}
	}

	/**
	 * Function Applicable only when the ExecutionMode used is <b>PERFECTO
	 * 
	 * @param context
	 *            - Context of App like NATIVE_APP or WEB
	 * @param appName
	 *            - Identifier of the App.
	 */
	protected void closeAppWithIdentifier(final String context,
			final String bundleId) {
		if (context.equals("NATIVE_APP")) {
			perfectoCommand.put("identifier", bundleId);
			try {
				driver.getAppiumDriver().executeScript(
						"mobile:application:close", perfectoCommand);
			} catch (final WebDriverException e) {
			}
		}
	}

	/**
	 * Function Applicable only when the ExecutionMode used is <b>PERFECTO
	 * 
	 * @param textToFind
	 *            - text that has to be searched
	 * @param timeout
	 */
	protected Boolean textCheckpoint(final String textToFind,
			final Integer timeout) {
		perfectoCommand.put("content", textToFind);
		perfectoCommand.put("timeout", timeout);
		final Object result = driver.getAppiumDriver().executeScript(
				"mobile:checkpoint:text", perfectoCommand);
		final Boolean resultBool = Boolean.valueOf(result.toString());
		perfectoCommand.clear();
		return resultBool;
	}

	/**
	 * Function Applicable only when the ExecutionMode used is <b>PERFECTO
	 * 
	 * @param textToFind
	 *            - text that has to be searched
	 * @param timeout
	 */
	protected void textClick(final String textToFind, final Integer timeout) {
		perfectoCommand.put("content", textToFind);
		perfectoCommand.put("timeout", timeout);
		driver.getAppiumDriver().executeScript("mobile:text:select",
				perfectoCommand);
		perfectoCommand.clear();

	}

	/**
	 * Function Applicable only when the ExecutionMode used is <b>PERFECTO
	 * 
	 * @param label
	 *            - text that has to be searched
	 * @param threshold
	 */
	protected void visualScrollToClick(final String label,
			final Integer threshold) {
		perfectoCommand.put("label", label);
		perfectoCommand.put("threshold", threshold);
		perfectoCommand.put("scrolling", "scroll");
		driver.getAppiumDriver().executeScript("mobile:button-text:click",
				perfectoCommand);
		perfectoCommand.clear();
	}

	/**
	 * Function Applicable only when the ExecutionMode used is <b>PERFECTO
	 * 
	 * @param label
	 *            - text that has to be searched
	 * @param timeout
	 * @param threshold
	 */
	protected void visualClick(final String label, final Integer timeout,
			final Integer threshold) {
		perfectoCommand.put("label", label);
		perfectoCommand.put("threshold", threshold);
		perfectoCommand.put("timeout", timeout);
		driver.getAppiumDriver().executeScript("mobile:button-text:click",
				perfectoCommand);
		perfectoCommand.clear();
	}

	/**
	 * Function Applicable only when the ExecutionMode used is <b>PERFECTO
	 * 
	 * @param label
	 *            - text that has to be searched
	 * @param timeout
	 * @param threshold
	 * @param labelDirection
	 * @param labelOffset
	 */
	protected void visualClick(final String label, final Integer timeout,
			final Integer threshold, final String labelDirection,
			final String labelOffset) {
		perfectoCommand.put("label", label);
		perfectoCommand.put("threshold", threshold);
		perfectoCommand.put("timeout", timeout);
		perfectoCommand.put("label.direction", labelDirection);
		perfectoCommand.put("label.offset", labelOffset);
		driver.getAppiumDriver().executeScript("mobile:button-text:click",
				perfectoCommand);
		perfectoCommand.clear();
	}

	/**
	 * Function Applicable only when the ExecutionMode used is <b>PERFECTO
	 * 
	 * @param imagePath
	 */
	protected void imageClick(String imagePath) {
		perfectoCommand.put("content", imagePath);
		perfectoCommand.put("timeout", "5");
		perfectoCommand.put("screen.top", "0%");
		perfectoCommand.put("screen.height", "100%");
		perfectoCommand.put("screen.left", "0%");
		perfectoCommand.put("screen.width", "100%");
		driver.executeScript("mobile:image:select", perfectoCommand);
		perfectoCommand.clear();
	}

	/**
	 * Function Applicable only when the ExecutionMode used is <b>PERFECTO
	 * 
	 * @param imagePath
	 */
	protected Boolean imageCheckpoint(String imagePath) {
		perfectoCommand.put("content", imagePath);
		perfectoCommand.put("threshold", "90");
		perfectoCommand.put("screen.top", "0%");
		perfectoCommand.put("screen.height", "100%");
		perfectoCommand.put("screen.left", "0%");
		perfectoCommand.put("screen.width", "100%");
		Object result = driver.executeScript("mobile:image:find",
				perfectoCommand);
		final Boolean resultBool = Boolean.valueOf(result.toString());
		perfectoCommand.clear();
		return resultBool;
	}

	/**
	 * Function Applicable only when the ExecutionMode used is <b>PERFECTO
	 * 
	 * @param repositoryFile
	 * @param handsetFile
	 */
	protected void putFileOnDevice(final String repositoryFile,
			final String handsetFile) {
		perfectoCommand.put("repositoryFile", repositoryFile);
		perfectoCommand.put("handsetFile", handsetFile);
		driver.getAppiumDriver().executeScript("mobile:media:put",
				perfectoCommand);
		perfectoCommand.clear();

	}

	/**
	 * Function Applicable only when the ExecutionMode used is <b>PERFECTO
	 * 
	 * @param handsetFile
	 * @param repositoryFile
	 */
	protected void getFileOnDevice(final String handsetFile,
			final String repositoryFile) {
		perfectoCommand.put("repositoryFile", repositoryFile);
		perfectoCommand.put("handsetFile", handsetFile);
		driver.getAppiumDriver().executeScript("mobile:media:get",
				perfectoCommand);
		perfectoCommand.clear();

	}

	/**
	 * Function Applicable only when the ExecutionMode used is <b>PERFECTO
	 * 
	 * @param handsetFile
	 */
	protected void deleteFromDevice(final String handsetFile) {
		perfectoCommand.put("handsetFile", handsetFile);
		driver.getAppiumDriver().executeScript("mobile:media:delete",
				perfectoCommand);
		perfectoCommand.clear();

	}

	/**
	 * Function Applicable only when the ExecutionMode used is <b>PERFECTO
	 * 
	 * @param repositoryFile
	 */
	protected void deleteFromRepository(final String repositoryFile) {
		perfectoCommand.put("repositoryFile", repositoryFile);
		driver.getAppiumDriver().executeScript("mobile:media:delete",
				perfectoCommand);
		perfectoCommand.clear();

	}

	/**
	 * Function Applicable only when the ExecutionMode used is <b>PERFECTO
	 * 
	 * @param keyPress
	 */
	protected void deviceKeyPress(final String keyPress) {

		perfectoCommand.put("keySequence", keyPress);
		driver.getAppiumDriver().executeScript("mobile:presskey",
				perfectoCommand);
		perfectoCommand.clear();
	}

	/**
	 * Function Applicable only when the ExecutionMode used is <b>PERFECTO
	 * 
	 * @param x1
	 * @param y1
	 * @param x2
	 * @param y2
	 */
	protected void swipe(final String x1, final String y1, final String x2,
			final String y2) {
		final List<String> swipeCoordinates = new ArrayList<>();
		swipeCoordinates.add(x1 + ',' + y1);
		swipeCoordinates.add(x2 + ',' + y2);
		perfectoCommand.put("location", swipeCoordinates);
		driver.getAppiumDriver().executeScript("mobile:touch:drag",
				perfectoCommand);
		perfectoCommand.clear();
	}

	/**
	 * Function Applicable only when the ExecutionMode used is <b>PERFECTO
	 * 
	 * @param textToFind
	 */
	protected void swipeTillText(String textToFind) {
		perfectoCommand.put("content", textToFind);
		perfectoCommand.put("scrolling", "scroll");
		perfectoCommand.put("maxscroll", "10");
		perfectoCommand.put("next", "SWIPE_UP");
		driver.executeScript("mobile:text:select", perfectoCommand);
		perfectoCommand.clear();
	}

	/**
	 * Function Applicable to Pause the Script, Generic Application
	 * 
	 * @param How_Long_To_Pause
	 */
	public void PauseScript(int How_Long_To_Pause) {
		How_Long_To_Pause = How_Long_To_Pause * 1000;

		try {
			Thread.sleep(How_Long_To_Pause);
		} catch (final InterruptedException ex) {
			Thread.currentThread().interrupt();
		}
	}

	/**
	 * All reusuable Selenium Functions with Perfecto
	 */

	/**
	 * Function to switch the Context
	 * 
	 * @param driver
	 * @RemoteWebDriver
	 * @param context
	 */
	protected static void switchToContext(RemoteWebDriver driver, String context) {
		RemoteExecuteMethod executeMethod = new RemoteExecuteMethod(driver);
		Map<String, String> params = new HashMap<String, String>();
		params.put("name", context);
		executeMethod.execute(DriverCommand.SWITCH_TO_CONTEXT, params);
	}

	/**
	 * Function Applicable only when the ExecutionMode used is <b>PERFECTO
	 * 
	 * @param driver
	 * @param list
	 */
	@SuppressWarnings("rawtypes")
	protected void scrollChecker(IOSDriver driver, String[] list) {
		for (int i = 0; i < list.length; i++) {

			MobileElement me = (MobileElement) driver.findElement(By
					.xpath("//UIAPickerWheel[" + (i + 1) + "]"));
			int mget = getMonthInt(me.getText().split(",")[0]);

			if (i == 0) {
				if (mget > getMonthInt(list[i])) {
					scrollAndSearch(driver, list[i], me, true);
				} else {
					scrollAndSearch(driver, list[i], me, false);
				}
			} else {
				if (Integer.parseInt(me.getText().split(",")[0]) > Integer
						.parseInt(list[i])) {
					scrollAndSearch(driver, list[i], me, true);
				} else {
					scrollAndSearch(driver, list[i], me, false);
				}
			}
		}
	}

	// Used to get the integer for a month based on the string of the month
	private int getMonthInt(String month) {
		int monthInt = 0;
		switch (month) {
		case "Jan":
			monthInt = 1;
			break;
		case "January":
			monthInt = 1;
			break;
		case "February":
			monthInt = 2;
			break;
		case "Feb":
			monthInt = 2;
			break;
		case "March":
			monthInt = 3;
			break;
		case "Mar":
			monthInt = 3;
			break;
		case "April":
			monthInt = 4;
			break;
		case "Apr":
			monthInt = 4;
			break;
		case "May":
			monthInt = 5;
			break;
		case "June":
			monthInt = 6;
			break;
		case "Jun":
			monthInt = 6;
			break;
		case "July":
			monthInt = 7;
			break;
		case "Jul":
			monthInt = 7;
			break;
		case "August":
			monthInt = 8;
			break;
		case "Aug":
			monthInt = 8;
			break;
		case "September":
			monthInt = 9;
			break;
		case "Sep":
			monthInt = 9;
			break;
		case "October":
			monthInt = 10;
			break;
		case "Oct":
			monthInt = 10;
			break;
		case "November":
			monthInt = 11;
			break;
		case "Nov":
			monthInt = 11;
			break;
		case "December":
			monthInt = 12;
			break;
		case "Dec":
			monthInt = 12;
			break;
		}
		return monthInt;
	}

	// Code here shouldn't be modified
	@SuppressWarnings("rawtypes")
	private void scrollAndSearch(IOSDriver driver, String value,
			MobileElement me, Boolean direction) {
		String x = getLocationX(me);
		String y = getLocationY(me);
		while (!driver.findElementByXPath(getXpathFromElement(me)).getText()
				.contains(value)) {
			swipe(driver, x, y, direction);
		}
	}

	// Performs the swipe and search operation
	// Code here shouldn't be modified
	@SuppressWarnings("rawtypes")
	private void swipe(IOSDriver driver, String start, String end, Boolean up) {
		String direction;
		if (up) {
			direction = start + "," + (Integer.parseInt(end) + 70);
		} else {
			direction = start + "," + (Integer.parseInt(end) - 70);
		}

		Map<String, Object> params1 = new HashMap<>();
		params1.put("location", start + "," + end);
		params1.put("operation", "down");
		driver.executeScript("mobile:touch:tap", params1);

		Map<String, Object> params2 = new HashMap<>();
		List<String> coordinates2 = new ArrayList<>();

		coordinates2.add(direction);
		params2.put("location", coordinates2);
		params2.put("auxiliary", "notap");
		params2.put("duration", "3");
		driver.executeScript("mobile:touch:drag", params2);

		Map<String, Object> params3 = new HashMap<>();
		params3.put("location", direction);
		params3.put("operation", "up");
		driver.executeScript("mobile:touch:tap", params3);
	}

	// Gets the objects X location in pixels
	private String getLocationX(MobileElement me) {
		int x = me.getLocation().x;
		int width = (Integer.parseInt(me.getAttribute("width")) / 2) + x;
		return width + "";
	}

	// Gets the objects X location in pixels
	private String getLocationY(MobileElement me) {
		int y = me.getLocation().y;
		int height = (Integer.parseInt(me.getAttribute("height")) / 2) + y;
		return height + "";
	}

	// Parses webelement to retrieve the xpath used for identification
	private String getXpathFromElement(MobileElement me) {
		return (me.toString().split("-> xpath: ")[1]).substring(0, (me
				.toString().split("-> xpath: ")[1]).length() - 1);
	}

	/**
	 * Function Applicable only when the ExecutionMode used is <b>PERFECTO
	 * 
	 * @param letter
	 */
	protected void drawLetter(final String letter) {
		final List<String> coordinates = new ArrayList<>();

		switch (letter) {
		case "A":

			break;
		case "B":

			break;
		case "C":

			break;
		case "D":

			break;
		case "E":
			coordinates.add("42%,40%");
			coordinates.add("42%,60%");
			perfectoCommand.put("location", coordinates);
			driver.executeScript("mobile:touch:drag", perfectoCommand);
			perfectoCommand.clear();
			coordinates.clear();
			coordinates.add("42%,40%");
			coordinates.add("52%,40%");
			perfectoCommand.put("location", coordinates);
			driver.executeScript("mobile:touch:drag", perfectoCommand);
			perfectoCommand.clear();
			coordinates.clear();
			coordinates.add("42%,48%");
			coordinates.add("52%,48%");
			perfectoCommand.put("location", coordinates);
			driver.executeScript("mobile:touch:drag", perfectoCommand);
			perfectoCommand.clear();
			coordinates.clear();
			coordinates.add("42%,56%");
			coordinates.add("52%,56%");
			perfectoCommand.put("location", coordinates);
			driver.executeScript("mobile:touch:drag", perfectoCommand);
			perfectoCommand.clear();
			coordinates.clear();
			break;
		case "F":

			break;
		case "G":

			break;
		case "H":

			break;
		case "I":

			break;
		case "J":

			break;
		case "K":

			break;
		case "L":

			break;
		case "M":

			break;
		case "N":

			break;
		case "O":

			break;
		case "P":
			coordinates.add("30%,40%");
			coordinates.add("30%,60%");
			perfectoCommand.put("location", coordinates);
			driver.executeScript("mobile:touch:drag", perfectoCommand);
			perfectoCommand.clear();
			coordinates.clear();
			coordinates.add("30%,40%");
			coordinates.add("40%,40%");
			perfectoCommand.put("location", coordinates);
			driver.executeScript("mobile:touch:drag", perfectoCommand);
			perfectoCommand.clear();
			coordinates.clear();
			coordinates.add("38%,40%");
			coordinates.add("38%,52%");
			perfectoCommand.put("location", coordinates);
			driver.executeScript("mobile:touch:drag", perfectoCommand);
			perfectoCommand.clear();
			coordinates.clear();
			coordinates.add("38%,48%");
			coordinates.add("28%,48%");
			perfectoCommand.put("location", coordinates);
			driver.executeScript("mobile:touch:drag", perfectoCommand);
			perfectoCommand.clear();
			coordinates.clear();
			break;
		case "Q":

			break;
		case "R":
			coordinates.add("54%,40%");
			coordinates.add("54%,60%");
			perfectoCommand.put("location", coordinates);
			driver.executeScript("mobile:touch:drag", perfectoCommand);
			perfectoCommand.clear();
			coordinates.clear();
			coordinates.add("54%,40%");
			coordinates.add("64%,40%");
			perfectoCommand.put("location", coordinates);
			driver.executeScript("mobile:touch:drag", perfectoCommand);
			perfectoCommand.clear();
			coordinates.clear();
			coordinates.add("62%,40%");
			coordinates.add("62%,52%");
			perfectoCommand.put("location", coordinates);
			driver.executeScript("mobile:touch:drag", perfectoCommand);
			perfectoCommand.clear();
			coordinates.clear();
			coordinates.add("62%,48%");
			coordinates.add("52%,48%");
			perfectoCommand.put("location", coordinates);
			driver.executeScript("mobile:touch:drag", perfectoCommand);
			perfectoCommand.clear();
			coordinates.clear();
			coordinates.add("54%,48%");
			coordinates.add("64%,60%");
			perfectoCommand.put("location", coordinates);
			driver.executeScript("mobile:touch:drag", perfectoCommand);
			perfectoCommand.clear();
			coordinates.clear();
			break;
		case "S":

			break;
		case "T":

			break;
		case "U":

			break;
		case "V":

			break;
		case "W":

			break;
		case "X":

			break;
		case "Y":

			break;
		case "Z":

			break;
		}
	}

	/**
	 * Function to check the bro
	 * 
	 * @param Url
	 */
	protected void brokenLinkValidator(String Url) {
		urlLinkStatus(validationOfLinks(Url));
	}

	private String[] validationOfLinks(String urlToValidate) {
		String[] responseArray = new String[3];
		try {
			URL url = new URL(urlToValidate);
			httpURLConnect = (HttpURLConnection) url.openConnection();
			httpURLConnect.setConnectTimeout(3000);
			httpURLConnect.connect();
			responseStatus = httpURLConnect.getResponseCode();
			responseCode = responseStatus / 100;
		} catch (Exception e) {
		}
		responseArray[0] = urlToValidate;
		responseArray[1] = String.valueOf(responseCode);
		responseArray[2] = String.valueOf(responseStatus);
		return responseArray;
	}

	private void urlLinkStatus(String[] responseArray) {
		try {
			String linkValue = responseArray[0];
			String responseValue = responseArray[1];
			responseCode = Integer.valueOf(responseValue);
			String responseStatus = responseArray[2];
			switch (responseCode) {
			case 2:
				/*
				 * System.out.println("It's a Non- Broken Link with URL " +
				 * linkValue + " - " + responseCode + " - with Response code " +
				 * HttpURLConnection.HTTP_OK + " OK ");
				 */
				report.updateTestLog(linkValue, "Response code : "
						+ responseStatus + " - OK", Status.PASS);
				break;
			case 3:
				report.updateTestLog(linkValue, "Unknown Responce Code",
						Status.FAIL);
				break;
			case 4:
				/*
				 * System.out.println("It's a Broken link with URL " + linkValue
				 * + " - " + responseCode + " - Client Error " +
				 * HttpURLConnection.HTTP_CLIENT_TIMEOUT + " Bad Request ");
				 */
				report.updateTestLog(linkValue, "Response code : "
						+ responseStatus + " - Client error", Status.FAIL);
				break;

			case 5:
				/*
				 * System.out.println("It's a Broken link with URL " + linkValue
				 * + " - " + responseCode + " - Internal Server Error " +
				 * HttpURLConnection.HTTP_SERVER_ERROR);
				 */
				report.updateTestLog(linkValue, "Response code : "
						+ responseStatus + " - Internal Server Error",
						Status.FAIL);
				break;
			default:
				report.updateTestLog(linkValue, "Unknown Responce Code",
						Status.FAIL);

				break;
			}

		} catch (Exception e) {

		} finally {
			httpURLConnect.disconnect();

		}
	}

	/**
	 * Function to validate Alexa report
	 * 
	 * @param utterances
	 * @param expectedValue
	 * @param actualValue
	 * 
	 */
	protected void updateReport(String utterances, String expectedValue,
			String actualValue) {

		if (expectedValue.equalsIgnoreCase(actualValue)) {
			report.updateTestLog(utterances, expectedValue, actualValue,
					Status.PASS);
		} else {
			report.updateTestLog(utterances, expectedValue, actualValue,
					Status.FAIL);
		}
	}
	
	/**
	 * Wait for Page load	  
	 * @param none
	 * @author 
	 *
	 */ 
	public void waitForPageToBeReady() 
	{
		JavascriptExecutor js = (JavascriptExecutor)driver.getWebDriver();
	    //This loop will rotate for 100 times to check If page Is ready after every 1 second.
	    //You can replace your if you wants to Increase or decrease wait time.
	    for (int i=0;i<600; i++)
	    { 
	        try 
	        {
	            Thread.sleep(1000);
	        }catch (InterruptedException e) {} 
	        //To check page ready state.

	        if (js.executeScript("return document.readyState").toString().equals("complete"))
	        { 
	            break; 
	        }   
	      }
	 }
	
	/**
	 * Wait for Ajax loader	  
	 * @param none
	 * @author 
	 * 
	 */ 
	public void waitForAjAXLoader() 
	{
		
		JavascriptExecutor js = (JavascriptExecutor)driver.getWebDriver();	    
		
		//This loop will rotate for 100 times to check If page Is ready after every 1 second.
	    //You can replace your if you wants to Increase or decrease wait time.
	    
		for (int i=0;i<600; i++)
	    { 
	        try 
	        {
	            Thread.sleep(1000);
	            waitMax.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//img[contains(@src,'ajax-loader.gif')]")));             
	            
	        }catch (InterruptedException e) {} 
	        
	        //To check page ready state.
	        if (js.executeScript("return document.readyState").toString().equals("complete"))
	        { 
	            break; 
	        }   
	      }
	 }
    
    /**
	 * @Method page load	  
	 * @author 
	 * @param menu name
	 * @return none
	 * 
	 */
	public boolean waitUntilElementLoad(final By by) 
	{	    				   		
		
		boolean blnFlag = true;
		
		try
		{
		
			WebDriverWait wait = new WebDriverWait(driver.getWebDriver(),60);
			//wait = new WebDriverWait(driver.getWebDriver(),60);    		
			wait.withTimeout(Integer.parseInt(properties.getProperty("ObjectSyncTimeout")), TimeUnit.SECONDS);
			wait.pollingEvery(5, TimeUnit.SECONDS);
			wait.ignoring(NoSuchElementException.class);
			wait.ignoring(StaleElementReferenceException.class);
			wait.until(new ExpectedCondition<Boolean>()
			{
			@Override
			public Boolean apply(WebDriver driver)
			{
			    WebElement ele=driver.findElement(by);
			    if(ele==null)
			        return false;
			    else
			    {
			        System.out.println("WebElement found");
			        return true;
			    }     
			}
			});
		}catch(Exception e)
		{
		
			blnFlag = false;
			
			/*try 
			{
				report.updateTestLog("Step", "Verify Object in application","Object should be available in application","Object does not available in application", Status.FAIL);
			} catch (UnsupportedFlavorException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (AWTException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}*/
		}
		
		return blnFlag;
	}
	
	/**
	 * @Method verifyValueInListBox	  
	 * @author 
	 * @param menu name
	 * @return none
	 * 
	 */	
	public boolean verifyValueInListBox(By by, String strListBoxValues)
	{			
		boolean blnExist =false;		
		WebElement selectElement = driver.getWebDriver().findElement(by);
		Select select = new Select(selectElement);
		List<WebElement> allOptions = select.getOptions();		
		// Loop to print one by one
	    for (int j = 0; j < allOptions.size(); j++) 
	    {
	    	//System.out.println(allOptions.get(j).getText());
	    	if(strListBoxValues.contains(allOptions.get(j).getText()))
	    	{
	    		System.out.println(allOptions.get(j).getText() + " Value does exist in List box");    				
				blnExist = true;
				break;
	    	}
	    }    	   
	    
	    return blnExist;
	}
	
	/**
	 * @Method java script mouse over	  
	 * @author 
	 * @param menu name
	 * @return none
	 * 
	 */	      	
	public boolean onJSMouseOver(By by)
    {
        
		WebElement element = driver.getWebDriver().findElement(by); 
		boolean result = false;
        try
        {
            String mouseOverScript = "if(document.createEvent){var evObj = document.createEvent('MouseEvents');evObj.initEvent('mouseover',true, false); arguments[0].dispatchEvent(evObj);} else if(document.createEventObject){ arguments[0].fireEvent('onmouseover');}";
            JavascriptExecutor js = (JavascriptExecutor) driver.getWebDriver();
            js.executeScript(mouseOverScript, element);
            Thread.sleep(1000);	            
            result = true;
        }
        catch (Exception e)
        {
            //e.printStackTrace();
            result = false;
        }	        
        return result;
    }
	
	/**
	 * @Function verifyInbox
	 * @param locator
	 * @throws InterruptedException 
	 */
	public boolean verifyInbox(String strSubject1, String strSubject2, String strBody) throws Exception 
	{
		
		boolean blnEmailUpdate = false;
		try
		{  
		  
	      String username = properties.getProperty("UserEmailID").trim();
	      String password = properties.getProperty("UserPwd").trim();    	      
	      ExchangeService service = new ExchangeService(ExchangeVersion.Exchange2010_SP2);
	      ExchangeCredentials credentials = new WebCredentials(username, password);
	      service.setCredentials(credentials);
	      //service.autodiscoverUrl(username); 	      
	      //service.setUrl(new java.net.URI("https://myhostname/EWS/Exchange.asmx"));
	      //service.setUrl(new java.net.URI("https://mailapneu.jnj.com/EWS/Exchange.asmx"));
	      service.setUrl(new java.net.URI("https://mailapneu.jnj.com/owa/"));
	      //service.setUrl(new java.net.URI("https://mailapneu.jnj.com/owa/languageselection.aspx"));
	      
	      
 		   //Verify Inbox
			ItemView view = new ItemView(100);
			view.getOrderBy().add(ItemSchema.DateTimeReceived, SortDirection.Ascending);
			view.setPropertySet(new PropertySet(BasePropertySet.IdOnly, ItemSchema.Subject, ItemSchema.DateTimeReceived));
	
			FindItemsResults<Item> findResults = 				
					service.findItems(WellKnownFolderName.Inbox,
		        	new SearchFilter.SearchFilterCollection(
						LogicalOperator.Or, new SearchFilter.ContainsSubstring(ItemSchema.Subject, strSubject1),
					new SearchFilter.ContainsSubstring(ItemSchema.Subject, strSubject1)), view); 			
			
	
		    //MOOOOOOST IMPORTANT: load items properties, before
			service.loadPropertiesForItems(findResults, PropertySet.FirstClassProperties);
			//System.out.println("Total number of items found inbox: " + findResults.getTotalCount());   			

			//convert lower case
			strBody = strBody.toLowerCase();    			
			for (Item item : findResults) 
			{
				/*System.out.println(item.getSubject());
				System.out.println(item.getBody());*/    				
				if(strBody.contains(";"))
				{
					//Verify more than one body of the message
					String[] arrBody = strBody.split(";");
					for(int i=0;i<arrBody.length;i++)
					{
    					if(item.getBody().toString().toLowerCase().contains(arrBody[i]))
        				{
        					blnEmailUpdate = true;	 
        					//System.out.println("Exist" + arrBody[i]);
        				}
    					else
    					{
    						blnEmailUpdate = false;
    						System.out.println("Does Not Exist" + arrBody[i]);
    					}
					}
					
					if(blnEmailUpdate)
					{
						break;
					}    					
				}    				
				else
				{
					//Verify Single content body of the message
					if(item.getBody().toString().toLowerCase().contains(strBody))
    				{
    					blnEmailUpdate = true;
    					System.out.println("Exist" + strBody);
    					break;
    				}
				}
			}
		}catch(Exception e)    		
		{
			blnEmailUpdate = false;
			System.out.println(e.getMessage());
		}
			return blnEmailUpdate;
	}
	
	//*******************************************************************************
	/**
	 * Switch to Window
	 * @param locator
	 * @throws InterruptedException 
	 */
	public boolean swithToWindow(By by) throws InterruptedException   
	{
		boolean blnElementExist = false;
		String strParentWindowName = driver.getWebDriver().getWindowHandle();
		Set<String> objWindowNames = driver.getWebDriver().getWindowHandles();
		
		System.out.println(strParentWindowName);
		System.out.println(objWindowNames.size());
		
		for(int i=1;i<=50;i++)
		{
			if(objWindowNames.size()>1)
			{
				break;    				
			}
			else
			{
				Thread.sleep(1000);
			}
			objWindowNames = driver.getWebDriver().getWindowHandles();
		}	
			
			
		try
		{
    		if(objWindowNames.size()>1)
    		{
    			for(String strChildWindowName : objWindowNames)
    			{
    				/*System.out.println(strParentWindowName);
    				System.out.println(strChildWindowName);*/
    				if(!strParentWindowName.equalsIgnoreCase(strChildWindowName))
    				{
	    				driver.getWebDriver().switchTo().window(strChildWindowName);
	    				if(driver.getWebDriver().findElements(by).size()>0)
	    				{
	    					//default maximied few sceanrios 
	    					//driver.getWebDriver().manage().window().maximize();
	    					System.out.println("Switch To Window success");
	    					blnElementExist = true;		    					
	    					break;
	    				}
    				}
    				
    				driver.getWebDriver().switchTo().defaultContent();
    			}
    		}
		}catch(Exception e)
		{
			System.out.println("Windows is swithced to :" + blnElementExist);
		}   	
		
		return blnElementExist;
	}
	
	//*******************************************************************************
		/**
		 * Switch to Window
		 * @param locator
		 * @throws InterruptedException 
		 */
	public boolean isAlertPresent()
	{
	    try 
	    { 
	        driver.switchTo().alert(); 
	        return true; 
	    }   // try 
	    catch (NoAlertPresentException Ex) 
	    { 
	        return false; 
	    }   // catch 
	}
	
	/**
	 * Switch to Window
	 * @param locator
	 * @throws InterruptedException 
	 */
	public boolean swithToWindowUsingURL(String strBrowserTitle) throws InterruptedException   
	{
		boolean blnElementExist = false;
		String strParentWindowName = driver.getWebDriver().getWindowHandle();
		Set<String> objWindowNames = driver.getWebDriver().getWindowHandles();
		
		for(int i=1;i<=50;i++)
		{
			if(objWindowNames.size()>1)
			{
				break;    				
			}
			else
			{
				Thread.sleep(1000);
			}
			objWindowNames = driver.getWebDriver().getWindowHandles();
		}	
			
			
		try
		{
    		if(objWindowNames.size()>1)
    		{
    			for(String strChildWindowName : objWindowNames)
    			{
    				if(!strParentWindowName.equalsIgnoreCase(strChildWindowName))
    				{
	    				driver.getWebDriver().switchTo().window(strChildWindowName);
	    				//Swith To Window using window url
	    				//System.out.println(driver.getWebDriver().getCurrentUrl());
	    				if(driver.getWebDriver().getCurrentUrl().contains(strBrowserTitle))
	    				{
	    					//driver.getWebDriver().manage().window().maximize();
	    					System.out.println("Switch To Window success");
	    					blnElementExist = true;		    					
	    					break;
	    				}
    				}
    				
    				driver.getWebDriver().switchTo().defaultContent();
    			}
    		}
		}catch(Exception e)
		{
			System.out.println("Windows is swithced to :" + blnElementExist);
		}   	
		
		return blnElementExist;
	}
	
	
	/**
	 * @Method closeUpdatedPrivacyPolicy	  
	 * @author 
	 * @param mandatory fields
	 * @return none
	 * @throws AWTException 
	 * @throws IOException 
	 * @throws UnsupportedFlavorException 
	 *
	 */
	public String getListboxValues(By by) throws UnsupportedFlavorException, IOException, AWTException
	{			
		boolean blnFlag = true;
		String strAllListboxValue = "";
		WebElement selectElement = driver.findElement(by);
		Select select = new Select(selectElement);
		List<WebElement> allOptions = select.getOptions();			
		for (int j = 0; j < allOptions.size(); j++) 
		{
	        System.out.println(allOptions.get(j).getText());
	        strAllListboxValue= strAllListboxValue + " " + allOptions.get(j).getText();
	    }
		
		return strAllListboxValue;		
	}
	
	/**
	 * @Method verifyValueExistInListBox	  
	 * @author 
	 * @param mandatory fields
	 * @return none
	 * @throws AWTException 
	 * @throws IOException 
	 * @throws UnsupportedFlavorException 
	 * 
	 */
	public boolean verifyValueExistInListBox(By by, String strExpValue) throws UnsupportedFlavorException, IOException, AWTException
	{			
		boolean blnFlag = false;
		String strActValue=null;
		String strAllListboxValue = "";
		WebElement selectElement = driver.findElement(by);
		Select select = new Select(selectElement);
		List<WebElement> allOptions = select.getOptions();			
		
		for (int j=0; j<allOptions.size(); j++) 
		{
			strActValue =  allOptions.get(j).getText();
			System.out.println(strActValue);
	        if(strActValue.trim().equalsIgnoreCase(strExpValue.trim()))
	        {
	        	blnFlag = true;
	        	break;
	        }
	    }	
		
		return blnFlag;		
	}
	
	/**
	 * @Method verifyPDFContent	  
	 * @author 
	 * @param menu name
	 * @return none
	 * 
	 */	
	public boolean verifyPDFContent(String strFilePath, String strFileName, String reqTextInPDF) throws IOException
	{
		
		boolean flag = true;
		
		PDFTextStripper pdfStripper = null;
		PDDocument pdDoc = null;
		COSDocument cosDoc = null;
		String parsedText = null;

		try 
		{     			

			String filePath = strFilePath +"\\"+ strFileName;
			
			//Verify file eixst or not
			File f = new File(filePath);
			if(f.exists())
			{
			    System.out.println("File existed");			    
			}
			else
			{
			    System.out.println("File not found!");
			    return false;
			}
			
			pdDoc = PDDocument.load(new File(filePath));
			pdfStripper = new PDFTextStripper();
			pdfStripper.setStartPage(1);
			pdfStripper.setEndPage(500);    	
			
			//System.out.println(pdfStripper.getText(pdDoc).replace("\n", "").replaceAll("\r", "").replaceAll("  ", " "));			
			///parsedText = pdfStripper.getText(pdDoc).replace("\n", "").replaceAll("\r", " ").replaceAll("  ", " ");			
			parsedText = pdfStripper.getText(pdDoc).replace("\n", "").replaceAll("\r", "").replaceAll("  ", " ").replaceAll(" ", "");
			
		} catch (MalformedURLException e2) 
		{
			//System.out.println("URL string could not be parsed "+e2.getMessage());
			//report.updateTestLog("Open PDF file","URL string could not be parsed", Status.FAIL);
			
		} catch (IOException e) 
		{
			System.out.println("Unable to open PDF Parser. " + e.getMessage());
			//report.updateTestLog("Open PDF file","Unable to open PDF Parser.", Status.FAIL);
			try {
				if (cosDoc != null)
					cosDoc.close();
				if (pdDoc != null)
					pdDoc.close();
			} catch (Exception e1) {
				//e.printStackTrace();
				//report.updateTestLog("Open PDF file","Unable to open PDF Parser.", Status.FAIL);
			}
		}
		
		System.out.println("+++++++++++++++++");
		System.out.println(parsedText);
		System.out.println("+++++++++++++++++");
		
		String strTempValue = null;
		String strExpValue = reqTextInPDF;
		String[]  arrReqTextInPDF= reqTextInPDF.split(";");
		for(int i=0;i<arrReqTextInPDF.length;i++)
		{    		
    		String strValue = arrReqTextInPDF[i].replaceAll(" ", "").trim();
    		if(!strValue.isEmpty())
    		{
    			if(!parsedText.contains(strValue))
	    		{
	    			if(i==0)
	    			{
	    				strTempValue = strValue;
	    			}
	    			else
	    			{
	    				strTempValue = strTempValue + ";"+ strValue;
	    			}
    				//report.updateTestLog("Verify Text In PDF File", strValue + " - value does not exist in PDF", Status.FAIL);
	    			flag=false;
	    		}
    		}
		}
	
		if(flag)
		{
			System.out.println("Verified PDF successfully");
			pdDoc.close();
			waitForPageToBeReady();
			//report.updateTestLog("Verify PDF File", "PDF File is verified successfully",Status.PASS);
			//report.updateTestLog("Verify Text In PDF File", strExpValue + " - value exist in PDF", Status.PASS);
		}
		else
		{
			//System.out.println("Verified PDF failed");
			//report.updateTestLog("Verify Text In PDF File", strTempValue + " - value does NOT exist in PDF", Status.FAIL);
		}   		
		
		return flag;
	}
	
	/**
	 * @Method verifyExcelContent	  
	 * @author 
	 * @param menu name
	 * @return none
	 * 
	 */	
	@SuppressWarnings("unchecked")
	public boolean verifyExcelContent(String strFilePath, String strFileName, String strSheetName, String reqTextInExcel, int intRowNum)
	{
		boolean blnValueExist = true;
		ArrayList<String> objColName = new ArrayList<String>(); 
		ExcelDataAccess obj = new ExcelDataAccess(strFilePath, strFileName);
		obj.setDatasheetName(strSheetName); 	
		
		objColName = obj.getSpecificRowAllCellValues(intRowNum);
		System.out.println(objColName);
		
		String strExpValue = reqTextInExcel;
		String strTempValue = null;
		String[]  arrReqTextInExcel= reqTextInExcel.split(";");	
		for(int i=0;i<arrReqTextInExcel.length;i++)
		{    		
    		String strValue = arrReqTextInExcel[i].trim();
    		if(!strValue.isEmpty())
    		{
    			if(!(objColName.indexOf(strValue) >=0))	    			
	    		{
	    			if(i==0)
	    			{
	    				strTempValue = strValue;
	    			}
	    			else
	    			{
	    				strTempValue = strTempValue + ";"+ strValue;
	    			}
    				//report.updateTestLog("Verify Text In PDF File", strValue + " - value does not exist in PDF", Status.FAIL);    	    			
	    			//System.out.println(strValue);
	    			blnValueExist=false;
	    		}
    		}
		}
	
		if(blnValueExist)
		{
			System.out.println("Verified Excel successfully");
			//report.updateTestLog("Verify Excel File", "Excel File is verified successfully",Status.PASS);
			//report.updateTestLog("Verify Text In Excel File", strExpValue + " - value exist in Excel", Status.PASS);
		}
		else
		{
			System.out.println("Verified Excel failed");
			//report.updateTestLog("Verify Text In Excel File", strTempValue + " - value does NOT exist in Excel", Status.FAIL);
		}  	
		
		return blnValueExist;    	
	}
	
	/**
	 * @Method highlighElement	  
	 * @author 
	 * @param none
	 * @return none
	 * @throws InterruptedException 
	 *
	 */
	public void highlightElement(By by)
	{
		WebElement element = driver.getWebDriver().findElement(by);
		JavascriptExecutor js=(JavascriptExecutor)driver.getWebDriver(); 
	    js.executeScript("arguments[0].setAttribute('style', 'background: yellow; border: 2px solid red;');", element);

	    try 
	    {
	    	Thread.sleep(1000);
	    } 
	    catch (InterruptedException e) 
	    {
	    	System.out.println(e.getMessage());
	    } 

	    js.executeScript("arguments[0].setAttribute('style','border: solid 2px white');", element);   	    
	}
	
	/**
	 * @Method scrollToElement	  
	 * @author 
	 * @param none
	 * @return none
	 * @throws InterruptedException 
	 * 
	 */
	public void scrollToElement(By by)
	{				
	    try 
	    {
	    	WebElement element = driver.getWebDriver().findElement(by);
	    	JavascriptExecutor js=(JavascriptExecutor)driver.getWebDriver();	    	
	    	js.executeScript("arguments[0].scrollIntoView(true);", element);
	    	System.out.println("Scroll To Element");
	    } 
	    catch (Exception e) 
	    {
	    	
	    }   	    
	}
	
	/**
	 * @Method wdActions	  
	 * @author 
	 * @param object and value
	 * @return none
	 * @throws AWTException 
	 * @throws IOException 
	 * @throws UnsupportedFlavorException 
	 * 
	 */
	public void wdActions(By byLocator, String strWebElementType, String strWebElementValue) throws UnsupportedFlavorException, IOException, AWTException
    {   
    	
		//Verify Object is loaded or not
		if(!waitUntilElementLoad(byLocator))
		{
			System.out.println(byLocator.toString());
			report.updateTestLog("Common Step", "Verify Object in application","Object should be displayed in application","Object does not displayed in application, Object Name:"+ byLocator.toString(), Status.FAIL);
			throw new FrameworkException("Verify Object in application","Object should be displayed in application");
	
		}	
		
		String strErrorMsg=null;
		String strWebElementLabelName ="";
    	boolean blnWebElementStatus = true;
    	WebElement eleWebElement = null;
    	By locator=null;        	        	
    	strWebElementType = strWebElementType.toLowerCase();
    	String strTempWebElementType = "'" + strWebElementValue + "'";
    	//String strTempWebElementType =  strWebElementValue;  
    	//Don't give single quote for Label name becuase html report won't generate
    	//String strTempWebElementLabelName = "'" + strWebElementLabelName + "'";        	        	
    	String strTempWebElementLabelName = strWebElementLabelName;
    	String strSteps = null;
    	String strExpectedSteps = null;
    	
    	//Perfor Operation
    	try
    	{         			
        		eleWebElement = driver.findElement(byLocator);         		
        		
        		//Updated on 03rd Nov 2017
        		//**********************************************************************            		
        		if(!properties.getProperty("DefaultBrowser").equalsIgnoreCase("CHROME"))
        		{
            		JavascriptExecutor js=(JavascriptExecutor)driver.getWebDriver();
            		js.executeScript("arguments[0].scrollIntoView(true);", eleWebElement);
            		onJSMouseOver(byLocator);
        		}
        		//**********************************************************************            		
        		
        		if(strWebElementType.equalsIgnoreCase(textbox))
	        	{
	        		//*************************************************************************
        			wait.until(ExpectedConditions.visibilityOf(eleWebElement));
        			eleWebElement.clear();
	        		eleWebElement.sendKeys(strWebElementValue);	        			
	        		strSteps = "Enter value in " + strTempWebElementLabelName + " textbox";
	        		strExpectedSteps = strTempWebElementType + " - Value is entered in " + strTempWebElementLabelName + " textbox successfully";
	        		System.out.println(strSteps +  "	" + strExpectedSteps);		        	  	
	        		//report.updateTestLog(strSteps, strExpectedSteps, Status.PASS);   
	        		//************************************************************************ 		
	        		
	        	}
	        	else if(strWebElementType.equalsIgnoreCase(textarea))
	        	{		        		
	        		eleWebElement.clear();
	        		eleWebElement.sendKeys(strWebElementValue);
	        		strSteps = "Enter value in " + strTempWebElementLabelName + " textarea";
	        		strExpectedSteps = strTempWebElementType + " - Value is entered in " + strTempWebElementLabelName + " textarea successfully";
	        		System.out.println(strSteps +  "	" + strExpectedSteps);		        	  	
	        		//report.updateTestLog(strSteps, strExpectedSteps, Status.PASS);		        		
	        	}
	        	else if(strWebElementType.equalsIgnoreCase(dropdown))
	        	{	        		
	        		if (verifyValueInListBox(byLocator,strWebElementValue))
	        		{
		        		Select eleDropDown = new Select(eleWebElement);		        		
		        		eleDropDown.selectByVisibleText(strWebElementValue);
		        		strSteps = "Select value from DropDown List";
		        		strExpectedSteps = strTempWebElementType + " - value is selected from " + strTempWebElementLabelName + " DropDown List";
		        		System.out.println(strSteps +  "	" + strExpectedSteps);		        	  	
		        		//report.updateTestLog(strSteps, strExpectedSteps, Status.PASS);	        			        		
	        		}
	        		else
	        		{
	        			System.out.println(strWebElementValue +"Value does not exist in List box");
	        			//report.updateTestLog(strSteps, strWebElementValue + "Value does not exist in List box", Status.FAIL);
	        			//report.updateTestLog("Select value in Listbox","Verify List box value", strWebElementValue +"Value should be exist in List box", strWebElementValue +"Value does not exist in List box",Status.FAIL,"No");		
	        		}
	        	}
	        	else if(strWebElementType.equalsIgnoreCase("dropdownSelectByValue"))
	        	{			
	        		Select eleDropDown = new Select(eleWebElement);
	        		eleDropDown.selectByValue(strWebElementValue);		        		
	        		strSteps = "Select value from DropDown List";
	        		strExpectedSteps = strTempWebElementType + " - value is selected from " + strTempWebElementLabelName + " DropDown List";
	        		System.out.println(strSteps +  "	" + strExpectedSteps);		        	  	
	        		//report.updateTestLog(strSteps, strExpectedSteps, Status.PASS);	        			        		
	        	}
	        	else if(strWebElementType.equalsIgnoreCase("dropdownSelectByIndex"))
	        	{	 		
	        		
	        		Select eleDropDown = new Select(eleWebElement);
	        		eleDropDown.selectByIndex(Integer.valueOf(strWebElementValue));		        		
	        		strSteps = "Select value from DropDown List";
	        		strExpectedSteps = strTempWebElementType + " - value is selected from " + strTempWebElementLabelName + " DropDown List";
	        		System.out.println(strSteps +  "	" + strExpectedSteps);		        	  	
	        		//report.updateTestLog(strSteps, strExpectedSteps, Status.PASS);	        			        		
	        	}
	        	else if(strWebElementType.equalsIgnoreCase(button))
	        	{		        		 
	        		strSteps = "Click On " + strTempWebElementLabelName + " button";
	        		strExpectedSteps = strTempWebElementLabelName + " button is clicked successfully";
	        		System.out.println(strSteps +  "	" + strExpectedSteps);		        	  	
	        		//report.updateTestLog(strSteps, strExpectedSteps, Status.PASS);		        		
	        		//WebDriverWait waitTemp = new WebDriverWait(driver.getWebDriver(),60);
	        		wait.until(ExpectedConditions.elementToBeClickable(eleWebElement));
	        		eleWebElement.click();  
	        	}
	        	else if(strWebElementType.equalsIgnoreCase(radio))
	        	{
	        		eleWebElement.click(); 
	        		strSteps = "Click On " + strTempWebElementLabelName + " radio button";
	        		strExpectedSteps = strTempWebElementLabelName + " radio button is clicked successfully";
	        		System.out.println(strSteps +  "	" + strExpectedSteps);		        	  	
	        		//report.updateTestLog(strSteps, strExpectedSteps, Status.PASS);		        		 	
	        	 }
	        	else if(strWebElementType.equalsIgnoreCase(checkbox))
	        	{
	        		eleWebElement.click();
	        		strSteps = "Click On " + strTempWebElementLabelName + " checkbox";
	        		strExpectedSteps = strTempWebElementLabelName + " checkbox is clicked successfully";
	        		System.out.println(strSteps +  "	" + strExpectedSteps);		        	  	
	        		//report.updateTestLog(strSteps, strExpectedSteps, Status.PASS);		        				        	  
	        	}
	        	else if(strWebElementType.equalsIgnoreCase("link"))
	        	{		        			
	        		strSteps = "Click On " + strTempWebElementLabelName + " link";
	        		strExpectedSteps = strTempWebElementLabelName + " link is clicked successfully";
	        		System.out.println(strSteps +  "	" + strExpectedSteps);		        	  	
	        		//report.updateTestLog(strSteps, strExpectedSteps, Status.PASS);
	        		wait.until(ExpectedConditions.elementToBeClickable(eleWebElement));
	        		eleWebElement.click();
	        			        	  	
	        	}
	        	else if(strWebElementType.equalsIgnoreCase("mouseclick"))
	        	{		        		
	        		strSteps = "Click On " + strTempWebElementLabelName + " WebElement";
	        		strExpectedSteps = strTempWebElementLabelName + " WebElement is clicked successfully";
	        		System.out.println(strSteps +  "	" + strExpectedSteps);		        	  	
	        		//report.updateTestLog(strSteps, strExpectedSteps, Status.PASS);
	        	
	        		/*Actions action = new Actions(driver.getWebDriver());
	        	  	action.moveToElement(eleWebElement).click(eleWebElement).build().perform();*/ 
	        	  	
	        		//updated on 11th Aug 2016
	        	  	if(!properties.getProperty("DefaultBrowser").equalsIgnoreCase("CHROME"))
            		{
	            		/*JavascriptExecutor js=(JavascriptExecutor)driver.getWebDriver();
	            		js.executeScript("arguments[0].scrollIntoView(true);", eleWebElement);
	            		onJSMouseOver(byLocator);	*/	            		
	            		//jsActions(byLocator, "button", "", strWebElementLabelName);
	            		jsActions(byLocator, button, "");
            		}
	        	  	else
	        	  	{
	        	  		Actions action = new Actions(driver.getWebDriver());
		        	  	action.moveToElement(eleWebElement).click(eleWebElement).build().perform();		        	
	        	  	}
	   		    }
	        	else if(strWebElementType.equalsIgnoreCase("mouseover"))
	        	{		        		
	        		strSteps = "MouseOver On " + strTempWebElementLabelName + " WebElement";
	        		strExpectedSteps = strTempWebElementLabelName + " WebElement is Mouse Over successfully";
	        		System.out.println(strSteps +  "	" + strExpectedSteps);		        	  	
	        		//report.updateTestLog(strSteps, strExpectedSteps, Status.PASS);
	        		Actions action = new Actions(driver.getWebDriver());
	        	  	action.moveToElement(eleWebElement).build().perform(); 
	   		    }
	        	else
	        	{		        		
	        		strSteps = "Click On " + strTempWebElementLabelName + " WebElement";
	        		strExpectedSteps = strTempWebElementLabelName + " WebElement is clicked successfully";
	        		System.out.println(strSteps +  "	" + strExpectedSteps);		        	  	
	        		//report.updateTestLog(strSteps, strExpectedSteps, Status.PASS);
	        		eleWebElement.click();		        			        	  	
	        	}            		
        		
        		//Wait for page load            		
        		waitForPageToBeReady() ; 
        		
        		
    	}catch(Exception e)
    	{
    		strErrorMsg=e.toString();
    		strSteps = "GUI operation failed";
    		strExpectedSteps = strTempWebElementLabelName + " WebElement is NOT clicked";    		
    		System.out.print(strTempWebElementLabelName + " - WebElement operation is not performed - Error Message:" + e.toString());
    		blnWebElementStatus = false;   		
    		
    		/*try
    		{
				report.updateTestLog("Step", "Verify Object","Object should be avaialble","Object does not avaialble", Status.FAIL);
				throw new FrameworkException("Verify Object in application","Object should be available in application");
			} catch (AWTException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
    		strSteps = "GUI operation failed";
    		strExpectedSteps = strTempWebElementLabelName + " WebElement is NOT clicked";    		
    		System.out.print(strTempWebElementLabelName + " - WebElement operation is not performed - Error Message:" + e.toString());
    		//report.updateTestLog(strSteps, strExpectedSteps, Status.FAIL);
    		blnWebElementStatus = false;
    		//System.exit(0);
    		throw new FrameworkException(strSteps,strExpectedSteps);*/
    	}    
    	
    	if(!blnWebElementStatus)
    	{    		
    		report.updateTestLog("Common Step", "Verify WebElement Operation","WebElement Operation shoud be performed", byLocator.toString() + " - WebElement Operation does not performed succesfully", Status.FAIL);
			throw new FrameworkException("Verify WebElement Operation","WebElement Operation does not performed succesfully");
    	}
    }
      
	
	/**
	 * @Method jsActions	  
	 * @author 
	 * @param object and value
	 * @return none
	 * @throws AWTException 
	 * @throws IOException 
	 * @throws UnsupportedFlavorException 
	 * 
	 */	
	
    public void jsActions(By byLocator, String strWebElementType, String strWebElementValue) throws UnsupportedFlavorException, IOException, AWTException
    {     	        	
        //Verify Object is loaded or not
		if(!waitUntilElementLoad(byLocator))
		{
			System.out.println(byLocator.toString());
			report.updateTestLog("Common Step", "Verify Object in application","Object should be displayed in application","Object does not displayed in application, Object Name:"+ byLocator.toString(), Status.FAIL);
			throw new FrameworkException("Verify Object in application","Object should be displayed in application");
		}	
    			
    	String strErrorMsg="";
		boolean blnWebElementStatus = true;
    	String strWebElementLabelName ="";
    	WebElement eleWebElement = null;
    	By locator=null;        	        	
    	strWebElementType = strWebElementType.toLowerCase();
    	String strTempWebElementType = "'" + strWebElementValue + "'";        	
    	//Don't give single quote for Label name becuase html report won't generate
    	//String strTempWebElementLabelName = "'" + strWebElementLabelName + "'";
    	String strTempWebElementLabelName = strWebElementLabelName;
    	String strSteps = null;
    	String strExpectedSteps = null;
    	
    	JavascriptExecutor js=(JavascriptExecutor)driver.getWebDriver();    	
            	        	       	
    	//Perfor Operation
    	try
    	{       		        			
        		eleWebElement = driver.findElement(byLocator);           	
        		
        		//js.executeScript("arguments[0].focus();", eleWebElement);  
        		if(!properties.getProperty("DefaultBrowser").equalsIgnoreCase("CHROME"))
        		{
        			js.executeScript("arguments[0].scrollIntoView(true);", eleWebElement);
        			onJSMouseOver(byLocator);
        		}            		
        		
        		if(strWebElementType.equalsIgnoreCase(textbox))
	        	{
	        		//******************************************************************************************
        			js.executeScript("arguments[0].value ='';",eleWebElement );	
	        		js.executeScript("arguments[0].value ='" + strWebElementValue + "';",eleWebElement );	
	        		
	        		if(!strWebElementLabelName.equalsIgnoreCase("password"))
	        		{
	        			strSteps = "Enter value in " + strTempWebElementLabelName + " textbox";
		        		strExpectedSteps = strTempWebElementType + " - Value is entered in " + strTempWebElementLabelName + " textbox successfully";
		        		System.out.println(strSteps +  "	" + strExpectedSteps); 	  	
		        		//report.updateTestLog(strSteps, strExpectedSteps, Status.PASS);
	        		}
	        		else
	        		{
	        			strSteps = "Enter value in " + strTempWebElementLabelName + " textbox";
		        		strExpectedSteps = "Password is entered in " + strTempWebElementLabelName + " textbox successfully";
		        		System.out.println(strSteps +  "	" + strExpectedSteps);
		        		//report.updateTestLog(strSteps, strExpectedSteps, Status.PASS);
	        		}	        		
	        		
	        	}
	        	else if(strWebElementType.equalsIgnoreCase(textarea))
	        	{
	        		js.executeScript("arguments[0].value ='';",eleWebElement );	
	        		js.executeScript("arguments[0].value ='" + strWebElementValue + "';",eleWebElement );
	        		strSteps = "Enter value in " + strTempWebElementLabelName + " textarea";
	        		strExpectedSteps = strTempWebElementType + " - Value is entered in " + strTempWebElementLabelName + " textarea successfully";
	        		System.out.println(strSteps +  "	" + strExpectedSteps);		        	  	
	        		//report.updateTestLog(strSteps, strExpectedSteps, Status.PASS);	
	        	}
	        	else if(strWebElementType.equalsIgnoreCase(dropdown))
	        	{
	        		js.executeScript("arguments[0].value ='" + strWebElementValue + "';",eleWebElement );		        		
	        		strSteps = "Select value from DropDown List";
	        		strExpectedSteps = strTempWebElementType + " - value is selected from " + strTempWebElementLabelName + " DropDown List";
	        		System.out.println(strSteps +  "	" + strExpectedSteps);		        	  	
	        		//report.updateTestLog(strSteps, strExpectedSteps, Status.PASS);	
	        	}
	        	else if(strWebElementType.equalsIgnoreCase(button))
	        	{		        		        		
	        		strSteps = "Click On " + strTempWebElementLabelName + " button";
	        		strExpectedSteps = strTempWebElementLabelName + " button is clicked successfully";
	        		System.out.println(strSteps +  "	" + strExpectedSteps);		        	  	
	        		//report.updateTestLog(strSteps, strExpectedSteps, Status.PASS);		
	        		wait.until(ExpectedConditions.elementToBeClickable(eleWebElement));       			        		
	        		js.executeScript("arguments[0].click();", eleWebElement);	
	        	}
	        	else if(strWebElementType.equalsIgnoreCase(radio))
	        	{
	        		js.executeScript("arguments[0].click();", eleWebElement);
	        		strSteps = "Click On " + strTempWebElementLabelName + " radio button";
	        		strExpectedSteps = strTempWebElementLabelName + " radio button is clicked successfully";
	        		System.out.println(strSteps +  "	" + strExpectedSteps);		        	  	
	        		//report.updateTestLog(strSteps, strExpectedSteps, Status.PASS);			        	  	
	        	}
	        	else if(strWebElementType.equalsIgnoreCase(checkbox))
	        	{		        		
	        		js.executeScript("arguments[0].click();", eleWebElement);
	        		strSteps = "Click On " + strTempWebElementLabelName + " checkbox";
	        		strExpectedSteps = strTempWebElementLabelName + " checkbox is clicked successfully";
	        		System.out.println(strSteps +  "	" + strExpectedSteps);		        	  	
	        		//report.updateTestLog(strSteps, strExpectedSteps, Status.PASS);			        	  	
	        	}
	        	else if(strWebElementType.equalsIgnoreCase("link"))
	        	{		        	
	        		strSteps = "Click On " + strTempWebElementLabelName + " link";
	        		strExpectedSteps = strTempWebElementLabelName + " link is clicked successfully";
	        		System.out.println(strSteps +  "	" + strExpectedSteps);		        	  	
	        		//report.updateTestLog(strSteps, strExpectedSteps, Status.PASS);
	        		js.executeScript("arguments[0].click();", eleWebElement);
	        	}
	        	else if(strWebElementType.equalsIgnoreCase("focus"))
	        	{
	        		js.executeScript("arguments[0].focus();", eleWebElement);  
	        	}
	        	else
	        	{		        			        		
	        		strSteps = "Click On " + strTempWebElementLabelName + " WebElement";
	        		strExpectedSteps = strTempWebElementLabelName + " WebElement is clicked successfully";
	        		System.out.println(strSteps +  "	" + strExpectedSteps);		        	  	
	        		//report.updateTestLog(strSteps, strExpectedSteps, Status.PASS);
	        		js.executeScript("arguments[0].click();", eleWebElement);
	        	}
        		
        		//Wait for page load        		
        		//waitForPageToBeReady();
        		
    	}catch(Exception e)
    	{        					
    		
    		strErrorMsg=e.toString();
    		strSteps = "GUI operation failed";
    		strExpectedSteps = strTempWebElementLabelName + " WebElement is NOT clicked";    		
    		System.out.print(strTempWebElementLabelName + " - WebElement operation is not performed - Error Message:" + e.toString());
    		blnWebElementStatus = false;   		
    		
    		/*try
    		{
				report.updateTestLog("Step", "Verify Object","Object should be avaialble","Object does not avaialble", Status.FAIL);
				throw new FrameworkException("Verify Object in application","Object should be available in application");
			} catch (AWTException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
    		strSteps = "GUI operation failed";
    		strExpectedSteps = strTempWebElementLabelName + " WebElement is NOT clicked";    		
    		System.out.print(strTempWebElementLabelName + " - WebElement operation is not performed - Error Message:" + e.toString());
    		//report.updateTestLog(strSteps, strExpectedSteps, Status.FAIL);
    		blnWebElementStatus = false;
    		//System.exit(0);
    		throw new FrameworkException(strSteps,strExpectedSteps);*/
    	}    
    	
    	if(!blnWebElementStatus)
    	{    		
    		report.updateTestLog("Common Step", "Verify WebElement Operation","WebElement Operation shoud be performed", byLocator.toString() + " - WebElement Operation does not performed succesfully", Status.FAIL);
			throw new FrameworkException("Verify WebElement Operation","WebElement Operation does not performed succesfully");
			    
    	}
 }	
    
    
    public boolean IsElementPresent(String Xpath)
	{
		if(driver.findElements(By.xpath(Xpath)).size()==1) 
        {
        	elementPresent=true; 
        	} else {
            elementPresent=false;
        }
        return elementPresent;
	}
    
    public boolean IsElementPresentByID(String ID)
	{
		if(driver.findElements(By.id(ID)).size()==1) 
        {
        	elementPresent=true; 
        	} else {
            elementPresent=false;
        }
        return elementPresent;
	}
 
    public void pageScrollDown()
	{
		JavascriptExecutor js=(JavascriptExecutor)driver.getWebDriver();    	
		js.executeScript("window.scrollBy(0,300)", "");
	}
	
	public void pageScrollUp()
	{
		JavascriptExecutor js=(JavascriptExecutor)driver.getWebDriver();   
		js.executeScript("window.scrollBy(0,-300)", "");
	}
	
	public String GetRandomString()
	{
		String SALTCHARS = "abcdefghijklmnopqrstuvwxyz1234567890";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < 7) 
        {
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        String saltStr = salt.toString();
        return saltStr;
	}
    
	/**
	 * @Method verifyExcelContentPartial	  
	 * @author 
	 * @param menu name
	 * @return none
	 * 
	 */	
	@SuppressWarnings("unchecked")
	public boolean verifyExcelContentPartial(String strFilePath, String strFileName, String strSheetName, String reqTextInExcel, int intRowNum)
	{
		boolean blnValueExist = true;
		ArrayList<String> objColName = new ArrayList<String>(); 
		ExcelDataAccess obj = new ExcelDataAccess(strFilePath, strFileName);
		obj.setDatasheetName(strSheetName); 	
		
		//intRowNum=9;
		objColName = obj.getSpecificRowAllCellValues(intRowNum);
		System.out.println(objColName);
		
		String strExpValue = reqTextInExcel;
		String strTempValue = null;
		String[]  arrReqTextInExcel= reqTextInExcel.split(";");	
		for(int i=0;i<arrReqTextInExcel.length;i++)
		{    		
    		String strValue = arrReqTextInExcel[i].trim();
    		if(!strValue.isEmpty())
    		{
    			
    			if(!(objColName.toString().contains(strValue)))
    			//if(!(objColName.indexOf(strValue) >=0))	    			
	    		{
	    			if(i==0)
	    			{
	    				strTempValue = strValue;
	    			}
	    			else
	    			{
	    				strTempValue = strTempValue + ";"+ strValue;
	    			}
    				//report.updateTestLog("Verify Text In PDF File", strValue + " - value does not exist in PDF", Status.FAIL);    	    			
	    			//System.out.println(strValue);
	    			blnValueExist=false;
	    		}
    		}
		}
	
		if(blnValueExist)
		{
			System.out.println("Verified Excel successfully");
			//report.updateTestLog("Verify Excel File", "Excel File is verified successfully",Status.PASS);
			//report.updateTestLog("Verify Text In Excel File", strExpValue + " - value exist in Excel", Status.PASS);
		}
		else
		{
			System.out.println("Verified Excel failed");
			//report.updateTestLog("Verify Text In Excel File", strTempValue + " - value does NOT exist in Excel", Status.FAIL);
		}  	
		
		return blnValueExist;    	
	}	

	/**
	 * Switch to Window
	 * @param locator
	 * @throws InterruptedException 
	 */
	public boolean swithToWindowUsingTitle(String strBrowserTitle) throws InterruptedException   
	{
		boolean blnElementExist = false;
		String strParentWindowName = driver.getWebDriver().getWindowHandle();
		Set<String> objWindowNames = driver.getWebDriver().getWindowHandles();
		
		for(int i=1;i<=50;i++)
		{
			if(objWindowNames.size()>1)
			{
				break;    				
			}
			else
			{
				Thread.sleep(1000);
			}
			objWindowNames = driver.getWebDriver().getWindowHandles();
		}	
			
			
		try
		{
    		if(objWindowNames.size()>=1)
    		{
    			
    			for(String strChildWindowName : objWindowNames)
    			{
    				
    				driver.getWebDriver().switchTo().window(strChildWindowName);    				
    				System.out.println(strChildWindowName);
    				System.out.println(driver.getWebDriver().getTitle());
    				System.out.println(driver.getWebDriver().getCurrentUrl());    				
    				if(driver.getWebDriver().getTitle().contains(strBrowserTitle))
	    			{	    				
	    				System.out.println("Switch To Window success");
	    				blnElementExist = true;		    					
	    				break;
	    			}				
    				
    				driver.getWebDriver().switchTo().defaultContent();
    			}
    		}
		}catch(Exception e)
		{
			System.out.println("Windows is swithced to :" + blnElementExist);
		}   	
		
		return blnElementExist;
	}
	
	//_________________________________________________________________________________________________________________
	
	public boolean VerifyPDFFromHttpsUrl(URL url, String TextToVerify) throws InterruptedException, IOException{
	       String newUrlString = url.toString().replaceFirst("https", "http");
	       URL newUrl = new URL(newUrlString);
	       driver.get(newUrl.toString());
	       Thread.sleep(5000);
	       BufferedInputStream file = new BufferedInputStream(newUrl.openStream());
	       PDDocument doc = PDDocument.load(file);
	        String text = new PDFTextStripper().getText(doc);
	        if(text.contains(TextToVerify)){
	        	return true;	
	        }
	        return false;
	}
	
	//___________________________________________________________________________________________________________________
	
	 public void SelectDropDownValue(String Xpath,String Value)
		{
			WebElement RequestDropDown = driver.findElement(By.xpath(Xpath));
			Select select = new Select(RequestDropDown);
			select.selectByValue(Value);
		}
	 
	 public void JScriptExecutor(String ElementToClick)
		{
		       WebElement element=driver.findElement(By.xpath(ElementToClick));
		       JavascriptExecutor js = (JavascriptExecutor)driver.getWebDriver();
		       js.executeScript("arguments[0].click();", element);
		}
	 
	 public String getValueFromSelectList(By locator)
	 {
		 WebElement element=driver.findElement(locator);
		 Select select = new Select(element);
		 return select.getFirstSelectedOption().getText().trim();
	 }
	 
	 
	//___________________________________________________________________________________________________________________
		
	 public void waitForElementToBeClickable(By locator) throws Exception {
			WebDriverWait wait = new WebDriverWait(driver.getWebDriver(),60);
			ExpectedCondition<WebElement> condition = ExpectedConditions.elementToBeClickable(locator);
			WebElement element = wait.until(condition);
			element.click();
			waitForPageToBeReady() ;
	
		}
		
		public  void clearAndSendKeysJS(By locator, CharSequence keysToSend) throws Exception {
			try{
				 WebElement element=driver.findElement(locator);
				WebDriverWait wait = new WebDriverWait(driver.getWebDriver(),10);
				wait.until(ExpectedConditions.elementToBeClickable(element));
				
				JavascriptExecutor jse = (JavascriptExecutor) driver;
				jse.executeScript("arguments[0].scrollIntoView(true);", element);
				element.clear();
				element.sendKeys(keysToSend);
			}
			catch(Exception e)
			{
				System.out.println(e.getMessage());
			}
		}

		public  void selectByVisibleText(WebElement locator, String visibleText) throws Exception {
			
			try {
			
				Select select = new Select(locator);
				select.selectByVisibleText(visibleText);
				
			} catch (Exception e) {

				System.out.println(e.getMessage());

			}

		}
		
		public static void setText(WebElement element, String textToFeed) throws Exception {

		

			try {
				
             	element.clear();
					element.sendKeys(textToFeed);
					element.sendKeys(Keys.TAB);

			} catch (Exception e) {

				System.out.println(e.getMessage());
			}
		}
		
		public boolean verifyErrorList(List<String> expectedErrorList, By locator)
		{
			List<WebElement> actualErrorListElements = driver.findElements(locator);
			ArrayList<String> actualErrorList = new ArrayList<>();
			for(WebElement actualError:actualErrorListElements )
			{
				actualErrorList.add(actualError.getText().trim());
			}
			actualErrorDifference = new ArrayList<>(expectedErrorList);
			actualErrorDifference.removeAll(actualErrorList);
			return actualErrorDifference.isEmpty(); // User can use actualErrorDifference Variable to see the difference
		}
		
		public static void sleep(long millis) {
			try {
				Thread.sleep(millis);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		public void pageScrollToTop()
		{
			JavascriptExecutor js=(JavascriptExecutor)driver.getWebDriver();   
			js.executeScript("window.scrollTo(0, -document.body.scrollHeight);");
		}
		public void pageScrollToButtom()
		{
			JavascriptExecutor js=(JavascriptExecutor)driver.getWebDriver();   
			//js.executeScript("window.scrollTo(0, document.body.scrollHeight);");
			js.executeScript("window.scrollTo(0,Math.max(document.documentElement.scrollHeight,document.body.scrollHeight,document.documentElement.clientHeight));");
		}
		 
		public boolean verifyElementExistORNot(By by, int timeoutInSeconds)
		{
		    boolean blnExist=true;
			try
		    {
		    	WebDriverWait wait = new WebDriverWait(driver.getWebDriver(), timeoutInSeconds);
		    	wait.until(ExpectedConditions.presenceOfElementLocated(by)); //throws a timeout exception if element not present after waiting <timeoutInSeconds> seconds
		    	
		    }catch(Exception e)
		    {
		    	blnExist=false;
		    }
			
			return blnExist;
		}
		
}