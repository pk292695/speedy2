package uriPage;

import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;

public class FillClusterPage extends ReusableLibrary{

	public FillClusterPage(ScriptHelper scriptHelper) {
		super(scriptHelper);
	}
	/*
	 * 
	 * Log-in
	 * Relative Path
	 */
	public static String Username = "//*[@id='username']";
	public static String Password = "//*[@id='password']";
	public static String Login = "//button[@id='login']";
	/*
	 * Home Button from All pages
	 */
	public static String HomeButton = "//*[@id='home']";
	/*
	 * All Projects
	 * Relative Path
	 */
	public static String AllprojButton = "//*[@id='all-projects']";
	/*
	 * All Projects Table
	 */
	public static String AllProjTable = "//*[@id='ProjVersionLinks']/div[2]/table";
	public static String AllProjHead = "//*[@id='ProjVersionLinks']/div[2]/table/thead";
	public static String AllProjStatus = "//*[@id='tablerowdata']/td[6]";
	
	/*
	 * All PRojectlink
	 */
	public static String Proj= "//*[@id=\'cache5\']";
	
	/*
	 * All Project Table Data
	 */
	public static String tableRow = "//*[@id='tablerowdata']"; 
	public static String tablerowname = "//*[@id='tablerowdata']/td[2]";
	/*
	 * Deactivate Button
	 */
	public static String DeactivateButton = "//button[contains(text(),'Deactivate')]";
	/*
	 * Deactivate confrim Button
	*/
	public static String DeactivateYesButton = "//*[@id='deactiveConfirmBtn']";
	/*
	 * Reactivate Button
	 */
	public static String ReactivateButton = "//*[@id='ProjVersionLinks']/div[2]/div/div[2]/div[2]/button[1]";
	/*
	 * Reactivate confirm Button
	 */
	public static String ReactivateConfirmButton = "//*[@id='reactiveConfirmBtn']";
	/*
	 * projectsButton 
	 */
	public static String secondaryAllProjectButton = "//span[contains(text(),'Projects')]";
	/*
	 * Sync with OMP
	 */
	public static String SyncwithOMPLink = "//*[@class='glyphicon glyphicon-refresh']";
	
	/*
	 * Show link Brightstock IMPA : OMP Trial
	 */
	public static String BrightstockIMPA_Button="//button[contains(text(),'Open Brightstock IMPA')]";
		/*
		 * Create a new Project
		 */
		public static String StartNewProject = "//*[@id='Create-new-project']";
		public static String ProjectId = "//*[@id='field_name']";
		public static String Compound = "//*[@id='field_compound']";
		public static String CancelProj = "//span[text()='Cancel']";
		public static String StartProjectcnfrmbutton = "//*[contains(text(),'Start project')]";
	/*
	/*
	 *  Kit type
	 */
	public static String KitTypeDropDown = "//*[@id='field_kitType_select']";
	public static String ompNumberlabel = "//label[contains(text(),'OMP Number')]";
	public static String ompnumber = "//*[@id='ompNumber']";
	public static String PacktypeId = "//*[@id='packTypeId]";
	public static String LabelgroupId = "//*[@id='labelGroupId']";
	
	public static String selectKitType = "//*[@id='frm-KitType']/div[3]/div[1]/span";
	public static String Addnew = "//*[contains(text(),'Add new')]";
	public static String newKitTypeCancel = "//*[contains(text(),'Cancel')]";
	public static String kitTypeName = "//input[@id='field_compound']";
	public static String KitsaveButton = "//span[contains(text(),'Add Kit Type')]";
	public static String removeKitNoButton = "//*[@class='btn btn-default']";
	 public static String yesButton = "//*[@id='removeKit']";
	
	public static String removeKitType = "//a[@id='removeKitType']";
	
	/*
	 * Information per kit type link
	 */
	public static String informationKitType = "//span[contains(text(),'Information per Kit Type')]";
	
	/*
	 * Download e-IMPA link
	 */
	public static String DownloadIMPA = "//abbr[text()='Download e-IMPA']";
	/*
	 * Save changes button
	 */
	public static String SaveChanges = "//*[@id='ProjVersionLinks']/div[2]/div/div[2]/div[2]/button[3]";
	public static String ContinueSave="//*[@id='ShowCommentssaveChanges']";
	public static String ReasonforChange ="//*[@id=\"reasonForSaveChange\"]";
	public static String CancelSaveChanges = "//button[text()='Cancel']";
	
	/*
	 * Attachment in General kit info
	 */
	public static String AddAttachment = "//*[@id='addAttachment']";
	public static String inputFile = "//input[@type='file']";
	public static String DeleteAttachment = "//*[@id='deleteAttachment']";
	/*
	 * All project Information tab
	 */
	public static String SubmitAllProj= "//*[@id='AllProjectSubmitForApproval']";
	public static String SaveSubmitAllProj= "//button[@id='submit&saveForAllProject']";
	public static String AllprojapproveButton= "//button[contains(text(),'Approve')]";
	
	/*
	 * Version number 
	 */
	
	public static String VerNumber = "//div[@class='version']";
	public static String CreateNewVersion = "//button[contains(text(),'Create New Version')]";
	public static String CheckGenproj = "//input[@id='generalProjectInfoDataClusterChkBox']";
	public static String Cancel = "//div[@class='modal-content']/form/div[@class='modal-footer']/button[@class='btn btn-default']";
	public static String newversion = "//button[@id='createNewVer1']";
	public static String ImpactText = "//label[@class='checkbox-inline']";
	

/*
 * FillCluster
 * General Project Information tab
 * Relative Path
 */
	public static String GenProjInfo = "//span[contains(text(),'General Project Information')]";
	public static String FieldImpa = "//input[@id='field_impa_pedDraftProtocoldraftAmendment']";
	public static String FieldDate = "//input[@id='field_dateOfFinalProtocol']";
	public static String FieldPhase = "//select[@id='field_phase']";
	public static String SponserName = "//select[@id='sponsorNameDrop']";
	public static String SponserAddr = "//select[@id='sponsorNameAddr']";
	public static String Therapeutic = "//input[@id='field_therapeuticArea']";
	public static String GcdoCro = "//input[@id='field_gcoOrCroRun_op2']";
	public static String GcdoCro2 = "//*[@id='field_gcoOrCroRun_op3']";
	public static String PatientAlert = "//input[@id='field_patientAlertCard_yes']";
	public static String Comments_generalProject = "//textarea[@class='form-control ng-pristine ng-valid ng-empty ng-valid-maxlength ng-touched']";
	public static String otherGcdoCro="//input[@id='field_gcoOrCroRun_op4']";
	/*
	 * General project save and submit
	 */
	public static String Submit = "//button[@id='GeneralTrialSubmitForApproval']";
	public static String SaveSubmit = "//button[@id='submit&saveForGeneralTriale']";
	
	public static String CSITSM = "//*[@id='generalProjCSITSMid']";
	public static String OpenCSITSM = "//label[text()='Open']";
	public static String RejectedCSITSM = "//label[text()='Rejected']";
	
	/*
	 * project approval
	 */
	public static String ProjectSearch="//*[@id='ProjVersionLinks']/div/div/div[1]/div/input";
	public static String WINameSearchbutton="//*[@id='ShowDataCluster']";
	
	public static String GenProjRejectButton = "(//button[@class='col-sm-5 btn btn-default'])[2]";
	public static String GenProjTextarea = "//textarea[@class='ng-pristine ng-untouched ng-empty ng-invalid ng-invalid-required']";
	public static String GenProjApproveButton = "//*[@id='CSITSMapprove']";
	public static String CheckApproveStatus = "//a[text()='Check approval status']";
	public static String AllProjRow = "//ul[@id='approveKitType']/li[1]";
	public static String GenProjRow = "//ul[@id='approveKitType']/li[2]";
	public static String StatusInsideCheckApproval = "//tbody/tr/td[2]/span";
	
	public static String CountryProjTextarea = "//textarea[@class='col-xs-12 ng-pristine ng-untouched ng-empty ng-invalid ng-invalid-required']";
	public static String CntryProjApproveButton = "//*[@id='countryApprove']";
	public static String CountryProjRow = "//ul[@id='approveKitType']/li[3]";
	
	public static String DrugPrimaryTextarea = "//textarea[@class='col-xs-12 ng-pristine ng-untouched ng-valid ng-empty']";
	public static String DrugPrimApproveButton = "//*[@id='cmc/PrimaryApprove']";
	public static String KitRow = "//ul[@id='approveKitType']/li[4]";
	public static String DrugPrimInsideCheckApproval = "//tbody/tr/td[3]/span";
	
	public static String GenKitPackTextarea = "(//textarea[@class='col-xs-12 ng-pristine ng-untouched ng-empty ng-invalid ng-invalid-required'])[2]";
	public static String GenKitPackApproveButton = "//*[@id='home']/div[2]/form/div/div/div/div[3]/div[2]/div/button[1]/text()";
	public static String GenKitPackInsideCheckApproval = "//tbody/tr/td[2]/span";
	
	public static String CheckApproveToProject = "//span[@class='ng-binding']";
	
	/*
	   * FillCluster
	   * Country Information
	   * Relative Path
	   */	
	public static String Country = "//span[contains(text(),'Country Information')]";
	public static String AddParticipatingCountry = "//i[@id='addParticipatingCountry']";
	public static String participatingCountry ="//span[@class='btn btn-default form-control ui-select-toggle'][1]";
	public static String selectCountry="//*[@id='ui-select-choices-row-0-0']/span";
	public static String countrySpecificInformation = "//*[@id='field_countrySpecificInfo1']";
	public static String localContact="//*[@id='field_localContact']";
	public static String labelApprover="//*[@id='field_labelApprover']";
	public static String addParticipatingCountryButton = "//*[@id='okAddParticipatingCountry1']";
	public static String dropdownAddParticipatingcountry = "//div[@class=\"col-xs-6\"]/div/div/span";
	public static String EditCountryLink = "//a[text()='Edit']";
	public static String deleteButton="//button[contains(text(),'Delete Country')]";
	
	public static String ShowCntry = "//span[contains(text(),'Show Countries')]";
	public static String Label1 = "//*[@id='labelGroupList']/li[1]";
	public static String Label2 = "//*[@id='labelGroupList']/li[2]";
	public static String country_comments = "//*[@id='field_remarks']";
	
	public static String countryingo_supplying_depot = "//label[contains(text(),'Supplying depot')]";
	public static String countryinfo_DepotCountry = "//label[contains(text(),'Depot country')]";
	public static String Country_cancelButton = "//button[contains(text(),'                                                                        Cancel')]";
	
	/* 
	 * Submit button for Country information
	 * */
	public static String CntrySubmit = "//*[@id='CountryDepotSubmitApproval']";
	public static String CntrySaveSubmit = "//*[@id='countryDepotSubmit&SaveChanges']";
	
	public static String LabelCountry = "//*[@id='countryDepotLabelID']";
	public static String OpenCountry = "//*[@id='countryDepotOpenID']";
	
	
	/*
	 * theja mel
	 */
	public static String editLabelGroupsButton="//span[text()='Edit label groups']";
	public static String Name="//*[@id='newLbl']";
	public static String countryNameCheckbox="//*[@id='country0']";
	public static String saveLabelGroupButton="//*[@id='saveLabelId']";
	public static String PackTypecheckbox1="//*[@id='packType0']";
	public static String Labelgroupcheckbox1="//*[@id='LblGroup0']";
	public static String melcombineButton="//*[@id='melCombinebtn']";
	public static String template="//input[@placeholder='Choose as template']";
	
	public static String templateOption="//*[text()='MELTEST']";
	public static String DoneButton="(//button[@class='btn btn-primary ng-scope'])[3]";
	public static String templateElements="//*[@class='melTmplSelEle']";
	
	public static String SearchButton="//i[@class='glyphicon glyphicon-search']";
	public static String searchField="//*[@id='category1']";
	public static String searchField9 = "//*[@id='category9']";
	public static String addToMEL="//*[@id='addMELiD']";
	public static String NGCStaticReference="//*[@id='ngcStat']";
	public static String ngcField="//*[@id='ngcStatic3']";
	public static String MELEditbutton = "//a[@id='melEditBtn1'] ";
	public static String removeMELElement = "//span[@id='eleRemove0']";
	public static String DeleteMEL = "//button[contains(text(),'Delete MEL')] ";
	public static String MELTemplateCancel = "//span[contains(text(),'Cancel')]";
	
	
	public static String AddnewLabelGroup ="//*[contains(text(),\"Add new label group\")]";
	public static String LabelName ="//*[@id=\"newLbl\"]";
	public static String SelectAllCountries = "//a[text()='Select All Countries']";
	public static String SaveLabel ="//*[@id=\"saveLabelId\"]";
	
	public static String MELSubmitButton = "//*[@id='submitMelCompilerID']";
	public static String MELSaveSubmit = "//*[@id='submit&saveMEL']";
	public static String LabelMEL = "//*[@id='MelLabelGroupID']";
	public static String OpenMELcompiler = "//*[@id='MelOpenLabelGroup']";
	
	/*
	 * MEL approval
	 */
	public static String MELApproveButton = "(//button[contains(text(),'Approve')])[3]";
	public static String MELTextarea = "//textarea[@class='col-xs-12 ng-pristine ng-empty ng-invalid ng-invalid-required ng-touched']";
	public static String MELRejectButton = "//*[@id='rejectQAid']";

	/*
	 * Create MEL Template/ MEl Manager
	 */
	public static String MELprojectManagement="//*[@class='navPrgMgmt']";
	public static String MELTemplateManagement="//a[@class='navMelMgmt']";
	public static String MELlongDescriptionField="//*[@id='melTemplate']";
	public static String MELlongDescriptionText="//input[@id='field_description']";
	public static String MELShortDescriptionText="//input[@id='field_name']";
	public static String MELAddTemplateButton="//span[contains(text(),'Add MEL template')]";
	
	public static String MELaddContextVariable="//button[contains(text(),'Add Context variable')]";
	public static String MELsearchButton="(//i[@class='glyphicon glyphicon-search'])[1]";
	public static String MELselectContextVariable="//*[@id='category4']";
	public static String MELaddToTemplateButton="(//button[contains(text(),'Add to MEL template')])[1]";
	
	public static String MELaddNGCTextReference="//button[contains(text(),'Add NGC text reference')]";
	public static String MELNGCsearchButton="(//i[@class='glyphicon glyphicon-search'])[2]";
	public static String MELNGCstatic="//*[@id='ngcStatic2']";
	public static String MELNGCaddToTem="(//button[contains(text(),'Add to MEL template')])[2]";
	
	public static String MELaddSpeedyVariable="//button[contains(text(),'Add Speedy variable')]";
	public static String MELspeedyVariable="//*[@id='spdyVariable2']";
	public static String MELaddToMELSpeedyVariable="//*[@id='addToMelTemSpdyVar']";
	
	public static String MELsaveChangesButton="//button[contains(text(),'Save Changes')]";
	
	
	/*
	 * FillCluster
	 * General Kit Type Info
	 * Relative Path
	 */	
	  public static String GeneralKitTypeInfo= "//*[@id='frm-KitType']/ul/li[6]/a";
	  public static String NarcoticRadioButton =  "//input[@id='field_narcotic_false']";

	  public static String BlindingType = "//*[@id='home']/div[1]/form/div/div[1]/div[2]/div/div/div[1]/input";
	  public static String childResistant = "//input[@id='field_childResistant_true']";
	  public static String AccessoriesOutsideKitConstrucation = "//input[@id='field_accessoriesOutsideKitConstruction_null']";
	  public static String SapCodes = "//textarea[@id='sapcodeoption']";
	  public static String inClinic= "//input[@id='field_inClinicOrHospitalAdministration_true']";
	  public static String packagingSitesPrimary =  "//*[@id='home']/div[1]/form/div/div[1]/div[5]/div/div/div[1]/input";
	  public static String packagingSitesSecondary = "//*[@id='home']/div[1]/form/div/div[1]/div[6]/div/div/div[1]/input";
	  public static String general_Kitcomments = "//*[@id='field_remarks_kittypeinfo']";
	 
	  public static String BlindingCloseError = "//div[@id='errorMessageModal']//button[@type='button'][contains(text(),'Close')]";
	  public static String BlindingErrorMsg = "//*[@id='errorMessage']";
	  
	  
	/*
	   * FillCluster
	   * Pack Type Infoo
	   * Relative Path
	   */	
	  public static String PacktypeTab = "//*[@id='frm-KitType']/ul/li[7]/a";
		public static String Packoption1 = "//input[@id='boxID']";
		public static String Packoption2 = " //*[@id='dosePackID'] ";
		public static String Packoption3 = "//*[@id='pouch']";
		public static String Packoption4 = "//*[@id='PolypropylenePot']";
		public static String Packoption5 = "//*[@id='SmartDosepak']";
		public static String ProdName = "//select[@id='packTypeInfo.productNameAndStrength.ngcTxtId']";
		public static String Dosage = "//select[@id='dosageForm']";
	  public static  String ROA = "//select[@id='rout1']";
	  public  static  String ROAPict = "//select[@id='rout2']";
	  public  static  String DoseInstruction = "//select[@id='doseinstru']";
	  public  static  String Tempstore = "//select[@id='temprid']";
	  public static   String OtherStore = "//select[@id='otherid']";
	  public  static  String HandIns = "//select[@id='handlingInst']";
	  public  static  String PeriodDes = "//select[@id='periodid']";
	  public  static  String Othervar = "//select[@id='othervarid']";
	  public  static  String TearOff ="//input[@name='optradio' and @value='No']";
	  public  static  String packtypeComments = "//textarea[@id='txatid2']";
	  public static String SubmitForApproval = "//*[@id='genKitPackTypeSubmitForApproval2']";
	  
	  public static String ProdNameVar = "//input[@class='form-control ng-pristine ng-untouched ng-empty ng-invalid ng-invalid-required']";
	  public static String DosageVar = "(//input[@class='form-control ng-pristine ng-untouched ng-empty ng-invalid ng-invalid-required'])[2]";
	 public static String ProdNameText = "//div[@class='col-xs-7 text1 ng-binding']";
	 public static String DosageText = "(//div[@class='col-xs-7 text2 ng-binding'])[1]";
	  /*
	   * Choose existing pack type
	   */
	  public static String CopyExistingPack ="//*[contains(text(),\"Copy info from existing pack type\")]";
	  public static String btnBox ="//*[@id=\"menu1\"]/div[1]/div[1]/div[2]/div/div[1]/button[5]";
	  public static String dropdownkit ="//*[@id='field_kitType_select_copy']";
	  public static String chkbxpackkit ="//*[@name=\"packTypeToCopy\"]";
		public static String Close ="//span[contains(text(),\"Cancel\")]";
		public static String Copy ="//span[contains(text(),\"Copy\")]";
	  
	  
	/*Submit for approval Pack type(General kit)*/
	  public  static  String GenKitPackSubmit = "//*[@id='genKitPackTypeSubmitForApproval2']";
	  public  static  String GenKitPackSaveSubmit = "//*[@id='submitGenKitType2']";
	  
	  public static String BatchdocGenKit = "(//*[text()='Batch Doc'])[1]";
	  public static String OpenGenkit = "(//*[text()='Open'])[1]";
	
	  public static String DrugProductInfoComments = "//*[@id='field_remarks_cmcproduct']";
	  public static String primaryPackTypeComments = " //*[@id='field_remarks_packtypeinfo']";
  /*
   * FillCluster
   * Drug Product Info
   * Relative Path
   */
  	public static String DrugProductInfo="//*[@id='frm-KitType']/ul/li[8]/a";
	public static String AddSapMaterial="//span[text()='Add SAP Material']";
	public static String SAPCompound = "//*[@id='field_compound']";
	public static String SAPmaterialnNm="//*[@id='field_materialNumber']";
	public static String SAPMaterialDes = "//*[@id='field_materialNumberDescription']";
	public static String SAPSearch="(//*[@class='btn btn-default'])[1]";
	public static String Sap19001342="//*[@id='sapMaterialSearchResult']/tbody/tr/td[1]/input";
	public static String AddSapMaterialButton="(//button[@class='btn btn-primary'])[1]";
	public static String DrugProdCountry="(//*[@id='field_drugProductCountryOfOrigin'])[1]";
	public static String DrugsubstanceCompany="(//input[@id='field_drugSubstanceCompany'])[1]";
	public static String DrugSubtanceCountry="(//*[@id='field_drugSubstanceCountryOfOrigin'])[1]";
	public static String Coldchain="(//*[@id='field_coldChainProduct_false'])[1]";
	public static String MoistureSensitive="(//*[@id='field_moistureSensitiveProduct_false'])[1]";
	public static String MaxMoisture="//*[@id='field_maxMoistureOxygenExposureTimePL'][1]";
	public static String OxygenSensitive="(//*[@id='field_oxygenSensitiveProduct_false'])[1]";
	public static String LightSensitive="(//input[@id='field_light_sensitive_false'])[1]";
	public static String Frozenproduct="//*[@id='field_frozen_product_na']";
  
	
	/*
	   * FillCluster
	   * Primary Pack Info
	   * Relative Path
	   */	
	public static String PrimaryPack="//*[@id='frm-KitType']/ul/li[9]/a";
	public static String primarypacktrue = "//*[@id='field_primary_Packaging_true1']";
	public static String PackInfoExecuted="//input[@id='field_primary_Packaging_false1']";
	public static String bottle = "//*[@id='menu3']/div[1]/form/div/div/div[1]/div[2]/div/div/div[1]/span";
	public static String cap = "//*[@id='menu3']/div[1]/form/div/div/div[1]/div[3]/div/div/div[1]/span";
	public static String Desiccant = "//*[@id='menu3']/div[1]/form/div/div/div[1]/div[4]/div/div/div[1]/span";
	public static String oxygen = "//*[@id='menu3']/div[1]/form/div/div/div[1]/div[5]/div/div/div[1]/span";
	public static String Lidding = "//*[@id='menu3']/div[1]/form/div/div/div[1]/div[6]/div/div/div[1]/span";
	public static String Forming = "//*[@id='menu3']/div[1]/form/div/div/div[1]/div[7]/div/div/div[1]/span";

/*
 * Submit for approval General kit and pack type
 */
	public static String SubmitDrugPrim = "//*[@id='PrimaryPackSubmitForApproval']";
	public static String SavesubmitDrugPrim = "//*[@id='submit&savePrimaryPackChanges']";
	
	public static String BatchdocDrug ="//*[@id='batchDClabel']";
	public static String OpenDrugProd = "//*[@id='batchDcOpen']";
	/*
	 * WorkFlow inbox Search
	 */
	public static String WIProjectSearch="//*[@id='ProjVersionLinks']/div/div/div[1]/div/input";
	public static String WISearchbutton="//*[@id='ShowDataCluster']";
	/*
	 * WorkFlow Inbox project inside
	 */
	public static String WITitle="//*[@id='mailModalTitle']";
	public static String WIContent="//div[@class='mailPopCon']//div[@class='form-group']//label";
	public static String WICompound="//label[contains(text(),'Compound')]";
	public static String WIContentCompound="//div[@class='mailPopCon']//div[2]//dd//span";
	public static String WIDataCluster="//label[contains(text(),'Data Cluster')]";
	public static String WIContentDataCluster="//div[@class='mailPopCon']//div[3]//dd//span";
	public static String WIVersionDes="//label[contains(text(),'Version description')]";
	public static String WIContentVersionDes="//div[@class='mailPopCon']//div[4]//dd//span";
	public static String WIsubmittedBy="//label[contains(text(),'Submitted by')]";
	public static String WIContentsubmittedBy="//div[@class='mailPopCon']//div[5]//dd//span";
	public static String WIDateSubmisison="//label[contains(text(),'Date of submission')]";
	public static String WIContentDateSubmisison="//div[@class='mailPopCon']//div[6]//dd//span";
	public static String WICancel="//*[@id='cancelID']";
	public static String WIShowdataCluster_Button="//*[@id='ShowDataClusterID']";
	/*
	 * MEL Compiler
	 */
	public static String MELcompilertab = "//*[@id='frm-KitType']/ul/li[10]/a";
	public static String Editlabel = "//*[@id='showConutry']";
	public static String MelComments = "//*[@id='field_remarks_melcompiler']";
	public static String MELpackType = "//*[@id='packType0']";
	
	public static String MELThanksButton = "(//button[contains(text(),'Thanks')])[3]";
	
/*public String PackTypeInfoButton="(//a[text()='Pack Type Info'])[2]";*/
	
  /*
   * FilterType
   * All Project
   * Relative Path
   */
	public static String filterButton = "//*[@id='showFilterProject']";
	public static String filterBrightStock = "//*[@id='BrightstockTrial']";
	public static String filterOmp = "//*[@id='OmpTrial']";
	public static String filterNonOmp = "//*[@id='NonOmpTrial']";
	public static String filterDraft = "//*[@id='Draft']";
	public static String applyFilterButton = "//*[@id='applyFiltersID']";
	public static String filterAwaitingApproval = "//*[@id='AwaitingApproval']";
	public static String filterPartiallyApproved = "//*[@id='PartiallyApproved']";
	public static String filterFullyApproved = "//*[@id='FullyApproved']";
	public static String filterActive = "//*[@id='activate']";
	public static String filterProjectname = "//*[@name = 'projectName']";
	public static String filterInActive = "//*[@id='inactivate']";
	
	/*
	 * Filters
	 * Workflowinbox
	 * Relative Path
	 */
	public static String WIFilterButton = "//*[@id='showFilter']";
	public static String WIFilterGeneralPrjInfo = "//*[@ng-model='isGeneralProjectInfo']";
	public static String WIFilterCountDepotinfo = "//input[@ng-model='isCountryDepotInfo']";
	public static String WIFilterGeneralKitPackInfo = "//input[@ng-model='isGeneralKitTypeandPackTypeInfo']";
	public static String WiFilterDrugProductPrimaryPackInfo = "//input[@ng-model='isCMCProductandPrimaryPackInfo']";
	public static String WIFilterAllProjectInfo = "//input[@ng-model='isAllProjectInfo']";
	public static String WIFilterMELcompilation = "//input[@ng-model='isMELCompiler']";
	public static String WIFilterBrightStockProject = "//input[@ng-model='isBrightStock']";
	public static String WIFilteropen = "//input[@ng-model='isOpen']";
	public static String WIFilterClosed = "//input[@ng-model='isClosed']";
	public static String WIFiltersubmmittedText = "//input[@ng-model='vm.searchActionItem.submittedBy']";
	public static String WIFilterCompoundText = "//input[@id='compound']";
	public static String WIFilterApplyFilter = "//*[@id='applyFiltersID']";
	public static String WIHideFilter = "//*[@id='showFilter']";
	
	/*
	 * WI table header
	 */
	public static String WIProjectHead = "//*[@id='workFloInbox']/thead/tr/th[1]";	
	public static String WICompoundHead = "//*[@id='workFloInbox']/thead/tr/th[2]";
	public static String WIDataClusterHead = "//*[@id='workFloInbox']/thead/tr/th[3]";
	public static String WISubmittedByHead = "//*[@id='workFloInbox']/thead/tr/th[4]";
	public static String WIDOSHead = "//*[@id='workFloInbox']/thead/tr/th[5]";
	public static String WIStatusHead = "//*[@id='workFloInbox']/thead/tr/th[6]";
	
	/*
	 * WorkFlow Inbox
	 * Table
	 * Relative Path
	 */
	public static String WITabledataUnavailable = "//*[contains(text(),'No data available')]";
	public static String WITabledataCluster = "//*[@id='tablerowdata1']/td[3]";
	public static String ShowDataCluster="//button[@id='ShowDataClusterID']"; 
	public static String WITableStatus = "//*[@id='tablerowdata1']/td[6]";
	public static String WITableCompound = "//*[@id='tablerowdata1']/td[2]";
	public static String WiTbaleSubmittedBy = "//*[@id='tablerowdata1']/td[4]";
	public static String WIdrugProductinfoApproveButton = "//*[@id='cmc/PrimaryApprove']";
	public static String WIGeneralProduct = "//*[@id='home']/div[2]/form/div/div/div/div[3]/div[2]/div/button[1]  ";
	public static String MelCompileQAApprove = "//*[@id='menu4']/form/div/div/div/div[4]/div[2]/div/button[1]";
	
	public static String ProjCol = "//th[@aria-label='Project: activate to sort column ascending']";
	public static String CompCol = "//th[@aria-label='Compound: activate to sort column ascending']"; 
	public static String DataCol = "//th[@aria-label='Data Cluster: activate to sort column ascending']";
	public static String submittedbyCol = "//th[@aria-label='Submitted By: activate to sort column ascending']";
	public static String SubmissionCol = "//th[@aria-label='Date of Submission: activate to sort column ascending']";
	public static String StatusCol = "//th[@aria-label='Status: activate to sort column ascending']";
	/*
	 * BulkSubmission 
	 */
	public static String BulkSubButton = "//button[text()='Bulk Submission']";
	public static String GenProjCheck = "//input[@id='generalProjectInfoDataClusterChkBox1']";
	public static String CancelBulk = "//*[@id='bulkModalBig']/div/div/form/div[2]/button[1]";
	public static String GenKitCheck = "//input[@id='generalKitTypePackTypeDataClusterChkBox1']";
	public static String DrugProdCheck = "//input[@id='cmcProductPrimaryPackInfoDataClusterChkBox1']";
	public static String MELCompCheck = "//input[@id='melCompilerDataClusterChkBox1']";
	public static String SubmitBulk = "//button[@id='bulkSubmissionModal']";
	public static String CntryInfoCheck = "//input[@id='countryDepotInfoDataClusterChkBox1ScenarioOthers']";
	
	/*
	 * General project Information (Brightstock Project)
	 * Fill cluster
	 */
	
	public static String BSGenProjInfo = "//span[contains(text(),'General Project Information')]";
	public static String BSSponserName = "//select[@id='sponsorName']";
	public static String BSSponserAddr = "//select[@id='form-select-md51']";
	public static String KitNumber = "//input[@id='field_totalKitNumbersRequested']";
	public static String Format = "//input[@type='text' and @id='field_format']";
	public static String Prefix = "//textarea[@id='field_preFix']";
	
	/*
	 * Brightstock Trial Submit button
	 */
	public static String BrightstkSubmit = "//button[@id='BrightStockProjectApproval']";
	
	/*
	 * PAgination All Project
	 */
	public static String LastArrow ="//*[@class=\"pagination-last ng-scope\"]/a";
	/*
	 * Log out
	 */
	public static String userIcon = "//*[@id='account-menu']";
	public static String LogOutButton = "//*[@id='logout']";
	public static String Signin = "//span[contains(text(),'Sign in')]";
  
}
