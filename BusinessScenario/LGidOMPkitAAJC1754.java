package BusinessScenario;

import java.awt.AWTException;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;
import uri.FillCluster;
import uriPage.FillClusterPage;
import AutomationCoreFramework.Status;
import CommonMethod.CommonMethod;

public class LGidOMPkitAAJC1754 extends ReusableLibrary{

	CommonMethod commonMethod = new CommonMethod(scriptHelper);
	FillCluster FC = new FillCluster(scriptHelper);
	public LGidOMPkitAAJC1754(ScriptHelper scriptHelper)
	{
		super(scriptHelper);
	}
	String username = "Username";
	String sheetname = "functional_flow";
	String password = "Password";
	public void LGidOMPKit() throws IOException, UnsupportedFlavorException, AWTException
	{
		commonMethod.navigateToUrl(dataTable.getData("functional_flow", "Application_URL"), driver);
		FC.LogIn(sheetname, username, password);
		commonMethod.clickOnElement(FillClusterPage.AllprojButton, "All project button", driver);
		FC.FilterBrightDraft();
		commonMethod.clickOnElement(FillClusterPage.tableRow, "Resulted project", driver);
		commonMethod.clickOnElement(FillClusterPage.SyncwithOMPLink, "Sync with OMP link", driver);
		commonMethod.clickOnElement(FillClusterPage.informationKitType, "Information per kit type section", driver);
		
		commonMethod.selectOptionFromDropDown(FillClusterPage.KitTypeDropDown, "Canagliflozin 100 mg_1x10", driver);
		commonMethod.verifyElementPresent(FillClusterPage.ompNumberlabel, "OMP number Text", driver);
		WebElement ompnumber = driver.findElement(By.xpath(FillClusterPage.ompnumber));
		
		
			if(ompnumber.isDisplayed()){
				System.out.println("pass");
				report.updateTestLog("OMP number visible", "Check omp number is visible", "OMP number should be visible", "OMP number is visible", Status.PASS);
			}
			else
			{System.out.println("fail");
			report.updateTestLog("OMP number visible", "Check omp number is visible", "OMP number should be visible", "OMP number is not visible", Status.FAIL);
			}
			
			WebElement packtypeId = driver.findElement(By.xpath(FillClusterPage.PacktypeId));
			
			
			if(packtypeId.isDisplayed()){
				System.out.println("pass");
				report.updateTestLog("Pack type Id visible", "Check Pack type id is visible", "Pack type id should be visible", "Packtype id is visible", Status.PASS);
			}
			else
			{System.out.println("fail");
			report.updateTestLog("Pack type Id visible", "Check Pack type id is visible", "Pack type id should be visible", "Packtype id is not visible", Status.FAIL);
			
			}
			
			WebElement lblgrpId = driver.findElement(By.xpath(FillClusterPage.LabelgroupId));
			
			
			if(lblgrpId.isDisplayed()){
				System.out.println("pass");
				report.updateTestLog("Labelgroup id visible", "Check Labelgroup id is visible", "Labelgroup id should be visible", "Labelgroup id is not visible", Status.PASS);
			}
			else
			{System.out.println("fail");
			report.updateTestLog("Labelgroup id visible", "Check Labelgroup id is visible", "Labelgroup id should be visible", "Labelgroup id is not visible", Status.FAIL);
			}
	
	
	}
}
