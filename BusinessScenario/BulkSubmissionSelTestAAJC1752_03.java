package BusinessScenario;

import java.awt.AWTException;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;

import uri.FillCluster;
import uriPage.FillClusterPage;
import AutomationCoreFramework.Status;
import BusinessScenarioPage.AddRemoveKitTypePage;
import CommonMethod.CommonMethod;

public class BulkSubmissionSelTestAAJC1752_03 extends ReusableLibrary{
	

	AddRemoveKitTypePage AddRemoveKiteTypPage = new AddRemoveKitTypePage(scriptHelper);

	CommonMethod commonMethod = new CommonMethod(scriptHelper);
	FillCluster FC = new FillCluster(scriptHelper);
	
	public BulkSubmissionSelTestAAJC1752_03(ScriptHelper scriptHelper)
	{
		super(scriptHelper);
	}
	String username = "Username";
	String username3 = "Username3";
	String username2 = "Username2";
	String sheetname = "functional_flow";
	String password = "Password";
	String KitTypeName = "JKP";
	
	
	public void BulkSubmit3() throws IOException, UnsupportedFlavorException, AWTException
	{
		commonMethod.navigateToUrl(dataTable.getData("functional_flow", "Application_URL"), driver);
		FC.LogIn(sheetname, username2, password);
		commonMethod.clickOnElement(FillClusterPage.AllprojButton, "All project page", driver);
		FC.FilterNonOMPDraft();
		commonMethod.clickOnElement(FillClusterPage.tableRow, "Resulted project", driver);
		WebElement BulkSubmit = driver.findElement(By.xpath(FillClusterPage.BulkSubButton));
		if(BulkSubmit.isDisplayed()== true)
		{
			report.updateTestLog("Verify Element not present", "Bulk submit button must not display", "Bulk submit button should not be displayed", "Bulk submit button is displayed", Status.FAIL);
		}
		else
		{
			report.updateTestLog("Verify Element not present", "Bulk submit button must not display", "Bulk submit button should not be displayed", "Bulk submit button is not displayed", Status.PASS);
		}
		
		
		//commonMethod.navigateToUrl(dataTable.getData("functional_flow", "Application_URL"), driver);
		FC.Logout();
		driver.navigate().refresh();
		FC.LogIn(sheetname, username3, password);
		commonMethod.clickOnElement(FillClusterPage.AllprojButton, "All project page", driver);
		FC.FilterNonOMPDraft();
		commonMethod.clickOnElement(FillClusterPage.tableRow, "Resulted project", driver);
		WebElement BulkSubmit_QA = driver.findElement(By.xpath(FillClusterPage.BulkSubButton));
		if(BulkSubmit_QA.isDisplayed()== true)
		{
			report.updateTestLog("Verify Element not present", "Bulk submit button must not display","Bulk submit button should not be displayed","Bulk submit button is displayed", Status.FAIL);
		}
		else
		{
			report.updateTestLog("Verify Element not present", "Bulk submit button must not display","Bulk submit button should not be displayed","Bulk submit button is not displayed", Status.PASS);
		}
			
		
	}
			
	}	




