package BusinessScenario;

import java.awt.AWTException;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;

import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;
import uri.FillCluster;
import uriPage.FillClusterPage;
import CommonMethod.CommonMethod;

public class AddRemarkFieldsAAJC1708_03 extends ReusableLibrary{

	CommonMethod commonMethod = new CommonMethod(scriptHelper);
	FillCluster FC = new FillCluster(scriptHelper);
	
	
	
	public AddRemarkFieldsAAJC1708_03(ScriptHelper scriptHelper)
	{
		super(scriptHelper);
	}
	String username = "Username";
	String sheetname = "functional_flow";
	String password = "Password";
	
	public void AddRemark_03() throws IOException, UnsupportedFlavorException, AWTException
	{
		commonMethod.navigateToUrl(dataTable.getData("functional_flow", "Application_URL"), driver);
		FC.LogIn(sheetname,username, password);
		FC.StartNewProject();
		FC.GeneralProjectInformation();
		commonMethod.enterText(FillClusterPage.Comments_generalProject, "Comments General project Info", dataTable.getData("Test_Data", "Comments"), driver);
		FC.SubmitGeneralProjectInformation();
		
		//General project Information Approval
				commonMethod.clickOnElement(FillClusterPage.HomeButton, "Continue save changes button", driver);
				commonMethod.enterText(FillClusterPage.ProjectSearch, "Workflow inbox Project search", FillCluster.name, driver);
				commonMethod.clickOnElement(FillClusterPage.WIProjectSearch, "Workflow inbox search", driver);
				commonMethod.clickOnElement(FillClusterPage.tableRow, "Resulted project", driver);
				commonMethod.clickOnElement(FillClusterPage.ShowDataCluster, "Show data cluster button", driver);
				commonMethod.clickOnElement(FillClusterPage.GenProjApproveButton, "Approve button", driver);
				FC.LogIn(sheetname, username, password);
				
		commonMethod.clickOnElement(FillClusterPage.DownloadIMPA, "Download e-IMPA button", driver);
	
	
	
	
	
	
	}
}
