package BusinessScenario;

import java.awt.AWTException;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;

import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;
import uri.FillCluster;
import uriPage.FillClusterPage;
import CommonMethod.CommonMethod;

public class SubmitApprovalMELCompiler_AAJC20 extends ReusableLibrary{
	CommonMethod commonMethod = new CommonMethod(scriptHelper);
	FillCluster FC = new FillCluster(scriptHelper);
	
	public SubmitApprovalMELCompiler_AAJC20(ScriptHelper scriptHelper)
	{
		super(scriptHelper);
	}
	String username = "Username";
	String username1 = "Username1";
	String sheetname = "functional_flow";
	String password = "Password";
	public void SubmitMELCompiler() throws IOException, UnsupportedFlavorException, AWTException, InterruptedException
	{
		commonMethod.navigateToUrl(dataTable.getData("functional_flow", "Application_URL"), driver);
		FC.LogIn(sheetname, username, password);
		FC.StartNewProject();
		FC.GeneralProjectInformation();
		FC.SubmitGeneralProjectInformation();
		FC.CountryInformation();
		FC.SubmitCountryInformation();
		commonMethod.clickOnElement(FillClusterPage.informationKitType, "Information per kit type section", driver);
		FC.createKitType(dataTable.getData("Test_Data", "Kitname"));
		FC.GeneralKitTypeInfo();
		FC.PackTypeInfo();
		FC.SubmitGeneralKitPackType();
		commonMethod.clickOnElement(FillClusterPage.informationKitType, "Information per kit type section", driver);
		FC.SelectKitType(dataTable.getData("Test_Data", "Kitname"));
		FC.DrugProductInfo();
		FC.PrimaryPackInfo();
		FC.SubmitDrugPrimary();
		
		//General project approveal
		commonMethod.clickOnElement(FillClusterPage.HomeButton, "Continue save changes button", driver);
		commonMethod.enterText(FillClusterPage.ProjectSearch, "Workflow inbox Project search", FillCluster.name, driver);
		commonMethod.clickOnElement(FillClusterPage.WIProjectSearch, "Workflow inbox search", driver);
		commonMethod.clickOnElement(FillClusterPage.tableRow, "Resulted project", driver);
		commonMethod.clickOnElement(FillClusterPage.ShowDataCluster, "Show data cluster button", driver);
		commonMethod.clickOnElement(FillClusterPage.GenProjApproveButton, "Approve button", driver);
		FC.LogIn(sheetname, username, password);
		driver.navigate().refresh();
		FC.Logout();
		
		//Country Information Approval
				driver.navigate().refresh();
				FC.LogIn(sheetname,"Username1", password);
				commonMethod.enterText(FillClusterPage.ProjectSearch, "Workflow inbox Project search", FillCluster.name, driver);
				commonMethod.clickOnElement(FillClusterPage.WIProjectSearch, "Workflow inbox search", driver);
				commonMethod.clickOnElement(FillClusterPage.tableRow, "Resulted project", driver);
				commonMethod.clickOnElement(FillClusterPage.ShowDataCluster, "Show data cluster button", driver);
				commonMethod.clickOnElement(FillClusterPage.CntryProjApproveButton, "Approve button", driver);
				FC.LogIn(sheetname, username1, password);
				
				FC.Logout();
				
				//General kit and Pack type Approval
				driver.navigate().refresh();
				FC.LogIn(sheetname, "Username2", password);
				commonMethod.enterText(FillClusterPage.ProjectSearch, "Workflow inbox Project search", FillCluster.name, driver);
				commonMethod.clickOnElement(FillClusterPage.WIProjectSearch, "Workflow inbox search", driver);
				commonMethod.clickOnElement(FillClusterPage.tableRow, "Resulted project", driver);
				commonMethod.clickOnElement(FillClusterPage.ShowDataCluster, "Show data cluster button", driver);
				commonMethod.clickOnElement(FillClusterPage.GenKitPackApproveButton, "Approve button", driver);
				FC.LogIn(sheetname, "Username2", password);
				
				FC.Logout();
				
				//Login as EDITOR to submit all project approval
				driver.navigate().refresh();
				FC.LogIn(sheetname, username, password);
				commonMethod.clickOnElement(FillClusterPage.AllprojButton, "All project button", driver);
				FC.FilterProjectnameactive(FillCluster.name);
				commonMethod.clickOnElement(FillClusterPage.tableRow, "Resulted projec", driver);
				commonMethod.clickOnElement(FillClusterPage.GenProjInfo, "General project information tab", driver);
				commonMethod.clickOnElement(FillClusterPage.SubmitAllProj, "Submit All project information", driver);
				
				//All project Information approval
				commonMethod.clickOnElement(FillClusterPage.HomeButton, "Continue save changes button", driver);
				commonMethod.enterText(FillClusterPage.ProjectSearch, "Workflow inbox Project search", FillCluster.name, driver);
				commonMethod.clickOnElement(FillClusterPage.WIProjectSearch, "Workflow inbox search", driver);
				commonMethod.clickOnElement(FillClusterPage.tableRow, "Resulted project", driver);
				commonMethod.clickOnElement(FillClusterPage.ShowDataCluster, "Show data cluster button", driver);
				commonMethod.clickOnElement(FillClusterPage.AllprojapproveButton, "Approve button", driver);
				FC.LogIn(sheetname, username, password);
				
				FC.Logout();
				
				//Login as Labelgroup to submit MEL compiler
				driver.navigate().refresh();
				FC.LogIn(sheetname, "Username5", "Password5");
				commonMethod.clickOnElement(FillClusterPage.AllprojButton, "All project button", driver);
				FC.FilterProjectnameactive(FillCluster.name);
				commonMethod.clickOnElement(FillClusterPage.tableRow, "Resulted projec", driver);
				commonMethod.clickOnElement(FillClusterPage.informationKitType, "Information per kit type section", driver);
				FC.SelectKitType(dataTable.getData("Test_Data", "Kitname"));
				commonMethod.clickOnElement(FillClusterPage.MELcompilertab, "MEL Compiler Tab", driver);
				commonMethod.clickOnElement(FillClusterPage.editLabelGroupsButton, "Edit Label Groups Button", driver);
				commonMethod.enterText(FillClusterPage.Name, "Name", dataTable.getData("Test_Data", "LabelName"), driver);
				commonMethod.clickOnElement(FillClusterPage.SelectAllCountries, "Select all countries link", driver);
				commonMethod.clickOnElement(FillClusterPage.saveLabelGroupButton, "Save Label Group Button", driver);
				
				
				//combine and Template flow
				commonMethod.clickOnElement(FillClusterPage.PackTypecheckbox1, "Pack Type checkbox", driver);
				
				commonMethod.clickOnElement(FillClusterPage.Labelgroupcheckbox1, "Label Group checkbox", driver);
				commonMethod.clickOnElement(FillClusterPage.melcombineButton, "Combine MEL", driver);
				commonMethod.clickOnElement(FillClusterPage.template, "Template", driver);
				
				commonMethod.enterText(FillClusterPage.template,"Template Option",dataTable.getData("Test_Data", "MELTemplate"), driver);
				commonMethod.clickDown(FillClusterPage.template,driver);
				commonMethod.clickEnter(FillClusterPage.template, driver);
				
				commonMethod.clickOnElement(FillClusterPage.SearchButton, "Search Button", driver);
				commonMethod.clickOnElement(FillClusterPage.searchField, "Element", driver);
				commonMethod.clickOnElement(FillClusterPage.addToMEL, "Element", driver);
				
				commonMethod.clickOnElement(FillClusterPage.NGCStaticReference, "NGC Static Reference button", driver);
				commonMethod.clickOnElement(FillClusterPage.SearchButton, "Search Button", driver);
				commonMethod.clickOnElement(FillClusterPage.ngcField, "Element", driver);
				commonMethod.clickOnElement(FillClusterPage.addToMEL, "Element", driver);
				commonMethod.scrollDown(driver);
				commonMethod.clickOnElement(FillClusterPage.DoneButton, "Done Button", driver);
				
				//MEL Submit
				commonMethod.VerifyElementEnabled(FillClusterPage.MELSubmitButton, "MEL compiler submit button", driver);
				commonMethod.clickOnElement(FillClusterPage.MELSubmitButton, "MEL compiler submit button", driver);
				commonMethod.clickOnElement(FillClusterPage.MELSaveSubmit, "Continue and save", driver);
				
				commonMethod.clickOnElement(FillClusterPage.informationKitType, "Information per kit type section", driver);
				commonMethod.clickOnElement(FillClusterPage.MELcompilertab, "Mel compiler tab", driver);
				commonMethod.getText(FillClusterPage.LabelMEL, "Labelgroup MEL compiler", driver);
				commonMethod.getText(FillClusterPage.OpenMELcompiler, "Open MEL compiler", driver);
		
	}
			
}




