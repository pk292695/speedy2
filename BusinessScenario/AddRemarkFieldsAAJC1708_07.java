package BusinessScenario;

import java.awt.AWTException;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;

import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;
import uri.FillCluster;
import uriPage.FillClusterPage;
import CommonMethod.CommonMethod;

public class AddRemarkFieldsAAJC1708_07 extends ReusableLibrary{

	CommonMethod commonMethod = new CommonMethod(scriptHelper);
	FillCluster FC = new FillCluster(scriptHelper);
	
	
	
	public AddRemarkFieldsAAJC1708_07(ScriptHelper scriptHelper)
	{
		super(scriptHelper);
	}
	String username = "Username";
	String sheetname = "functional_flow";
	String password = "Password";
	String username1 = "Username1";
	public void AddRemark_07() throws IOException, UnsupportedFlavorException, AWTException
	{
		commonMethod.navigateToUrl(dataTable.getData("functional_flow", "Application_URL"), driver);
		FC.LogIn(sheetname,username, password);
		FC.StartNewProject();
		FC.CountryInformation();
		
		FC.SubmitCountryInformation();
		
		FC.Logout();
		
		//Country Information Approval
				driver.navigate().refresh();
				FC.LogIn(sheetname, username1, password);
				commonMethod.enterText(FillClusterPage.ProjectSearch, "Workflow inbox Project search", FillCluster.name, driver);
				commonMethod.clickOnElement(FillClusterPage.WIProjectSearch, "Workflow inbox search", driver);
				commonMethod.clickOnElement(FillClusterPage.tableRow, "Resulted project", driver);
				commonMethod.clickOnElement(FillClusterPage.ShowDataCluster, "Show data cluster button", driver);
				commonMethod.clickOnElement(FillClusterPage.CntryProjApproveButton, "Approve button", driver);
				FC.LogIn(sheetname, username1, password);
				
		commonMethod.clickOnElement(FillClusterPage.DownloadIMPA, "Download e-IMPA button", driver);
	
	
	
	
	
	
	}
}
