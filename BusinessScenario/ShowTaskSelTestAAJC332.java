package BusinessScenario;

import java.awt.AWTException;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;


import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;
import uri.FillCluster;
import uriPage.FillClusterPage;
import AutomationCoreFramework.Status;
import BusinessScenarioPage.AddRemoveKitTypePage;
import CommonMethod.CommonMethod;

public class ShowTaskSelTestAAJC332 extends ReusableLibrary{
	AddRemoveKitTypePage AddRemoveKiteTypPage = new AddRemoveKitTypePage(scriptHelper);

	CommonMethod commonMethod = new CommonMethod(scriptHelper);
	FillCluster FC = new FillCluster(scriptHelper);
	
	public ShowTaskSelTestAAJC332(ScriptHelper scriptHelper)
	{
		super(scriptHelper);
	}
	String username = "Username";
	String username2= "Username2";
	String sheetname = "functional_flow";
	String password = "Password";
	String KitTypeName = "JKP";
	public void ShowTask() throws IOException, UnsupportedFlavorException, AWTException
	{
		commonMethod.navigateToUrl(dataTable.getData("functional_flow", "Application_URL"), driver);
		FC.LogIn(sheetname,username, password);
		commonMethod.clickOnElement(FillClusterPage.AllprojButton, "All projects button", driver);
		FC.StartNewProject();
		commonMethod.clickOnElement(FillClusterPage.informationKitType, "Information Per kit type", driver);
		FC.createKitType("JKP");
		FC.DrugProductInfo();
		FC.PrimaryPackInfo();
		FC.SubmitDrugPrimary();
		FC.Logout();
		FC.LogIn(sheetname, username2, password);
		commonMethod.enterText(FillClusterPage.WIProjectSearch, "Project name", FC.name, driver);
		commonMethod.clickOnElement(FillClusterPage.WISearchbutton, "Search button", driver);
		commonMethod.clickOnElement(FillClusterPage.tableRow, "Resulted project", driver);	
		
		commonMethod.getText(FillClusterPage.WITitle, "Title", driver);
		commonMethod.getText(FillClusterPage.WIContent, "Title", driver);
		commonMethod.verifyExpectedText(FillClusterPage.WICompound, "Compound", driver);
		commonMethod.getText(FillClusterPage.WIContentCompound, "Compound name", driver);
		commonMethod.verifyExpectedText(FillClusterPage.WIDataCluster, "Data Cluster", driver);
		commonMethod.getText(FillClusterPage.WIContentDataCluster, "Data cluster name", driver);
		commonMethod.verifyExpectedText(FillClusterPage.WIVersionDes, "Version description", driver);
		commonMethod.getText(FillClusterPage.WIContentVersionDes, "Version number", driver);
		commonMethod.verifyExpectedText(FillClusterPage.WIsubmittedBy, "Submitted by", driver);
		commonMethod.getText(FillClusterPage.WIContentsubmittedBy, "Submitted by name", driver);
		commonMethod.verifyExpectedText(FillClusterPage.WIDateSubmisison, "Date of submission", driver);
		commonMethod.getText(FillClusterPage.WIContentDateSubmisison, "Date of submission", driver);
		commonMethod.VerifyElementEnabled(FillClusterPage.WICancel, "Cancel button", driver);
		commonMethod.VerifyElementEnabled(FillClusterPage.WIShowdataCluster_Button, "Show cluster button", driver);
		commonMethod.clickOnElement(FillClusterPage.WIShowdataCluster_Button, "show data cluster button", driver);
	}
}




