package BusinessScenario;

import java.awt.AWTException;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;

import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;
import uri.FillCluster;
import uriPage.FillClusterPage;
import CommonMethod.CommonMethod;

public class FillInVariablesAAJC1753 extends ReusableLibrary{

	CommonMethod commonMethod = new CommonMethod(scriptHelper);
	FillCluster FC = new FillCluster(scriptHelper);
	
	
	
	public FillInVariablesAAJC1753(ScriptHelper scriptHelper)
	{
		super(scriptHelper);
	}
	String username = "Username";
	String username1 = "Username1";
	String username2 = "Username2";
	String sheetname = "functional_flow";
	String password = "Password";
	
	public void FillVariable() throws IOException, UnsupportedFlavorException, AWTException, InterruptedException
	{
		commonMethod.navigateToUrl(dataTable.getData("functional_flow", "Application_URL"), driver);
		FC.LogIn(sheetname, username, password);
		FC.StartNewProject();
		
		commonMethod.clickOnElement(FillClusterPage.informationKitType, "Information per kit type section", driver);
		FC.createKitType(dataTable.getData("Test_Data", "Kitname"));
		FC.GeneralKitTypeInfo();

		commonMethod.clickOnElement(FillClusterPage.PacktypeTab, "Pack type tab", driver);
		commonMethod.clickOnElement(FillClusterPage.Packoption1, "Pack type option", driver);
		commonMethod.selectOptionFromDropDown(FillClusterPage.ProdName, dataTable.getData("Test_Data", "ProdName_Variable"), driver);
		commonMethod.enterText(FillClusterPage.ProdNameVar, "Product Name variable", "23", driver);
		commonMethod.selectOptionFromDropDown(FillClusterPage.Dosage, dataTable.getData("Test_Data", "DDoseForm_Variable"), driver);
		commonMethod.enterText(FillClusterPage.DosageVar, "Dosage variable", "23", driver);
		commonMethod.selectOptionFromDropDown(FillClusterPage.ROA, dataTable.getData("Test_Data", "ROA"), driver);
		commonMethod.selectOptionFromDropDown(FillClusterPage.ROAPict, dataTable.getData("Test_Data", "ROApict"), driver);
		commonMethod.selectOptionFromDropDown(FillClusterPage.DoseInstruction, dataTable.getData("Test_Data", "DosingInst"), driver);
		commonMethod.selectOptionFromDropDown(FillClusterPage.Tempstore, dataTable.getData("Test_Data", "TempStore"), driver);
		commonMethod.selectOptionFromDropDown(FillClusterPage.OtherStore, dataTable.getData("Test_Data", "OtherStore"), driver);
		commonMethod.selectOptionFromDropDown(FillClusterPage.HandIns, dataTable.getData("Test_Data", "HandInst"), driver);
		commonMethod.selectOptionFromDropDown(FillClusterPage.PeriodDes, dataTable.getData("Test_Data", "PeriodDes"), driver);
		commonMethod.selectOptionFromDropDown(FillClusterPage.Othervar, dataTable.getData("Test_Data", "OtherVariable"), driver);
		commonMethod.clickOnElement(FillClusterPage.TearOff, "Tear-off", driver);
		
		FC.SubmitGeneralKitPackType();
		
		commonMethod.clickOnElement(FillClusterPage.CheckApproveStatus, "Check approval status", driver);
		commonMethod.clickOnElement(FillClusterPage.KitRow, "Kit list select", driver);
		commonMethod.getText(FillClusterPage.GenKitPackInsideCheckApproval, "Status of Drugproduct & primaryPack", driver);
		commonMethod.clickOnElement(FillClusterPage.CheckApproveToProject, "Project details page", driver);
		
		commonMethod.clickOnElement(FillClusterPage.informationKitType, "Information per kit type section", driver);
		commonMethod.clickOnElement(FillClusterPage.PacktypeTab, "Pack type tab", driver);
		commonMethod.getText(FillClusterPage.ProdNameText, "Product Name Text variable", driver);
	commonMethod.getText(FillClusterPage.DosageText, "Dosage form Text variable", driver);
	
	FC.Logout();
	
	//General kit and Pack type Approval
	driver.navigate().refresh();
	FC.LogIn(sheetname, "Username2", password);
	commonMethod.enterText(FillClusterPage.ProjectSearch, "Workflow inbox Project search", FillCluster.name, driver);
	commonMethod.clickOnElement(FillClusterPage.WIProjectSearch, "Workflow inbox search", driver);
	commonMethod.clickOnElement(FillClusterPage.tableRow, "Resulted project", driver);
	commonMethod.clickOnElement(FillClusterPage.ShowDataCluster, "Show data cluster button", driver);
	commonMethod.clickOnElement(FillClusterPage.GenKitPackApproveButton, "Approve button", driver);
	FC.LogIn(sheetname, "Username2", password);
	
	
	FC.Logout();
	
//Login as Labelgroup for MEL compiler
	driver.navigate().refresh();
	FC.LogIn(sheetname, "Username1", password);
	commonMethod.clickOnElement(FillClusterPage.AllprojButton, "All projects button", driver);
	FC.FilterProjectnameactive(FillCluster.name);
	commonMethod.clickOnElement(FillClusterPage.tableRow, "Resulted project", driver);
	commonMethod.clickOnElement(FillClusterPage.informationKitType, "Information per kit type section", driver);
	commonMethod.clickOnElement(FillClusterPage.MELcompilertab, "MEL compiler tab", driver);
	
	commonMethod.clickOnElement(FillClusterPage.editLabelGroupsButton, "Edit Label Groups Button", driver);
	commonMethod.enterText(FillClusterPage.LabelName, "Label Name", dataTable.getData("Test_Data", "LabelName"), driver);
	commonMethod.clickOnElement(FillClusterPage.countryNameCheckbox, "Country Name Checkbox", driver);
	commonMethod.clickOnElement(FillClusterPage.saveLabelGroupButton, "Save Label Group Button", driver);

	commonMethod.clickOnElement(FillClusterPage.PackTypecheckbox1, "Pack type", driver);
	commonMethod.clickOnElement(FillClusterPage.Labelgroupcheckbox1, "Label Group", driver);
	commonMethod.clickOnElement(FillClusterPage.melcombineButton, "Combine", driver);
	commonMethod.clickOnElement(FillClusterPage.template, "Template", driver);
	
	commonMethod.enterText(FillClusterPage.template,"Template Option",dataTable.getData("Test_Data", "MELTemplate"), driver);
	commonMethod.clickDown(FillClusterPage.template,driver);
	commonMethod.clickEnter(FillClusterPage.template, driver);
	
	commonMethod.getText(FillClusterPage.templateElements, "Template Elements", driver);
	commonMethod.clickOnElement(FillClusterPage.DoneButton, "Done", driver);
	
	}
}
