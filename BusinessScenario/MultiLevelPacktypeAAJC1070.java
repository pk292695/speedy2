package BusinessScenario;

import java.awt.AWTException;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;

import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;
import BusinessScenarioPage.Multilevel_packtype_page;
import CommonMethod.CommonMethod;

public class MultiLevelPacktypeAAJC1070 extends ReusableLibrary{

	Multilevel_packtype_page MELTemplatePage = new Multilevel_packtype_page(scriptHelper);

	CommonMethod commonMethod = new CommonMethod(scriptHelper);
	public MultiLevelPacktypeAAJC1070(ScriptHelper scriptHelper)
	{
		super(scriptHelper);
	}
	
	public void Doseform_packtype() throws IOException, UnsupportedFlavorException, AWTException
	{
		commonMethod.navigateToUrl(dataTable.getData("functional_flow", "Application_URL"), driver);
		commonMethod.enterText(Multilevel_packtype_page.Username, "Username", dataTable.getData("functional_flow", "Username"), driver);
		commonMethod.enterText(Multilevel_packtype_page.Password, "Password", dataTable.getData("functional_flow", "Password"), driver);
		commonMethod.clickOnElement(Multilevel_packtype_page.Login, "Login button", driver);
		commonMethod.clickOnElement(Multilevel_packtype_page.Allproj, "All projects button", driver);
		commonMethod.clickOnElement(Multilevel_packtype_page.Arrow, "Next page arrow", driver);
		commonMethod.clickOnElement(Multilevel_packtype_page.Project, "Project", driver);
		commonMethod.clickOnElement(Multilevel_packtype_page.InfoPerKit, "Info Per kit type section", driver);
		commonMethod.clickOnElement(Multilevel_packtype_page.PackType, "Pack type button", driver);
		commonMethod.selectOptionFromDropDown(Multilevel_packtype_page.Doseform, dataTable.getData("Test_Data", "DoseForm_Quantity"), driver);;
		commonMethod.clickOnElement(Multilevel_packtype_page.AddDoseform, "Add button", driver);
	}
	
	
}
