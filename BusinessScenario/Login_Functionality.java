package BusinessScenario;

import java.awt.AWTException;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;
import BusinessScenarioPage.Login_page;
import CommonMethod.CommonMethod;
import CommonMethod.ExpectedData;

// Login with NT UserName and Password AAJC-1
public class Login_Functionality extends ReusableLibrary{
	public Properties OR_properties;
	static boolean elementPresent;
	Login_page Loginpage = new Login_page(scriptHelper);

	CommonMethod commonMethod = new CommonMethod(scriptHelper);
	ExpectedData expectedData = new ExpectedData(scriptHelper);

	String packName = this.getClass().getPackage().getName();
	String className = this.getClass().getSimpleName();
	
	public Login_Functionality(ScriptHelper scriptHelper)
	{
		super(scriptHelper);
	}
	public void Login1() throws IOException, UnsupportedFlavorException, AWTException
	{
		commonMethod.navigateToUrl(dataTable.getData("functional_flow", "Application_URL"), driver);
		commonMethod.enterText(Login_page.Username, "User Name", dataTable.getData("functional_flow", "Username"), driver);
		commonMethod.enterText(Login_page.Password, "Password", dataTable.getData("functional_flow", "Password"), driver);
		commonMethod.clickOnElement(Login_page.Login, "Login button", driver);
		System.out.println("Showing Editor Inbox");
		commonMethod.clickOnElement(Login_page.StartProject, "Start Project button", driver);
		commonMethod.clickOnElement(Login_page.Cancel, "Cancel button", driver);
		commonMethod.clickOnElement(Login_page.Account, "Account button", driver);
		commonMethod.clickOnElement(Login_page.Signout, "Sign out button", driver);
	}
	public void Login2() throws IOException, UnsupportedFlavorException, AWTException
	{
		commonMethod.clickOnElement(Login_page.Account, "Account button", driver);
		commonMethod.clickOnElement(Login_page.Signin, "Sign-in button", driver);
		commonMethod.enterText(Login_page.Username, "User Name", dataTable.getData("functional_flow", "Username1"), driver);
		commonMethod.enterText(Login_page.Password, "Password", dataTable.getData("functional_flow", "Password1"), driver);
		commonMethod.clickOnElement(Login_page.Login, "Login button", driver);
		commonMethod.clickOnElement(Login_page.Showfilter, "Show filter button", driver);
		commonMethod.clickOnElement(Login_page.CheckCntry, "Country Depot checkbox", driver);
		commonMethod.clickOnElement(Login_page.Applyfilter, "Apply filter button", driver);
		System.out.println("Showing Labelgroup approver Inbox");
		commonMethod.clickOnElement(Login_page.Account, "Account button", driver);
		commonMethod.clickOnElement(Login_page.Signout, "Sign out button", driver);
	}
	public void Login3() throws IOException, UnsupportedFlavorException, AWTException
	{
		commonMethod.clickOnElement(Login_page.Account, "Account button", driver);
		commonMethod.clickOnElement(Login_page.Signin, "Sign-in button", driver);
		commonMethod.enterText(Login_page.Username, "User Name", dataTable.getData("functional_flow", "Username2"), driver);
		commonMethod.enterText(Login_page.Password, "Password", dataTable.getData("functional_flow", "Password2"), driver);
		commonMethod.clickOnElement(Login_page.Login, "Login button", driver);
		commonMethod.clickOnElement(Login_page.Showfilter, "Show filter button", driver);
		commonMethod.clickOnElement(Login_page.CheckGenKit, "General kit type checkbox", driver);
		commonMethod.clickOnElement(Login_page.Applyfilter, "Apply filter button", driver);
		System.out.println("Showing Batchdoc approver Inbox");
		commonMethod.clickOnElement(Login_page.Account, "Account button", driver);
		commonMethod.clickOnElement(Login_page.Signout, "Sign out button", driver);
	}
	public void Login4() throws IOException, UnsupportedFlavorException, AWTException
	{
		commonMethod.clickOnElement(Login_page.Account, "Account button", driver);
		commonMethod.clickOnElement(Login_page.Signin, "Sign-in button", driver);
		commonMethod.enterText(Login_page.Username, "User Name", dataTable.getData("functional_flow", "Username3"), driver);
		commonMethod.enterText(Login_page.Password, "Password", dataTable.getData("functional_flow", "Password3"), driver);
		commonMethod.clickOnElement(Login_page.Login, "Login button", driver);
		WebElement title = driver.findElement(By.xpath(Login_page.Title));
		commonMethod.getText(title, "Speedy", driver);
		System.out.println("Showing QA approver Inbox");
		//commonMethod.clickOnElement(Login_page.Account, "Account button", driver);
		//commonMethod.clickOnElement(Login_page.Signout, "Sign out button", driver);
	}
	

}
