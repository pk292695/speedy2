package BusinessScenario;

import java.awt.AWTException;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;

import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;
import uri.FillCluster;
import BusinessScenarioPage.Delete_Edit_MEL_Page;
import CommonMethod.CommonMethod;

public class DeleteMELAAJC23 extends ReusableLibrary{
	
	Delete_Edit_MEL_Page MELTemplatePage = new Delete_Edit_MEL_Page(scriptHelper);
	FillCluster FC = new FillCluster(scriptHelper);
	CommonMethod commonMethod = new CommonMethod(scriptHelper);
	public DeleteMELAAJC23(ScriptHelper scriptHelper)
	{
		super(scriptHelper);
	}
	
	public void MELTemplateDelete() throws IOException, UnsupportedFlavorException, AWTException
	{
		commonMethod.navigateToUrl(dataTable.getData("functional_flow", "Application_URL"), driver);
		FC.LogIn("Sheetname", "Username4", "Password4");
		commonMethod.clickOnElement(Delete_Edit_MEL_Page.ProjMgmt, "Project management button", driver);
		commonMethod.clickOnElement(Delete_Edit_MEL_Page.MELMgmt, "MEL management button", driver);
		
		commonMethod.selectOptionFromDropDown(Delete_Edit_MEL_Page.Long_Description, dataTable.getData("Test_Data", "Long_Description"), driver);
		commonMethod.clickOnElement(Delete_Edit_MEL_Page.RemoveMEL, "Remove MEL template button", driver);
		commonMethod.clickOnElement(Delete_Edit_MEL_Page.Delete, "Delete button", driver);

		
		
		
		
		
	}
	
}
