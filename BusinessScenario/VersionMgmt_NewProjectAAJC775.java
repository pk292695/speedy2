package BusinessScenario;

import java.awt.AWTException;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.util.List;


import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;
import uri.FillCluster;
import uriPage.FillClusterPage;
import AutomationCoreFramework.Status;

import CommonMethod.CommonMethod;


public class VersionMgmt_NewProjectAAJC775 extends ReusableLibrary{

	CommonMethod commonMethod = new CommonMethod(scriptHelper);
	FillCluster FC = new FillCluster(scriptHelper);
	
	public VersionMgmt_NewProjectAAJC775(ScriptHelper scriptHelper)
	{
		super(scriptHelper);
	}
	String username = "Username";
	String sheetname = "functional_flow";
	String password = "Password";
	public void VersionMgmt_NewProject() throws IOException, UnsupportedFlavorException, AWTException
	{
		commonMethod.navigateToUrl(dataTable.getData("functional_flow", "Application_URL"), driver);
		FC.LogIn(sheetname, username, password);
		FC.StartNewProject();
		commonMethod.getText(FillClusterPage.VerNumber, "Version number", driver);
		FC.GeneralProjectInformation();
		commonMethod.clickOnElement(FillClusterPage.SaveChanges, "Save changes", driver);
		commonMethod.clickOnElement(FillClusterPage.ContinueSave, "Continue Save changes", driver);
	}
	
	
}
