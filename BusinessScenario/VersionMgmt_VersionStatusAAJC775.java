package BusinessScenario;

import java.awt.AWTException;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.util.List;


import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;
import uri.FillCluster;
import uriPage.FillClusterPage;
import AutomationCoreFramework.Status;

import CommonMethod.CommonMethod;


public class VersionMgmt_VersionStatusAAJC775 extends ReusableLibrary{

	CommonMethod commonMethod = new CommonMethod(scriptHelper);
	FillCluster FC = new FillCluster(scriptHelper);
	
	public VersionMgmt_VersionStatusAAJC775(ScriptHelper scriptHelper)
	{
		super(scriptHelper);
	}
	String username = "Username";
	String sheetname = "functional_flow";
	String password = "Password";
	public void Validate_VersionStatuses() throws IOException, UnsupportedFlavorException, AWTException
	{
		commonMethod.navigateToUrl(dataTable.getData("functional_flow", "Application_URL"), driver);
		FC.LogIn(sheetname, username, password);
		FC.StartNewProject();
		String vn = driver.findElement(By.xpath(FillClusterPage.VerNumber)).getText();
		commonMethod.getText(FillClusterPage.VerNumber,"Version number: "+vn , driver);
		FC.GeneralProjectInformation();
		FC.SubmitGeneralProjectInformation();
		commonMethod.clickOnElement(FillClusterPage.CreateNewVersion, "Create new version button", driver);
		commonMethod.clickOnElement(FillClusterPage.GenProjCheck, "General proj checkbox", driver);
		commonMethod.clickOnElement(FillClusterPage.newversion, "Create new version button", driver);
		String vn1 = driver.findElement(By.xpath(FillClusterPage.VerNumber)).getText();
		commonMethod.getText(FillClusterPage.VerNumber, "Version number:"+vn1, driver);
		commonMethod.clickOnElement(FillClusterPage.Proj, "All projects page", driver);
		commonMethod.clickOnElement(FillClusterPage.LastArrow, "Last page", driver);
		List<WebElement> e = driver.findElements(By.xpath(FillClusterPage.tableRow));
		int q = e.size();
		System.out.println(q);
		
		//commonMethod.getText("//tr["+q+"]/td[2]", "Last Created Project Name", driver);
		//String p =driver.findElement(By.xpath("//tr["+q+"][@id='tablerowdata']/td[2]")).getText();
		//System.out.println(p);
		
		//commonMethod.getText("//tr["+q+"]/td[6]", "Last Created Project status", driver);
		//String k =driver.findElement(By.xpath("//tr["+q+"][@id='tablerowdata']/td[6]")).getText();
		//System.out.println(k);
		
		
		WebElement ProjName = driver.findElement(By.xpath("//tr["+q+"]/td[2]"));
		String project = ProjName.getText();
		commonMethod.getText("//tr["+q+"]/td[2]", project, driver);
		WebElement ProjStatus = driver.findElement(By.xpath("//tr["+q+"]/td[6]"));
		String status = ProjStatus.getText();
		commonMethod.getText("//tr["+q+"]/td[6]", status, driver);
	}
	
	
}
