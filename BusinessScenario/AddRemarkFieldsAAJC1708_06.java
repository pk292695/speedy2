package BusinessScenario;

import java.awt.AWTException;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;

import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;
import uri.FillCluster;
import uriPage.FillClusterPage;
import CommonMethod.CommonMethod;

public class AddRemarkFieldsAAJC1708_06 extends ReusableLibrary{

	CommonMethod commonMethod = new CommonMethod(scriptHelper);
	FillCluster FC = new FillCluster(scriptHelper);
	
	
	
	public AddRemarkFieldsAAJC1708_06(ScriptHelper scriptHelper)
	{
		super(scriptHelper);
	}
	String username = "Username";
	String sheetname = "functional_flow";
	String password = "Password";
	String username2 = "Username2";
	public void AddRemark_06() throws IOException, UnsupportedFlavorException, AWTException
	{
		commonMethod.navigateToUrl(dataTable.getData("functional_flow", "Application_URL"), driver);
		FC.LogIn(sheetname,username, password);
		FC.StartNewProject();
		
		commonMethod.clickOnElement(FillClusterPage.informationKitType, "Information per kit type section", driver);
		FC.createKitType(dataTable.getData("Test_Data", "Kitname"));
		FC.GeneralKitTypeInfo();
		FC.PackTypeInfo();
		FC.SubmitGeneralKitPackType();
		
		FC.DrugProductInfo();
		commonMethod.clickOnElement(FillClusterPage.DrugProductInfoComments, "Comments Drug product info", driver);
		FC.PrimaryPackInfo();
		commonMethod.clickOnElement(FillClusterPage.primaryPackTypeComments, "Comments Primary pack info", driver);
		FC.SubmitDrugPrimary();
		
		FC.Logout();
		//General kit and Pack type Approval
				driver.navigate().refresh();
				FC.LogIn(sheetname, username2, password);
				commonMethod.enterText(FillClusterPage.ProjectSearch, "Workflow inbox Project search", FillCluster.name, driver);
				commonMethod.clickOnElement(FillClusterPage.WIProjectSearch, "Workflow inbox search", driver);
				commonMethod.clickOnElement(FillClusterPage.tableRow, "Resulted project", driver);
				commonMethod.clickOnElement(FillClusterPage.ShowDataCluster, "Show data cluster button", driver);
				commonMethod.clickOnElement(FillClusterPage.GenKitPackApproveButton, "Approve button", driver);
				FC.LogIn(sheetname, username2, password);
				
				commonMethod.clickOnElement(FillClusterPage.HomeButton, "Home button", driver);
				
				commonMethod.enterText(FillClusterPage.ProjectSearch, "Workflow inbox Project search", FillCluster.name, driver);
				commonMethod.clickOnElement(FillClusterPage.WIProjectSearch, "Workflow inbox search", driver);
				commonMethod.clickOnElement(FillClusterPage.tableRow, "Resulted project", driver);
				commonMethod.clickOnElement(FillClusterPage.ShowDataCluster, "Show data cluster button", driver);
				commonMethod.clickOnElement(FillClusterPage.DrugPrimApproveButton, "Approve button", driver);
				FC.LogIn(sheetname, username2, password);
		
		
		commonMethod.clickOnElement(FillClusterPage.DownloadIMPA, "Download e-IMPA button", driver);
	
	
	
	
	
	
	}
}
