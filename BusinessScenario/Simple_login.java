package BusinessScenario;

import java.awt.AWTException;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.util.Properties;
import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;
import BusinessScenarioPage.Simple_login_page;
import CommonMethod.CommonMethod;
import CommonMethod.ExpectedData;

public class Simple_login extends ReusableLibrary{
	public Properties OR_properties;
	static boolean elementPresent;
	Simple_login_page Loginpage = new Simple_login_page(scriptHelper);

	CommonMethod commonMethod = new CommonMethod(scriptHelper);
	ExpectedData expectedData = new ExpectedData(scriptHelper);

	String packName = this.getClass().getPackage().getName();
	String className = this.getClass().getSimpleName();
	
	public Simple_login(ScriptHelper scriptHelper)
	{
		super(scriptHelper);
	}
	
	public void Login_Editor() throws IOException, UnsupportedFlavorException, AWTException
	{
		commonMethod.navigateToUrl(dataTable.getData("functional_flow", "Application_URL"), driver);
		commonMethod.enterText(Simple_login_page.Username, "User Name", dataTable.getData("functional_flow", "Username"), driver);
		commonMethod.enterText(Simple_login_page.Password, "Password", dataTable.getData("functional_flow", "Password"), driver);
		commonMethod.clickOnElement(Simple_login_page.Login, "Login button", driver);
		commonMethod.clickOnElement(Simple_login_page.StartProject, "Start Project button", driver);
		commonMethod.clickOnElement(Simple_login_page.Cancel, "Cancel button", driver);
		commonMethod.clickOnElement(Simple_login_page.Allproj, "All projects button", driver);
		commonMethod.clickOnElement(Simple_login_page.NextPage, "Next page button", driver);
		commonMethod.clickOnElement(Simple_login_page.DraftProj, "Draft project", driver);
		commonMethod.clickOnElement(Simple_login_page.GenProjInfo, "General proj Info tab", driver);
		commonMethod.clickOnElement(Simple_login_page.ImpaField, "Impa field", driver);
		commonMethod.clickOnElement(Simple_login_page.CntryInfo, "Country Info tab", driver);
		commonMethod.clickOnElement(Simple_login_page.InfoKit, "Info per kit type tab", driver);
		commonMethod.clickOnElement(Simple_login_page.GenKitInfo, "General kit tab", driver);
		commonMethod.clickOnElement(Simple_login_page.PackTypeInfo, "Pack type tab", driver);
		commonMethod.clickOnElement(Simple_login_page.DrugProdInfo, "Drug product info tab", driver);
		commonMethod.clickOnElement(Simple_login_page.PrimPackInfo, "Primary pack tab", driver);
		commonMethod.clickOnElement(Simple_login_page.MelCompTab, "Mel compiler tab", driver);
		commonMethod.clickOnElement(Simple_login_page.Account, "Account button", driver);
		commonMethod.clickOnElement(Simple_login_page.Signout, "Sign-out button", driver);
		
	}
	public void Login_Lblgrp() throws IOException, UnsupportedFlavorException, AWTException
	{
		//commonMethod.clickOnElement(Simple_login_page.Account, "Account button", driver);
		//commonMethod.clickOnElement(Simple_login_page.Signin, "Sign-in button", driver);
		commonMethod.enterText(Simple_login_page.Username, "User Name", dataTable.getData("functional_flow", "Username1"), driver);
		commonMethod.enterText(Simple_login_page.Password, "Password", dataTable.getData("functional_flow", "Password1"), driver);
		commonMethod.clickOnElement(Simple_login_page.Login, "Login button", driver);
		commonMethod.clickOnElement(Simple_login_page.Showfilter, "Show filter button", driver);
		commonMethod.clickOnElement(Simple_login_page.CheckCntry, "Country depot checkbox", driver);
		commonMethod.clickOnElement(Simple_login_page.OpenStatus, "Open checkbox", driver);
		commonMethod.clickOnElement(Simple_login_page.Applyfilter, "Apply filter button", driver);
		commonMethod.scrollDown(driver);
		commonMethod.clickOnElement(Simple_login_page.FrstProj, "First project", driver);
		commonMethod.clickOnElement(Simple_login_page.ShowClusterbutton, "Show data cluster button", driver);
		commonMethod.clickOnElement(Simple_login_page.Account, "Account button", driver);
		commonMethod.clickOnElement(Simple_login_page.Signout, "Sign-out button", driver);
		
		
	}
	public void Login_Batchdoc() throws IOException, UnsupportedFlavorException, AWTException
	{
		//commonMethod.clickOnElement(Simple_login_page.Account, "Account button", driver);
		//commonMethod.clickOnElement(Simple_login_page.Signin, "Sign-in button", driver);
		commonMethod.enterText(Simple_login_page.Username, "User Name", dataTable.getData("functional_flow", "Username2"), driver);
		commonMethod.enterText(Simple_login_page.Password, "Password", dataTable.getData("functional_flow", "Password2"), driver);
		commonMethod.clickOnElement(Simple_login_page.Login, "Login button", driver);
		
		commonMethod.enterText(Simple_login_page.Searchbox, "Search box", dataTable.getData("Test_Data", "ProjectName"), driver);
		commonMethod.clickOnElement(Simple_login_page.Searchbutton, "Search button", driver);
		commonMethod.clickOnElement(Simple_login_page.ResultProj, "Resulted project", driver);
		commonMethod.clickOnElement(Simple_login_page.ShowClusterbutton, "Show cluster button", driver);
		/*commonMethod.clickOnElement(Simple_login_page.Showfilter, "Show filter button", driver);
		commonMethod.clickOnElement(Simple_login_page.CheckGenKit, "General kit checkbox", driver);
		commonMethod.clickOnElement(Simple_login_page.OpenStatus, "Open checkbox", driver);
		commonMethod.clickOnElement(Simple_login_page.Applyfilter, "Apply filter button", driver);
		commonMethod.scrollDown(driver);
		commonMethod.clickOnElement(Simple_login_page.FrstProj, "First project", driver);
		commonMethod.clickOnElement(Simple_login_page.ShowClusterbutton, "Show data cluster button", driver);*/
		commonMethod.clickOnElement(Simple_login_page.Account, "Account button", driver);
		commonMethod.clickOnElement(Simple_login_page.Signout, "Sign-out button", driver);
		
	}
	public void Login_QA() throws IOException, UnsupportedFlavorException, AWTException
	{
		commonMethod.enterText(Simple_login_page.Username, "User Name", dataTable.getData("functional_flow", "Username3"), driver);
		commonMethod.enterText(Simple_login_page.Password, "Password", dataTable.getData("functional_flow", "Password3"), driver);
		commonMethod.clickOnElement(Simple_login_page.Login, "Login button", driver);
		//commonMethod.clickOnElement(Simple_login_page.Showfilter, "Show filter button", driver);
		//commonMethod.clickOnElement(Simple_login_page.CheckGenKit, "General kit checkbox", driver);
		//commonMethod.clickOnElement(Simple_login_page.OpenStatus, "Open checkbox", driver);
		//commonMethod.clickOnElement(Simple_login_page.Applyfilter, "Apply filter button", driver);
		//commonMethod.scrollDown(driver);
		commonMethod.clickOnElement(Simple_login_page.FrstProj, "First project", driver);
		commonMethod.clickOnElement(Simple_login_page.ShowClusterbutton, "Show data cluster button", driver);
		commonMethod.clickOnElement(Simple_login_page.Account, "Account button", driver);
		commonMethod.clickOnElement(Simple_login_page.Signout, "Sign-out button", driver);
	}

}
