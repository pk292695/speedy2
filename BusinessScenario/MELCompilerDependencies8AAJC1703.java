package BusinessScenario;

import java.awt.AWTException;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;

import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;
import uri.FillCluster;
import uriPage.FillClusterPage;
import CommonMethod.CommonMethod;

public class MELCompilerDependencies8AAJC1703 extends ReusableLibrary{

	CommonMethod commonMethod = new CommonMethod(scriptHelper);
	FillCluster FC = new FillCluster(scriptHelper);
	
	
	
	public MELCompilerDependencies8AAJC1703(ScriptHelper scriptHelper)
	{
		super(scriptHelper);
	}
	String username = "Username";
	String username1 = "Username1";
	String username2 = "Username2";
	String sheetname = "functional_flow";
	String password = "Password";
	
	public void MELDependency8() throws IOException, UnsupportedFlavorException, AWTException
	{
		commonMethod.navigateToUrl(dataTable.getData("functional_flow", "Application_URL"), driver);
		FC.LogIn(sheetname, username, password);
		FC.StartNewProject();
		FC.GeneralProjectInformation();
		FC.SubmitGeneralProjectInformation();
		FC.CountryInformation();
		FC.SubmitCountryInformation();
		commonMethod.clickOnElement(FillClusterPage.informationKitType, "Information per kit type section", driver);
		FC.createKitType(dataTable.getData("Test_Data", "Kitname"));
		FC.GeneralKitTypeInfo();
		FC.PackTypeInfo();
		FC.SubmitGeneralKitPackType();
		commonMethod.clickOnElement(FillClusterPage.informationKitType, "Information per kit type section", driver);
		FC.SelectKitType(dataTable.getData("Test_Data", "Kitname"));
		FC.DrugProductInfo();
		FC.PrimaryPackInfo();
		FC.SubmitDrugPrimary();
		
		FC.Logout();
				
				//Country Information Approval
						driver.navigate().refresh();
						FC.LogIn(sheetname,"Username1", password);
						commonMethod.enterText(FillClusterPage.ProjectSearch, "Workflow inbox Project search", FillCluster.name, driver);
						commonMethod.clickOnElement(FillClusterPage.WIProjectSearch, "Workflow inbox search", driver);
						commonMethod.clickOnElement(FillClusterPage.tableRow, "Resulted project", driver);
						commonMethod.clickOnElement(FillClusterPage.ShowDataCluster, "Show data cluster button", driver);
						commonMethod.clickOnElement(FillClusterPage.CntryProjApproveButton, "Approve button", driver);
						FC.LogIn(sheetname, username1, password);
						
						FC.Logout();
						
						//General kit and Pack type Approval
						driver.navigate().refresh();
						FC.LogIn(sheetname, "Username2", password);
						commonMethod.enterText(FillClusterPage.ProjectSearch, "Workflow inbox Project search", FillCluster.name, driver);
						commonMethod.clickOnElement(FillClusterPage.WIProjectSearch, "Workflow inbox search", driver);
						commonMethod.clickOnElement(FillClusterPage.tableRow, "Resulted project", driver);
						commonMethod.clickOnElement(FillClusterPage.ShowDataCluster, "Show data cluster button", driver);
						commonMethod.clickOnElement(FillClusterPage.GenKitPackApproveButton, "Approve button", driver);
						FC.LogIn(sheetname, "Username2", password);
						
					FC.Logout();
			
						//MEL compiler submit button check
		driver.navigate().refresh();
		FC.LogIn(sheetname,username1, password);
		commonMethod.clickOnElement(FillClusterPage.AllprojButton, "All projects button", driver);
		FC.FilterProjectnameactive(FillCluster.name);
		commonMethod.clickOnElement(FillClusterPage.tableRow, "Resulted project", driver);
		commonMethod.clickOnElement(FillClusterPage.informationKitType, "Information Kit type tab", driver);
		commonMethod.clickOnElement(FillClusterPage.MELcompilertab, "MEL Compiler tab", driver);
		
		commonMethod.verifyElementNotEnabled(FillClusterPage.MELSubmitButton, "MEL compiler submit button", driver);
	}
}
