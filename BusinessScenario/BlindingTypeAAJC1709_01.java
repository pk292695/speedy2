package BusinessScenario;

import java.awt.AWTException;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;
import uri.FillCluster;
import uriPage.FillClusterPage;
import CommonMethod.CommonMethod;

public class BlindingTypeAAJC1709_01 extends ReusableLibrary{
	CommonMethod commonMethod = new CommonMethod(scriptHelper);
	FillCluster FC = new FillCluster(scriptHelper);
	
	public BlindingTypeAAJC1709_01(ScriptHelper scriptHelper)
	{
		super(scriptHelper);
	}
	String username = "Username";
	
	String sheetname = "functional_flow";
	String password = "Password";
	public void BlindingType1() throws IOException, UnsupportedFlavorException, AWTException
	{
		commonMethod.navigateToUrl(dataTable.getData("functional_flow", "Application_URL"), driver);
		FC.LogIn(sheetname, username, password);
		FC.StartNewProject();
		commonMethod.clickOnElement(FillClusterPage.informationKitType, "Information Kit Type", driver);
		FC.createKitType(dataTable.getData("Test_Data", "Kitname"));
		commonMethod.clickOnElement(FillClusterPage.GeneralKitTypeInfo, "General kit type info", driver);
		commonMethod.clickOnElement(FillClusterPage.BlindingType, "Blinding type", driver);
		
		commonMethod.enterText(FillClusterPage.BlindingType, "Blinding Text Area", "Open ", driver);
		WebElement enter0 = driver.findElement(By.xpath(FillClusterPage.BlindingType));
		enter0.sendKeys(Keys.ENTER);
		commonMethod.enterText(FillClusterPage.BlindingType, "Blinding Text Area", "Double Blind 1 ", driver);
		WebElement enter1 = driver.findElement(By.xpath(FillClusterPage.BlindingType));
		enter1.sendKeys(Keys.ENTER);
		
		commonMethod.clearElement(FillClusterPage.BlindingType, "Blinding Type option 1", driver);
		commonMethod.clearElement(FillClusterPage.BlindingType, "Blinding Type option 2", driver);
		
		commonMethod.enterText(FillClusterPage.BlindingType, "Blinding Text Area", "Double Blind 1 ", driver);
		WebElement enter2 = driver.findElement(By.xpath(FillClusterPage.BlindingType));
		enter2.sendKeys(Keys.ENTER);
		commonMethod.enterText(FillClusterPage.BlindingType, "Blinding Text Area", "Double Blind 2 ", driver);
		WebElement enter3 = driver.findElement(By.xpath(FillClusterPage.BlindingType));
		enter3.sendKeys(Keys.ENTER);
		commonMethod.verifyElementPresent(FillClusterPage.BlindingErrorMsg, "Error Message", driver);
		commonMethod.clickOnElement(FillClusterPage.BlindingCloseError, "Close button Error message", driver);
		
	}
}
