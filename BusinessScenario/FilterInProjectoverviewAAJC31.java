package BusinessScenario;

import java.awt.AWTException;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;
import uri.FillCluster;
import uriPage.FillClusterPage;
import AutomationCoreFramework.Status;
import BusinessScenarioPage.AddRemoveKitTypePage;
import CommonMethod.CommonMethod;

public class FilterInProjectoverviewAAJC31 extends ReusableLibrary{
	AddRemoveKitTypePage AddRemoveKiteTypPage = new AddRemoveKitTypePage(scriptHelper);

	CommonMethod commonMethod = new CommonMethod(scriptHelper);
	FillCluster FC = new FillCluster(scriptHelper);
	
	public FilterInProjectoverviewAAJC31(ScriptHelper scriptHelper)
	{
		super(scriptHelper);
	}
	String username = "Username";
	String sheetname = "functional_flow";
	String password = "Password";
	String KitTypeName = "JKP";
	public void FilterAllProject() throws IOException, UnsupportedFlavorException, AWTException
	{
		commonMethod.navigateToUrl(dataTable.getData("functional_flow", "Application_URL"), driver);
		FC.LogIn(sheetname,username, password);
		commonMethod.clickOnElement(FillClusterPage.AllprojButton, "All projects button", driver);
		//Show Filter
		commonMethod.clickOnElement(FillClusterPage.filterButton, "Show Filter", driver);
		//Hide Filter
		commonMethod.clickOnElement(FillClusterPage.filterButton, "hide Filter", driver);
		FC.FilterProjectnameactive("test");
		driver.navigate().refresh();
		commonMethod.clickOnElement(FillClusterPage.filterButton, "Show Filter", driver);
		commonMethod.clickOnElement(FillClusterPage.applyFilterButton, "apply filter Button", driver);
		driver.navigate().refresh();
		FC.FilterOMPDraft();
		driver.navigate().refresh();
		FC.FilterNonOMPAwaiting();
		driver.navigate().refresh();
		FC.FilterBrightFull();
		
		
	
	}	
					
	}	




