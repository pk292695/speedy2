package BusinessScenario;

import java.awt.AWTException;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;
import AutomationCoreFramework.Status;
import BusinessScenarioPage.Version_mgmt_page;
import CommonMethod.CommonMethod;
import CommonMethod.ExpectedData;

//AAJC-775 : Version Management
public class Version_Management extends ReusableLibrary{

	public Properties OR_properties;
	static boolean elementPresent;
	Version_mgmt_page Versionpage = new Version_mgmt_page(scriptHelper);

	CommonMethod commonMethod = new CommonMethod(scriptHelper);
	ExpectedData expectedData = new ExpectedData(scriptHelper);

	String packName = this.getClass().getPackage().getName();
	String className = this.getClass().getSimpleName();
	
	public Version_Management(ScriptHelper scriptHelper)
	{
		super(scriptHelper);
	}
	public void Validate_Allprojects() throws IOException, UnsupportedFlavorException, AWTException
	{
		commonMethod.navigateToUrl(dataTable.getData("functional_flow", "Application_URL"), driver);
		commonMethod.enterText(Version_mgmt_page.Username, "Username", dataTable.getData("functional_flow", "Username"), driver);
		commonMethod.enterText(Version_mgmt_page.Password, "Password", dataTable.getData("functional_flow", "Password"), driver);
		commonMethod.clickOnElement(Version_mgmt_page.Login, "Login button", driver);
		commonMethod.clickOnElement(Version_mgmt_page.Allproj, "All projects button", driver);
		commonMethod.clickOnElement(Version_mgmt_page.Arrow, "Next page", driver);
		commonMethod.clickOnElement(Version_mgmt_page.Arrow, "Next page", driver);
		commonMethod.clickOnElement(Version_mgmt_page.Expandbutton, "Expand button", driver);
		commonMethod.clickOnElement(Version_mgmt_page.OtherVersion, "Other version", driver);
	}
	public void Impacted_cluster() throws IOException, UnsupportedFlavorException, AWTException
	{
		commonMethod.clickOnElement(Version_mgmt_page.CreateNewVersion, "Create New version button", driver);
		commonMethod.clickOnElement(Version_mgmt_page.CheckGenproj, "General project checkbox", driver);
		commonMethod.clickOnElement(Version_mgmt_page.Cancel, "Cancel button", driver);
		commonMethod.clickOnElement(Version_mgmt_page.Home, "Home button", driver);
	}
	public void Version_control_naming() throws IOException, UnsupportedFlavorException, AWTException
	{
		commonMethod.clickOnElement(Version_mgmt_page.StartProject, "Start Project button", driver);
		commonMethod.enterText(Version_mgmt_page.ProjectName, "Project Name", dataTable.getData("Test_Data", "ProjectName"), driver);
		commonMethod.enterText(Version_mgmt_page.Compound, "Compound Name", dataTable.getData("Test_Data", "Compound"), driver);
		commonMethod.clickOnElement(Version_mgmt_page.StartButton, "Start Button", driver);
		WebElement VersionNum = driver.findElement(By.xpath(Version_mgmt_page.VerNumber));
		commonMethod.getText(VersionNum, "Version", driver);
		
		commonMethod.clickOnElement(Version_mgmt_page.GenProjInfo, "General Project Info link", driver);
		commonMethod.clickOnElement(Version_mgmt_page.FieldImpa, "Field IMPA ", driver);
		commonMethod.clearElement(Version_mgmt_page.FieldDate, "Field Date clear", driver);
		commonMethod.enterText(Version_mgmt_page.FieldDate, "Field Date Entered", dataTable.getData("Test_Data", "FieldDate"), driver);
		commonMethod.selectOptionFromDropDown(Version_mgmt_page.FieldPhase,( dataTable.getData("Test_Data", "FieldPhase")) , driver);
		commonMethod.selectOptionFromDropDown(Version_mgmt_page.SponserName,( dataTable.getData("Test_Data", "SponserName")) , driver);
		commonMethod.selectOptionFromDropDown(Version_mgmt_page.SponserAddr,( dataTable.getData("Test_Data", "SponserAddr")) , driver);
		commonMethod.enterText(Version_mgmt_page.Therapeutic, "Therapeutic Area", dataTable.getData("Test_Data", "Therapeutic"), driver);
		commonMethod.clickOnElement(Version_mgmt_page.GcdoCro, "GCDO or CRO", driver);
		commonMethod.clickOnElement(Version_mgmt_page.PatientAlert, "Patient Alert card", driver);
		commonMethod.clickOnElement(Version_mgmt_page.Submit, "Submit for Approval", driver);
		commonMethod.clickOnElement(Version_mgmt_page.SaveSubmit, "Save&Submit for approval", driver);
		
		commonMethod.clickOnElement(Version_mgmt_page.CreateNewVersion, "Create New version button", driver);
		commonMethod.clickOnElement(Version_mgmt_page.CheckGenproj, "General project checkbox", driver);
		commonMethod.clickOnElement(Version_mgmt_page.newversion, "New version pop-up button", driver);
		WebElement VersionNum1 = driver.findElement(By.xpath(Version_mgmt_page.VerNumber));
		commonMethod.getText(VersionNum1, "Version", driver);
		commonMethod.clickOnElement(Version_mgmt_page.Projectpage, "All projects page", driver);
		commonMethod.clickOnElement(Version_mgmt_page.LastArrow, "Last page", driver);
		commonMethod.scrollDown(driver);
		
		List<WebElement> projOverviewTableRow = driver.findElements(By.xpath("//tbody//tr"));
		int i = projOverviewTableRow.size();
		WebElement insidetableVersion = driver.findElement(By.xpath("//tbody/tr[" + i + "]/td[1]/button"));
		String elementName = "New version project";
		wait.until(ExpectedConditions.elementToBeClickable(insidetableVersion));
		try {
			
			report.updateTestLog("Click Element ","Click on element " +elementName,elementName +" should be displayed and clicked.", elementName + " is displayed and clicked as expected .", Status.PASS);
			insidetableVersion.click();
			
		} catch (Exception e) {
			report.updateTestLog("Click Element ","Click on element " +elementName,elementName +" should be displayed and clicked.", elementName + " is not displayed and clicked as expected .", Status.FAIL);
		}
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
	}
	public void Version_workflow_task() throws IOException, UnsupportedFlavorException, AWTException
	{
		commonMethod.clickOnElement(Version_mgmt_page.Home, "Home button", driver);
		commonMethod.clickOnElement(Version_mgmt_page.StartProject, "Start Project button", driver);
		commonMethod.enterText(Version_mgmt_page.ProjectName, "Project Name", dataTable.getData("Test_Data", "Workflow_project"), driver);
		commonMethod.enterText(Version_mgmt_page.Compound, "Compound Name", dataTable.getData("Test_Data", "Workflow_compound"), driver);
		commonMethod.clickOnElement(Version_mgmt_page.StartButton, "Start Button", driver);
		
		commonMethod.clickOnElement(Version_mgmt_page.GenProjInfo, "General Project Info link", driver);
		commonMethod.clickOnElement(Version_mgmt_page.FieldImpa, "Field IMPA ", driver);
		commonMethod.clearElement(Version_mgmt_page.FieldDate, "Field Date clear", driver);
		commonMethod.enterText(Version_mgmt_page.FieldDate, "Field Date Entered", dataTable.getData("Test_Data", "FieldDate"), driver);
		commonMethod.selectOptionFromDropDown(Version_mgmt_page.FieldPhase,( dataTable.getData("Test_Data", "FieldPhase")) , driver);
		commonMethod.selectOptionFromDropDown(Version_mgmt_page.SponserName,( dataTable.getData("Test_Data", "SponserName")) , driver);
		commonMethod.selectOptionFromDropDown(Version_mgmt_page.SponserAddr,( dataTable.getData("Test_Data", "SponserAddr")) , driver);
		commonMethod.enterText(Version_mgmt_page.Therapeutic, "Therapeutic Area", dataTable.getData("Test_Data", "Therapeutic"), driver);
		commonMethod.clickOnElement(Version_mgmt_page.GcdoCro, "GCDO or CRO", driver);
		commonMethod.clickOnElement(Version_mgmt_page.PatientAlert, "Patient Alert card", driver);
		commonMethod.clickOnElement(Version_mgmt_page.Submit, "Submit for Approval", driver);
		commonMethod.clickOnElement(Version_mgmt_page.SaveSubmit, "Save&Submit for approval", driver);
		
		commonMethod.clickOnElement(Version_mgmt_page.Home, "Home button", driver);
		commonMethod.enterText(Version_mgmt_page.Searchbox, "Search box", dataTable.getData("Test_Data", "Workflow_project"), driver);
		commonMethod.clickOnElement(Version_mgmt_page.Searchbutton, "Search button", driver);
		commonMethod.clickOnElement(Version_mgmt_page.ResultProj, "Resulted project", driver);
		WebElement status = driver.findElement(By.xpath(Version_mgmt_page.Status));
		commonMethod.getText(status, "Status", driver);
		
		commonMethod.clickOnElement(Version_mgmt_page.ShowClusterbutton, "Show data cluster button", driver);
		commonMethod.clickOnElement(Version_mgmt_page.CreateNewVersion, "Create New version button", driver);
		commonMethod.clickOnElement(Version_mgmt_page.CheckGenproj, "General project checkbox", driver);
		commonMethod.clickOnElement(Version_mgmt_page.newversion, "New version pop-up button", driver);
		WebElement VersionNum2 = driver.findElement(By.xpath(Version_mgmt_page.VerNumber));
		commonMethod.getText(VersionNum2, "Version", driver);
		
		commonMethod.clickOnElement(Version_mgmt_page.Home, "Home button", driver);
		commonMethod.enterText(Version_mgmt_page.Searchbox, "Search box", dataTable.getData("Test_Data", "Workflow_project"), driver);
		commonMethod.clickOnElement(Version_mgmt_page.Searchbutton, "Search button", driver);
		commonMethod.clickOnElement(Version_mgmt_page.ResultProj, "Resulted project", driver);
		WebElement status1 = driver.findElement(By.xpath(Version_mgmt_page.Status));
		commonMethod.getText(status1, "Status", driver);
		
	}
	
}
