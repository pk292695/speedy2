package BusinessScenario;

import java.awt.AWTException;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.util.Properties;
import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;
import BusinessScenarioPage.Select_task_page;
import CommonMethod.CommonMethod;
import CommonMethod.ExpectedData;

public class Select_task extends ReusableLibrary{
	public Properties OR_properties;
	static boolean elementPresent;
	Select_task_page Selectpage = new Select_task_page(scriptHelper);

	CommonMethod commonMethod = new CommonMethod(scriptHelper);
	ExpectedData expectedData = new ExpectedData(scriptHelper);

	String packName = this.getClass().getPackage().getName();
	String className = this.getClass().getSimpleName();
	
	public Select_task(ScriptHelper scriptHelper)
	{
		super(scriptHelper);
	}
	public void Select_task_functionality() throws IOException, UnsupportedFlavorException, AWTException
	{
		commonMethod.navigateToUrl(dataTable.getData("functional_flow", "Application_URL"), driver);
		commonMethod.enterText(Select_task_page.Username, "User Name", dataTable.getData("functional_flow", "Username"), driver);
		commonMethod.enterText(Select_task_page.Password, "Password", dataTable.getData("functional_flow", "Password"), driver);
		commonMethod.clickOnElement(Select_task_page.Login, "Login button", driver);
		commonMethod.clickOnElement(Select_task_page.ProjCol, "Project Header", driver);
		commonMethod.clickOnElement(Select_task_page.CompCol, "Compound Header", driver);
		commonMethod.clickOnElement(Select_task_page.DataCol, "Data cluster Header", driver);
		commonMethod.clickOnElement(Select_task_page.submittedbyCol, "Submittedby Header", driver);
		commonMethod.clickOnElement(Select_task_page.SubmissionCol, "submission Header", driver);
		commonMethod.clickOnElement(Select_task_page.StatusCol, "Status Header", driver);
		commonMethod.clickOnElement(Select_task_page.FrstProj, "First project", driver);
		commonMethod.clickOnElement(Select_task_page.ShowClusterbutton, "Show data cluster button", driver);
		commonMethod.clickOnElement(Select_task_page.Account, "Account button", driver);
		commonMethod.clickOnElement(Select_task_page.Signout, "Sign-out button", driver);
	
	}
	public void Select_task_lblgrp() throws IOException, UnsupportedFlavorException, AWTException
	{
		//commonMethod.clickOnElement(Select_task_page.Account, "Account button", driver);
		//commonMethod.clickOnElement(Select_task_page.Signin, "Sign-in button", driver);
		commonMethod.enterText(Select_task_page.Username, "User Name", dataTable.getData("functional_flow", "Username1"), driver);
		commonMethod.enterText(Select_task_page.Password, "Password", dataTable.getData("functional_flow", "Password1"), driver);
		commonMethod.clickOnElement(Select_task_page.Login, "Login button", driver);
		commonMethod.clickOnElement(Select_task_page.Showfilter, "Show filter button", driver);
		commonMethod.clickOnElement(Select_task_page.CheckCntry, "Country depot checkbox", driver);
		commonMethod.clickOnElement(Select_task_page.OpenStatus, "Open checkbox", driver);
		commonMethod.clickOnElement(Select_task_page.Applyfilter, "Apply filter button", driver);
		commonMethod.scrollDown(driver);
		commonMethod.clickOnElement(Select_task_page.FrstProj, "First project", driver);
		commonMethod.clickOnElement(Select_task_page.ShowClusterbutton, "Show data cluster button", driver);
		commonMethod.clickOnElement(Select_task_page.Account, "Account button", driver);
		commonMethod.clickOnElement(Select_task_page.Signout, "Sign-out button", driver);
		
		
	}
	public void Select_task_btchdc() throws IOException, UnsupportedFlavorException, AWTException
	{
		//commonMethod.clickOnElement(Select_task_page.Account, "Account button", driver);
		//commonMethod.clickOnElement(Select_task_page.Signin, "Sign-in button", driver);
		commonMethod.enterText(Select_task_page.Username, "User Name", dataTable.getData("functional_flow", "Username2"), driver);
		commonMethod.enterText(Select_task_page.Password, "Password", dataTable.getData("functional_flow", "Password2"), driver);
		commonMethod.clickOnElement(Select_task_page.Login, "Login button", driver);
		
		commonMethod.enterText(Select_task_page.Searchbox, "Search box", dataTable.getData("Test_Data", "ProjectName"), driver);
		commonMethod.clickOnElement(Select_task_page.Searchbutton, "Search button", driver);
		commonMethod.clickOnElement(Select_task_page.ResultProj, "Resulted project", driver);
		commonMethod.clickOnElement(Select_task_page.ShowClusterbutton, "Show cluster button", driver);
		/*commonMethod.clickOnElement(Select_task_page.Showfilter, "Show filter button", driver);
		commonMethod.clickOnElement(Select_task_page.CheckGenKit, "General kit checkbox", driver);
		commonMethod.clickOnElement(Select_task_page.OpenStatus, "Open checkbox", driver);
		commonMethod.clickOnElement(Select_task_page.Applyfilter, "Apply filter button", driver);
		commonMethod.scrollDown(driver);
		commonMethod.clickOnElement(Select_task_page.FrstProj, "First project", driver);
		commonMethod.clickOnElement(Select_task_page.ShowClusterbutton, "Show data cluster button", driver);*/
		commonMethod.clickOnElement(Select_task_page.Account, "Account button", driver);
		commonMethod.clickOnElement(Select_task_page.Signout, "Sign-out button", driver);
		
	}
	public void Select_task_QA() throws IOException, UnsupportedFlavorException, AWTException
	{
		commonMethod.enterText(Select_task_page.Username, "User Name", dataTable.getData("functional_flow", "Username3"), driver);
		commonMethod.enterText(Select_task_page.Password, "Password", dataTable.getData("functional_flow", "Password3"), driver);
		commonMethod.clickOnElement(Select_task_page.Login, "Login button", driver);
		//commonMethod.clickOnElement(Select_task_page.Showfilter, "Show filter button", driver);
		//commonMethod.clickOnElement(Select_task_page.CheckGenKit, "General kit checkbox", driver);
		//commonMethod.clickOnElement(Select_task_page.OpenStatus, "Open checkbox", driver);
		//commonMethod.clickOnElement(Select_task_page.Applyfilter, "Apply filter button", driver);
		//commonMethod.scrollDown(driver);
		commonMethod.clickOnElement(Select_task_page.FrstProj, "First project", driver);
		commonMethod.clickOnElement(Select_task_page.ShowClusterbutton, "Show data cluster button", driver);
		commonMethod.clickOnElement(Select_task_page.Account, "Account button", driver);
		commonMethod.clickOnElement(Select_task_page.Signout, "Sign-out button", driver);
	}
	public void Select_task_melmgr() throws IOException, UnsupportedFlavorException, AWTException
	{
		commonMethod.enterText(Select_task_page.Username, "User Name", dataTable.getData("functional_flow", "Username4"), driver);
		commonMethod.enterText(Select_task_page.Password, "Password", dataTable.getData("functional_flow", "Password4"), driver);
		commonMethod.clickOnElement(Select_task_page.Login, "Login button", driver);
		commonMethod.clickOnElement(Select_task_page.Projectmgmt, "Project management button", driver);
		commonMethod.clickOnElement(Select_task_page.MelTempMgmt, "Mel template mgmt button", driver);
	}

}
