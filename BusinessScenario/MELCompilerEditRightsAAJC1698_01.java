package BusinessScenario;

import java.awt.AWTException;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;

import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;
import uri.FillCluster;
import uriPage.FillClusterPage;
import CommonMethod.CommonMethod;

public class MELCompilerEditRightsAAJC1698_01 extends ReusableLibrary{

	CommonMethod commonMethod = new CommonMethod(scriptHelper);
	FillCluster FC = new FillCluster(scriptHelper);
	
	public MELCompilerEditRightsAAJC1698_01(ScriptHelper scriptHelper)
	{
		super(scriptHelper);
	}
	String username = "Username";
	String username1 = "Username1";
	String sheetname = "functional_flow";
	String password = "Password";
	public void MELEditRights1() throws IOException, UnsupportedFlavorException, AWTException
	{
		commonMethod.navigateToUrl(dataTable.getData("functional_flow", "Application_URL"), driver);
		FC.LogIn(sheetname, username, password);
		commonMethod.clickOnElement(FillClusterPage.AllprojButton, "All projects button", driver);
		FC.FilterOMPDraft();
		commonMethod.clickOnElement(FillClusterPage.tableRow, "Resulted project", driver);
		commonMethod.clickOnElement(FillClusterPage.SyncwithOMPLink, "Sync wtih omp link", driver);
		commonMethod.clickOnElement(FillClusterPage.informationKitType, "Information per kit type section", driver);
		FC.SelectKitTypeomp();
		
		commonMethod.clickOnElement(FillClusterPage.MELcompilertab, "MEL compiler tab", driver);
		commonMethod.verifyElementNotPresent(FillClusterPage.melcombineButton, "MEL combine button", driver);
	commonMethod.verifyElementNotPresent(FillClusterPage.MELSubmitButton, "MEL submit button", driver);
	commonMethod.verifyElementPresent(FillClusterPage.ShowCntry, "Show countries link", driver);
	
	commonMethod.clickOnElement(FillClusterPage.ShowCntry, "Show countries link", driver);
	
	
	}
}
