package BusinessScenario;

import java.awt.AWTException;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;
import uri.FillCluster;
import uriPage.FillClusterPage;
import AutomationCoreFramework.Status;
import BusinessScenarioPage.AddRemoveKitTypePage;
import CommonMethod.CommonMethod;

public class CompleteGeneralTrialInfoAAJC4 extends ReusableLibrary{
	AddRemoveKitTypePage AddRemoveKiteTypPage = new AddRemoveKitTypePage(scriptHelper);

	CommonMethod commonMethod = new CommonMethod(scriptHelper);
	FillCluster FC = new FillCluster(scriptHelper);
	
	public CompleteGeneralTrialInfoAAJC4(ScriptHelper scriptHelper)
	{
		super(scriptHelper);
	}
	String username = "Username";
	String sheetname = "functional_flow";
	String password = "Password";
	public void GeneralProject() throws IOException, UnsupportedFlavorException, AWTException
	{
		commonMethod.navigateToUrl(dataTable.getData("functional_flow", "Application_URL"), driver);
		FC.LogIn(sheetname,username, password);
		FC.StartNewProject();
		commonMethod.clickOnElement(FillClusterPage.GenProjInfo, "general Project Info", driver);
		commonMethod.verifyElementPresent(FillClusterPage.FieldImpa, "Field IMPA", driver);
		commonMethod.verifyElementPresent(FillClusterPage.FieldDate, "Date Field", driver);
		commonMethod.verifyElementPresent(FillClusterPage.FieldPhase, "Phase Field", driver);
		commonMethod.verifyElementPresent(FillClusterPage.SponserName, "Sponser Name", driver);
		commonMethod.verifyElementPresent(FillClusterPage.SponserAddr, "Sponser Address", driver);
		commonMethod.verifyElementPresent(FillClusterPage.Therapeutic, "Therapeuitic", driver);
		commonMethod.verifyElementPresent(FillClusterPage.GcdoCro, "GCDO/CRO field", driver);
		commonMethod.verifyElementPresent(FillClusterPage.PatientAlert, "Patient Alert Field", driver);
		commonMethod.clickOnElement(FillClusterPage.SaveChanges, " save changes ", driver);
		commonMethod.clickOnElement(FillClusterPage.ContinueSave, "Continue save changes button", driver);
		
		FC.GeneralProjectInformation();
		commonMethod.clickOnElement(FillClusterPage.SaveChanges, " save changes ", driver);
		commonMethod.clickOnElement(FillClusterPage.ContinueSave, "Continue save changes button", driver);
		commonMethod.clickOnElement(FillClusterPage.HomeButton, "Continue save changes button", driver);
		commonMethod.clickOnElement(FillClusterPage.AllprojButton, "Continue save changes button", driver);
		
		commonMethod.clickOnElement(FillClusterPage.LastArrow, "Last Page", driver);
			
			List<WebElement> e = driver.findElements(By.xpath(FillClusterPage.tableRow));
					int q = e.size();
					System.out.println(q);
					
									
					WebElement ProjName = driver.findElement(By.xpath("//tr["+q+"]/td[2]"));
					String project = ProjName.getText();
					commonMethod.clickOnElementWeb(ProjName, "Selected the last saved Project", driver);
					commonMethod.clickOnElement(FillClusterPage.GenProjInfo, "general Project Info", driver);
					
			 

		
		
		
	}
			
		}




