package BusinessScenario;

import java.awt.AWTException;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;

import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;
import uri.FillCluster;
import uriPage.FillClusterPage;
import CommonMethod.CommonMethod;

public class TextSignOff3_AAJC1061 extends ReusableLibrary{

	CommonMethod commonMethod = new CommonMethod(scriptHelper);
	FillCluster FC = new FillCluster(scriptHelper);
	
	public TextSignOff3_AAJC1061(ScriptHelper scriptHelper)
	{
		super(scriptHelper);
	}
	String username = "Username";
	String sheetname = "functional_flow";
	String password = "Password";
	String username2 = "Username2";
	
	public void TextSignOff3() throws IOException, UnsupportedFlavorException, AWTException
	{
		commonMethod.navigateToUrl(dataTable.getData("functional_flow", "Application_URL"), driver);
		FC.LogIn(sheetname,username, password);
		FC.StartNewProject();
		commonMethod.clickOnElement(FillClusterPage.informationKitType, "Information Kit Type", driver);
		FC.createKitType(dataTable.getData("Test_Data", "Kitname"));
	
		FC.GeneralKitTypeInfo();
		FC.PackTypeInfo();
		FC.SubmitGeneralKitPackType();
		
		commonMethod.clickOnElement(FillClusterPage.informationKitType, "Information per kit type section", driver);
		commonMethod.clickOnElement(FillClusterPage.GeneralKitTypeInfo, "General kit Type tab", driver);
		commonMethod.getText(FillClusterPage.BatchdocGenKit, "Batchdoc approval", driver);
		commonMethod.getText(FillClusterPage.OpenGenkit, "Open status", driver);
		FC.Logout();
		
		//General kit and Pack type Approval
		driver.navigate().refresh();
		FC.LogIn(sheetname, username2, password);
		commonMethod.enterText(FillClusterPage.ProjectSearch, "Workflow inbox Project search", FillCluster.name, driver);
		commonMethod.clickOnElement(FillClusterPage.WIProjectSearch, "Workflow inbox search", driver);
		commonMethod.clickOnElement(FillClusterPage.tableRow, "Resulted project", driver);
		commonMethod.clickOnElement(FillClusterPage.ShowDataCluster, "Show data cluster button", driver);
		commonMethod.clickOnElement(FillClusterPage.GenKitPackApproveButton, "Approve button", driver);
	}
}
