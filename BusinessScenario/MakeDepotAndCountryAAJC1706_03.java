package BusinessScenario;

import java.awt.AWTException;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;
import uri.FillCluster;
import uriPage.FillClusterPage;
import AutomationCoreFramework.Status;
import BusinessScenarioPage.AddRemoveKitTypePage;
import CommonMethod.CommonMethod;

public class MakeDepotAndCountryAAJC1706_03 extends ReusableLibrary{
	AddRemoveKitTypePage AddRemoveKiteTypPage = new AddRemoveKitTypePage(scriptHelper);

	CommonMethod commonMethod = new CommonMethod(scriptHelper);
	FillCluster FC = new FillCluster(scriptHelper);
	
	public MakeDepotAndCountryAAJC1706_03(ScriptHelper scriptHelper)
	{
		super(scriptHelper);
	}
	String username = "Username";
	String username1 = "Username1";
	String sheetname = "functional_flow";
	String password = "Password";
	String KitTypeName = "JKP";
	public void MakeDepotAndCountry3() throws IOException, UnsupportedFlavorException, AWTException
	{
		commonMethod.navigateToUrl(dataTable.getData("functional_flow", "Application_URL"), driver);
		FC.LogIn(sheetname,username, password);
		FC.StartNewProject();
		commonMethod.clickOnElement(FillClusterPage.Country, "Country Information", driver);
		commonMethod.clickOnElement(FillClusterPage.AddParticipatingCountry, "Add Participating country Button", driver);
		commonMethod.verifyElementNotPresent(FillClusterPage.countryinfo_DepotCountry, "Depot Country", driver);
		commonMethod.verifyElementNotPresent(FillClusterPage.countryingo_supplying_depot, "Supplying Depot", driver);
		commonMethod.clickOnElement(FillClusterPage.Country_cancelButton, "Country Cancel Button", driver);
		FC.CountryInformation();
		FC.SubmitCountryInformation();
		
		FC.Logout();
	}
	public void MakeDepotCountryApproval3() throws IOException, UnsupportedFlavorException, AWTException
	{
		driver.navigate().refresh();
		FC.LogIn(sheetname, username1, password);
		commonMethod.enterText(FillClusterPage.ProjectSearch, "Workflow inbox Project search", FillCluster.name, driver);
		commonMethod.clickOnElement(FillClusterPage.WIProjectSearch, "Workflow inbox search", driver);
		commonMethod.clickOnElement(FillClusterPage.tableRow, "Resulted project", driver);
		commonMethod.clickOnElement(FillClusterPage.ShowDataCluster, "Show data cluster button", driver);
		commonMethod.clickOnElement(FillClusterPage.CntryProjApproveButton, "Approve button", driver);
		FC.LogIn(sheetname, username1, password);
	}
				
	}	




