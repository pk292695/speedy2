package BusinessScenario;

import java.awt.AWTException;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;

import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;
import uri.FillCluster;
import uriPage.FillClusterPage;
import BusinessScenarioPage.BrightstockSubmitPage;
import CommonMethod.CommonMethod;

public class SubmitApprovalBrightstockAAJC295 extends ReusableLibrary{

	FillCluster FC = new FillCluster(scriptHelper);
	CommonMethod commonMethod = new CommonMethod(scriptHelper);
	
	public SubmitApprovalBrightstockAAJC295(ScriptHelper scriptHelper)
	{
		super(scriptHelper);
	}
	String username = "Username";
	String sheetname = "functional_flow";
	String password = "Password";
	String username1 = "Username1";
	public void Submit_brightstock() throws IOException, UnsupportedFlavorException, AWTException
	{
		commonMethod.navigateToUrl(dataTable.getData("functional_flow", "Application_URL"), driver);
		FC.LogIn(sheetname, username, password);
		commonMethod.clickOnElement(FillClusterPage.AllprojButton, "All projects button", driver);
		FC.FilterProjectnameactive(dataTable.getData("Test_Data", "ProjectName"));
		commonMethod.clickOnElement(FillClusterPage.tableRow, "Resulted project", driver);
		commonMethod.clickOnElement(FillClusterPage.SyncwithOMPLink, "Sync with omp link", driver);
		FC.BSGenProjInfo();
		commonMethod.clickOnElement(FillClusterPage.Country, "Country Information tab", driver);
		commonMethod.clickOnElement(FillClusterPage.informationKitType, "Information per kit type", driver);
		FC.SelectKitTypeomp();
		FC.BSGeneralKit();
		FC.PackTypeInfo();
		FC.BSDrugproduct();
		FC.PrimaryPackInfo();
		commonMethod.clickOnElement(FillClusterPage.SaveChanges, "Save changes", driver);
		commonMethod.clickOnElement(FillClusterPage.ContinueSave, "Continue save changes", driver);
		FC.Logout();
	}
	public void Brightstock_LBLGRP() throws IOException, UnsupportedFlavorException, AWTException, InterruptedException
	{
		driver.navigate().refresh();
		FC.LogIn(sheetname, username1, password);
		commonMethod.clickOnElement(FillClusterPage.AllprojButton, "All projects button", driver);
		FC.FilterProjectnameactive(dataTable.getData("Test_Data", "ProjectName"));
		commonMethod.clickOnElement(FillClusterPage.tableRow, "Resulted project", driver);
		
		
		commonMethod.clickOnElement(BrightstockSubmitPage.MelCompTab, "MEL compiler tab", driver);
		commonMethod.clickOnElement(BrightstockSubmitPage.PackOpt, "Pack type checkbox", driver);
		commonMethod.clickOnElement(BrightstockSubmitPage.LabelOpt, "Labelgroup checkbox", driver);
		commonMethod.clickOnElement(FillClusterPage.melcombineButton, "Combine MEL", driver);
		commonMethod.clickOnElement(FillClusterPage.template, "Template", driver);
		
		commonMethod.enterText(FillClusterPage.template,"Template Option",dataTable.getData("Test_Data", "MELTemplate"), driver);
		commonMethod.clickDown(FillClusterPage.template,driver);
		commonMethod.clickEnter(FillClusterPage.template, driver);
		commonMethod.clickOnElement(FillClusterPage.SearchButton, "Search Button", driver);
		commonMethod.clickOnElement(FillClusterPage.searchField, "Element", driver);
		commonMethod.clickOnElement(FillClusterPage.addToMEL, "Element", driver);
		
		commonMethod.clickOnElement(FillClusterPage.NGCStaticReference, "NGC Static Reference button", driver);
		commonMethod.clickOnElement(FillClusterPage.SearchButton, "Search Button", driver);
		commonMethod.clickOnElement(FillClusterPage.ngcField, "Element", driver);
		commonMethod.clickOnElement(FillClusterPage.addToMEL, "Element", driver);
		commonMethod.scrollDown(driver);
		
		commonMethod.clickOnElement(FillClusterPage.DoneButton, "Done Button", driver);
		commonMethod.clickOnElement(FillClusterPage.SaveChanges, "Save Changes Button", driver);
		commonMethod.clickOnElement(FillClusterPage.ContinueSave, "Save Changes Button", driver);
		FC.Logout();
	
	}
	public void Submit_Editor() throws IOException, UnsupportedFlavorException, AWTException
	{
		driver.navigate().refresh();
		FC.LogIn(sheetname, username, password);
		commonMethod.clickOnElement(FillClusterPage.AllprojButton, "All projects button", driver);
		FC.FilterProjectnameactive(dataTable.getData("Test_Data", "ProjectName"));
		commonMethod.clickOnElement(FillClusterPage.tableRow, "Resulted project", driver);
		commonMethod.clickOnElement(BrightstockSubmitPage.GenProjInfo, "General project Info button", driver);
		commonMethod.VerifyElementEnabled(BrightstockSubmitPage.BrightstkSubmit, "Submit button", driver);
		commonMethod.clickOnElement(FillClusterPage.BrightstkSubmit, "Brightstock submit button", driver);
	}

}
