package BusinessScenario;

import java.awt.AWTException;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;

import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;
import uri.FillCluster;
import uriPage.FillClusterPage;
import CommonMethod.CommonMethod;

public class RemoveCountriesAAJC1010 extends ReusableLibrary{

	CommonMethod commonMethod = new CommonMethod(scriptHelper);
	FillCluster FC = new FillCluster(scriptHelper);
	
	public RemoveCountriesAAJC1010(ScriptHelper scriptHelper)
	{
		super(scriptHelper);
	}
	String username = "Username";
	String sheetname = "functional_flow";
	String password = "Password";
	public void RemoveCountries() throws IOException, UnsupportedFlavorException, AWTException
	{
		commonMethod.navigateToUrl(dataTable.getData("functional_flow", "Application_URL"), driver);
		FC.LogIn(sheetname,username, password);
		FC.StartNewProject();
		FC.CountryInformation();
		commonMethod.clickOnElement(FillClusterPage.EditCountryLink, "Edit Button", driver);
		commonMethod.clickOnElement(FillClusterPage.deleteButton, "Delete Button", driver);
		
		
	}
}
