package BusinessScenario;

import java.awt.AWTException;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;


import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;


import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;
import uri.FillCluster;
import uriPage.FillClusterPage;
import AutomationCoreFramework.Status;
import BusinessScenarioPage.AddRemoveKitTypePage;
import CommonMethod.CommonMethod;

public class ShowOMPProductIDAAJC943 extends ReusableLibrary{
	AddRemoveKitTypePage AddRemoveKiteTypPage = new AddRemoveKitTypePage(scriptHelper);

	CommonMethod commonMethod = new CommonMethod(scriptHelper);
	FillCluster FC = new FillCluster(scriptHelper);
	
	public ShowOMPProductIDAAJC943(ScriptHelper scriptHelper)
	{
		super(scriptHelper);
	}
	String username = "Username";
	String sheetname = "functional_flow";
	String password = "Password";
	public void showOMPprod() throws IOException, UnsupportedFlavorException, AWTException
	{
		commonMethod.navigateToUrl(dataTable.getData("functional_flow", "Application_URL"), driver);
		FC.LogIn(sheetname,username, password);
		commonMethod.clickOnElement(FillClusterPage.AllprojButton, "All Project Button", driver);
		FC.FilterBrightDraft();
		commonMethod.clickOnElement(FillClusterPage.tableRow, "Select First row of data", driver);
		commonMethod.clickOnElement(FillClusterPage.SyncwithOMPLink, "Sync With OMP Link", driver);
		commonMethod.clickOnElement(FillClusterPage.informationKitType, "information Kit type Button", driver);
		commonMethod.selectOptionFromDropDown(FillClusterPage.KitTypeDropDown, "Canagliflozin 100 mg_1x10", driver);
		commonMethod.verifyElementPresent(FillClusterPage.ompNumberlabel, "OMP number Text", driver);
		WebElement ompnumber = driver.findElement(By.xpath(FillClusterPage.ompnumber));
		
		
			if(ompnumber.isDisplayed()){
				System.out.println("pass");
				report.updateTestLog("Omp number Visible","check whether OMP Number should be visible" +ompnumber.getText(),ompnumber.getText()+ "OmpNumber is displayed" + " is displayed", Status.PASS);
			}
			else
			{System.out.println("fail");
			report.updateTestLog("Omp number Visible","check whether OMP Number should be visible" , "OmpNumber is not displayed" + " is displayed", Status.FAIL);
			}
		}
		
	}

