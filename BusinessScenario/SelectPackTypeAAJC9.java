package BusinessScenario;

import java.awt.AWTException;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;
import uri.FillCluster;
import uriPage.FillClusterPage;
import AutomationCoreFramework.Status;
import BusinessScenarioPage.AddRemoveKitTypePage;
import CommonMethod.CommonMethod;

public class SelectPackTypeAAJC9 extends ReusableLibrary{
	AddRemoveKitTypePage AddRemoveKiteTypPage = new AddRemoveKitTypePage(scriptHelper);

	CommonMethod commonMethod = new CommonMethod(scriptHelper);
	FillCluster FC = new FillCluster(scriptHelper);
	
	public SelectPackTypeAAJC9(ScriptHelper scriptHelper)
	{
		super(scriptHelper);
	}
	String username = "Username";
	String sheetname = "functional_flow";
	String password = "Password";
	public void infoperkittypeOMP() throws IOException, UnsupportedFlavorException, AWTException
	{
		commonMethod.navigateToUrl(dataTable.getData("functional_flow", "Application_URL"), driver);
		FC.LogIn(sheetname,username, password);
		commonMethod.clickOnElement(FillClusterPage.AllprojButton, "All Project Button", driver);
		FC.FilterOMPDraft();
		commonMethod.clickOnElement(FillClusterPage.tableRow, "First Row Data", driver);
		commonMethod.clickOnElement(FillClusterPage.SyncwithOMPLink, "Sync with OMP link", driver);
		commonMethod.clickOnElement(FillClusterPage.informationKitType, "Information Per Kit type", driver);
		FC.SelectKitTypeomp();
		commonMethod.clickOnElement(FillClusterPage.PacktypeTab, "Pack type tab", driver);
		commonMethod.clickOnElement(FillClusterPage.Packoption1, "Pack type option", driver);
		
		}
	public void infoperkittypenonOMP() throws IOException, UnsupportedFlavorException, AWTException{
		commonMethod.clickOnElement(FillClusterPage.Proj, "All projects page", driver); 
		FC.FilterNonOMPDraft();
		commonMethod.clickOnElement(FillClusterPage.tableRow, "First Row Data", driver);
		FC.createKitType(dataTable.getData("Test_Data", "Kitname"));
		commonMethod.clickOnElement(FillClusterPage.informationKitType, "Information Kit Type", driver);
		commonMethod.clickOnElement(FillClusterPage.PacktypeTab, "Pack Type Tab", driver);
		commonMethod.clickOnElement(FillClusterPage.Packoption1, "Pack type option", driver);
	}
			
			
		}



