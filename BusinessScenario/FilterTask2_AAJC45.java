package BusinessScenario;

import java.awt.AWTException;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;
import uri.FillCluster;
import uriPage.FillClusterPage;
import AutomationCoreFramework.Status;
import BusinessScenarioPage.AddRemoveKitTypePage;
import CommonMethod.CommonMethod;

public class FilterTask2_AAJC45 extends ReusableLibrary{
	AddRemoveKitTypePage AddRemoveKiteTypPage = new AddRemoveKitTypePage(scriptHelper);

	CommonMethod commonMethod = new CommonMethod(scriptHelper);
	FillCluster FC = new FillCluster(scriptHelper);
	
	public FilterTask2_AAJC45(ScriptHelper scriptHelper)
	{
		super(scriptHelper);
	}
	String username = "Username";
	String sheetname = "functional_flow";
	String password = "Password";
	String KitTypeName = "JKP";
	public void Filter_Task_2() throws IOException, UnsupportedFlavorException, AWTException
	{
		commonMethod.navigateToUrl(dataTable.getData("functional_flow", "Application_URL"), driver);
		FC.LogIn(sheetname,username, password);
		commonMethod.clickOnElement(FillClusterPage.WIFilterButton, "show filter", driver);
		commonMethod.clickOnElement(FillClusterPage.WIHideFilter, "Hide filter", driver);
		commonMethod.verifyElementPresent(FillClusterPage.WIDOSHead, "Workflow Date of submission", driver);
		commonMethod.clickOnElement(FillClusterPage.WIProjectHead, "Workflow Project header", driver);
	commonMethod.clickOnElement(FillClusterPage.WICompoundHead, "Workflow compound header", driver);
	commonMethod.clickOnElement(FillClusterPage.WIDataClusterHead, "Workflow Data cluster header", driver);
	commonMethod.clickOnElement(FillClusterPage.WISubmittedByHead, "Workflow submitted by header", driver);
	commonMethod.clickOnElement(FillClusterPage.WIStatusHead, "Workflow status header", driver);
	}	


}


