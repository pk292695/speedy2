package BusinessScenario;

import java.awt.AWTException;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;
import uri.FillCluster;
import uriPage.FillClusterPage;
import BusinessScenarioPage.Login_page;
import CommonMethod.CommonMethod;
import CommonMethod.ExpectedData;

// Login with NT UserName and Password AAJC-1
public class LoginAAJC54_01 extends ReusableLibrary{
	public Properties OR_properties;
	static boolean elementPresent;
	Login_page Loginpage = new Login_page(scriptHelper);
	FillCluster FC = new FillCluster(scriptHelper);
	CommonMethod commonMethod = new CommonMethod(scriptHelper);
	ExpectedData expectedData = new ExpectedData(scriptHelper);

	String packName = this.getClass().getPackage().getName();
	String className = this.getClass().getSimpleName();
	
	String Sheetname = "functional_flow";
	String username0 = "UserName";
	String username1 = "UserName1";
	String username2 = "UserName2";
	String username3 = "UserName3";
	String username4 = "UserName4";
	String passowrd = "Password";
	String password = "Password4";
	
	
	
	public LoginAAJC54_01(ScriptHelper scriptHelper)
	{
		super(scriptHelper);
	}
	public void Login1() throws IOException, UnsupportedFlavorException, AWTException
	{
		commonMethod.navigateToUrl(dataTable.getData(Sheetname, "Application_URL"), driver);
		FC.LogIn(Sheetname, username0, password);
		commonMethod.VerifyElementEnabled(FillClusterPage.StartNewProject, "Start new Project Button", driver);
		commonMethod.clickOnElement(FillClusterPage.AllprojButton, "All Project button", driver);
		FC.Logout();
		
		
		
	}
	public void Login2() throws IOException, UnsupportedFlavorException, AWTException
	{
		driver.navigate().refresh();
		FC.LogIn(Sheetname, username1, passowrd);
		commonMethod.clickOnElement(FillClusterPage.AllprojButton, "All Project button", driver);
		FC.FilterNonOMPDraft();
		commonMethod.clickOnElement(FillClusterPage.tableRow, "Click on resulted project", driver);
		commonMethod.clickOnElement(FillClusterPage.informationKitType, "Information Kit type", driver);
		commonMethod.clickOnElement(FillClusterPage.MELcompilertab, "MEL compiler Tab", driver);
		commonMethod.VerifyElementEnabled(FillClusterPage.Editlabel, "Edit Lable Group", driver);
		commonMethod.enterText(FillClusterPage.MelComments, "Comments", dataTable.getData("Test_Data", "Comments"), driver);
		
	
	}
	
	}
	


