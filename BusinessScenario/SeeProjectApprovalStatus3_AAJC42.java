package BusinessScenario;

import java.awt.AWTException;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;

import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;
import uri.FillCluster;
import uriPage.FillClusterPage;
import CommonMethod.CommonMethod;

public class SeeProjectApprovalStatus3_AAJC42 extends ReusableLibrary{

	CommonMethod commonMethod = new CommonMethod(scriptHelper);
	FillCluster FC = new FillCluster(scriptHelper);
	
	public SeeProjectApprovalStatus3_AAJC42(ScriptHelper scriptHelper)
	{
		super(scriptHelper);
	}
	String username = "Username";
	String username1 = "Username1";
	String username2 = "Username2";
	String sheetname = "functional_flow";
	String password = "Password";
	public void SeeProjectApproval3() throws IOException, UnsupportedFlavorException, AWTException
	{
		commonMethod.navigateToUrl(dataTable.getData("functional_flow", "Application_URL"), driver);
		FC.LogIn(sheetname,username, password);
		FC.StartNewProject();
		FC.GeneralProjectInformation();
		FC.SubmitGeneralProjectInformation();
		FC.CountryInformation();
		FC.SubmitCountryInformation();

		commonMethod.clickOnElement(FillClusterPage.informationKitType, "Information per kit type", driver);
		FC.createKitType(dataTable.getData("Test_Data", "Kitname"));
		FC.GeneralKitTypeInfo();
		FC.SubmitGeneralKitPackType();
		FC.DrugProductInfo();
		FC.SubmitDrugPrimary();
		
		//General project Information Approval
				commonMethod.clickOnElement(FillClusterPage.HomeButton, "Continue save changes button", driver);
				commonMethod.enterText(FillClusterPage.ProjectSearch, "Workflow inbox Project search", FillCluster.name, driver);
				commonMethod.clickOnElement(FillClusterPage.WIProjectSearch, "Workflow inbox search", driver);
				commonMethod.clickOnElement(FillClusterPage.tableRow, "Resulted project", driver);
				commonMethod.clickOnElement(FillClusterPage.ShowDataCluster, "Show data cluster button", driver);
				commonMethod.clickOnElement(FillClusterPage.GenProjApproveButton, "Approve button", driver);
				FC.LogIn(sheetname, username, password);
				
				FC.Logout();
				//Country Information Approval
				driver.navigate().refresh();
				FC.LogIn(sheetname, username1, password);
				commonMethod.enterText(FillClusterPage.ProjectSearch, "Workflow inbox Project search", FillCluster.name, driver);
				commonMethod.clickOnElement(FillClusterPage.WIProjectSearch, "Workflow inbox search", driver);
				commonMethod.clickOnElement(FillClusterPage.tableRow, "Resulted project", driver);
				commonMethod.clickOnElement(FillClusterPage.ShowDataCluster, "Show data cluster button", driver);
				commonMethod.clickOnElement(FillClusterPage.CntryProjApproveButton, "Approve button", driver);
				FC.LogIn(sheetname, username1, password);
				
				FC.Logout();
				//General kit and Pack type Approval
				driver.navigate().refresh();
				FC.LogIn(sheetname, username2, password);
				commonMethod.enterText(FillClusterPage.ProjectSearch, "Workflow inbox Project search", FillCluster.name, driver);
				commonMethod.clickOnElement(FillClusterPage.WIProjectSearch, "Workflow inbox search", driver);
				commonMethod.clickOnElement(FillClusterPage.tableRow, "Resulted project", driver);
				commonMethod.clickOnElement(FillClusterPage.ShowDataCluster, "Show data cluster button", driver);
				commonMethod.clickOnElement(FillClusterPage.GenKitPackApproveButton, "Approve button", driver);
				FC.LogIn(sheetname, username2, password);
		
				FC.Logout();
		
		//Checking approval status
				driver.navigate().refresh();
				FC.LogIn(sheetname, username, password);		
		commonMethod.clickOnElement(FillClusterPage.AllprojButton, "All projects button", driver);
		FC.FilterProjectnameactive(FillCluster.name);
		commonMethod.clickOnElement(FillClusterPage.tableRow, "Resulted project", driver);
		
		commonMethod.clickOnElement(FillClusterPage.CheckApproveStatus, "Check approval status", driver);
		commonMethod.getText(FillClusterPage.AllProjRow, "All project info", driver);
		commonMethod.clickOnElement(FillClusterPage.GenProjRow, "General project", driver);
		commonMethod.getText(FillClusterPage.StatusInsideCheckApproval, "General project Info status", driver);
		commonMethod.clickOnElement(FillClusterPage.CountryProjRow, "Country Information", driver);
		commonMethod.getText(FillClusterPage.StatusInsideCheckApproval, "General project Info status", driver);
		commonMethod.clickOnElement(FillClusterPage.KitRow, "Kit information", driver);
		commonMethod.getText(FillClusterPage.GenKitPackInsideCheckApproval, "General kit & pack type Info status", driver);
		commonMethod.getText(FillClusterPage.DrugPrimInsideCheckApproval, "Drug primary Info status", driver);
	
	
	}
}
