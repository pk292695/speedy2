package BusinessScenario;

import java.awt.AWTException;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.util.Arrays;

import org.apache.commons.io.comparator.LastModifiedFileComparator;
import org.apache.commons.io.filefilter.WildcardFileFilter;

import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;
import uri.FillCluster;
import uriPage.FillClusterPage;
import CommonMethod.CommonMethod;

public class DownloadIMPA_AAJC33 extends ReusableLibrary{

	CommonMethod commonMethod = new CommonMethod(scriptHelper);
	FillCluster FC = new FillCluster(scriptHelper);
	
	public static String dwnloadpath = "D:\\Users\\" + System.getProperty("user.name") + "\\Downloads";
	public DownloadIMPA_AAJC33(ScriptHelper scriptHelper)
	{
		super(scriptHelper);
	}
	String username = "Username";
	String sheetname = "functional_flow";
	String password = "Password";
	
	public void DownloadIMPA() throws IOException, UnsupportedFlavorException, AWTException
	{
		commonMethod.navigateToUrl(dataTable.getData("functional_flow", "Application_URL"), driver);
		FC.LogIn(sheetname,username, password);
		commonMethod.clickOnElement(FillClusterPage.AllprojButton, "All project button", driver);
		FC.FilterNonOMPAwaiting();
		commonMethod.clickOnElement(FillClusterPage.tableRow, "Resulted project", driver);
		commonMethod.clickOnElement(FillClusterPage.DownloadIMPA, "Download e-IMPA button", driver);
		 File getLatestFile = getLatestFilefromDir(dwnloadpath);
         String fileName = getLatestFile.getName();
         System.out.println(fileName);
         commonMethod.getText(fileName, "Name of PDF", driver);
		
     }
	
	private File getLatestFilefromDir(String filePath) {
        File theNewestFile = null;
        File dir = new File(filePath);
        FileFilter fileFilter = new WildcardFileFilter("*.pdf");
        File[] files = dir.listFiles(fileFilter);

        if (files.length > 0) {
            /** The latest file comes first **/
            Arrays.sort(files, LastModifiedFileComparator.LASTMODIFIED_REVERSE);
            theNewestFile = files[0];
            System.out.println(String.valueOf(theNewestFile));
            //LOG.debug(String.valueOf(theNewestFile));
        }

        return theNewestFile;
    }
}
