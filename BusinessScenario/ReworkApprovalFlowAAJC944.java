package BusinessScenario;

import java.awt.AWTException;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;
import uri.FillCluster;
import uriPage.FillClusterPage;
import AutomationCoreFramework.Status;
import BusinessScenarioPage.AddRemoveKitTypePage;
import CommonMethod.CommonMethod;

public class ReworkApprovalFlowAAJC944 extends ReusableLibrary{

	CommonMethod commonMethod = new CommonMethod(scriptHelper);
	FillCluster FC = new FillCluster(scriptHelper);
	
	public ReworkApprovalFlowAAJC944(ScriptHelper scriptHelper)
	{
		super(scriptHelper);
	}
	String username = "Username";
	String sheetname = "functional_flow";
	String password = "Password";
	
	public void Multiple_packtype() throws IOException, UnsupportedFlavorException, AWTException
	{
		commonMethod.navigateToUrl(dataTable.getData("functional_flow", "Application_URL"), driver);
		FC.LogIn(sheetname,username, password);
		FC.StartNewProject();
		
		FC.GeneralProjectInformation();
		FC.SubmitGeneralProjectInformation();
		commonMethod.clickOnElement(FillClusterPage.GenProjInfo, "General project Info tab", driver);
		commonMethod.getText(FillClusterPage.CSITSM, "CSI/TSM approval", driver);
		commonMethod.getText(FillClusterPage.OpenCSITSM, "Open status", driver);
		
		FC.CountryInformation();
		FC.SubmitCountryInformation();
		commonMethod.clickOnElement(FillClusterPage.Country, "Country Info tab", driver);
		commonMethod.getText(FillClusterPage.LabelCountry, "Label approval", driver);
		commonMethod.getText(FillClusterPage.OpenCountry, "Open status", driver);
		
		commonMethod.clickOnElement(FillClusterPage.informationKitType, "Information per kit type section", driver);
		FC.createKitType(dataTable.getData("Test_Data", "Kitname"));
		
		FC.GeneralKitTypeInfo();
		FC.PackTypeInfo();
		FC.SubmitGeneralKitPackType();
		commonMethod.clickOnElement(FillClusterPage.informationKitType, "Information per kit type section", driver);
		commonMethod.clickOnElement(FillClusterPage.GeneralKitTypeInfo, "General kit Type tab", driver);
		commonMethod.getText(FillClusterPage.BatchdocGenKit, "Batchdoc approval", driver);
		commonMethod.getText(FillClusterPage.OpenGenkit, "Open status", driver);
		
		FC.DrugProductInfo();
		FC.PrimaryPackInfo();
		FC.SubmitDrugPrimary();
		commonMethod.clickOnElement(FillClusterPage.informationKitType, "Information per kit type section", driver);
		commonMethod.clickOnElement(FillClusterPage.DrugProductInfo, "Drug product Info tab", driver);
		commonMethod.getText(FillClusterPage.BatchdocDrug, "Batchdoc approval", driver);
		commonMethod.getText(FillClusterPage.OpenDrugProd, "Open status", driver);
	}	

}


