package BusinessScenario;

import java.awt.AWTException;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.util.List;




import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;
import uri.FillCluster;
import uriPage.FillClusterPage;
import AutomationCoreFramework.Status;
import CommonMethod.CommonMethod;


public class VersionMgmt_ControlNamingAAJC775 extends ReusableLibrary{

	CommonMethod commonMethod = new CommonMethod(scriptHelper);
	FillCluster FC = new FillCluster(scriptHelper);
	
	public VersionMgmt_ControlNamingAAJC775(ScriptHelper scriptHelper)
	{
		super(scriptHelper);
	}
	String username = "Username";
	String sheetname = "functional_flow";
	String password = "Password";
	
	public void Version_control_naming() throws IOException, UnsupportedFlavorException, AWTException
	{
		commonMethod.navigateToUrl(dataTable.getData("functional_flow", "Application_URL"), driver);
		FC.LogIn(sheetname, username, password);
		FC.StartNewProject();
		commonMethod.getText(FillClusterPage.VerNumber, "Version number", driver);
		FC.GeneralProjectInformation();
		FC.SubmitGeneralProjectInformation();
		commonMethod.clickOnElement(FillClusterPage.CreateNewVersion, "Create New version button", driver);
		commonMethod.clickOnElement(FillClusterPage.CheckGenproj, "General project checkbox", driver);
		commonMethod.clickOnElement(FillClusterPage.newversion, "New version pop-up button", driver);
		commonMethod.getText(FillClusterPage.VerNumber, "Version number", driver);
		
		commonMethod.clickOnElement(FillClusterPage.Proj, "All projects page", driver);
		commonMethod.clickOnElement(FillClusterPage.LastArrow, "Last page", driver);
		commonMethod.scrollDown(driver);
		
		List<WebElement> projOverviewTableRow = driver.findElements(By.xpath(FillClusterPage.tableRow));
		int i = projOverviewTableRow.size();
		WebElement insidetableVersion = driver.findElement(By.xpath("//tbody/tr[" + i + "]/td[1]/button"));
		String elementName = "New version project";
		wait.until(ExpectedConditions.elementToBeClickable(insidetableVersion));
		try {
			
			report.updateTestLog("Click Element ","Click on element " +elementName,elementName +" should be displayed and clicked.", elementName + " is displayed and clicked as expected .", Status.PASS);
			insidetableVersion.click();
			
		} catch (Exception e) {
			report.updateTestLog("Click Element ","Click on element " +elementName,elementName +" should be displayed and clicked.", elementName + " is not displayed and clicked as expected .", Status.FAIL);
		}
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
}
	
}
