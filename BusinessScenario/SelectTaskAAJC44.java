package BusinessScenario;

import java.awt.AWTException;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;

import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;
import uri.FillCluster;
import uriPage.FillClusterPage;
import BusinessScenarioPage.Select_task_page;
import CommonMethod.CommonMethod;

public class SelectTaskAAJC44 extends ReusableLibrary{

	CommonMethod commonMethod = new CommonMethod(scriptHelper);
	FillCluster FC = new FillCluster(scriptHelper);
	public SelectTaskAAJC44(ScriptHelper scriptHelper)
	{
		super(scriptHelper);
	}
	String sheetname = "functional_flow";
	String username = "Username";
	String password = "Password";
	String username1 = "Username1";
	String username2 = "Username2";
	String username3 = "Username3";
	String username4= "Username4";
	String password4 = "Password4";
	public void Select_task_functionality() throws IOException, UnsupportedFlavorException, AWTException
	{
		commonMethod.navigateToUrl(dataTable.getData("functional_flow", "Application_URL"), driver);
		FC.LogIn(sheetname, username, password);
		commonMethod.clickOnElement(FillClusterPage.ProjCol, "Project Header", driver);
		commonMethod.clickOnElement(FillClusterPage.CompCol, "Compound Header", driver);
		commonMethod.clickOnElement(FillClusterPage.DataCol, "Data cluster Header", driver);
		commonMethod.clickOnElement(FillClusterPage.submittedbyCol, "Submittedby Header", driver);
		commonMethod.clickOnElement(FillClusterPage.SubmissionCol, "submission Header", driver);
		commonMethod.clickOnElement(FillClusterPage.StatusCol, "Status Header", driver);
		commonMethod.clickOnElement(FillClusterPage.tableRow, "First project", driver);
		commonMethod.clickOnElement(FillClusterPage.ShowDataCluster, "Show data cluster button", driver);
		FC.Logout();
	
	}
	public void Select_task_lblgrp() throws IOException, UnsupportedFlavorException, AWTException
	{
		commonMethod.navigateToUrl(dataTable.getData("functional_flow", "Application_URL"), driver);
		FC.LogIn(sheetname, username1, password);
		FC.WIFilterCountryDepot();
		FC.WIFilterOpen();
		commonMethod.scrollDown(driver);
		commonMethod.clickOnElement(FillClusterPage.tableRow, "First project", driver);
		commonMethod.clickOnElement(FillClusterPage.ShowDataCluster, "Show data cluster button", driver);
		FC.Logout();
		
		
	}
	public void Select_task_btchdc() throws IOException, UnsupportedFlavorException, AWTException
	{
		commonMethod.navigateToUrl(dataTable.getData("functional_flow", "Application_URL"), driver);
		FC.LogIn(sheetname, username2, password);
		FC.WIFilterGeneralKitPack();
		FC.WIFilterOpen();
		commonMethod.scrollDown(driver);
		commonMethod.clickOnElement(FillClusterPage.tableRow, "First project", driver);
		commonMethod.clickOnElement(FillClusterPage.ShowDataCluster, "Show data cluster button", driver);
		FC.Logout();		
	}
	public void Select_task_QA() throws IOException, UnsupportedFlavorException, AWTException
	{
		commonMethod.navigateToUrl(dataTable.getData("functional_flow", "Application_URL"), driver);
		FC.LogIn(sheetname, username3, password);
		commonMethod.clickOnElement(FillClusterPage.tableRow, "First project", driver);
		commonMethod.clickOnElement(FillClusterPage.ShowDataCluster, "Show data cluster button", driver);
		FC.Logout();
	}
	public void Select_task_melmgr() throws IOException, UnsupportedFlavorException, AWTException
	{
		commonMethod.navigateToUrl(dataTable.getData("functional_flow", "Application_URL"), driver);
		FC.LogIn(sheetname, username4, password4);
		commonMethod.clickOnElement(Select_task_page.Projectmgmt, "Project management button", driver);
		commonMethod.clickOnElement(Select_task_page.MelTempMgmt, "Mel template mgmt button", driver);
	}

}
