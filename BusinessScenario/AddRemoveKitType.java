package BusinessScenario;

import java.awt.AWTException;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;
import uri.FillCluster;
import AutomationCoreFramework.Status;
import BusinessScenarioPage.AddRemoveKitTypePage;
import CommonMethod.CommonMethod;

public class AddRemoveKitType extends ReusableLibrary{
	AddRemoveKitTypePage AddRemoveKiteTypPage = new AddRemoveKitTypePage(scriptHelper);

	CommonMethod commonMethod = new CommonMethod(scriptHelper);
	FillCluster FC = new FillCluster(scriptHelper);
	
	public AddRemoveKitType(ScriptHelper scriptHelper)
	{
		super(scriptHelper);
	}
	
	public void AddRemove() throws IOException, UnsupportedFlavorException, AWTException
	{
		commonMethod.navigateToUrl(dataTable.getData("functional_flow", "Application_URL"), driver);
		FC.LogIn();
		commonMethod.clickOnElement(AddRemoveKitTypePage.Allproj, "All projects button", driver);
		
		commonMethod.clickOnElement(AddRemoveKitTypePage.filterButton, "Filter Button", driver);
		commonMethod.clickOnElement(AddRemoveKitTypePage.filterDraft, "Draft Radio Button on Filter", driver);
		commonMethod.clickOnElement(AddRemoveKitTypePage.filterNonOmp, "Non-Omp Filter Button", driver);
		commonMethod.clickOnElement(AddRemoveKitTypePage.applyFilterButton, "Apply filter", driver);
		commonMethod.clickOnElement(AddRemoveKitTypePage.tableRow, "First row selected", driver);
		commonMethod.clickOnElement(AddRemoveKitTypePage.informationKitType, "information Kita type column", driver);
		
		commonMethod.clickOnElement(AddRemoveKitTypePage.selectKitType, "drop down kit type", driver);
		
		List<WebElement> kittypeoptionwithnew =  driver.findElements(By.xpath(AddRemoveKitTypePage.selectKitType));
		int list1 = kittypeoptionwithnew.size();
		System.out.println(list1);
		
		commonMethod.clickOnElement(AddRemoveKitTypePage.Addnew, "Add New Kit type", driver);
		//String newadd= "Test Kit Infy";
		
		//commonMethod.selectoption(AddRemoveKitTypePage.selectKitType,"Add new", driver);
		System.out.println(dataTable.getData("Test_Data", "Kit_type"));
		commonMethod.enterText(AddRemoveKitTypePage.fieldCompound, "New Kit Type Name", dataTable.getData("Test_Data", "Kit_type"), driver);
		commonMethod.clickOnElement(AddRemoveKitTypePage.saveButton, "New Filter is added", driver);
		
		commonMethod.clickOnElement(AddRemoveKitTypePage.selectKitType, "drop down kit type", driver);
		
		List<WebElement> kittypeoptionwithnew2 =  driver.findElements(By.xpath(AddRemoveKitTypePage.selectKitType));
		System.out.println(kittypeoptionwithnew.toString());
		commonMethod.clickOnElement(AddRemoveKitTypePage.selectKitType, "drop down kit type", driver);
		for (int i = 0; i <= kittypeoptionwithnew.size(); i++) {
			
			String temp = kittypeoptionwithnew2.get(i).getText();
			System.out.println(temp);
			System.out.println("going into for loop");
			if(temp.equals(dataTable.getData("Test_Data", "Kit_type"))){
				System.out.println("going into if block");
				kittypeoptionwithnew2.get(i).click();
				break;
			}
		}
		
		
		
		commonMethod.clickOnElement(AddRemoveKitTypePage.removeKitType, "Selected Kit Type Removed", driver);
		commonMethod.clickOnElement(AddRemoveKitTypePage.yesButton, "Remove Yes Button", driver);
		commonMethod.clickOnElement(AddRemoveKitTypePage.selectKitType, "drop down kit type", driver);
		for (int i = 0; i < kittypeoptionwithnew.size(); i++) {
			
			String temp = kittypeoptionwithnew2.get(i).getText();
			System.out.println(temp);
			System.out.println("going into for loop");
			if(temp.equals(dataTable.getData("Test_Data", "Kit_type"))){
				System.out.println("fail");
				report.updateTestLog("New Kit is not Present","Kit Type name " +dataTable.getData("Test_Data", "Kit_type"),dataTable.getData("Test_Data", "Kit_type") +" should not be displayed in drop down.", dataTable.getData("Test_Data", "Kit_type") + " is displayed", Status.FAIL);
				kittypeoptionwithnew2.get(i).click();
				break;
			}
			else
			{System.out.println("pass");
			report.updateTestLog("New Kit is not Present","Kit Type name " +dataTable.getData("Test_Data", "Kit_type"),dataTable.getData("Test_Data", "Kit_type") +" should not be displayed in drop down.", dataTable.getData("Test_Data", "Kit_type") + " is not displayed", Status.PASS);
			}
		}
		
	}

}
