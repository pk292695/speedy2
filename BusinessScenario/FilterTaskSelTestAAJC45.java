package BusinessScenario;

import java.awt.AWTException;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.server.handler.Status;

import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;
import uri.FillCluster;
import uriPage.FillClusterPage;
import BusinessScenarioPage.Delete_Edit_MEL_Page;
import CommonMethod.CommonMethod;
import AutomationCoreFramework.Report;

public class FilterTaskSelTestAAJC45 extends ReusableLibrary{
	
	Delete_Edit_MEL_Page MELTemplatePage = new Delete_Edit_MEL_Page(scriptHelper);

	CommonMethod commonMethod = new CommonMethod(scriptHelper);
	FillCluster fc = new FillCluster(scriptHelper);
	String e = null;
	public String UserName = "Username";
	public String UserName1 = "Username1";
	public String UserName2 = "Username2";
	public String UserName3 = "Username3";
	public String UserName4 = "Username4";
	public String password = "Password";
	public String SheetName = "functional_flow";
	public FilterTaskSelTestAAJC45(ScriptHelper scriptHelper)
	{
		super(scriptHelper);
	}
	
	public void WIFilterTask() throws IOException, UnsupportedFlavorException, AWTException
	{
		commonMethod.navigateToUrl(dataTable.getData("functional_flow", "Application_URL"), driver);
		fc.LogIn(SheetName,UserName, password);
		fc.WIFilterGPI();
		List <WebElement> e;
		if(driver.findElements(By.xpath(FillClusterPage.WITabledataUnavailable)).size()== 0){
			 e = driver.findElements(By.xpath(FillClusterPage.WITabledataCluster));
			for(int i=0;i <e.size();i++){
				String f = e.get(i).getText();
				if(f.equals("General Project Information")){
					System.out.println("General Project Information" +"  "+ i);
				}
				else System.out.println("Fail"+i);
			}
		}
		else{
			System.out.println("No data Available General Project information" );
		}
		
	driver.navigate().refresh();
	fc.WIFilterAllProjectInfo();
	if(driver.findElements(By.xpath(FillClusterPage.WITabledataUnavailable)).size()== 0){
		 e = driver.findElements(By.xpath(FillClusterPage.WITabledataCluster));
		for(int i=0;i <e.size();i++){
			String f = e.get(i).getText();
			if(f.equals("All Project Information")){
				System.out.println("All Project Information" +"  "+ i);
			}
			else System.out.println("Fail"+i);
		}
	}
	else{
		System.out.println("No data Available on All Project Information" );
	}
	driver.navigate().refresh();
	fc.WIFilterOpen();
	if(driver.findElements(By.xpath(FillClusterPage.WITabledataUnavailable)).size()== 0){
		 e = driver.findElements(By.xpath(FillClusterPage.WITableStatus));
		for(int i=0;i <e.size();i++){
			String f = e.get(i).getText();
			if(f.equals("Open")){
				System.out.println("Open Filter" +"  "+ i);
			}
			else System.out.println("Fail"+i);
		}
	}
	else{
		System.out.println("No data Available on Open Status" );
	}
	driver.navigate().refresh();
	fc.WIFilterclosed();
	if(driver.findElements(By.xpath(FillClusterPage.WITabledataUnavailable)).size()== 0){
		 e = driver.findElements(By.xpath(FillClusterPage.WITableStatus));
		for(int i=0;i <e.size();i++){
			String f = e.get(i).getText();
			if(f.equals("Closed")){
				System.out.println("Closed Filter" +"  "+ i);
			}
			else System.out.println("Fail"+i);
		}
	}
	else{
		System.out.println("No data Available on CLosed Status" );
	}
	driver.navigate().refresh();
	fc.WiFilterSubmittedBy("WILLEMS, JASMINE");
	if(driver.findElements(By.xpath(FillClusterPage.WITabledataUnavailable)).size()== 0){
		 e = driver.findElements(By.xpath(FillClusterPage.WIFilterCompoundText));
		for(int i=0;i <e.size();i++){
			String f = e.get(i).getText();
			if(f.equals("WILLEMS, JASMINE")){
				System.out.println("Submitted By Filter" +"  "+ i);
			}
			else System.out.println("Fail"+i);
		}
	}
	else{
		System.out.println("No data Available on Submitted By Row" );
	}
	driver.navigate().refresh();
	fc.WiFilterCompound("TMC278");
	if(driver.findElements(By.xpath(FillClusterPage.WITabledataUnavailable)).size()== 0){
		 e = driver.findElements(By.xpath(FillClusterPage.WIFilterCompoundText));
		for(int i=0;i <e.size();i++){
			String f = e.get(i).getText();
			if(f.equals("TMC278")){
				System.out.println("Compound Filter" +"  "+ i);
			}
			else System.out.println("Fail"+i);
		}
	}
	else{
		System.out.println("No data Available on compound Row" );
	}
	fc.Logout();
	driver.navigate().refresh();
	fc.LogIn(SheetName, UserName1, password);
	fc.WIFilterCountryDepot();
	if(driver.findElements(By.xpath(FillClusterPage.WITabledataUnavailable)).size()== 0){
		 e = driver.findElements(By.xpath(FillClusterPage.WITabledataCluster));
		for(int i=0;i <e.size();i++){
			String f = e.get(i).getText();
			if(f.equals("Country & Depot Info")){
				System.out.println("Country 7 Depot Info" +"  "+ i);
			}
			else System.out.println("Fail"+i);
		}
	}
	else{
		System.out.println("No data Available Country & Depot Info" );
	}
	driver.navigate().refresh();
	fc.WIFilterMelCompilation();if(driver.findElements(By.xpath(FillClusterPage.WITabledataUnavailable)).size()==0){
		 e = driver.findElements(By.xpath(FillClusterPage.WITabledataCluster));
			for(int i=0;i <e.size();i++){
				String f = e.get(i).getText();
				if(f.equals("MEL compilation")){
					System.out.println("Country 7 Depot Info" +"  "+ i);
				}
				else System.out.println("Fail"+i);
			}
		}
		else{
			System.out.println("No data Available MEL compilation" );
		}
	fc.Logout();
	driver.navigate().refresh();
	fc.LogIn(SheetName, UserName2, password);
	fc.WIFilterGeneralKitPack();
	if(driver.findElements(By.xpath(FillClusterPage.WITabledataUnavailable)).size()== 0){
		 e = driver.findElements(By.xpath(FillClusterPage.WITabledataCluster));
		for(int i=0;i <e.size();i++){
			String f = e.get(i).getText();
			if(f.equals("General Kit Type & Pack Type Info")){
				System.out.println("General Kit Type & Pack Type Info" +"  "+ i);
			}
			else System.out.println("Fail"+i);
		}
	}
	else{
		System.out.println("No data Available General Kit Type & Pack Type Info" );
	}
	driver.navigate().refresh();
	fc.WIFilterDrugProductPrimaryPack();
	if(driver.findElements(By.xpath(FillClusterPage.WITabledataUnavailable)).size()==0){
		 e = driver.findElements(By.xpath(FillClusterPage.WITabledataCluster));
			for(int i=0;i <e.size();i++){
				String f = e.get(i).getText();
				if(f.equals("Drug Product & Primary Pack Info")){
					System.out.println("Drug Product & Primary Pack Info" +"  "+ i);
				}
				else System.out.println("Fail"+i);
			}
		}
		else{
			System.out.println("No data Available Drug Product & Primary Pack Info" );
		}
	driver.navigate().refresh();
	fc.WIFilterBrightStockProject();
	if(driver.findElements(By.xpath(FillClusterPage.WITabledataUnavailable)).size()==0){
		 e = driver.findElements(By.xpath(FillClusterPage.WITabledataCluster));
			for(int i=0;i <e.size();i++){
				String f = e.get(i).getText();
				if(f.equals("Bright Stock Project")){
					System.out.println("Bright Stock Project" +"  "+ i);
				}
				else System.out.println("Fail"+i);
			}
		}
		else{
			System.out.println("Bright Stock Project" );
		}
	}
	
	
		
	
	
	}
