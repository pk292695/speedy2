package BusinessScenario;

import java.awt.AWTException;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;
import uri.FillCluster;
import uriPage.FillClusterPage;
import AutomationCoreFramework.Status;
import BusinessScenarioPage.Version_mgmt_page;
import CommonMethod.CommonMethod;
import CommonMethod.ExpectedData;

public class VersionMgmt_WorkflowTaskAAJC775 extends ReusableLibrary{

	CommonMethod commonMethod = new CommonMethod(scriptHelper);
	FillCluster FC = new FillCluster(scriptHelper);
	
	public VersionMgmt_WorkflowTaskAAJC775(ScriptHelper scriptHelper)
	{
		super(scriptHelper);
	}
	String username = "Username";
	String sheetname = "functional_flow";
	String password = "Password";
	

	public void Version_workflow_task() throws IOException, UnsupportedFlavorException, AWTException
	{
		commonMethod.navigateToUrl(dataTable.getData("functional_flow", "Application_URL"), driver);
		FC.LogIn(sheetname, username, password);
		FC.StartNewProject();
		FC.GeneralProjectInformation();
		FC.SubmitGeneralProjectInformation();
		
		
		commonMethod.clickOnElement(FillClusterPage.HomeButton, "Home button", driver);
		commonMethod.enterText(FillClusterPage.ProjectSearch, "Search box", FillCluster.name, driver);
		commonMethod.clickOnElement(FillClusterPage.WISearchbutton, "Search button", driver);
		commonMethod.clickOnElement(FillClusterPage.tableRow, "Resulted project", driver);
		commonMethod.getText(FillClusterPage.WITableStatus, "Status of the project", driver);

		commonMethod.clickOnElement(FillClusterPage.ShowDataCluster, "Show data cluster button", driver);
		commonMethod.clickOnElement(FillClusterPage.CreateNewVersion, "Create New version button", driver);
		commonMethod.clickOnElement(FillClusterPage.CheckGenproj, "General project checkbox", driver);
		commonMethod.clickOnElement(FillClusterPage.newversion, "New version pop-up button", driver);
		commonMethod.clickOnElement(FillClusterPage.VerNumber, "Verison number", driver);
		
		commonMethod.clickOnElement(FillClusterPage.HomeButton, "Home button", driver);
		commonMethod.enterText(FillClusterPage.ProjectSearch, "Search box", dataTable.getData("Test_Data", "Workflow_project"), driver);
		commonMethod.clickOnElement(FillClusterPage.WISearchbutton, "Search button", driver);
		commonMethod.clickOnElement(FillClusterPage.tableRow, "Resulted project", driver);
		commonMethod.getText(FillClusterPage.WITableStatus, "Status of the project", driver);
	}
	
}
