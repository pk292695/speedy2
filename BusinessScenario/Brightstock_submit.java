package BusinessScenario;

import java.awt.AWTException;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;

import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;
import BusinessScenarioPage.BrightstockSubmitPage;
import CommonMethod.CommonMethod;

public class Brightstock_submit extends ReusableLibrary{
	BrightstockSubmitPage BrightStockpage = new BrightstockSubmitPage(scriptHelper);

	CommonMethod commonMethod = new CommonMethod(scriptHelper);
	
	public Brightstock_submit(ScriptHelper scriptHelper)
	{
		super(scriptHelper);
	}
	
	public void Submit_brightstock() throws IOException, UnsupportedFlavorException, AWTException
	{
		commonMethod.navigateToUrl(dataTable.getData("functional_flow", "Application_URL"), driver);
		commonMethod.enterText(BrightstockSubmitPage.Username, "User Name", dataTable.getData("functional_flow", "Username"), driver);
		commonMethod.enterText(BrightstockSubmitPage.Password, "Password", dataTable.getData("functional_flow", "Password"), driver);
		commonMethod.clickOnElement(BrightstockSubmitPage.Login, "Login button", driver);
		commonMethod.clickOnElement(BrightstockSubmitPage.Allproj, "All projects button", driver);
		commonMethod.clickOnElement(BrightstockSubmitPage.LastArrow, "Last page button", driver);
		commonMethod.clickOnElement(BrightstockSubmitPage.PrevPage, "Previous page button", driver);
		commonMethod.clickOnElement(BrightstockSubmitPage.BrightstkProj, "Brightstock project", driver);
		commonMethod.clickOnElement(BrightstockSubmitPage.Syncbutton, "Sync with OMP link", driver);
		
		commonMethod.clickOnElement(BrightstockSubmitPage.GenProjInfo, "General project Info button", driver);
		commonMethod.selectOptionFromDropDown(BrightstockSubmitPage.SponserName, dataTable.getData("Test_Data", "SponserName"), driver);
		commonMethod.selectOptionFromDropDown(BrightstockSubmitPage.SponserAddr, dataTable.getData("Test_Data", "SponserAddr"), driver);
		commonMethod.clearElement(BrightstockSubmitPage.KitNumber, "Kit number field", driver);
		commonMethod.clearElement(BrightstockSubmitPage.Format, "Format field", driver);
		commonMethod.clearElement(BrightstockSubmitPage.Prefix, "Pre-fix field", driver);
		commonMethod.scrollDown(driver);
		commonMethod.verifyElementNotEnabled(BrightstockSubmitPage.BrightstkSubmit, "Submit button", driver);
		
		commonMethod.clickOnElement(BrightstockSubmitPage.CntryInfo, "Country Info button", driver);
		commonMethod.clickOnElement(BrightstockSubmitPage.InfoKit, "Info per kit type section", driver);
		
		commonMethod.clickOnElement(BrightstockSubmitPage.GenKitInfo, "General kit type Info tab", driver);
		commonMethod.clickOnElement(BrightstockSubmitPage.BlindType, "Blinding Type radio button", driver);
		commonMethod.clickOnElement(BrightstockSubmitPage.ChildRes, "Child resistance", driver);
		//commonMethod.clickOnElement(BrightstockSubmitPage.PackSitesPrim, "Pack sites Primary", driver);
		//commonMethod.clickOnElement(BrightstockSubmitPage.Option1, "Option 1", driver);
		//commonMethod.clickOnElement(BrightstockSubmitPage.PackSitesSec, "Pack sites secondary", driver);
		//commonMethod.clickOnElement(BrightstockSubmitPage.Option2, "Option 2", driver);
		
		commonMethod.clickOnElement(BrightstockSubmitPage.PackTypeInfo, "Pack type Info tab", driver);
		commonMethod.selectOptionFromDropDown(BrightstockSubmitPage.ProdAndStrength, dataTable.getData("Test_Data", "ProductName_Strength"), driver);
		commonMethod.selectOptionFromDropDown(BrightstockSubmitPage.DoseForm, dataTable.getData("Test_Data", "DoseForm_Quantity"), driver);
		commonMethod.selectOptionFromDropDown(BrightstockSubmitPage.ROA, dataTable.getData("Test_Data", "ROA"), driver);
		commonMethod.selectOptionFromDropDown(BrightstockSubmitPage.TempStore, dataTable.getData("Test_Data", "TempStoreCondition"), driver);
		commonMethod.clickOnElement(BrightstockSubmitPage.TearOff, "Tear off Labels", driver);
		
		commonMethod.clickOnElement(BrightstockSubmitPage.DrugProdInfo, "Drug Product Info tab", driver);
		//commonMethod.selectOptionFromDropDown(BrightstockSubmitPage.DrugProduct, dataTable.getData("Test_Data", "Drug_product"), driver);
		//commonMethod.enterText(BrightstockSubmitPage.DrugSubCompany, "Drug substance company", dataTable.getData("Test_Data", "DrugSubstanceCompany"), driver);
		//commonMethod.selectOptionFromDropDown(BrightstockSubmitPage.Drugsubstance, dataTable.getData("Test_Data", "Drug_substance"), driver);
		//commonMethod.clickOnElement(BrightstockSubmitPage.ColdChain, "Cold chain drug product", driver);
		//commonMethod.clickOnElement(BrightstockSubmitPage.MoistureProd, "Moisture sensitive drug product", driver);
		//commonMethod.clickOnElement(BrightstockSubmitPage.OxygenSens, "Oxygen sensitive drug product", driver);
		//commonMethod.clickOnElement(BrightstockSubmitPage.Lightsense, "Light sensitive drug product", driver);
		//commonMethod.clickOnElement(BrightstockSubmitPage.FrozenProd, "Frozen product", driver);
		
		commonMethod.clickOnElement(BrightstockSubmitPage.PrimPackInfo, "Primary pack Info tab", driver);
		commonMethod.clickOnElement(BrightstockSubmitPage.OptionPrim, "Primary packaging CSC P&L", driver);
		
		commonMethod.clickOnElement(BrightstockSubmitPage.MelCompTab, "MEL compiler tab", driver);
		commonMethod.clickOnElement(BrightstockSubmitPage.PackOpt, "Pack type checkbox", driver);
		commonMethod.clickOnElement(BrightstockSubmitPage.LabelOpt, "Labelgroup checkbox", driver);
		//commonMethod.clickOnElement(BrightstockSubmitPage.Combine, "combine button", driver);
		//commonMethod.selectOptionFromDropDown(BrightstockSubmitPage.ChooseTemp, dataTable.getData("Test_Data", "Choose_temp"), driver);
		//commonMethod.clickOnElement(BrightstockSubmitPage.Done, "Done button", driver);
		
		commonMethod.clickOnElement(BrightstockSubmitPage.GenProjInfo, "General project Info button", driver);
		commonMethod.enterText(BrightstockSubmitPage.KitNumber, "Kit number", dataTable.getData("Test_Data", "Kit_Number"), driver);
		commonMethod.enterText(BrightstockSubmitPage.Format, "Format", dataTable.getData("Test_Data", "Format"), driver);
		commonMethod.enterText(BrightstockSubmitPage.Prefix, "Pre-fix", dataTable.getData("Test_Data", "Prefix"), driver);
		commonMethod.VerifyElementEnabled(BrightstockSubmitPage.BrightstkSubmit, "Submit button", driver);
		
	}

}
