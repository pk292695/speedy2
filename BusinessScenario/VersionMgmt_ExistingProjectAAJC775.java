package BusinessScenario;

import java.awt.AWTException;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.util.List;


import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;
import uri.FillCluster;
import uriPage.FillClusterPage;
import AutomationCoreFramework.Status;

import CommonMethod.CommonMethod;


public class VersionMgmt_ExistingProjectAAJC775 extends ReusableLibrary{

	CommonMethod commonMethod = new CommonMethod(scriptHelper);
	FillCluster FC = new FillCluster(scriptHelper);
	
	public VersionMgmt_ExistingProjectAAJC775(ScriptHelper scriptHelper)
	{
		super(scriptHelper);
	}
	String username = "Username";
	String sheetname = "functional_flow";
	String password = "Password";
	public void VersionMgmt_ExistingProject() throws IOException, UnsupportedFlavorException, AWTException
	{
		commonMethod.navigateToUrl(dataTable.getData("functional_flow", "Application_URL"), driver);
		FC.LogIn(sheetname, username, password);
		commonMethod.clickOnElement(FillClusterPage.AllprojButton, "All project button", driver);
		FC.FilterNonOMPAwaiting();
		commonMethod.clickOnElement(FillClusterPage.tableRow, "Resulted project", driver);
		commonMethod.getText(FillClusterPage.VerNumber, "Version number", driver);
		FC.GeneralProjectInformation();
		FC.SubmitGeneralProjectInformation();
		commonMethod.clickOnElement(FillClusterPage.CreateNewVersion, "Create new version button", driver);
		commonMethod.clickOnElement(FillClusterPage.GenProjCheck, "General proj checkbox", driver);
		commonMethod.clickOnElement(FillClusterPage.newversion, "Create new version button", driver);
		commonMethod.getText(FillClusterPage.VerNumber, "Version number", driver);
		commonMethod.clickOnElement(FillClusterPage.GenProjInfo, "General project tab", driver);
		commonMethod.clearElement(FillClusterPage.Therapeutic, "Therapeutic", driver);
		commonMethod.enterText(FillClusterPage.Therapeutic, "Therapeutic", dataTable.getData("Test_Data", "TA_Replace"), driver);
		FC.SubmitGeneralProjectInformation();
		commonMethod.clickOnElement(FillClusterPage.CreateNewVersion, "Create new version button", driver);
		commonMethod.verifyExpectedText(FillClusterPage.ImpactText, "General Project Information(AWAITING_APPROVAL)", driver);
	}
	
	
}
