package BusinessScenario;

import java.awt.AWTException;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;
import uri.FillCluster;
import uriPage.FillClusterPage;
import AutomationCoreFramework.Status;
import BusinessScenarioPage.AddRemoveKitTypePage;
import CommonMethod.CommonMethod;

public class SaveProjectDetailsFuncationalityAAJC121 extends ReusableLibrary{
	AddRemoveKitTypePage AddRemoveKiteTypPage = new AddRemoveKitTypePage(scriptHelper);

	CommonMethod commonMethod = new CommonMethod(scriptHelper);
	FillCluster FC = new FillCluster(scriptHelper);
	
	public SaveProjectDetailsFuncationalityAAJC121(ScriptHelper scriptHelper)
	{
		super(scriptHelper);
	}
	String username = "Username";
	String sheetname = "functional_flow";
	String password = "Password";
	String KitTypeName = "JKP";
	public void AddkitType() throws IOException, UnsupportedFlavorException, AWTException
	{
		commonMethod.navigateToUrl(dataTable.getData("functional_flow", "Application_URL"), driver);
		FC.LogIn(sheetname,username, password);
		commonMethod.clickOnElement(FillClusterPage.AllprojButton, "All projects button", driver);
		FC.FilterNonOMPDraft();
		commonMethod.clickOnElement(FillClusterPage.tableRow, "Table Row", driver);
		commonMethod.clickOnElement(FillClusterPage.informationKitType, "Information Kit Type", driver);
		FC.CanKitType();
		FC.createKitType(KitTypeName);
		commonMethod.clickOnElement(FillClusterPage.selectKitType, "drop down kit type", driver);
		
		List<WebElement> kittypeoptionwithnew =  driver.findElements(By.xpath(FillClusterPage.selectKitType));
	//	int list1 = kittypeoptionwithnew.size();
	//	System.out.println(list1);
	//	List<WebElement> kittypeoptionwithnew2 =  driver.findElements(By.xpath(AddRemoveKitTypePage.selectKitType));
		for (int i = 0; i <= kittypeoptionwithnew.size(); i++) {
			
			String temp = kittypeoptionwithnew.get(i).getText();
			System.out.println(temp);
			System.out.println("going into for loop");
			if(temp.equals(KitTypeName)){
				System.out.println("going into if block");
				if(kittypeoptionwithnew.get(i).isDisplayed()){
					report.updateTestLog("newly created Kit Type"	, "Verify Created kit type is displaying in the drop down box	", "Created "+temp+" must be displayed", "Created "+temp+ " is displayed", Status.PASS);
				}else{
					report.updateTestLog("newly created Kit Type"	, "Verify Created kit type is displaying in the drop down box	", "Created "+temp+" must be displayed", "Created "+temp+ " is not displayed", Status.FAIL);
				}
				
				break;
			}
		}
	
	}	
		/*public void CheckAddKittypeonOMP() throws IOException, UnsupportedFlavorException, AWTException{
		commonMethod.clickOnElement(FillClusterPage.HomeButton, "Home Page", driver);
		commonMethod.clickOnElement(FillClusterPage.AllprojButton, "All Project Table page", driver);
		FC.FilterOMPDraft();
		commonMethod.clickOnElement(FillClusterPage.tableRow, "Select the project", driver);
		commonMethod.clickOnElement(FillClusterPage.informationKitType, "Information Kit Type tab", driver);
		commonMethod.clickOnElement(FillClusterPage.selectKitType, "drop down kit type", driver);
		List<WebElement> kittypeoptionwithnew3 =  driver.findElements(By.xpath(FillClusterPage.selectKitType));
		int list12 = kittypeoptionwithnew3.size();
		System.out.println(list12);
		
		if (driver.findElements(By.xpath(FillClusterPage.Addnew)).size()!=0) {
			report.updateTestLog("Add new Kit Type Button should not available", "Add new Kit type button should not be available to OMP", "Add new Kit type should not be displayed", "Add new Kit is displayed", Status.FAIL);
			}
		else{
			report.updateTestLog("Add new Kit Type Button should not available", "Add new Kit type button should not be available to OMP", "Add new Kit type should not be displayed", "Add new Kit is not displayed", Status.PASS);
		}
		
		
		}*/
			
	}	




