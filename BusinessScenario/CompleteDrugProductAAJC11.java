package BusinessScenario;

import java.awt.AWTException;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;
import uri.FillCluster;
import uriPage.FillClusterPage;
import AutomationCoreFramework.Status;
import BusinessScenarioPage.AddRemoveKitTypePage;
import CommonMethod.CommonMethod;

public class CompleteDrugProductAAJC11 extends ReusableLibrary{
	CommonMethod commonMethod = new CommonMethod(scriptHelper);
	FillCluster FC = new FillCluster(scriptHelper);
	
	public CompleteDrugProductAAJC11(ScriptHelper scriptHelper)
	{
		super(scriptHelper);
	}
	String username = "Username";
	String sheetname = "functional_flow";
	String password = "Password";
	public void DrugProductComplete() throws IOException, UnsupportedFlavorException, AWTException
	{
		commonMethod.navigateToUrl(dataTable.getData("functional_flow", "Application_URL"), driver);
		FC.LogIn(sheetname,username, password);
		FC.StartNewProject();
		commonMethod.clickOnElement(FillClusterPage.informationKitType, "Information per kit type", driver);
		FC.createKitType(dataTable.getData("Test_Data", "Kitname"));
		commonMethod.clickOnElement(FillClusterPage.DrugProductInfo, "Drug product Info", driver);
		commonMethod.clickOnElement(FillClusterPage.AddSapMaterial, "Add SAP material number", driver);
		commonMethod.verifyElementPresent(FillClusterPage.SAPCompound, "SAP compound", driver);
		commonMethod.verifyElementPresent(FillClusterPage.SAPmaterialnNm, "SAP material number", driver);
		commonMethod.verifyElementPresent(FillClusterPage.SAPMaterialDes, "SAP Material description", driver);
		
		commonMethod.enterText(FillClusterPage.SAPmaterialnNm, "SAP Material master number", dataTable.getData("Test_Data", "SAP"), driver);
		commonMethod.clickOnElement(FillClusterPage.SAPSearch, "Click on search button", driver);
		commonMethod.clickOnElement(FillClusterPage.Sap19001342, "Click on 19001342", driver);
		commonMethod.clickOnElement(FillClusterPage.AddSapMaterialButton, "Click on ADD SAP for 19001342", driver);
		
		commonMethod.selectOptionFromDropDown(FillClusterPage.DrugProdCountry, dataTable.getData("Test_Data", "DrugCountry"), driver);
		commonMethod.enterText(FillClusterPage.DrugsubstanceCompany, "Drug Substance Company", dataTable.getData("Test_Data", "DrugSubstanceCompany"), driver);
		commonMethod.selectOptionFromDropDown(FillClusterPage.DrugSubtanceCountry, dataTable.getData("Test_Data", "DrugSubstanceCountry"), driver);
		commonMethod.clickOnElement(FillClusterPage.Coldchain, "Cold chain drug product", driver);
		commonMethod.clickOnElement(FillClusterPage.MoistureSensitive, "Moisture Sensitive drug product", driver);
		commonMethod.clickOnElement(FillClusterPage.LightSensitive, "Light Sensitive", driver);
		commonMethod.clickOnElement(FillClusterPage.OxygenSensitive, "Oxygen Sensitive", driver);
		commonMethod.clickOnElement(FillClusterPage.Frozenproduct, "Frozen product", driver);
		
		commonMethod.clickOnElement(FillClusterPage.SaveChanges, " save changes ", driver);
		commonMethod.clickOnElement(FillClusterPage.ContinueSave, "Continue save changes button", driver);
		
		
	}
	
}




