package BusinessScenario;

import java.awt.AWTException;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;

import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;
import uri.FillCluster;
import uriPage.FillClusterPage;
import CommonMethod.CommonMethod;

public class TextSignOff2_AAJC1061 extends ReusableLibrary{

	CommonMethod commonMethod = new CommonMethod(scriptHelper);
	FillCluster FC = new FillCluster(scriptHelper);
	
	public TextSignOff2_AAJC1061(ScriptHelper scriptHelper)
	{
		super(scriptHelper);
	}
	String username = "Username";
	String sheetname = "functional_flow";
	String password = "Password";
	String username1 = "Username1";
	public void TextSignOff2() throws IOException, UnsupportedFlavorException, AWTException
	{
		commonMethod.navigateToUrl(dataTable.getData("functional_flow", "Application_URL"), driver);
		FC.LogIn(sheetname,username, password);
		FC.StartNewProject();
		
		FC.CountryInformation();
		FC.SubmitCountryInformation();
		commonMethod.clickOnElement(FillClusterPage.Country, "Country Info tab", driver);
		commonMethod.getText(FillClusterPage.LabelCountry, "Label approval", driver);
		commonMethod.getText(FillClusterPage.OpenCountry, "Open status", driver);
		FC.Logout();
		//Country Information Approval
		driver.navigate().refresh();
		FC.LogIn(sheetname, username1, password);
		commonMethod.enterText(FillClusterPage.ProjectSearch, "Workflow inbox Project search", FillCluster.name, driver);
		commonMethod.clickOnElement(FillClusterPage.WIProjectSearch, "Workflow inbox search", driver);
		commonMethod.clickOnElement(FillClusterPage.tableRow, "Resulted project", driver);
		commonMethod.clickOnElement(FillClusterPage.ShowDataCluster, "Show data cluster button", driver);
		commonMethod.clickOnElement(FillClusterPage.CntryProjApproveButton, "Approve button", driver);
	}
}
