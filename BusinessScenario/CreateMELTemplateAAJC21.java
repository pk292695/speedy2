package BusinessScenario;

import java.awt.AWTException;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;
import uri.FillCluster;
import uriPage.FillClusterPage;
import AutomationCoreFramework.Status;
import BusinessScenarioPage.AddRemoveKitTypePage;
import CommonMethod.CommonMethod;

public class CreateMELTemplateAAJC21 extends ReusableLibrary{
	AddRemoveKitTypePage AddRemoveKiteTypPage = new AddRemoveKitTypePage(scriptHelper);

	CommonMethod commonMethod = new CommonMethod(scriptHelper);
	FillCluster FC = new FillCluster(scriptHelper);
	
	public CreateMELTemplateAAJC21(ScriptHelper scriptHelper)
	{
		super(scriptHelper);
	}
	String username = "Username4";
	String sheetname = "functional_flow";
	String password = "Password4";
	String KitTypeName = "JKP";
	public void CreateMEL() throws IOException, UnsupportedFlavorException, AWTException
	{
		commonMethod.navigateToUrl(dataTable.getData("functional_flow", "Application_URL"), driver);
		FC.LogIn(sheetname,username, password);
		commonMethod.clickOnElement(FillClusterPage.MELprojectManagement, "Project Management button", driver);
		commonMethod.clickOnElement(FillClusterPage.MELTemplateManagement, "MEL template Management button", driver);
		
		commonMethod.selectOptionFromDropDown(FillClusterPage.MELlongDescriptionField, "Create new MEL template", driver);
		commonMethod.enterText(FillClusterPage.MELlongDescriptionText, "Long description", dataTable.getData("Test_Data", "Long_Description"), driver);
		commonMethod.clickOnElement(FillClusterPage.MELTemplateCancel, "Cancel Button", driver);
		
		commonMethod.enterText(FillClusterPage.MELlongDescriptionText, "Long description", dataTable.getData("Test_Data", "Long_Description"), driver);
		commonMethod.enterText(FillClusterPage.MELlongDescriptionText, "Long description", dataTable.getData("Test_Data", "Long_Description"), driver);
		commonMethod.enterText(FillClusterPage.MELShortDescriptionText, "Short description", dataTable.getData("Test_Data", "Short_Description"), driver);
		commonMethod.clickOnElement(FillClusterPage.MELAddTemplateButton, "Add MEL Template Button", driver);
		commonMethod.selectOptionFromDropDown(FillClusterPage.MELlongDescriptionField,dataTable.getData("Test_Data", "Long_Description") , driver);
		
		commonMethod.clickOnElement(FillClusterPage.MELaddContextVariable, "Add Context Variable Button", driver);
		commonMethod.clickOnElement(FillClusterPage.MELsearchButton, "Search Button", driver);
		commonMethod.clickOnElement(FillClusterPage.MELselectContextVariable, "Context Variable", driver);
		commonMethod.clickOnElement(FillClusterPage.MELaddToTemplateButton, "Add To Template Button", driver);
		
		commonMethod.clickOnElement(FillClusterPage.MELaddNGCTextReference, "Add NGC Text Reference Button", driver);
		commonMethod.clickOnElement(FillClusterPage.MELNGCsearchButton, "Search Button", driver);
		commonMethod.clickOnElement(FillClusterPage.MELNGCstatic, "NGC Static Variable", driver);
		commonMethod.clickOnElement(FillClusterPage.MELNGCaddToTem, "Add To Template Button", driver);
		
		commonMethod.clickOnElement(FillClusterPage.MELaddSpeedyVariable, "Add Speedy Variable Button", driver);
		commonMethod.clickOnElement(FillClusterPage.MELspeedyVariable, "Speedy Variable", driver);
		commonMethod.clickOnElement(FillClusterPage.MELaddToMELSpeedyVariable, "Add To Template Button", driver);
		
		commonMethod.clickOnElement(FillClusterPage.MELsaveChangesButton, "Save Changes Button", driver);
		
	}	
	}	




