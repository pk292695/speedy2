package BusinessScenario;

import java.awt.AWTException;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;

import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;
import BusinessScenarioPage.Delete_Edit_MEL_Page;
import CommonMethod.CommonMethod;

public class Delete_Edit_MEL extends ReusableLibrary{
	
	Delete_Edit_MEL_Page MELTemplatePage = new Delete_Edit_MEL_Page(scriptHelper);

	CommonMethod commonMethod = new CommonMethod(scriptHelper);
	public Delete_Edit_MEL(ScriptHelper scriptHelper)
	{
		super(scriptHelper);
	}
	
	public void MELDelete() throws IOException, UnsupportedFlavorException, AWTException
	{
		commonMethod.navigateToUrl(dataTable.getData("functional_flow", "Application_URL"), driver);
		commonMethod.enterText(Delete_Edit_MEL_Page.Username, "User Name", dataTable.getData("functional_flow", "Username4"), driver);
		commonMethod.enterText(Delete_Edit_MEL_Page.Password, "Password", dataTable.getData("functional_flow", "Password4"), driver);
		commonMethod.clickOnElement(Delete_Edit_MEL_Page.Login, "Login button", driver);
		commonMethod.clickOnElement(Delete_Edit_MEL_Page.ProjMgmt, "Project management button", driver);
		commonMethod.clickOnElement(Delete_Edit_MEL_Page.MELMgmt, "MEL management button", driver);
		
		commonMethod.selectOptionFromDropDown(Delete_Edit_MEL_Page.Long_Description, dataTable.getData("Test_Data", "Long_Description"), driver);
		commonMethod.clickOnElement(Delete_Edit_MEL_Page.RemoveMEL, "Remove MEL template button", driver);
		commonMethod.clickOnElement(Delete_Edit_MEL_Page.Delete, "Delete button", driver);
		
		commonMethod.clickOnElement(Delete_Edit_MEL_Page.Account, "Account button", driver);
		commonMethod.clickOnElement(Delete_Edit_MEL_Page.Signout, "Sign out button", driver);
		
		//commonMethod.clickOnElement(Delete_Edit_MEL_Page.Account, "Account button", driver);
		//commonMethod.clickOnElement(Delete_Edit_MEL_Page.Signin, "Sign-in button", driver);
		commonMethod.enterText(Delete_Edit_MEL_Page.Username, "User Name", dataTable.getData("functional_flow", "Username"), driver);
		commonMethod.enterText(Delete_Edit_MEL_Page.Password, "Password", dataTable.getData("functional_flow", "Password"), driver);
		commonMethod.clickOnElement(Delete_Edit_MEL_Page.Login, "Login button", driver);
		commonMethod.clickOnElement(Delete_Edit_MEL_Page.Allproj, "All project button", driver);
		commonMethod.clickOnElement(Delete_Edit_MEL_Page.Arrow, "Next page button", driver);
		commonMethod.clickOnElement(Delete_Edit_MEL_Page.Non_Omp_Draft, "Non-omp button", driver);
		commonMethod.clickOnElement(Delete_Edit_MEL_Page.Info_Per_Kit, "Info per kit type tab", driver);
		commonMethod.clickOnElement(Delete_Edit_MEL_Page.MELCompiler, "MEL compiler tab", driver);
		
		commonMethod.clickOnElement(Delete_Edit_MEL_Page.Edit_label, "Edit labelgroup link", driver);
		commonMethod.enterText(Delete_Edit_MEL_Page.Name, "Label Name", dataTable.getData("Test_Data", "Label_Name"), driver);
		commonMethod.clickOnElement(Delete_Edit_MEL_Page.SaveLabel, "Save label button", driver);
		
		commonMethod.clickOnElement(Delete_Edit_MEL_Page.Pack_Type, "Pack type option", driver);
		commonMethod.clickOnElement(Delete_Edit_MEL_Page.LabelGroup, "Labelgroup option", driver);
		commonMethod.clickOnElement(Delete_Edit_MEL_Page.Combinebtn, "Combine button", driver);
		
		
		
		
		
	}
	
}
