package BusinessScenario;

import java.awt.AWTException;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;

import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;
import uri.FillCluster;
import uriPage.FillClusterPage;
import CommonMethod.CommonMethod;

public class ShowLinkBrightstockIMPA_AAJC123 extends ReusableLibrary{
	CommonMethod commonMethod = new CommonMethod(scriptHelper);
	FillCluster FC = new FillCluster(scriptHelper);
	
	
	public ShowLinkBrightstockIMPA_AAJC123(ScriptHelper scriptHelper)
	{
		super(scriptHelper);
	}
	String username = "Username";
	String sheetname = "functional_flow";
	String password = "Password";
	
	public void BrightstockIMPA() throws IOException, UnsupportedFlavorException, AWTException
	{
		commonMethod.navigateToUrl(dataTable.getData("functional_flow", "Application_URL"), driver);
		FC.LogIn(sheetname,username, password);
		commonMethod.clickOnElement(FillClusterPage.AllprojButton, "All project page", driver);
		FC.FilterProjectnameactive(dataTable.getData("Test_Data", "ProjectName"));
		commonMethod.clickOnElement(FillClusterPage.tableRow, "Resulted project", driver);
		//commonMethod.clickOnElement(FillClusterPage.SyncwithOMPLink, "Sync with OMP link", driver);
		commonMethod.clickOnElement(FillClusterPage.informationKitType, "Info per kit type section", driver);
		commonMethod.clickOnElement(FillClusterPage.DrugProductInfo, "Drug product Info tab", driver);
		commonMethod.getText(FillClusterPage.BrightstockIMPA_Button, "Brightstock IMPA button", driver);
		commonMethod.clickOnElement(FillClusterPage.BrightstockIMPA_Button, "Brightstock IMPA button", driver);
	}
}
