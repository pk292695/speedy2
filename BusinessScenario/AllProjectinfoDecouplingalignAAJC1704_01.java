package BusinessScenario;

import java.awt.AWTException;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;

import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;
import uri.FillCluster;
import uriPage.FillClusterPage;
import CommonMethod.CommonMethod;

public class AllProjectinfoDecouplingalignAAJC1704_01 extends ReusableLibrary{

	CommonMethod commonMethod = new CommonMethod(scriptHelper);
	FillCluster FC = new FillCluster(scriptHelper);
	
	public AllProjectinfoDecouplingalignAAJC1704_01(ScriptHelper scriptHelper)
	{
		super(scriptHelper);
	}
	String username = "Username";
	String username1 = "Username1";
	String sheetname = "functional_flow";
	String password = "Password";
	public void AllProjectInfo1() throws IOException, UnsupportedFlavorException, AWTException
	{
		commonMethod.navigateToUrl(dataTable.getData("functional_flow", "Application_URL"), driver);
		FC.LogIn(sheetname, username, password);
		FC.StartNewProject();
		FC.GeneralProjectInformation();
		FC.SubmitGeneralProjectInformation();
		FC.CountryInformation();
		FC.SubmitCountryInformation();
		commonMethod.clickOnElement(FillClusterPage.informationKitType, "Information per kit type section", driver);
		FC.createKitType(dataTable.getData("Test_Data", "Kitname"));
		FC.GeneralKitTypeInfo();
		FC.PackTypeInfo();
		FC.SubmitGeneralKitPackType();
		
		//General project approveal
				commonMethod.clickOnElement(FillClusterPage.HomeButton, "Continue save changes button", driver);
				commonMethod.enterText(FillClusterPage.ProjectSearch, "Workflow inbox Project search", FillCluster.name, driver);
				commonMethod.clickOnElement(FillClusterPage.WIProjectSearch, "Workflow inbox search", driver);
				commonMethod.clickOnElement(FillClusterPage.tableRow, "Resulted project", driver);
				commonMethod.clickOnElement(FillClusterPage.ShowDataCluster, "Show data cluster button", driver);
				commonMethod.clickOnElement(FillClusterPage.GenProjApproveButton, "Approve button", driver);
				FC.LogIn(sheetname, username, password);
				driver.navigate().refresh();
				FC.Logout();
				
				//Country Information Approval
						driver.navigate().refresh();
						FC.LogIn(sheetname,username1, password);
						commonMethod.enterText(FillClusterPage.ProjectSearch, "Workflow inbox Project search", FillCluster.name, driver);
						commonMethod.clickOnElement(FillClusterPage.WIProjectSearch, "Workflow inbox search", driver);
						commonMethod.clickOnElement(FillClusterPage.tableRow, "Resulted project", driver);
						commonMethod.clickOnElement(FillClusterPage.ShowDataCluster, "Show data cluster button", driver);
						commonMethod.clickOnElement(FillClusterPage.CntryProjApproveButton, "Approve button", driver);
						FC.LogIn(sheetname, username1, password);
						
						FC.Logout();
						
						
						//Login as EDITOR to submit all project approval
						driver.navigate().refresh();
						FC.LogIn(sheetname, username, password);
						commonMethod.clickOnElement(FillClusterPage.AllprojButton, "All project button", driver);
						FC.FilterProjectnameactive(FillCluster.name);
						commonMethod.clickOnElement(FillClusterPage.tableRow, "Resulted projec", driver);
						commonMethod.clickOnElement(FillClusterPage.GenProjInfo, "General project information tab", driver);
						commonMethod.verifyElementNotEnabled(FillClusterPage.SubmitAllProj, "All project submit button", driver);
	}
}
