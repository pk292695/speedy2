package BusinessScenario;

import java.awt.AWTException;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;
import uri.FillCluster;
import uriPage.FillClusterPage;
import AutomationCoreFramework.Status;
import BusinessScenarioPage.AddRemoveKitTypePage;
import CommonMethod.CommonMethod;

public class VerifySelectPackTypeAAJC9 extends ReusableLibrary{
	AddRemoveKitTypePage AddRemoveKiteTypPage = new AddRemoveKitTypePage(scriptHelper);

	CommonMethod commonMethod = new CommonMethod(scriptHelper);
	FillCluster FC = new FillCluster(scriptHelper);
	
	public VerifySelectPackTypeAAJC9(ScriptHelper scriptHelper)
	{
		super(scriptHelper);
	}
	String username = "Username";
	String sheetname = "functional_flow";
	String password = "Password";
	public void SelectKitType() throws IOException, UnsupportedFlavorException, AWTException
	{
		commonMethod.navigateToUrl(dataTable.getData("functional_flow", "Application_URL"), driver);
		FC.LogIn(sheetname,username, password);
		commonMethod.clickOnElement(FillClusterPage.AllprojButton, "All projects button", driver);
		FC.FilterNonOMPDraft();
		commonMethod.clickOnElement(FillClusterPage.tableRow, "Table Row", driver);
		commonMethod.clickOnElement(FillClusterPage.informationKitType, "Information Kit Type", driver);
		//commonMethod.clickOnElement(FillClusterPage.KitTypeDropDown, "Kit Type drop Down", driver);
		WebElement e = driver.findElement(By.xpath(FillClusterPage.KitTypeDropDown));
		if(e.isDisplayed()){
			report.updateTestLog("Kit Type Drop Down", "To check whether Kit Type Drop down is displyed", "Kit Type Drop down should be displyed", "Kit Type Drop Down is displayed", Status.PASS);
		}
		else{
			report.updateTestLog("Kit Type Drop Down", "To check whether Kit Type Drop down is displyed", "Kit Type Drop down should be displyed", "Kit Type Drop Down is not displayed", Status.FAIL);
		}
		String kitTypename = "K2";
		
		FC.SelectKitType(kitTypename);
		commonMethod.scrollDown(driver);
		
		commonMethod.verifyElementPresent(FillClusterPage.GeneralKitTypeInfo, "General Kit Type infor Tab", driver);
		commonMethod.verifyElementPresent(FillClusterPage.PacktypeTab, "pack type Info tab", driver);
		commonMethod.verifyElementPresent(FillClusterPage.DrugProductInfo, "Drug Product Info", driver);
		commonMethod.verifyElementPresent(FillClusterPage.PrimaryPack, "Primary Pack Info", driver);
		commonMethod.verifyElementPresent(FillClusterPage.MELcompilertab, "MEL Complier Tab", driver);
		
		}
			
			
		}



