package BusinessScenario;

import java.awt.AWTException;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.sikuli.basics.FileManager.fileFilter;

import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;
import uri.FillCluster;
import uriPage.FillClusterPage;
import AutomationCoreFramework.Status;
import BusinessScenarioPage.AddRemoveKitTypePage;
import CommonMethod.CommonMethod;

public class SubmitApprovalGenKitPackTypeAAJC13 extends ReusableLibrary{
	AddRemoveKitTypePage AddRemoveKiteTypPage = new AddRemoveKitTypePage(scriptHelper);

	CommonMethod commonMethod = new CommonMethod(scriptHelper);
	FillCluster FC = new FillCluster(scriptHelper);
	
	public SubmitApprovalGenKitPackTypeAAJC13(ScriptHelper scriptHelper)
	{
		super(scriptHelper);
	}
	String username = "Username";
	String sheetname = "functional_flow";
	String password = "Password";
	String username2 = "Username2";
	public void SubmitApprovalGenkitPack() throws IOException, UnsupportedFlavorException, AWTException
	{
		commonMethod.navigateToUrl(dataTable.getData("functional_flow", "Application_URL"), driver);
		FC.LogIn(sheetname,username, password);
		FC.StartNewProject();
		commonMethod.clickOnElement(FillClusterPage.informationKitType, "Information Kit Type", driver);
		FC.createKitType(dataTable.getData("Test_Data", "Kitname"));
	
		FC.GeneralKitTypeInfo();
		FC.PackTypeInfo();
		FC.SubmitGeneralKitPackType();
		
		commonMethod.clickOnElement(FillClusterPage.informationKitType, "Information per kit type section", driver);
		commonMethod.clickOnElement(FillClusterPage.GeneralKitTypeInfo, "General kit Type tab", driver);
		commonMethod.getText(FillClusterPage.BatchdocGenKit, "Batchdoc approval", driver);
		commonMethod.getText(FillClusterPage.OpenGenkit, "Open status", driver);
		FC.Logout();
		
		//General kit and Pack type Approval
		driver.navigate().refresh();
		FC.LogIn(sheetname, username2, password);
		commonMethod.enterText(FillClusterPage.ProjectSearch, "Workflow inbox Project search", FillCluster.name, driver);
		commonMethod.clickOnElement(FillClusterPage.WIProjectSearch, "Workflow inbox search", driver);
		commonMethod.clickOnElement(FillClusterPage.tableRow, "Resulted project", driver);
		commonMethod.clickOnElement(FillClusterPage.ShowDataCluster, "Show data cluster button", driver);
		commonMethod.clickOnElement(FillClusterPage.GenKitPackApproveButton, "Approve button", driver);
		FC.LogIn(sheetname, username2, password);
		commonMethod.clickOnElement(FillClusterPage.CheckApproveStatus, "Check approval status", driver);
		commonMethod.clickOnElement(FillClusterPage.KitRow, "Kit list select", driver);
		commonMethod.getText(FillClusterPage.GenKitPackInsideCheckApproval, "Status of Drugproduct & primaryPack", driver);
		
		
		
		
		
			
		}
		
	}

