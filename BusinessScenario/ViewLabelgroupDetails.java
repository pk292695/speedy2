package BusinessScenario;

import java.awt.AWTException;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.util.Properties;

import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;
import BusinessScenarioPage.ViewLblgrpDetailsPage;
import CommonMethod.CommonMethod;
import CommonMethod.ExpectedData;

public class ViewLabelgroupDetails extends ReusableLibrary {
	
	public Properties OR_properties;
	static boolean elementPresent;
	ViewLblgrpDetailsPage Viewlblgrppage = new ViewLblgrpDetailsPage(scriptHelper);

	CommonMethod commonMethod = new CommonMethod(scriptHelper);
	ExpectedData expectedData = new ExpectedData(scriptHelper);

	String packName = this.getClass().getPackage().getName();
	String className = this.getClass().getSimpleName();
	
	public ViewLabelgroupDetails(ScriptHelper scriptHelper)
	{
		super(scriptHelper);
	}

	public void Labelgroup_showcountry() throws IOException, UnsupportedFlavorException, AWTException
	{
	commonMethod.navigateToUrl(dataTable.getData("functional_flow", "Application_URL"), driver);
	commonMethod.enterText(ViewLblgrpDetailsPage.Username, "User Name", dataTable.getData("functional_flow", "Username"), driver);
	commonMethod.enterText(ViewLblgrpDetailsPage.Password, "Password", dataTable.getData("functional_flow", "Password"), driver);
	commonMethod.clickOnElement(ViewLblgrpDetailsPage.Login, "Login button", driver);
	commonMethod.clickOnElement(ViewLblgrpDetailsPage.Allproj, "All projects button", driver);
	commonMethod.clickOnElement(ViewLblgrpDetailsPage.OMPproj, "OMP Project", driver);
	commonMethod.clickOnElement(ViewLblgrpDetailsPage.Syncbutton, "sync with omp link", driver);
	commonMethod.clickOnElement(ViewLblgrpDetailsPage.InfoPerkit, "Info per kit type section", driver);
	commonMethod.clickOnElement(ViewLblgrpDetailsPage.MelComp, "Mel compiler button", driver);
	commonMethod.clickOnElement(ViewLblgrpDetailsPage.ShowCntry, "Show country link", driver);
	commonMethod.clickOnElement(ViewLblgrpDetailsPage.Label1, "Label group name1", driver);
	commonMethod.clickOnElement(ViewLblgrpDetailsPage.Label2, "Label group name2", driver);

	}
}
