package BusinessScenario;

import java.awt.AWTException;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;
import uri.FillCluster;
import uriPage.FillClusterPage;
import AutomationCoreFramework.Status;
import BusinessScenarioPage.AddRemoveKitTypePage;
import CommonMethod.CommonMethod;

public class RemoveKitTypeAAJC37 extends ReusableLibrary{
	AddRemoveKitTypePage AddRemoveKiteTypPage = new AddRemoveKitTypePage(scriptHelper);

	CommonMethod commonMethod = new CommonMethod(scriptHelper);
	FillCluster FC = new FillCluster(scriptHelper);
	
	public RemoveKitTypeAAJC37(ScriptHelper scriptHelper)
	{
		super(scriptHelper);
	}
	String username = "Username";
	String sheetname = "functional_flow";
	String password = "Password";
	String KitTypeName = "JKP";
	public void RemoveKitType() throws IOException, UnsupportedFlavorException, AWTException
	{
		commonMethod.navigateToUrl(dataTable.getData("functional_flow", "Application_URL"), driver);
		FC.LogIn(sheetname,username, password);
		commonMethod.clickOnElement(FillClusterPage.AllprojButton, "All projects button", driver);
		FC.FilterNonOMPDraft();
		commonMethod.clickOnElement(FillClusterPage.tableRow, "Table Row", driver);
		commonMethod.clickOnElement(FillClusterPage.informationKitType, "Information Kit Type", driver);
		
		
		commonMethod.clickOnElement(FillClusterPage.removeKitType, "Selected Kit Type Remove Button", driver);
		commonMethod.clickOnElement(FillClusterPage.removeKitNoButton	, "Remove No Button", driver);
		
		commonMethod.clickOnElement(FillClusterPage.removeKitType, "Selected Kit Type Remove Button", driver);
		commonMethod.clickOnElement(FillClusterPage.yesButton, "Remove Yes Button", driver);
		commonMethod.clickOnElement(FillClusterPage.selectKitType, "drop down kit type and Kit type removed", driver);
	
	}
	
}	




