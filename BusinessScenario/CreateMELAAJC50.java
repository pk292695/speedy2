package BusinessScenario;

import java.awt.AWTException;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;
import uri.FillCluster;
import uriPage.FillClusterPage;
import AutomationCoreFramework.Status;
import BusinessScenarioPage.AddRemoveKitTypePage;
import CommonMethod.CommonMethod;

public class CreateMELAAJC50 extends ReusableLibrary{
	AddRemoveKitTypePage AddRemoveKiteTypPage = new AddRemoveKitTypePage(scriptHelper);

	CommonMethod commonMethod = new CommonMethod(scriptHelper);
	FillCluster FC = new FillCluster(scriptHelper);
	
	public CreateMELAAJC50(ScriptHelper scriptHelper)
	{
		super(scriptHelper);
	}
	String username = "Username";
	String sheetname = "functional_flow";
	String password = "Password";
	String username1 = "Username1";
	
	public void CreateMEL() throws IOException, UnsupportedFlavorException, AWTException, InterruptedException
	{
		commonMethod.navigateToUrl(dataTable.getData("functional_flow", "Application_URL"), driver);
		FC.LogIn(sheetname,username, password);
		FC.StartNewProject();
		
		FC.CountryInformation();
		commonMethod.clickOnElement(FillClusterPage.informationKitType, "Information Kit Type", driver);
		FC.createKitType(dataTable.getData("Test_Data", "Kitname"));
		commonMethod.clickOnElement(FillClusterPage.PacktypeTab, "Pack type tab", driver);
		
		commonMethod.clickOnElement(FillClusterPage.Packoption1, "Pack type option", driver);
			
		commonMethod.clickOnElement(FillClusterPage.SaveChanges, "Save the changes", driver);
		commonMethod.clickOnElement(FillClusterPage.ContinueSave, "Save confirm Button", driver);
		FC.Logout();
		driver.navigate().refresh();
		FC.LogIn(sheetname, username1, password);
		commonMethod.clickOnElement(FillClusterPage.AllprojButton, "All projects button", driver);
		System.out.println(FillCluster.name+ ":     name project");
		FC.FilterProjectnameactive(FillCluster.name);
		commonMethod.clickOnElement(FillClusterPage.tableRow, "Table Row", driver);
		commonMethod.clickOnElement(FillClusterPage.informationKitType, "Information Kit Type", driver);
		FC.SelectKitType(dataTable.getData("Test_Data", "Kitname"));
		commonMethod.clickOnElement(FillClusterPage.MELcompilertab, "MEL Compiler Tab", driver);
		commonMethod.clickOnElement(FillClusterPage.editLabelGroupsButton, "Edit Label Groups Button", driver);
		commonMethod.enterText(FillClusterPage.Name, "Name", dataTable.getData("Test_Data", "LabelName"), driver);
		commonMethod.clickOnElement(FillClusterPage.countryNameCheckbox, "Country checkbox", driver);
		commonMethod.clickOnElement(FillClusterPage.saveLabelGroupButton, "Save Label Group Button", driver);
		
		
		//combine and Template flow
		commonMethod.clickOnElement(FillClusterPage.PackTypecheckbox1, "Pack Type checkbox", driver);
		
		commonMethod.clickOnElement(FillClusterPage.Labelgroupcheckbox1, "Label Group checkbox", driver);
		commonMethod.clickOnElement(FillClusterPage.melcombineButton, "Combine MEL", driver);
		WebElement j = driver.findElement(By.xpath(FillClusterPage.DoneButton));
		if(j.isEnabled()){
			report.updateTestLog("Verify the done Button is not enabled", "Verify Done button is enabled or not", "Done Button Should not be enabled", "Done Button is enabled", Status.FAIL);
		}
		else{
			report.updateTestLog("Verify the done Button is not enabled", "Verify Done button is enabled or not", "Done Button Should not be enabled", "Done Button is not enabled", Status.PASS);
		}
		commonMethod.clickOnElement(FillClusterPage.template, "Template", driver);
		
		commonMethod.enterText(FillClusterPage.template,"Template Option",dataTable.getData("Test_Data", "MELTemplate"), driver);
		commonMethod.clickDown(FillClusterPage.template,driver);
		commonMethod.clickEnter(FillClusterPage.template, driver);
		
		commonMethod.clickOnElement(FillClusterPage.SearchButton, "Search Button", driver);
		commonMethod.clickOnElement(FillClusterPage.searchField, "Element", driver);
		commonMethod.clickOnElement(FillClusterPage.addToMEL, "Element", driver);
		
		commonMethod.clickOnElement(FillClusterPage.NGCStaticReference, "NGC Static Reference button", driver);
		commonMethod.clickOnElement(FillClusterPage.SearchButton, "Search Button", driver);
		commonMethod.clickOnElement(FillClusterPage.ngcField, "Element", driver);
		commonMethod.clickOnElement(FillClusterPage.addToMEL, "Element", driver);
		commonMethod.scrollDown(driver);
		
		commonMethod.clickOnElement(FillClusterPage.DoneButton, "Done Button", driver);
		commonMethod.clickOnElement(FillClusterPage.SaveChanges, "Save Changes Button", driver);
		commonMethod.clickOnElement(FillClusterPage.ContinueSave, "Save Changes Button", driver);
		
		}
public void EditMELAAJC15() throws IOException, UnsupportedFlavorException, AWTException{
	commonMethod.clickOnElement(FillClusterPage.informationKitType, "Information Kit Type", driver);
	commonMethod.clickOnElement(FillClusterPage.MELcompilertab, "Mel Compile tab Button", driver);
	commonMethod.clickOnElement(FillClusterPage.MELEditbutton, "MEL Edit Button", driver);
		
}
public void RemoveMELelementsAAJC19
() throws IOException, UnsupportedFlavorException, AWTException{
	commonMethod.clickOnElement(FillClusterPage.removeMELElement, "Remove Element", driver);
	commonMethod.clickOnElement(FillClusterPage.DoneButton, "Done Button", driver);
	commonMethod.clickOnElement(FillClusterPage.MELEditbutton, "MEL Edit Button", driver);
	
}
public void addMELElmentsAAJC17() throws IOException, UnsupportedFlavorException, AWTException{
	//commonMethod.clickOnElement(FillClusterPage.MELEditbutton, "MEL Edit Button", driver);
	commonMethod.clickOnElement(FillClusterPage.SearchButton, "Search Button", driver);
	commonMethod.clickOnElement(FillClusterPage.searchField9, "Add Element", driver);
	commonMethod.clickOnElement(FillClusterPage.addToMEL, "Add MEL Element button", driver);
	
	commonMethod.clickOnElement(FillClusterPage.DoneButton, "Done Button", driver);
	commonMethod.clickOnElement(FillClusterPage.MELEditbutton, "MEL Edit Button", driver);
	
}
public void RemoveMELAAJC18
()throws IOException, UnsupportedFlavorException, AWTException{
	commonMethod.clickOnElement(FillClusterPage.DeleteMEL, "Delete MEL Template", driver);
}

				
	}	




