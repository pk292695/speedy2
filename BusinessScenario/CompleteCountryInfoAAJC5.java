package BusinessScenario;

import java.awt.AWTException;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;
import uri.FillCluster;
import uriPage.FillClusterPage;
import AutomationCoreFramework.Status;
import BusinessScenarioPage.AddRemoveKitTypePage;
import CommonMethod.CommonMethod;

public class CompleteCountryInfoAAJC5 extends ReusableLibrary{
	AddRemoveKitTypePage AddRemoveKiteTypPage = new AddRemoveKitTypePage(scriptHelper);

	CommonMethod commonMethod = new CommonMethod(scriptHelper);
	FillCluster FC = new FillCluster(scriptHelper);
	
	public CompleteCountryInfoAAJC5(ScriptHelper scriptHelper)
	{
		super(scriptHelper);
	}
	String username = "Username";
	String sheetname = "functional_flow";
	String password = "Password";
	public void CountryInfoComplete() throws IOException, UnsupportedFlavorException, AWTException
	{
		commonMethod.navigateToUrl(dataTable.getData("functional_flow", "Application_URL"), driver);
		FC.LogIn(sheetname,username, password);
		FC.StartNewProject();
		commonMethod.clickOnElement(FillClusterPage.GenProjInfo, "General project info", driver);
		commonMethod.clickOnElement(FillClusterPage.otherGcdoCro, "Other option GCDO or CRO", driver);
		
		commonMethod.clickOnElement(FillClusterPage.Country, "Country Information tab", driver);
		commonMethod.clickOnElement(FillClusterPage.AddParticipatingCountry, "Add participating country", driver);
		commonMethod.verifyElementPresent(FillClusterPage.participatingCountry, "Participating country", driver);
		commonMethod.verifyElementPresent(FillClusterPage.countrySpecificInformation, "Country specific field", driver);
		commonMethod.verifyElementPresent(FillClusterPage.localContact, "Local contact Field", driver);
		commonMethod.verifyElementPresent(FillClusterPage.labelApprover, "Label approver field", driver);
		
		commonMethod.clickOnElement(FillClusterPage.participatingCountry, "Country",  driver);
		commonMethod.clickOnElement(FillClusterPage.dropdownAddParticipatingcountry, "dropdownAddParticipatingcountry", driver);
		commonMethod.enterText(FillClusterPage.countrySpecificInformation, "Country Specific Information", dataTable.getData("Test_Data", "CountrySpecific"), driver);
		commonMethod.enterText(FillClusterPage.localContact, "Local contact", dataTable.getData("Test_Data", "LocalContact"), driver);
		commonMethod.enterText(FillClusterPage.labelApprover, "Label approver", dataTable.getData("Test_Data", "LocalApprover"), driver);
		commonMethod.clickOnElement(FillClusterPage.addParticipatingCountryButton, "Add Participating Country Button", driver);
		
		commonMethod.clickOnElement(FillClusterPage.EditCountryLink, "Edit link", driver);
		commonMethod.clearElement(FillClusterPage.countrySpecificInformation, "Country specific", driver);
		commonMethod.enterText(FillClusterPage.countrySpecificInformation, "Country specific info", dataTable.getData("Test_Data", "CountrySpecific_replace"), driver);
		commonMethod.clickOnElement(FillClusterPage.addParticipatingCountryButton, "Add Participating Country Button", driver);
		
	}
}




