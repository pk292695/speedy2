package BusinessScenario;

import java.awt.AWTException;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;


import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;
import uri.FillCluster;
import uriPage.FillClusterPage;
import CommonMethod.CommonMethod;

public class BlindingTypeAAJC1709_02 extends ReusableLibrary{
	CommonMethod commonMethod = new CommonMethod(scriptHelper);
	FillCluster FC = new FillCluster(scriptHelper);
	
	public BlindingTypeAAJC1709_02(ScriptHelper scriptHelper)
	{
		super(scriptHelper);
	}
	String username = "Username";
	String username2 = "Username2";
	String sheetname = "functional_flow";
	String password = "Password";
	public void BlindingType1() throws IOException, UnsupportedFlavorException, AWTException
	{
		commonMethod.navigateToUrl(dataTable.getData("functional_flow", "Application_URL"), driver);
		FC.LogIn(sheetname, username, password);
		
		commonMethod.clickOnElement(FillClusterPage.AllprojButton, "All projects button", driver);
		FC.FilternonOMPFull();
		commonMethod.clickOnElement(FillClusterPage.tableRow, "Table Row", driver);
		commonMethod.clickOnElement(FillClusterPage.CheckApproveStatus, "Check approval status", driver);
		commonMethod.clickOnElement(FillClusterPage.KitRow, "Kit list select", driver);
		commonMethod.getText(FillClusterPage.GenKitPackInsideCheckApproval, "Status of Drugproduct & primaryPack", driver);
		
		commonMethod.clickOnElement(FillClusterPage.CheckApproveToProject, "Project link", driver);
		commonMethod.clickOnElement(FillClusterPage.informationKitType, "Information Kit Type", driver);
		commonMethod.clickOnElement(FillClusterPage.GeneralKitTypeInfo, "General kit type info", driver);
		commonMethod.clickOnElement(FillClusterPage.BlindingType, "Blinding type", driver);
		
	}
}
