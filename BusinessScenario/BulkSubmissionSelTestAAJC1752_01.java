package BusinessScenario;

import java.awt.AWTException;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;

import uri.FillCluster;
import uriPage.FillClusterPage;
import AutomationCoreFramework.Status;
import BusinessScenarioPage.AddRemoveKitTypePage;
import CommonMethod.CommonMethod;

public class BulkSubmissionSelTestAAJC1752_01 extends ReusableLibrary{
	

	AddRemoveKitTypePage AddRemoveKiteTypPage = new AddRemoveKitTypePage(scriptHelper);

	CommonMethod commonMethod = new CommonMethod(scriptHelper);
	FillCluster FC = new FillCluster(scriptHelper);
	
	public BulkSubmissionSelTestAAJC1752_01(ScriptHelper scriptHelper)
	{
		super(scriptHelper);
	}
	String username = "Username";

	String sheetname = "functional_flow";
	String password = "Password";
	String KitTypeName = "JKP";
	public void BulkSubmission() throws IOException, UnsupportedFlavorException, AWTException
	{
		commonMethod.navigateToUrl(dataTable.getData("functional_flow", "Application_URL"), driver);
		FC.LogIn(sheetname,username, password);
		FC.StartNewProject();
	/*	commonMethod.clickOnElement(FillClusterPage.AllprojButton, "All projects button", driver);
		FC.FilterNonOMPDraft();
		commonMethod.clickOnElement(FillClusterPage.tableRow, "Resulted project", driver);*/
		
		commonMethod.verifyElementPresent(FillClusterPage.BulkSubButton, "Bulk submit button", driver);
		commonMethod.VerifyElementEnabled(FillClusterPage.BulkSubButton, "Bulk submit button", driver);
		
		FC.GeneralProjectInformation();
		commonMethod.clearElement(FillClusterPage.Therapeutic, "Therapeutic Area", driver);
		commonMethod.clickOnElement(FillClusterPage.BulkSubButton, "Bulk Submission button", driver);
		commonMethod.verifyElementNotEnabled(FillClusterPage.GenProjCheck, "General project checkbox", driver);
		commonMethod.clickOnElement(FillClusterPage.CancelBulk, "Bulk Cancel button", driver);
		commonMethod.enterText(FillClusterPage.Therapeutic, "Therapeutic Area", dataTable.getData("Test_Data", "Therapeutic"), driver);
		
		FC.CountryInformation();
		
		commonMethod.clickOnElement(FillClusterPage.informationKitType, "Information Per kit type", driver);
		FC.createKitType("JKP");
		
		FC.GeneralKitTypeInfo();
		FC.PackTypeInfo();
		FC.DrugProductInfo();
		FC.PrimaryPackInfo();
		commonMethod.clickOnElement(FillClusterPage.BulkSubButton, "Bulk Submission button", driver);
		
		commonMethod.VerifyElementEnabled(FillClusterPage.GenProjCheck, "General project checkbox", driver);
		commonMethod.VerifyElementEnabled(FillClusterPage.CntryInfoCheck, "Country Info checkbox", driver);
		commonMethod.VerifyElementEnabled(FillClusterPage.GeneralKitTypeInfo, "General Kit checkbox", driver);
		commonMethod.VerifyElementEnabled(FillClusterPage.DrugProdCheck, "Drugproduct & Primary pack checkbox", driver);
		commonMethod.VerifyElementEnabled(FillClusterPage.SubmitBulk, "Bulk submit pop-up", driver);
		driver.close();
	}
}	




