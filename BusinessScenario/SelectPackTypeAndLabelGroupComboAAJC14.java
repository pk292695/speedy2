package BusinessScenario;

import java.awt.AWTException;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;
import uri.FillCluster;
import uriPage.FillClusterPage;
import AutomationCoreFramework.Status;
import BusinessScenarioPage.AddRemoveKitTypePage;
import CommonMethod.CommonMethod;

public class SelectPackTypeAndLabelGroupComboAAJC14 extends ReusableLibrary{
	

	CommonMethod commonMethod = new CommonMethod(scriptHelper);
	FillCluster FC = new FillCluster(scriptHelper);
	
	public SelectPackTypeAndLabelGroupComboAAJC14(ScriptHelper scriptHelper)
	{
		super(scriptHelper);
	}
	String username = "Username";
	String sheetname = "functional_flow";
	String password = "Password";
	String username1 = "Username1";
	
	public void CreateMelSelFlow() throws IOException, UnsupportedFlavorException, AWTException
	{
		commonMethod.navigateToUrl(dataTable.getData("functional_flow", "Application_URL"), driver);
		FC.LogIn(sheetname,username, password);
		commonMethod.clickOnElement(FillClusterPage.AllprojButton, "All projects button", driver);
		FC.FilterNonOMPDraft();
		WebElement projectName = driver.findElement(By.xpath(FillClusterPage.tablerowname));
		String projectnamestore = projectName.getText();
		commonMethod.clickOnElement(FillClusterPage.tableRow, "Table Row", driver);
	
		
		FC.CountryInformation();
		commonMethod.clickOnElement(FillClusterPage.informationKitType, "Information Kit Type", driver);
		FC.createKitType(dataTable.getData("Test_Data", "Kitname"));
		commonMethod.clickOnElement(FillClusterPage.PacktypeTab, "Pack type tab", driver);
		commonMethod.clickOnElement(FillClusterPage.MELcompilertab, "MEL Compiler Tab", driver);
		WebElement e = driver.findElement(By.xpath(FillClusterPage.MELpackType));
		if(e.isDisplayed()){
			report.updateTestLog("Verify pack type element not present", "Verify pack Type element not present", "Pack Type Name SHould not be present on MEL compiler", "PAck Type name is present on Mel Compiler", Status.FAIL);
			}
		else{
			report.updateTestLog("Verify pack type element not present", "Verify pack Type element not present", "Pack Type Name SHould not be present on MEL compiler", "PAck Type name is not present on Mel Compiler", Status.PASS);
		}
		commonMethod.clickOnElement(FillClusterPage.PacktypeTab, "Pack type tab", driver);
		commonMethod.clickOnElement(FillClusterPage.Packoption1, "Pack type option", driver);
		commonMethod.clickOnElement(FillClusterPage.MELcompilertab, "MEL Compiler Tab", driver);
		
		if(e.isDisplayed()){
			report.updateTestLog("Verify pack type element present", "Verify pack Type element present", "Pack Type Name SHould  be present on MEL compiler", "PAck Type name is present on Mel Compiler", Status.PASS);
			}
		else{
			report.updateTestLog("Verify pack type element present", "Verify pack Type element present", "Pack Type Name SHould  be present on MEL compiler", "PAck Type name is not present on Mel Compiler", Status.FAIL);
		}
		commonMethod.clickOnElement(FillClusterPage.SaveChanges, "Save the changes", driver);
		commonMethod.clickOnElement(FillClusterPage.ContinueSave, "Save confirm Button", driver);
		FC.Logout();
		driver.navigate().refresh();
		FC.LogIn(sheetname, username1, password);
		commonMethod.clickOnElement(FillClusterPage.AllprojButton, "All projects button", driver);
		FC.FilterProjectnameactive(projectnamestore);
		commonMethod.clickOnElement(FillClusterPage.tableRow, "Table Row", driver);
		commonMethod.clickOnElement(FillClusterPage.informationKitType, "Information Kit Type", driver);
		FC.SelectKitType(dataTable.getData("Test_Data", "Kitname"));
		commonMethod.clickOnElement(FillClusterPage.MELcompilertab, "MEL Compiler Tab", driver);
		commonMethod.clickOnElement(FillClusterPage.editLabelGroupsButton, "Edit Label Groups Button", driver);
		commonMethod.enterText(FillClusterPage.Name, "Name", "Speedy Country", driver);
		commonMethod.clickOnElement(FillClusterPage.countryNameCheckbox, "Country checkbox", driver);
		commonMethod.clickOnElement(FillClusterPage.saveLabelGroupButton, "Save Label Group Button", driver);
		
		
		//combine and Template flow
		commonMethod.clickOnElement(FillClusterPage.PackTypecheckbox1, "Pack Type checkbox", driver);
		
		commonMethod.clickOnElement(FillClusterPage.Labelgroupcheckbox1, "Label Group checkbox", driver);

	}	
		
			
	}	




