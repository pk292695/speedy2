package BusinessScenario;

import java.awt.AWTException;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.interactions.Keyboard;

import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;
import uri.FillCluster;
import uriPage.FillClusterPage;
import AutomationCoreFramework.Status;
import BusinessScenarioPage.AddRemoveKitTypePage;
import BusinessScenarioPage.BrightstockSubmitPage;
import CommonMethod.CommonMethod;

public class ManageAttachmentAAJC124 extends ReusableLibrary{
	AddRemoveKitTypePage AddRemoveKiteTypPage = new AddRemoveKitTypePage(scriptHelper);

	CommonMethod commonMethod = new CommonMethod(scriptHelper);
	FillCluster FC = new FillCluster(scriptHelper);
	public ManageAttachmentAAJC124(ScriptHelper scriptHelper)
	{
		super(scriptHelper);
	}
	String username = "Username";
	String sheetname = "functional_flow";
	String password = "Password";
	
	public void AddRemoveAttachment() throws IOException, UnsupportedFlavorException, AWTException
	{
		commonMethod.navigateToUrl(dataTable.getData("functional_flow", "Application_URL"), driver);
		FC.LogIn(sheetname, username, password);
		FC.StartNewProject();
		commonMethod.clickOnElement(FillClusterPage.informationKitType, "information Kita type column", driver);
		FC.createKitType(dataTable.getData("Test_Data", "Kitname"));
		
		commonMethod.clickOnElement(FillClusterPage.GeneralKitTypeInfo, "General kit type info tab", driver);

		WebElement text = driver.findElement(By.xpath(FillClusterPage.AddAttachment));
		text.sendKeys("C:\\Users\\Public\\Documents\\APPROVAL_Approval superscript comments_1.pdf"); 
		
		
		
	
	}

}
