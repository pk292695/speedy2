package BusinessScenario;

import java.awt.AWTException;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.util.Properties;

import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;
import uri.FillCluster;
import BusinessScenarioPage.Update_task_page;
import CommonMethod.CommonMethod;
import CommonMethod.ExpectedData;

//AAJC-436 : Update task overview screen
public class Update_task_overview extends ReusableLibrary {
	public Properties OR_properties;
	static boolean elementPresent;
	Update_task_page updatePage = new Update_task_page(scriptHelper);

	CommonMethod commonMethod = new CommonMethod(scriptHelper);
	ExpectedData expectedData = new ExpectedData(scriptHelper);
	FillCluster fillCluster = new FillCluster(scriptHelper);

	String packName = this.getClass().getPackage().getName();
	String className = this.getClass().getSimpleName();
	
	public Update_task_overview(ScriptHelper scriptHelper)
	{
		super(scriptHelper);
	}
	public void Task_Overview() throws IOException, UnsupportedFlavorException, AWTException
	{
		
		commonMethod.navigateToUrl(dataTable.getData("functional_flow", "Application_URL"), driver);
		commonMethod.enterText(Update_task_page.Username, "User Name", dataTable.getData("functional_flow", "Username"), driver);
		commonMethod.enterText(Update_task_page.Password, "Password", dataTable.getData("functional_flow", "Password"), driver);
		commonMethod.clickOnElement(Update_task_page.Login, "Login button", driver);
		
		commonMethod.clickOnElement(Update_task_page.StartProject, "Start Project button", driver);
		commonMethod.enterText(Update_task_page.ProjectName, "Project Name", dataTable.getData("Test_Data", "ProjectName"), driver);
		commonMethod.enterText(Update_task_page.Compound, "Compound Name", dataTable.getData("Test_Data", "Compound"), driver);
		commonMethod.clickOnElement(Update_task_page.StartButton, "Start Button", driver);
		
		fillCluster.GeneralProjectInformation();
		
		commonMethod.clickOnElement(Update_task_page.Home, "Home button", driver);
		commonMethod.enterText(Update_task_page.Searchbox, "Search box", dataTable.getData("Test_Data", "ProjectName"), driver);
		commonMethod.clickOnElement(Update_task_page.SearchButton, "Search button", driver);
		commonMethod.clickOnElement(Update_task_page.ResultProj, "Resulted project", driver);
		commonMethod.clickOnElement(Update_task_page.Showcluster, "Show cluster button", driver);
		//Approval is not working "Access denied" occurs
		commonMethod.clickOnElement(Update_task_page.ApproveButton, "Approve button", driver);
		
		commonMethod.enterText(Update_task_page.Username, "User Name", dataTable.getData("functional_flow", "Username"), driver);
		commonMethod.enterText(Update_task_page.Password, "Password", dataTable.getData("functional_flow", "Password"), driver);
		commonMethod.clickOnElement(Update_task_page.Login, "Login button", driver);
		
		commonMethod.clickOnElement(Update_task_page.Home, "Home button", driver);
		commonMethod.enterText(Update_task_page.Searchbox, "Search box", dataTable.getData("Test_Data", "ProjectName"), driver);
		commonMethod.clickOnElement(Update_task_page.SearchButton, "Search button", driver);
		
	}
	

}
