package BusinessScenario;

import java.awt.AWTException;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;
import uri.FillCluster;
import uriPage.FillClusterPage;
import AutomationCoreFramework.Status;
import BusinessScenarioPage.AddRemoveKitTypePage;
import CommonMethod.CommonMethod;

public class SaveProjectDetailsAAJC121 extends ReusableLibrary{
	AddRemoveKitTypePage AddRemoveKiteTypPage = new AddRemoveKitTypePage(scriptHelper);

	CommonMethod commonMethod = new CommonMethod(scriptHelper);
	FillCluster FC = new FillCluster(scriptHelper);
	
	public SaveProjectDetailsAAJC121(ScriptHelper scriptHelper)
	{
		super(scriptHelper);
	}
	String username = "Username";
	String sheetname = "functional_flow";
	String password = "Password";
	String KitTypeName = "JKP";
	public void SaveProject() throws IOException, UnsupportedFlavorException, AWTException
	{
		commonMethod.navigateToUrl(dataTable.getData("functional_flow", "Application_URL"), driver);
		FC.LogIn(sheetname,username, password);
		commonMethod.clickOnElement(FillClusterPage.AllprojButton, "All projects button", driver);
		FC.FilterNonOMPDraft();
		commonMethod.clickOnElement(FillClusterPage.tableRow, "First Table Row", driver);
		FC.GeneralProjectInformation();
		FC.CountryInformation();
		commonMethod.clickOnElement(FillClusterPage.informationKitType, "Information per kit type", driver);
		FC.createKitType(dataTable.getData("Test_Data", "Kitname"));
		FC.GeneralKitTypeInfo();
		FC.PackTypeInfo();
		FC.DrugProductInfo();
		FC.PrimaryPackInfo();
		commonMethod.clickOnElement(FillClusterPage.SaveChanges, "Save changes", driver);
		commonMethod.clickOnElement(FillClusterPage.CancelSaveChanges, "Cancel button", driver);
		commonMethod.clickOnElement(FillClusterPage.SaveChanges, "Save changes", driver);
		commonMethod.clickOnElement(FillClusterPage.ContinueSave, "Continue save changes", driver);
		}
	
	}	
		
			
	




