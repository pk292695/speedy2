package BusinessScenario;

import java.awt.AWTException;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.server.handler.Status;

import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;
import uri.FillCluster;
import uriPage.FillClusterPage;
import BusinessScenarioPage.Delete_Edit_MEL_Page;
import CommonMethod.CommonMethod;
import AutomationCoreFramework.Report;

public class DeactivateAndReactivateProjSelTestAAJC1751 extends ReusableLibrary{
	
//	Delete_Edit_MEL_Page MELTemplatePage = new Delete_Edit_MEL_Page(scriptHelper);

	CommonMethod commonMethod = new CommonMethod(scriptHelper);
	FillCluster fc = new FillCluster(scriptHelper);
	String e = null;
	public String UserName = "Username";
	public String SheetName = "functional_flow";
	public String password = "Password";
	public DeactivateAndReactivateProjSelTestAAJC1751(ScriptHelper scriptHelper)
	{
		super(scriptHelper);
	}
	
	public void DeactivateProjSelTest() throws IOException, UnsupportedFlavorException, AWTException
	{
		commonMethod.navigateToUrl(dataTable.getData("functional_flow", "Application_URL"), driver);
		fc.LogIn(SheetName,UserName,password);
		commonMethod.clickOnElement(FillClusterPage.AllprojButton, "All Projects Button", driver);
		fc.FilterNonOMPAwaiting();
		e = driver.findElement(By.xpath(FillClusterPage.tableRow+"/td[2]")).getText();
		e.toLowerCase();

		System.out.println(e);
		commonMethod.clickOnElement(FillClusterPage.tableRow, "Selecting table row", driver);
	
		if(driver.findElements(By.xpath(FillClusterPage.DeactivateButton)).size() != 0){
			System.out.println("1st if block");
			commonMethod.clickOnElement(FillClusterPage.DeactivateButton, "Click on Deactivate Button", driver);
			commonMethod.clickOnElement(FillClusterPage.DeactivateYesButton, "Confirm deactivate Button", driver);
			fc.LogIn(SheetName, UserName, password);
			Reactivatemodule();
			
		}
		else{
			System.out.println("1st else block");
	
			if(driver.findElements(By.xpath(FillClusterPage.ReactivateButton)).size()!= 0){
				System.out.println("2nd if block");
			commonMethod.clickOnElement(FillClusterPage.ReactivateButton, "Reactivate Button Pressed", driver);
			commonMethod.clickOnElement(FillClusterPage.ReactivateConfirmButton, "confirm reactivate button", driver);
			fc.LogIn(SheetName, UserName,password );
			DeactivateModule();
			
			}
			else{
				System.out.println("fail");
				System.out.println("2nd else block");
			}
		}
	}	
	public void Reactivatemodule() throws IOException, UnsupportedFlavorException, AWTException{
		commonMethod.clickOnElement(FillClusterPage.secondaryAllProjectButton, "All Projects Button", driver);
		driver.navigate().refresh();
		System.out.println(e+ "reactivateModule");
	//	fc.FilteronlyName(e);
		fc.FilterProjectnameInactive(e);
		if(driver.findElements(By.xpath("//*[contains(text(),e)])")).size()!= 0){
			commonMethod.clickOnElement(FillClusterPage.tableRow, "Selecting table row", driver);
			if(driver.findElements(By.xpath(FillClusterPage.ReactivateButton)).size()!= 0){
				System.out.println("2nd if block");
			commonMethod.clickOnElement(FillClusterPage.ReactivateButton, "Reactivate Button Pressed", driver);
			commonMethod.clickOnElement(FillClusterPage.ReactivateConfirmButton, "confirm reactivate button", driver);
			fc.LogIn(SheetName, UserName, password);
			}
		}
		else {
			System.out.println("reactivate module else block fail");
		}
	}
	public void DeactivateModule() throws IOException, UnsupportedFlavorException, AWTException{
		commonMethod.clickOnElement(FillClusterPage.secondaryAllProjectButton, "All Projects Button", driver);
		System.out.println(e+ "reactivateModule");
		driver.navigate().refresh();
	//	fc.FilteronlyName(e);
		fc.FilterProjectnameactive(e);
		if(driver.findElements(By.xpath("//*[contains(text(),e)])")).size()!= 0){
		commonMethod.clickOnElement(FillClusterPage.tableRow, "Selecting table row", driver);
		if(driver.findElements(By.xpath(FillClusterPage.ReactivateButton)).size()!= 0){
			System.out.println("2nd if block");
		commonMethod.clickOnElement(FillClusterPage.ReactivateButton, "Reactivate Button Pressed", driver);
		commonMethod.clickOnElement(FillClusterPage.ReactivateConfirmButton, "confirm reactivate button", driver);
		fc.LogIn(SheetName, UserName, password);
		}
	}
		else{
			System.out.println("DeactivateModule else block fail");
		}
	}
	
}
