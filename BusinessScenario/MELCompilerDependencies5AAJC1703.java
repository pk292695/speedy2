package BusinessScenario;

import java.awt.AWTException;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;

import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;
import uri.FillCluster;
import uriPage.FillClusterPage;
import CommonMethod.CommonMethod;

public class MELCompilerDependencies5AAJC1703 extends ReusableLibrary{

	CommonMethod commonMethod = new CommonMethod(scriptHelper);
	FillCluster FC = new FillCluster(scriptHelper);
	
	
	
	public MELCompilerDependencies5AAJC1703(ScriptHelper scriptHelper)
	{
		super(scriptHelper);
	}
	String username1 = "Username1";
	String sheetname = "functional_flow";
	String password = "Password";
	
	public void MELDependency5() throws IOException, UnsupportedFlavorException, AWTException
	{
		commonMethod.navigateToUrl(dataTable.getData("functional_flow", "Application_URL"), driver);
		FC.LogIn(sheetname,username1, password);
		commonMethod.clickOnElement(FillClusterPage.AllprojButton, "All projects button", driver);
		FC.FilterNonOMPDraft();
		commonMethod.clickOnElement(FillClusterPage.tableRow, "Resulted table", driver);
		commonMethod.clickOnElement(FillClusterPage.informationKitType, "Information Kit type tab", driver);
		commonMethod.clickOnElement(FillClusterPage.MELcompilertab, "MEL Compiler tab", driver);
		
		commonMethod.verifyElementNotEnabled(FillClusterPage.MELSubmitButton, "MEL compiler submit button", driver);
	}
}
