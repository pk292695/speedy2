package BusinessScenario;

import java.awt.AWTException;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;



import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;
import uri.FillCluster;
import uriPage.FillClusterPage;

import BusinessScenarioPage.AddRemoveKitTypePage;
import CommonMethod.CommonMethod;

public class AddRemarkFieldsAAJC1708_05 extends ReusableLibrary{
	AddRemoveKitTypePage AddRemoveKiteTypPage = new AddRemoveKitTypePage(scriptHelper);

	CommonMethod commonMethod = new CommonMethod(scriptHelper);
	FillCluster FC = new FillCluster(scriptHelper);
	
	
	
	public AddRemarkFieldsAAJC1708_05(ScriptHelper scriptHelper)
	{
		super(scriptHelper);
	}
	String username1 = "Username1";
	String sheetname = "functional_flow";
	String password = "Password";
	String KitTypeName = "JKP";
	public void AddRemark_05() throws IOException, UnsupportedFlavorException, AWTException
	{
		commonMethod.navigateToUrl(dataTable.getData("functional_flow", "Application_URL"), driver);
		FC.LogIn(sheetname,username1, password);
		commonMethod.clickOnElement(FillClusterPage.AllprojButton, "All projects button", driver);
		FC.FilterNonOMPDraft();
		commonMethod.clickOnElement(FillClusterPage.tableRow, "Resulted table", driver);
		commonMethod.clickOnElement(FillClusterPage.informationKitType, "Information Kit type tab", driver);
		commonMethod.clickOnElement(FillClusterPage.MELcompilertab, "MEL Compiler tab", driver);
		commonMethod.enterText(FillClusterPage.MelComments, "MEL commments", dataTable.getData("Test_Data", "Comments"), driver);
		commonMethod.scrollDown(driver);
	}	
		
			
	}	




