package BusinessScenario;

import java.awt.AWTException;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;
import uri.FillCluster;
import uriPage.FillClusterPage;
import BusinessScenarioPage.Login_page;
import CommonMethod.CommonMethod;
import CommonMethod.ExpectedData;

// Login with NT UserName and Password AAJC-1
public class LoginAAJC54_02 extends ReusableLibrary{
	public Properties OR_properties;
	static boolean elementPresent;
	Login_page Loginpage = new Login_page(scriptHelper);
	FillCluster FC = new FillCluster(scriptHelper);
	CommonMethod commonMethod = new CommonMethod(scriptHelper);
	ExpectedData expectedData = new ExpectedData(scriptHelper);

	String packName = this.getClass().getPackage().getName();
	String className = this.getClass().getSimpleName();
	
	String Sheetname = "functional_flow";
	String username0 = "UserName";
	String username1 = "UserName1";
	String username2 = "UserName2";
	String username3 = "UserName3";
	String username4 = "UserName4";
	String passowrd = "Password";
	String password = "Password4";
	
	
	
	public LoginAAJC54_02(ScriptHelper scriptHelper)
	{
		super(scriptHelper);
	}

		
	
	public void Login3() throws IOException, UnsupportedFlavorException, AWTException
	{
		commonMethod.navigateToUrl(dataTable.getData(Sheetname, "Application_URL"), driver);
		FC.LogIn(Sheetname, username2, passowrd);
		
		
		FC.WIFilterGeneralKitPack();
		FC.WIFilterOpen();
		commonMethod.clickOnElement(FillClusterPage.WITabledataCluster, "Table row", driver);
		commonMethod.clickOnElement(FillClusterPage.ShowDataCluster, "Show Data Cluster", driver);
		commonMethod.VerifyElementEnabled(FillClusterPage.WIGeneralProduct, "Approve Button For General Kit Type Info", driver);
		FC.Logout();
	}
	public void Login4() throws IOException, UnsupportedFlavorException, AWTException
	{
		driver.navigate().refresh();
		FC.LogIn(Sheetname, username3, passowrd);
		
		
		FC.WIFilterMelCompilation();
		FC.WIFilterOpen();
		commonMethod.clickOnElement(FillClusterPage.WITabledataCluster, "Table row", driver);
		commonMethod.clickOnElement(FillClusterPage.ShowDataCluster, "Show Data Cluster", driver);
		commonMethod.VerifyElementEnabled(FillClusterPage.MelCompileQAApprove, "Approve Button For MEL Compiler", driver);
		FC.Logout();
	}
	
	
	
	
	
	}
	


