package BusinessScenario;

import java.awt.AWTException;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;

import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;
import uri.FillCluster;
import uriPage.FillClusterPage;
import CommonMethod.CommonMethod;

public class SubmitApprovalDrugProductAAJC293 extends ReusableLibrary{
	CommonMethod commonMethod = new CommonMethod(scriptHelper);
	FillCluster FC = new FillCluster(scriptHelper);
	
	public SubmitApprovalDrugProductAAJC293(ScriptHelper scriptHelper)
	{
		super(scriptHelper);
	}
	String username = "Username";
	String sheetname = "functional_flow";
	String password = "Password";
	String username2 = "Username2";
	
	public void DrugProductPrimaryPack() throws IOException, UnsupportedFlavorException, AWTException
	{
		commonMethod.navigateToUrl(dataTable.getData("functional_flow", "Application_URL"), driver);
		FC.LogIn(sheetname,username, password);
		FC.FilterNonOMPDraft();
		
		commonMethod.clickOnElement(FillClusterPage.informationKitType, "Information per kit type section", driver);
		FC.createKitType(dataTable.getData("Test_Data", "Kitname"));
		
		
		FC.DrugProductInfo();
		FC.PrimaryPackInfo();
		FC.SubmitDrugPrimary();
		commonMethod.clickOnElement(FillClusterPage.informationKitType, "Information per kit type section", driver);
		commonMethod.clickOnElement(FillClusterPage.DrugProductInfo, "Drug product Info tab", driver);
		commonMethod.getText(FillClusterPage.BatchdocDrug, "Batchdoc approval", driver);
		commonMethod.getText(FillClusterPage.OpenDrugProd, "Open status", driver);
		FC.Logout();
		//Drug product and Primary pack Approval
		FC.LogIn(sheetname, username2, password);
		commonMethod.enterText(FillClusterPage.ProjectSearch, "Workflow inbox Project search", FillCluster.name, driver);
		commonMethod.clickOnElement(FillClusterPage.WIProjectSearch, "Workflow inbox search", driver);
		commonMethod.clickOnElement(FillClusterPage.tableRow, "Resulted project", driver);
		commonMethod.clickOnElement(FillClusterPage.ShowDataCluster, "Show data cluster button", driver);
		commonMethod.clickOnElement(FillClusterPage.DrugPrimApproveButton, "Approve button", driver);
		FC.LogIn(sheetname, username2, password);
		commonMethod.clickOnElement(FillClusterPage.CheckApproveStatus, "Check approval status", driver);
		commonMethod.clickOnElement(FillClusterPage.KitRow, "Kit list select", driver);
		commonMethod.getText(FillClusterPage.DrugPrimInsideCheckApproval, "Status of Drugproduct & primaryPack", driver);
		
	}
			
}




