package BusinessScenario;

import java.awt.AWTException;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;

import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;
import uri.FillCluster;
import uriPage.FillClusterPage;
import CommonMethod.CommonMethod;

public class ReplicateDataforPackTypeAAJC1069_02 extends ReusableLibrary{
	CommonMethod commonMethod = new CommonMethod(scriptHelper);
	FillCluster FC = new FillCluster(scriptHelper);
	
	public ReplicateDataforPackTypeAAJC1069_02(ScriptHelper scriptHelper)
	{
		super(scriptHelper);
	}
	String username = "Username";
	String sheetname = "functional_flow";
	String password = "Password";
	
	public void ReplicatePacktype1() throws IOException, UnsupportedFlavorException, AWTException
	{
		commonMethod.navigateToUrl(dataTable.getData("functional_flow", "Application_URL"), driver);
		FC.LogIn(sheetname, username, password);

		FC.StartNewProject();

		commonMethod.clickOnElement(FillClusterPage.informationKitType, "Information per kit type section", driver);	
		FC.createKitType(dataTable.getData("Test_Data", "Kitname"));
		
		//Click on Pack type Info
		commonMethod.clickOnElement(FillClusterPage.PacktypeTab, "PackTypeInfo", driver);
		commonMethod.clickOnElement(FillClusterPage.Packoption2, "Dosepak", driver);
		commonMethod.verifyElementNotEnabled(FillClusterPage.CopyExistingPack, "Copy existing pack type", driver);

		commonMethod.selectOptionFromDropDown(FillClusterPage.ProdName, dataTable.getData("Test_Data", "Prodnamestrength"), driver);
		commonMethod.selectOptionFromDropDown(FillClusterPage.Dosage, dataTable.getData("Test_Data", "DoseForm"), driver);
		commonMethod.selectOptionFromDropDown(FillClusterPage.ROA, dataTable.getData("Test_Data", "ROA"), driver);
		commonMethod.selectOptionFromDropDown(FillClusterPage.ROAPict, dataTable.getData("Test_Data", "ROApict"), driver);
		commonMethod.selectOptionFromDropDown(FillClusterPage.DoseInstruction, dataTable.getData("Test_Data", "DosingInst"), driver);
		commonMethod.selectOptionFromDropDown(FillClusterPage.Tempstore, dataTable.getData("Test_Data", "TempStore"), driver);
		commonMethod.selectOptionFromDropDown(FillClusterPage.OtherStore, dataTable.getData("Test_Data", "OtherStore"), driver);
		commonMethod.selectOptionFromDropDown(FillClusterPage.HandIns, dataTable.getData("Test_Data", "HandInst"), driver);
		commonMethod.selectOptionFromDropDown(FillClusterPage.PeriodDes, dataTable.getData("Test_Data", "PeriodDes"), driver);
		commonMethod.selectOptionFromDropDown(FillClusterPage.Othervar, dataTable.getData("Test_Data", "OtherVariable"), driver);
		commonMethod.clickOnElement(FillClusterPage.TearOff, "Tear-off", driver);
		commonMethod.enterText(FillClusterPage.packtypeComments, "Comments",dataTable.getData("Test_Data", "Comments"), driver);
		
		//Reason and Save Changes
				commonMethod.clickOnElement(FillClusterPage.SaveChanges, "SaveChanges", driver);
				commonMethod.enterText(FillClusterPage.ReasonforChange, "ReasonforChange", dataTable.getData("Test_Data", "Reason"), driver);
				commonMethod.clickOnElement(FillClusterPage.ContinueSave,"SaveReason",driver);
				
		//Select the Kit Type
				commonMethod.clickOnElement(FillClusterPage.informationKitType, "Information per kit type section", driver);	
				FC.SelectKitType(dataTable.getData("Test_Data", "Kitname"));

				commonMethod.clickOnElement(FillClusterPage.PacktypeTab, "PackTypeInfo", driver);
				commonMethod.clickOnElement(FillClusterPage.Packoption1, "Box id", driver);
		
				//Click on Box button'
				commonMethod.clickOnElement(FillClusterPage.btnBox, "btnBox", driver);
				//Copy Existing pack
				commonMethod.clickOnElement(FillClusterPage.CopyExistingPack, "CopyExistingPack", driver);
		
				//Select the Kit Type and copy
		commonMethod.clickOnElement(FillClusterPage.dropdownkit, "drop down kit type", driver);
		commonMethod.enterText(FillClusterPage.dropdownkit, "dropdownkit", dataTable.getData("Test_Data", "Kitname"), driver);
		commonMethod.clickOnElement(FillClusterPage.dropdownkit, "drop down kit type", driver);
		commonMethod.clickOnElement(FillClusterPage.chkbxpackkit, "chkbxpackkit", driver);
		commonMethod.clickOnElement(FillClusterPage.Copy, "Copy", driver);

		//Reason and Save Changes
		commonMethod.clickOnElement(FillClusterPage.SaveChanges, "SaveChanges", driver);
		commonMethod.enterText(FillClusterPage.ReasonforChange, "ReasonforChange", dataTable.getData("Test_Data", "Reason"), driver);
		commonMethod.clickOnElement(FillClusterPage.ContinueSave,"SaveReason",driver);

		//Click on Information Kit Type and check Pack Type info
		commonMethod.clickOnElement(FillClusterPage.informationKitType, "InformationperKitType", driver);
		commonMethod.clickOnElement(FillClusterPage.PacktypeTab, "PackTypeInfo", driver);


		//Click on Box button'
		commonMethod.clickOnElement(FillClusterPage.btnBox, "btnBox", driver);
	}
}
