package BusinessScenario;

import java.awt.AWTException;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;

import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;
import uri.FillCluster;
import uriPage.FillClusterPage;
import BusinessScenarioPage.Update_task_page;
import CommonMethod.CommonMethod;
import CommonMethod.ExpectedData;

//AAJC-436 : Update task overview screen
public class UpdateTaskOverviewAAJC436 extends ReusableLibrary {
	

	CommonMethod commonMethod = new CommonMethod(scriptHelper);
	FillCluster FC = new FillCluster(scriptHelper);
	
	public UpdateTaskOverviewAAJC436(ScriptHelper scriptHelper)
	{
		super(scriptHelper);
	}
	String username = "Username";
	String sheetname = "functional_flow";
	String password = "Password";
	public void Task_Overview() throws IOException, UnsupportedFlavorException, AWTException
	{
		
		commonMethod.navigateToUrl(dataTable.getData("functional_flow", "Application_URL"), driver);
		FC.LogIn(sheetname, username, password);
		FC.StartNewProject();
		
		FC.GeneralProjectInformation();
		FC.SubmitGeneralProjectInformation();
		commonMethod.clickOnElement(FillClusterPage.HomeButton, "Continue save changes button", driver);
		commonMethod.enterText(FillClusterPage.ProjectSearch, "Workflow inbox Project search", FillCluster.name, driver);
		commonMethod.clickOnElement(FillClusterPage.WIProjectSearch, "Workflow inbox search", driver);
		commonMethod.getText(FillClusterPage.WITableStatus, "Project status", driver);
		
		commonMethod.clickOnElement(FillClusterPage.tableRow, "Resulted project", driver);
		commonMethod.clickOnElement(FillClusterPage.ShowDataCluster, "Show data cluster button", driver);
		commonMethod.clickOnElement(FillClusterPage.GenProjApproveButton, "Approve button", driver);
		FC.LogIn(sheetname, username, password);
		commonMethod.clickOnElement(FillClusterPage.HomeButton, "Continue save changes button", driver);
		commonMethod.enterText(FillClusterPage.ProjectSearch, "Workflow inbox Project search", FillCluster.name, driver);
		commonMethod.clickOnElement(FillClusterPage.WIProjectSearch, "Workflow inbox search", driver);
		commonMethod.getText(FillClusterPage.WITableStatus, "Project status", driver);
		
	}
	

}
