package BusinessScenario;

import java.awt.AWTException;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;



import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;
import uri.FillCluster;
import uriPage.FillClusterPage;

import CommonMethod.CommonMethod;
import AutomationCoreFramework.Report;
import AutomationCoreFramework.*;




public class RemoveOpenTaskDeactivateProjectAAJC1852 extends ReusableLibrary{
	


	CommonMethod commonMethod = new CommonMethod(scriptHelper);
	FillCluster fc = new FillCluster(scriptHelper);
	String e = null;
	public String UserName = "Username";
	public String SheetName = "functional_flow";
	public String password = "Password";
	public RemoveOpenTaskDeactivateProjectAAJC1852(ScriptHelper scriptHelper)
	{
		super(scriptHelper);
	}
	
	public void DeactivateProjSelTest() throws IOException, UnsupportedFlavorException, AWTException
	{
		commonMethod.navigateToUrl(dataTable.getData("functional_flow", "Application_URL"), driver);
		fc.LogIn(SheetName,UserName,password);
		fc.StartNewProject();
		fc.GeneralProjectInformation();
		fc.SubmitGeneralProjectInformation();
		
		commonMethod.clickOnElement(FillClusterPage.HomeButton, "Home button", driver);
		commonMethod.enterText(FillClusterPage.WIProjectSearch, "Project name", FillCluster.name, driver);
		commonMethod.clickOnElement(FillClusterPage.WISearchbutton, "Search button", driver);		
		commonMethod.getText(FillClusterPage.WITableStatus, "Workflow inbox status", driver);
		commonMethod.clickOnElement(FillClusterPage.AllprojButton, "All projects button", driver);
		fc.FilterProjectnameactive(FillCluster.name);
		commonMethod.clickOnElement(FillClusterPage.tableRow, "Resulted project", driver);
		commonMethod.clickOnElement(FillClusterPage.DeactivateButton, "Deactivate button", driver);
		commonMethod.clickOnElement(FillClusterPage.DeactivateYesButton, "Confirm deactivate Button", driver);
		fc.LogIn(SheetName, UserName, password);
		commonMethod.clickOnElement(FillClusterPage.HomeButton, "Home Button", driver);
		commonMethod.enterText(FillClusterPage.WIProjectSearch, "Project name", FillCluster.name, driver);
		commonMethod.clickOnElement(FillClusterPage.WISearchbutton, "Search button", driver);
		commonMethod.verifyElementPresent(FillClusterPage.WITabledataUnavailable, "Table Data unavailable", driver);
		
		
		

	
	
	}	
}
