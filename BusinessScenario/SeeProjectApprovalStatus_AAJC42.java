package BusinessScenario;

import java.awt.AWTException;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;

import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;
import uri.FillCluster;
import uriPage.FillClusterPage;
import CommonMethod.CommonMethod;

public class SeeProjectApprovalStatus_AAJC42 extends ReusableLibrary{

	CommonMethod commonMethod = new CommonMethod(scriptHelper);
	FillCluster FC = new FillCluster(scriptHelper);
	
	public SeeProjectApprovalStatus_AAJC42(ScriptHelper scriptHelper)
	{
		super(scriptHelper);
	}
	String username = "Username";
	String sheetname = "functional_flow";
	String password = "Password";
	public void SeeProjectApproval() throws IOException, UnsupportedFlavorException, AWTException
	{
		commonMethod.navigateToUrl(dataTable.getData("functional_flow", "Application_URL"), driver);
		FC.LogIn(sheetname,username, password);
		FC.StartNewProject();
		commonMethod.clickOnElement(FillClusterPage.informationKitType, "Information per kit type", driver);
		FC.createKitType(dataTable.getData("Test_Data", "Kitname"));
		
		commonMethod.clickOnElement(FillClusterPage.CheckApproveStatus, "Check approval status", driver);
		commonMethod.getText(FillClusterPage.AllProjRow, "All project info", driver);
		commonMethod.clickOnElement(FillClusterPage.GenProjRow, "General project", driver);
		commonMethod.clickOnElement(FillClusterPage.CountryProjRow, "Country Information", driver);
		commonMethod.clickOnElement(FillClusterPage.KitRow, "Kit information", driver);
		commonMethod.getText(FillClusterPage.StatusInsideCheckApproval, "General project Info status", driver);
	
	
	}
}
