package BusinessScenario;

import java.awt.AWTException;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;

import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;
import uri.FillCluster;
import uriPage.FillClusterPage;
import CommonMethod.CommonMethod;

public class MELCompilerEditRightsAAJC1698_03 extends ReusableLibrary{

	CommonMethod commonMethod = new CommonMethod(scriptHelper);
	FillCluster FC = new FillCluster(scriptHelper);
	
	public MELCompilerEditRightsAAJC1698_03(ScriptHelper scriptHelper)
	{
		super(scriptHelper);
	}
	String username = "Username";
	String username1 = "Username1";
	String sheetname = "functional_flow";
	String password = "Password";
	public void MELEditRights3() throws IOException, UnsupportedFlavorException, AWTException, InterruptedException
	{
		
		commonMethod.navigateToUrl(dataTable.getData("functional_flow", "Application_URL"), driver);
		FC.LogIn(sheetname, username, password);
		FC.StartNewProject();
		FC.CountryInformation();
		commonMethod.clickOnElement(FillClusterPage.informationKitType, "Information per kit type section", driver);
		commonMethod.clickOnElement(FillClusterPage.PacktypeTab, "Pack type tab", driver);
		commonMethod.clickOnElement(FillClusterPage.Packoption1, "Pack type option", driver);
		
		commonMethod.clickOnElement(FillClusterPage.SaveChanges, "Save changes button", driver);
		commonMethod.clickOnElement(FillClusterPage.ContinueSave, "Continue and save changes", driver);
		
		//MEL compiler Labelgroup login
		driver.navigate().refresh();
		FC.LogIn(sheetname, username1, password);
		commonMethod.clickOnElement(FillClusterPage.AllprojButton, "All projects button", driver);
		FC.FilterProjectnameactive(FillCluster.name);
		commonMethod.clickOnElement(FillClusterPage.tableRow, "Resulted project", driver);
		
		commonMethod.clickOnElement(FillClusterPage.informationKitType, "Information per kit type section", driver);
		FC.SelectKitType(dataTable.getData("Test_Data", "Kitname"));
		
		commonMethod.clickOnElement(FillClusterPage.MELcompilertab, "MEL compiler tab", driver);
		commonMethod.verifyElementPresent(FillClusterPage.melcombineButton, "MEL combine button", driver);
	commonMethod.verifyElementPresent(FillClusterPage.MELSubmitButton, "MEL submit button", driver);
	commonMethod.verifyElementPresent(FillClusterPage.editLabelGroupsButton, "Edit labelgroups button", driver);
	
	commonMethod.clickOnElement(FillClusterPage.editLabelGroupsButton, "Edit Label Groups Button", driver);
	commonMethod.enterText(FillClusterPage.Name, "Name", dataTable.getData("Test_Data", "LabelName"), driver);
	commonMethod.clickOnElement(FillClusterPage.SelectAllCountries, "Select all countries link", driver);
	commonMethod.clickOnElement(FillClusterPage.saveLabelGroupButton, "Save Label Group Button", driver);
	
	
	//combine and Template flow
	commonMethod.clickOnElement(FillClusterPage.PackTypecheckbox1, "Pack Type checkbox", driver);
	
	commonMethod.clickOnElement(FillClusterPage.Labelgroupcheckbox1, "Label Group checkbox", driver);
	commonMethod.clickOnElement(FillClusterPage.melcombineButton, "Combine MEL", driver);
	
	commonMethod.VerifyElementEnabled(FillClusterPage.melcombineButton, "MEL combine button", driver);
	
	}
}
