package BusinessScenario;

import java.awt.AWTException;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;

import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;
import uri.FillCluster;
import uriPage.FillClusterPage;
import CommonMethod.CommonMethod;

public class HandlingInstructionAAJC1071 extends ReusableLibrary{

	CommonMethod commonMethod = new CommonMethod(scriptHelper);
	FillCluster FC = new FillCluster(scriptHelper);
	
	public HandlingInstructionAAJC1071(ScriptHelper scriptHelper)
	{
		super(scriptHelper);
	}
	String username = "Username";
	String sheetname = "functional_flow";
	String password = "Password";
	
	public void HandlingInst() throws IOException, UnsupportedFlavorException, AWTException
	{
		commonMethod.navigateToUrl(dataTable.getData("functional_flow", "Application_URL"), driver);
		FC.LogIn(sheetname,username, password);
		FC.StartNewProject();
		commonMethod.clickOnElement(FillClusterPage.informationKitType, "Information per kit type section", driver);
		FC.SelectKitType(dataTable.getData("Test_Data", "Kitname"));
		commonMethod.clickOnElement(FillClusterPage.PacktypeTab, "Pack type tab", driver);
		commonMethod.clickOnElement(FillClusterPage.Packoption1, "Pack type option", driver);
		commonMethod.clickOnElement(FillClusterPage.HandIns, "Handling Instruction", driver);
	}
}
