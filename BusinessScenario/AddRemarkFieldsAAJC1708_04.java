package BusinessScenario;

import java.awt.AWTException;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;


import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;
import uri.FillCluster;
import uriPage.FillClusterPage;

import CommonMethod.CommonMethod;

public class AddRemarkFieldsAAJC1708_04 extends ReusableLibrary{
	

	CommonMethod commonMethod = new CommonMethod(scriptHelper);
	FillCluster FC = new FillCluster(scriptHelper);
	
	
	
	public AddRemarkFieldsAAJC1708_04(ScriptHelper scriptHelper)
	{
		super(scriptHelper);
	}
	String username = "Username";
	String sheetname = "functional_flow";
	String password = "Password";
	String KitTypeName = "JKP";
	public void AddRemark_04() throws IOException, UnsupportedFlavorException, AWTException
	{
		commonMethod.navigateToUrl(dataTable.getData("functional_flow", "Application_URL"), driver);
		FC.LogIn(sheetname,username, password);
		commonMethod.clickOnElement(FillClusterPage.AllprojButton, "All projects button", driver);
		FC.FilterNonOMPDraft();
		commonMethod.clickOnElement(FillClusterPage.tableRow, "Click on Resulted Project", driver);
		commonMethod.clickOnElement(FillClusterPage.GenProjInfo, "General Project Info", driver);
		commonMethod.enterText(FillClusterPage.Comments_generalProject,"Comments on General Project Info", dataTable.getData("Test_Data","Comments2048"), driver);
		
		 commonMethod.clickOnElement(FillClusterPage.Country, "Country information", driver);
		 commonMethod.enterText(FillClusterPage.country_comments, "Country Comments", dataTable.getData("Data_Table", "Comments2048"), driver);
		 commonMethod.clickOnElement(FillClusterPage.informationKitType, "information Kit Type", driver);
		 commonMethod.clickOnElement(FillClusterPage.GeneralKitTypeInfo, "General Kit Type info", driver);
		 commonMethod.enterText(FillClusterPage.general_Kitcomments	, "General Kit Type comments", dataTable.getData("Test_Data", "Comments2048"), driver);
		 commonMethod.clickOnElement(FillClusterPage.PacktypeTab, "Pack Type Tab", driver);
		 commonMethod.enterText(FillClusterPage.packtypeComments, "Pack Type Comments",dataTable.getData("Test_Data", "Comments2048"),driver);
		 commonMethod.clickOnElement(FillClusterPage.DrugProductInfo, "Drug Product Info Tab", driver);
		 commonMethod.enterText(FillClusterPage.DrugProductInfoComments, "Drug product Info Comments",dataTable.getData("Test_Data", "Comments2048"), driver);
		 commonMethod.clickOnElement(FillClusterPage.PrimaryPack, "Primary Pack Info Tab", driver);
		 commonMethod.enterText(FillClusterPage.primaryPackTypeComments, "Primary pack Type commments", dataTable.getData("Test_Data", "Comments2048"), driver);
		 commonMethod.scrollDown(driver);
	}	
		
			
	}	




