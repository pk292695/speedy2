package BusinessScenario;

import java.awt.AWTException;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;

import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;
import uri.FillCluster;
import uriPage.FillClusterPage;
import CommonMethod.CommonMethod;

public class TextSignOff_AAJC1061 extends ReusableLibrary{

	CommonMethod commonMethod = new CommonMethod(scriptHelper);
	FillCluster FC = new FillCluster(scriptHelper);
	
	public TextSignOff_AAJC1061(ScriptHelper scriptHelper)
	{
		super(scriptHelper);
	}
	String username = "Username";
	String sheetname = "functional_flow";
	String password = "Password";
	
	public void TextSignOff1() throws IOException, UnsupportedFlavorException, AWTException
	{
		commonMethod.navigateToUrl(dataTable.getData("functional_flow", "Application_URL"), driver);
		FC.LogIn(sheetname,username, password);
		FC.StartNewProject();
		
		FC.GeneralProjectInformation();
		FC.SubmitGeneralProjectInformation();
		commonMethod.clickOnElement(FillClusterPage.GenProjInfo, "General project Info tab", driver);
		commonMethod.getText(FillClusterPage.CSITSM, "CSI/TSM approval", driver);
		commonMethod.getText(FillClusterPage.OpenCSITSM, "Open status", driver);
		
		//General project Information Approval
		commonMethod.clickOnElement(FillClusterPage.HomeButton, "Continue save changes button", driver);
		commonMethod.enterText(FillClusterPage.ProjectSearch, "Workflow inbox Project search", FillCluster.name, driver);
		commonMethod.clickOnElement(FillClusterPage.WIProjectSearch, "Workflow inbox search", driver);
		commonMethod.clickOnElement(FillClusterPage.tableRow, "Resulted project", driver);
		commonMethod.clickOnElement(FillClusterPage.ShowDataCluster, "Show data cluster button", driver);
		commonMethod.clickOnElement(FillClusterPage.GenProjApproveButton, "Approve button", driver);
	}
}
