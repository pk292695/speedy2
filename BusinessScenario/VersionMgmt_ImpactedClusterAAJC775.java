package BusinessScenario;

import java.awt.AWTException;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;
import uri.FillCluster;
import uriPage.FillClusterPage;
import AutomationCoreFramework.Status;
import BusinessScenarioPage.Version_mgmt_page;
import CommonMethod.CommonMethod;
import CommonMethod.ExpectedData;

public class VersionMgmt_ImpactedClusterAAJC775 extends ReusableLibrary{

	CommonMethod commonMethod = new CommonMethod(scriptHelper);
	FillCluster FC = new FillCluster(scriptHelper);
	
	public VersionMgmt_ImpactedClusterAAJC775(ScriptHelper scriptHelper)
	{
		super(scriptHelper);
	}
	String username = "Username";
	String sheetname = "functional_flow";
	String password = "Password";
	

	public void Impacted_cluster() throws IOException, UnsupportedFlavorException, AWTException
	{
		commonMethod.navigateToUrl(dataTable.getData("functional_flow", "Application_URL"), driver);
		FC.LogIn(sheetname, username, password);
		FC.StartNewProject();
		FC.GeneralProjectInformation();
		FC.SubmitGeneralProjectInformation();
		commonMethod.clickOnElement(Version_mgmt_page.CreateNewVersion, "Create New version button", driver);
		commonMethod.clickOnElement(Version_mgmt_page.CheckGenproj, "General project checkbox", driver);
		commonMethod.clickOnElement(Version_mgmt_page.Cancel, "Cancel button", driver);
		commonMethod.clickOnElement(Version_mgmt_page.Home, "Home button", driver);
	}
	
}
