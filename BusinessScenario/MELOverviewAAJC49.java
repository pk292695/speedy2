package BusinessScenario;

import java.awt.AWTException;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;
import uri.FillCluster;
import uriPage.FillClusterPage;
import AutomationCoreFramework.Status;
import CommonMethod.CommonMethod;

public class MELOverviewAAJC49 extends ReusableLibrary{

	CommonMethod commonMethod = new CommonMethod(scriptHelper);
	FillCluster FC = new FillCluster(scriptHelper);
	
	public MELOverviewAAJC49(ScriptHelper scriptHelper)
	{
		super(scriptHelper);
	}
	String username = "Username";
	String username1 = "Username1";
	String sheetname = "functional_flow";
	String password = "Password";
	public void MEL_Overview() throws IOException, UnsupportedFlavorException, AWTException, InterruptedException
	{
		commonMethod.navigateToUrl(dataTable.getData("functional_flow", "Application_URL"), driver);
		FC.LogIn(sheetname, username, password);
		FC.StartNewProject();
		FC.CountryInformation();
		commonMethod.clickOnElement(FillClusterPage.informationKitType, "information Kita type column", driver);
		FC.createKitType(dataTable.getData("Test_Data", "Kitname"));
		
		//Click on Packtype Info
		commonMethod.clickOnElement(FillClusterPage.PacktypeTab, "PackTypeInfo", driver);
		commonMethod.clickOnElement(FillClusterPage.Packoption1, "BoxID", driver);
		
		//Reason and Save Changes
		commonMethod.clickOnElement(FillClusterPage.SaveChanges, "SaveChanges", driver);
		commonMethod.enterText(FillClusterPage.ReasonforChange, "ReasonforChange", dataTable.getData("Test_Data", "Reason"), driver);
		commonMethod.clickOnElement(FillClusterPage.ContinueSave,"Continue save changes",driver);

		FC.Logout();
		
		driver.navigate().refresh();
		FC.LogIn(sheetname, username1, password);
		commonMethod.clickOnElement(FillClusterPage.AllprojButton, "All Project Button", driver);
		FC.FilterProjectnameactive(FC.name);
		commonMethod.clickOnElement(FillClusterPage.tableRow, "Click on resulted Project", driver);
		commonMethod.clickOnElement(FillClusterPage.informationKitType, "Information Kit Type Tab", driver);
		commonMethod.clickOnElement(FillClusterPage.MELcompilertab, "MEL compiler Tab", driver);
		commonMethod.verifyElementNotEnabled(FillClusterPage.melcombineButton, "Combine button disabled", driver);
		
		//AddNewLabel Group
		commonMethod.clickOnElement(FillClusterPage.editLabelGroupsButton, "EditLabelGroup", driver);
		commonMethod.verifyElementNotEnabled(FillClusterPage.SaveLabel, "Save label button", driver);
		commonMethod.enterText(FillClusterPage.LabelName, "LabelName", dataTable.getData("Test_Data", "LabelName"), driver);
		commonMethod.clickOnElement(FillClusterPage.SelectAllCountries, "Show all countries link", driver);
		commonMethod.clickOnElement(FillClusterPage.SaveLabel, "SaveLabel", driver);
			
		commonMethod.clickOnElement(FillClusterPage.PackTypecheckbox1, "Pack Type checkbox", driver);
		commonMethod.clickOnElement(FillClusterPage.Labelgroupcheckbox1, "Label Group checkbox", driver);
		commonMethod.clickOnElement(FillClusterPage.melcombineButton, "Combine MEL", driver);
				
		WebElement j = driver.findElement(By.xpath(FillClusterPage.DoneButton));
		if(j.isEnabled()){
			report.updateTestLog("Verify the done Button is not enabled", "Verify Done button is enabled or not", "Done Button Should not be enabled", "Done Button is enabled", Status.FAIL);
		}
		else{
			report.updateTestLog("Verify the done Button is not enabled", "Verify Done button is enabled or not", "Done Button Should not be enabled", "Done Button is not enabled", Status.PASS);
		}
		commonMethod.clickOnElement(FillClusterPage.template, "Template", driver);
			
		commonMethod.enterText(FillClusterPage.template,"Template Option",dataTable.getData("Test_Data", "MELTemplate"), driver);
		commonMethod.clickDown(FillClusterPage.template,driver);
		commonMethod.clickEnter(FillClusterPage.template, driver);
		commonMethod.clickOnElement(FillClusterPage.DoneButton, "Done Button", driver);
		commonMethod.clickOnElement(FillClusterPage.editLabelGroupsButton, "EditLabelGroup", driver);
		commonMethod.verifyElementPresent(FillClusterPage.Label1, "Verify Label Present", driver);
			
				
	
	
	}
}
