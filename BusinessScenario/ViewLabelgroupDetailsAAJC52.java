package BusinessScenario;

import java.awt.AWTException;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;


import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;
import uri.FillCluster;
import uriPage.FillClusterPage;
import CommonMethod.CommonMethod;

public class ViewLabelgroupDetailsAAJC52 extends ReusableLibrary {
	FillCluster FC = new FillCluster(scriptHelper);
	CommonMethod commonMethod = new CommonMethod(scriptHelper);
	
	String username = "Username";
	String sheetname = "functional_flow";
	String password = "Password";
	public ViewLabelgroupDetailsAAJC52(ScriptHelper scriptHelper)
	{
		super(scriptHelper);
	}

	public void Labelgroup_showcountry() throws IOException, UnsupportedFlavorException, AWTException
	{
	commonMethod.navigateToUrl(dataTable.getData("functional_flow", "Application_URL"), driver);
	FC.LogIn(sheetname, username, password);
	commonMethod.clickOnElement(FillClusterPage.AllprojButton, "All projects button", driver);
	FC.FilterBrightDraft();
	
	commonMethod.clickOnElement(FillClusterPage.SyncwithOMPLink, "sync with omp link", driver);
	commonMethod.clickOnElement(FillClusterPage.informationKitType, "Info per kit type section", driver);
	commonMethod.clickOnElement(FillClusterPage.MELcompilertab, "Mel compiler button", driver);
	commonMethod.clickOnElement(FillClusterPage.ShowCntry, "Show country link", driver);
	commonMethod.clickOnElement(FillClusterPage.Label1, "Label group name1", driver);
	commonMethod.clickOnElement(FillClusterPage.Label2, "Label group name2", driver);

	}
}
