package BusinessScenario;

import java.awt.AWTException;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;

import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;
import uri.FillCluster;
import uriPage.FillClusterPage;
import CommonMethod.CommonMethod;

public class MELDisplayContent_AAJC1022 extends ReusableLibrary{

	CommonMethod commonMethod = new CommonMethod(scriptHelper);
	FillCluster FC = new FillCluster(scriptHelper);
	
	public MELDisplayContent_AAJC1022(ScriptHelper scriptHelper)
	{
		super(scriptHelper);
	}
	String username = "Username";
	String sheetname = "functional_flow";
	String password = "Password";
	public void MELDisplayContent() throws IOException, UnsupportedFlavorException, AWTException, InterruptedException
	{
		commonMethod.navigateToUrl(dataTable.getData("functional_flow", "Application_URL"), driver);
		FC.LogIn(sheetname, username, password);
		
		commonMethod.clickOnElement(FillClusterPage.AllprojButton, "All projects", driver);
		FC.FilterNonOMPDraft();
		
		commonMethod.clickOnElement(FillClusterPage.tableRow, "Select Project", driver);
		commonMethod.clickOnElement(FillClusterPage.informationKitType, "Information Per Kit type", driver);
		commonMethod.clickOnElement(FillClusterPage.MELcompilertab, "MEL Compiler", driver);
		
		commonMethod.clickOnElement(FillClusterPage.editLabelGroupsButton, "Edit Label Groups Button", driver);
		commonMethod.enterText(FillClusterPage.LabelName, "Label Name", dataTable.getData("Test_Data", "LabelName"), driver);
		commonMethod.clickOnElement(FillClusterPage.SelectAllCountries, "Country Name Checkbox", driver);
		commonMethod.clickOnElement(FillClusterPage.saveLabelGroupButton, "Save Label Group Button", driver);
	
		commonMethod.clickOnElement(FillClusterPage.PackTypecheckbox1, "Pack type", driver);
		commonMethod.clickOnElement(FillClusterPage.Labelgroupcheckbox1, "Label Group", driver);
		commonMethod.clickOnElement(FillClusterPage.melcombineButton, "Combine", driver);
		
		commonMethod.enterText(FillClusterPage.template,"Template Option",dataTable.getData("Test_Data", "MELTemplate"), driver);
		commonMethod.clickDown(FillClusterPage.template,driver);
		commonMethod.clickEnter(FillClusterPage.template, driver);
			
		commonMethod.getText(FillClusterPage.templateElements, "Template Elements", driver);
		commonMethod.clickOnElement(FillClusterPage.DoneButton, "Done", driver);
	}
}
