package BusinessScenario;

import java.awt.AWTException;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;
import uri.FillCluster;
import uriPage.FillClusterPage;
import CommonMethod.CommonMethod;

public class RemoveAttachmentAAJC1719 extends ReusableLibrary{
	CommonMethod commonMethod = new CommonMethod(scriptHelper);
	FillCluster FC = new FillCluster(scriptHelper);
	public RemoveAttachmentAAJC1719(ScriptHelper scriptHelper)
	{
		super(scriptHelper);
	}
	String username = "Username";
	String sheetname = "functional_flow";
	String password = "Password";
	
	public void RemoveAttach() throws IOException, UnsupportedFlavorException, AWTException
	{
		commonMethod.navigateToUrl(dataTable.getData("functional_flow", "Application_URL"), driver);
		FC.LogIn(sheetname, username, password);
		FC.StartNewProject();
		commonMethod.clickOnElement(FillClusterPage.informationKitType, "information Kita type column", driver);
		FC.createKitType(dataTable.getData("Test_Data", "Kitname"));
		
		commonMethod.clickOnElement(FillClusterPage.GeneralKitTypeInfo, "General kit type info tab", driver);
		commonMethod.clickOnElement(FillClusterPage.AddAttachment, "Add attachment button", driver);
		
		WebElement text = driver.findElement(By.xpath(FillClusterPage.AddAttachment));
		text.sendKeys("C:\\Users\\Public\\Documents\\APPROVAL_Approval superscript comments_1.pdf");
		
        commonMethod.clickOnElement(FillClusterPage.DeleteAttachment, "Delete attachment button", driver);
	}
}
