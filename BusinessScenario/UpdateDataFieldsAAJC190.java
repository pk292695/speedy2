package BusinessScenario;

import java.awt.AWTException;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;
import BusinessScenarioPage.Updates_data_page;
import CommonMethod.CommonMethod;

public class UpdateDataFieldsAAJC190 extends ReusableLibrary{
	
	Updates_data_page MELTemplatePage = new Updates_data_page(scriptHelper);

	CommonMethod commonMethod = new CommonMethod(scriptHelper);
	public UpdateDataFieldsAAJC190(ScriptHelper scriptHelper)
	{
		super(scriptHelper);
	}
 
	public void DataField_update() throws IOException, UnsupportedFlavorException, AWTException
	{
		commonMethod.navigateToUrl(dataTable.getData("functional_flow", "Application_URL"), driver);
		commonMethod.enterText(Updates_data_page.Username, "Username", dataTable.getData("functional_flow", "Username"), driver);
		commonMethod.enterText(Updates_data_page.Password, "Password", dataTable.getData("functional_flow", "Password"), driver);
		commonMethod.clickOnElement(Updates_data_page.Login, "Login button", driver);
		commonMethod.clickOnElement(Updates_data_page.Allproj, "All projects button", driver);
		commonMethod.clickOnElement(Updates_data_page.Arrow, "Next page arrow", driver);
		commonMethod.clickOnElement(Updates_data_page.Project, "Project", driver);
		
		WebElement GeneralProj = driver.findElement(By.xpath(Updates_data_page.GenProj));
		//commonMethod.getText(GeneralProj, "General Project tab", driver);
		commonMethod.clickOnElement(Updates_data_page.GenProj, "General Project tab", driver);
		commonMethod.verifyElementPresent(Updates_data_page.IMPALabel, "IMPA Label", driver);
		commonMethod.verifyElementPresent(Updates_data_page.SignOffDate, "Sign-Off Date", driver);
		commonMethod.verifyElementPresent(Updates_data_page.GCDO, "GCDO or CRO run", driver);
		commonMethod.clickOnElement(Updates_data_page.GCDOCheckOther, "General project Other button", driver);
		commonMethod.enterText(Updates_data_page.GenProjOther, "Other project value", dataTable.getData("Test_Data", "GCDO_Other_Value"), driver);
		
		commonMethod.clickOnElement(Updates_data_page.InfoPerkit, "Info Per kit type section", driver);
		commonMethod.clickOnElement(Updates_data_page.GenKit, "General kit type tab", driver);
		commonMethod.clickOnElement(Updates_data_page.PackPrim, "Package sites - primary", driver);
		commonMethod.clickOnElement(Updates_data_page.PackPrimOpt, "Package sites - primary option", driver);
		commonMethod.clickOnElement(Updates_data_page.SecPrim, "Package sites - secondary", driver);
		commonMethod.clickOnElement(Updates_data_page.SecPrimOpt, "Package sites - secondary", driver);
		
		commonMethod.clickOnElement(Updates_data_page.PackType, "Pack type tab", driver);
		commonMethod.selectOptionFromDropDown(Updates_data_page.DoseForm, dataTable.getData("Test_Data", "DoseForm_Quantity"), driver);
		
		commonMethod.clickOnElement(Updates_data_page.DrugProd, "Drug product tab", driver);
		commonMethod.clickOnElement(Updates_data_page.OxySens, "Oxygen sensitive button", driver);
		commonMethod.clickOnElement(Updates_data_page.FrozenProd, "Frozen product button", driver);
		
		commonMethod.clickOnElement(Updates_data_page.Primary_pack, "Primary Pack tab", driver);
		//commonMethod.selectOptionFromDropDown(Updates_data_page.OxyScavenger, dataTable.getData("Test_Data", "Oxy_scavenger"), driver);
	}
}
